﻿Shader "Hidden/show_mapping_renderer"
{
	Properties
	{
		_MappingTex ("MappingTex", 2D) = "white" {}
		map_width ("Maximum of width and height", Int) = 8
		map_height ("Maximum of width and height", Int) = 8
		real_map_width ("Real width of mapping texture", Int) = 10
		real_map_height("Real height of mapping texture", Int) = 10
	}
	SubShader
	{
		Tags { "RenderType"="Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert Lambert
			#pragma fragment frag Lambert
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MappingTex;
			float4 _MappingTex_ST;
			int map_width;
			int map_height;
			int real_map_width;
			int real_map_height;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
//				o.uv = TRANSFORM_TEX(v.uv, _MappingTex);
				o.uv = v.uv;
				return o;
			}

			float4 bilinear_lookup(float2 coord, float width, float height)
			{
				float shift = 0;
				float shift_first = 0;
				float width_2 = width;
				float height_2 = height;
				float x = coord.x*width+shift_first;
				float y = coord.y*height+shift_first;
				float x_floor = (floor(x)-shift)/width_2;
				float x_ceil = (ceil(x)-shift)/width_2;
				float y_floor = (floor(y)-shift)/height_2;
				float y_ceil = (ceil(y)-shift)/height_2;

				float4 col_top_left = tex2D(_MappingTex, float2(x_floor, y_ceil));
				float4 col_top_right = tex2D(_MappingTex, float2(x_ceil, y_ceil));
				float4 col_bottom_left = tex2D(_MappingTex, float2(x_floor, y_floor));
				float4 col_bottom_right = tex2D(_MappingTex, float2(x_ceil, y_floor));
				float dx = (coord.x-x_floor)/(x_ceil-x_floor);
				float dy = (coord.y-y_floor)/(y_ceil-y_floor);
				float4 col = (dy)*((1-dx)*col_top_left+dx*col_top_right) + (1-dy)*((1-dx)*col_bottom_left + dx*col_bottom_right);
				return col_bottom_right;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				float u = (i.uv.x * (map_width-1) + 1.5)/real_map_width;
				float v = (i.uv.y * (map_height-1) + 1.5)/real_map_height;
				float4 col = tex2D(_MappingTex, float2(u,v));
//				float4 col = bilinear_lookup(i.uv, real_map_width+1, real_map_height+1);

				if(col.a < 1 || (col.r == 0 && col.g ==0 && col.b == 1 && col.a == 1))
				{
					return float4(0,0,1,1);// float4(col.r/map_width, col.g/map_height, 0, 0.5);//
				}
				return float4(col.r/map_width, col.g/map_height, 0,1);
			}
			ENDCG
		}
	}
}
