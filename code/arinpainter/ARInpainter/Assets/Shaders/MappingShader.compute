﻿#define COLOR_NEIGHBOUR_COUNT 12
#pragma kernel InitHolePixelsMapping8 INIT_RESOLUTION=8
#pragma kernel InitHolePixelsMapping16 INIT_RESOLUTION=16
#pragma kernel RefineMapping INIT_RESOLUTION=1
#pragma enable_d3d11_debug_symbols 

struct InitInputData
{
	uint mipmapLevel ;
	uint nonHolePixelCount;
	uint holePixelCount;
	uint minNeighboursMappedCountFiveByFive;
	uint minNeighboursMappedCountEight;
	uint minNeighboursMappedCountColor;
	uint currentWidth;
	uint currentHeight;
	uint realMapWidth;
	uint realMapHeight;
	float maxSpatialComponentDist;
	float colorErrorWeight;
	float spatialErrorWeight;
	float typicalColorError;
	float colorStepSize;
	float spatialStepSize;
	float normalPixelColorWeight;
	float2 centerOfMass; //TODO: THIS IS FOR TESTING ONLY
};

struct InitOutputData
{
	uint isPixelMapped;
};

StructuredBuffer<InitInputData> inputBuffer;
RWStructuredBuffer<InitOutputData> outputBuffer;
RWStructuredBuffer<float4> errorBuffer;

groupshared float3 initError[INIT_RESOLUTION*INIT_RESOLUTION]; 
groupshared int bestTargetIndex[INIT_RESOLUTION*INIT_RESOLUTION];

Texture2D<float4> RandomTexture;
RWTexture2D<float4> NewMapping;
Texture2D<float4> OldMapping;
SamplerState samplerOldMapping;
RWTexture2D<float4> ErrorTexture;
RWTexture2D<float4> UpdateTexture;
//Texture2D<float4> holePixelCoordTexture;
//Texture2D<float4> nonHolePixelCoordTexture;
Texture2D<float4> HoleNonHoleCoordinatesRenderTexture;
Texture2D<float4> Background;
SamplerState samplerBackground;
Texture2D<float4> Mask;
SamplerState samplerMask;



float4 mapGetOldNonInterpolated(float2 coord)
{
	//ALL MAPPING ACCESS IS SHIFTED BY 1 PIXEL. THIS GUARANTEES A SAFETY MARGIN.
	return OldMapping[coord+float2(1,1)];
}

float4 mapGetOld(float2 coord)
{
	float x = (coord.x+1.5)/inputBuffer[0].realMapWidth;
	float y = (coord.y+1.5)/inputBuffer[0].realMapHeight;
	float2 correctedCoord = float2(x,y);
	return OldMapping.SampleLevel(samplerOldMapping, correctedCoord, 0); 
}

void mapSetNew(float2 coord, float4 value)
{
	//ALL MAPPING ACCESS IS SHIFTED BY 1 PIXEL. THIS GUARANTEES A SAFETY MARGIN.
	NewMapping[coord+float2(1,1)] = value;
}
float3 randomTex(uint i)
{
	return RandomTexture[uint2(i,0)].xyz;
}
float2 holePixelCoord(uint index)
 {
 	return HoleNonHoleCoordinatesRenderTexture[uint2(index,0)].xy;
// 	return holePixelCoordTexture[float2(index, 0)].xy;
 }
 float4 background(float2 coord){
 	
	float2 coordTransformed = (coord + float2(0.5, 0.5)) / float2(inputBuffer[0].currentWidth, inputBuffer[0].currentHeight);
 	return Background.SampleLevel(samplerBackground, coordTransformed, inputBuffer[0].mipmapLevel); 
 }

float4 mask(float2 coord){
	return Mask.SampleLevel(samplerMask,float2((coord.x+0.5)/inputBuffer[0].currentWidth, (coord.y+0.5)/inputBuffer[0].currentHeight), inputBuffer[0].mipmapLevel); 
}

float2 nonHolePixelCoord(uint index)
 {
	return HoleNonHoleCoordinatesRenderTexture[uint2(index,0)].zw;
// 	return nonHolePixelCoordTexture[float2(index,0)].xy;
 }
 void setOutputData(uint index, uint mapped)
 {
 	outputBuffer[index].isPixelMapped = mapped;
 }
 void setErrorBufferData(uint index, float4 error)
 {
 	errorBuffer[index] = error;
 }
 void setErrorTextureData(float2 index, float4 error)
 {
 	ErrorTexture[index] = error;
 }
 void setUpdateTextureData(float2 index, float totalGradientNorm, float colorGradientNorm, float spatialGradientNorm){
 	UpdateTexture[index] = float4(colorGradientNorm, spatialGradientNorm, 0, totalGradientNorm);
 }
 uint hasPixelBeenMapped(uint index)
 {
 	return outputBuffer[index].isPixelMapped;
 }

bool isInHole(float2 coord)
{
	return mask(coord).r >= 0.5;
}


void FourNeighbours(float2 source, out float2 neighbours[4])
{
	//topNeighbour
	neighbours[0]  = source + float2(0,1);

	// bottomNeighbour 
	neighbours[1] = source + float2(0,-1);

	// rightNeighbour
	neighbours[2]  = source + float2(1,0);

	// leftNeighbour
	neighbours[3] = source + float2(-1,0);
}

void FiveByFiveNeighbours(float2 source, out float2 neighbours[24])
{
	uint totalIndex = 0;
	for(int y=-2; y<=2; y++)
	{
		for(int x=-2; x<=2; x++)
		{
			if(x!=0 || y!=0)
			{
				neighbours[totalIndex] = source + int2(x,y);
				totalIndex++;
			}
		}
	}
}

void EightNeighbours(float2 source, out float2 neighbours[8])
{
	uint totalIndex = 0;
	for(int y=-1; y<=1; y++)
	{
		for(int x=-1; x<=1; x++)
		{
			if(x!=0 || y!=0)
			{
				neighbours[totalIndex] = source + int2(x,y);
				totalIndex++;
			}
		}
	}
}

void EightNeighbours12(float2 source, out float2 neighbours[12])
{
	uint totalIndex = 0;
	for(int y=-1; y<=1; y++)
	{
		for(int x=-1; x<=1; x++)
		{
			if(x!=0 || y!=0)
			{
				neighbours[totalIndex] = source + int2(x,y);
				totalIndex++;
			}
		}
	}
}

void ColorNeighbours(float2 source, out float2 neighbours[12])
{
	uint totalIndex = 0;
	for(int y=-1; y<=1; y++)
	{
		for(int x=-1; x<=1; x++)
		{
			if(x!=0 || y!=0)
			{
				neighbours[totalIndex] = source + int2(x,y);
				totalIndex++;
			}
		}
	}
	//Add some more pixels
	neighbours[8] = source + int2(-2,0);
	neighbours[9] = source + int2(2,0);
	neighbours[10] = source + int2(0,-2);
	neighbours[11] = source + int2(0,2);
}

uint IsMapped(float2 source)
{
	if (mapGetOld(source).w >= 1.0)
	{
		return 1;
	}
	else
	{
		return 0;
	}

}

uint IsValidMapping(float4 mapping)
{
	if(mapping.w < 1.0)
	{
		return 0;
	}
	else
	{
		return 1;
	}	
}

uint IsHolePixelMapping(float4 mapping)
{
	if(mapping.z < 1)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}


bool AreFiveByFiveNeighboursMapped(float2 source)
{
	uint mappedCount = 0;
	float2 neighbour;
	for(int y=-2; y<=2; y++)
	{
		for(int x=-2; x<=2; x++)
		{
			if(x!=0 || y!=0)
			{
				neighbour = source + float2(x,y);
				mappedCount = mappedCount + IsMapped(neighbour);
			}
		}
	}
	return mappedCount >= inputBuffer[0].minNeighboursMappedCountFiveByFive;
}

bool AreEightNeighboursMapped(float2 source)
{
	uint mappedCount = 0;
	float2 neighbour;
	for(int y=-1; y<=1; y++)
	{
		for(int x=-1; x<=1; x++)
		{
			if(x!=0 || y!=0)
			{
				neighbour = source + float2(x,y);
				mappedCount = mappedCount + IsMapped(neighbour);
			}
		}
	}
	return mappedCount >= inputBuffer[0].minNeighboursMappedCountEight;
}

bool AreColorNeighboursMapped(float2 source)
{
	float2 colorNeighbours[12];
	ColorNeighbours(source, colorNeighbours);
	uint mappedCount = 0;
	for(uint i=0; i<12; i++)
	{
		mappedCount = mappedCount + IsMapped(colorNeighbours[i]);
	}
	return mappedCount >= inputBuffer[0].minNeighboursMappedCountColor;
}


bool AreFourNeighboursMapped(float2 source)
{
	
	float2 colorNeighbours[4];
	FourNeighbours(source, colorNeighbours);
	uint mappedCount = 0;
	for(uint i=0; i<4; i++)
	{
		mappedCount = mappedCount + IsMapped(colorNeighbours[i]);
	}
	return mappedCount >= inputBuffer[0].minNeighboursMappedCountColor;
}

void FourNeighboursMapping(float2 neighbours[4], out float4 mapping[4])
{
	mapping[0] = mapGetOld(neighbours[0]);
	mapping[1] = mapGetOld(neighbours[1]);
	mapping[2] = mapGetOld(neighbours[2]);
	mapping[3] = mapGetOld(neighbours[3]);
}


float2 computeSpatialDirection(float2 source, float2 target, uint3 id, bool useNonHoleNeighbours, out bool invalid)
{
	//Get neighbours and their mapping
//	float2 neighbours[24];
//	int neighbourCount = 24;
//	FiveByFiveNeighbours(source, neighbours);

	float2 neighbours[8];
	int neighbourCount = 8;
	EightNeighbours(source, neighbours);

	float2 direction = float2(0,0);
	float validMappings = 0;

	float2 dist;
	float distNorm;
	uint usePixel;
	float4 neighbourMapped;
	float distNormClamped;
	float distToCenterOfNeighbourhood;

	for(uint i=0; i<neighbourCount; i++)
	{
		neighbourMapped = mapGetOld(neighbours[i]);
		dist = neighbourMapped.xy - target;
		distNorm = max(length(dist) ,0.1);
		distNormClamped = min(distNorm, inputBuffer[0].maxSpatialComponentDist);
		usePixel = IsHolePixelMapping(neighbourMapped) * IsValidMapping(neighbourMapped);
		if(useNonHoleNeighbours){
			usePixel = 1;
		}

		//TODO: -1 should be replaced with the distance between the neighbour and the center of the neighbourhood
		distToCenterOfNeighbourhood = length(neighbours[i] - source);
		direction = direction + dist * ((distNormClamped - distToCenterOfNeighbourhood)/distNorm)*usePixel;

		//Only consider pixel when its a hole pixel, when z component is 1
		validMappings = validMappings +  IsValidMapping(neighbourMapped)*usePixel;
	}

	//If there are no valid neighbour pairs are existing, or the total direction is of length 0
	if (validMappings < 0.0001 || length(direction) < 0.0000001)
	{
		invalid = true;
		return float2(0,0);
	}

	direction = direction / validMappings;
	float2 directionNormClamped = min(length(direction), inputBuffer[0].maxSpatialComponentDist);
	direction = direction / length(direction) * directionNormClamped;
	invalid = false;
	return direction;
}


float computeSpatialError(float2 source, float2 target, uint3 id)
{
	bool invalid;
	float2 spatialDirection = computeSpatialDirection(source, target, id, true, invalid);
	if(invalid){
		return inputBuffer[0].maxSpatialComponentDist;
	}
	float error = length(spatialDirection);
	return error;
}

float computeColorError(float2 source, float2 target, uint3 id, bool init)
{
	//Get the mapping of the target's and sources' neighbours
	uint neighbourCount;
	float2 neighbours[12];
	float2 targetNeighbours[12];

	// The neghbourhood used is depending on whether this is done during the initialization or the optimization
	if(init)
	{
		neighbourCount = 8;
		EightNeighbours12(source, neighbours);
		EightNeighbours12(target, targetNeighbours);
	}
	else
	{
		neighbourCount = 12;
		ColorNeighbours(source, neighbours);
		ColorNeighbours(target, targetNeighbours);
	}
//	uint neighbourCount = 4;
//	float2 neighbours[4];
//	float2 targetNeighbours[4];
//	FourNeighbours(source, neighbours);
//	FourNeighbours(target, targetNeighbours);

	

//	uint neighbourCount = 24;
//	float2 neighbours[24];
//	float2 targetNeighbours[24];
//	FiveByFiveNeighbours(source, neighbours);
//	FiveByFiveNeighbours(target, targetNeighbours);

	float4 neighbourMapped;
	float4 targetNeighbourMapped;
	float3 neighbourColor;
	float colorError;
	float3 targetNeighbourColor;
	uint sourceNeighbourValid;
	uint targetNeighbourValid;

	//Sum of all neighbours error
	float totalColorError = 0;

	//For how many pixel-pairs (source to target neighbours) both mappings are valid
	float validCount = 0;

	//The number of properly mapped neighbours around the source
	float sourceNeighbourMappingValidCount = 0;

//	//DEBUG
//	int validTargets = 0;
//	int validSources = 0;
//	float3 totalColor = float3(0,0,0);
//	float3 totalTargetColor = float3(0,0,0);
//	//\DEBUG

	for(uint i=0; i<neighbourCount; i++){
		//Get mappings
		neighbourMapped = mapGetOld(neighbours[i]);
		targetNeighbourMapped = mapGetOld(targetNeighbours[i]);

		sourceNeighbourValid = IsValidMapping(neighbourMapped);
		targetNeighbourValid = IsValidMapping(targetNeighbourMapped);

		//get the colors of the sources neighbours at the given mipmap level
		neighbourColor = background(neighbourMapped.xy).rgb;
	
		//get the colors of the targets neighbours at the given mipmap level
		targetNeighbourColor = background(targetNeighbourMapped.xy).rgb;

		//compute the L2 norm of the color difference. Multiply it with the w component of the mapping. It is 0 if the value is not mapped (inside the hole or outside the image boundry) and 1 if properly mapped.
		colorError = length(neighbourColor - targetNeighbourColor) * sourceNeighbourValid * targetNeighbourValid;

		//If the source neighbour pixel is a non-hole pixel, apply the weight to error and the totalCount
		if(neighbourMapped.z >= 0){
			colorError = colorError * inputBuffer[0].normalPixelColorWeight;
			sourceNeighbourMappingValidCount = sourceNeighbourMappingValidCount + sourceNeighbourValid* inputBuffer[0].normalPixelColorWeight;
			validCount = validCount + sourceNeighbourValid*targetNeighbourValid*inputBuffer[0].normalPixelColorWeight;
		}
		else
		{
			sourceNeighbourMappingValidCount = sourceNeighbourMappingValidCount + sourceNeighbourValid;
			validCount = validCount + sourceNeighbourValid*targetNeighbourValid;
		}


		totalColorError = totalColorError + colorError;

//		if(id.x==23 && id.y == 124)
//			{
//				setErrorBufferData(1+i*4, float4(targetNeighbourMapped.xy, targetNeighbourValid,0));
//				setErrorBufferData(2+i*4, float4(neighbourMapped.xy, sourceNeighbourValid,1));
//				setErrorBufferData(3+i*4, float4(targetNeighbourColor, 2));
//				setErrorBufferData(4+i*4, float4(neighbourColor, 3));
//
//			}

//		//DEBUG
//		totalColor = totalColor + (neighbourColor);
//		totalTargetColor = totalTargetColor + (targetNeighbourColor*targetNeighbourMapped.w);
//		validTargets = validTargets + targetNeighbourMapped.w;
//		validSources = validSources + neighbourMapped.w;
//		//\DEBUG
//		if(id.x == 6 && id.y == 20){
////			setErrorBufferData(i, float4(neighbourColor, neighbourMapped.w));
//		}
	}

//	if(id.x == 23)
//	{
//		setErrorBufferData(id.y,float4());
//	}

	// If there are less valid pixel-pairs than valid source-neighbour mappings, return max error.
	// This is equivalent to the case, where the source neighbourhood has a neighbour (lets say the top-right pixel from the source) that is mapped, but the same neighbour of the target (top-right from the target) is invalid
	// This means we'd map to a pixel that has a poorly mapped neighbourhood, which is undesirable
	if (validCount < sourceNeighbourMappingValidCount)
	{
		totalColorError = 1.01;
	}
	else
	{

		totalColorError = totalColorError / validCount;

		//normalize error to be 1 at max 1.7320508075688772 = norm([1,1,1]) = sqrt(3)
		totalColorError = totalColorError / 1.7320508075688772;
	}
	//scale by value derived from image statistics
	totalColorError = totalColorError / inputBuffer[0].typicalColorError;

	return totalColorError;
}

float2 computeColorDirection(float2 source, float2 target, uint3 id)
{
	float bias = 0.51;
	float topError = computeColorError(source, target + float2(0, bias), id, false);
	float bottomError = computeColorError(source, target + float2(0, -bias), id, false);
	float rightError = computeColorError(source, target + float2(bias, 0), id, false);
	float leftError = computeColorError(source, target + float2(-bias, 0), id, false);

	float xGradient = rightError - leftError;
	float yGradient = topError - bottomError;
	float2 gradient = float2(xGradient, yGradient);
	return -gradient;
}


float totalError(float2 source, float2 target, out float weightedColorError, out float weightedSpatialError, uint3 id, bool init)
{
	float spatialError = computeSpatialError(source, target, id);
	float colorError = computeColorError(source, target, id, init);
	weightedColorError = colorError * inputBuffer[0].colorErrorWeight;
	weightedSpatialError = spatialError * inputBuffer[0].spatialErrorWeight;
	float totalError = weightedSpatialError + weightedColorError;

	return totalError;
}



void selectMin(uint indexOffset, uint3 tid)
{
	// Use lower faction of threads to aggregate data
	if(tid.y < indexOffset)
	{
		if(tid.y + indexOffset < inputBuffer[0].nonHolePixelCount){

			//Pick the target with the lowest TOTAL error
			if (initError[tid.y].x > initError[tid.y + indexOffset].x)
			{
				initError[tid.y] = initError[tid.y + indexOffset];
				bestTargetIndex[tid.y] =  bestTargetIndex[tid.y + indexOffset];

			}


		}
	}
}



void InitHolePixelsMapping (uint3 tid)
{
//	setErrorBufferData(tid.x, float4(holePixelCoord(3),0,1));

	// Initialize with high error. Some threads are not processing valid pixels, these should be excluded from consideration
	initError[tid.y] = float3(9999,9999,9999);
	bestTargetIndex[tid.y] = tid.y;

	//Whether this pixel should be processed or ignored
	// Since the number of threads has to be set statically, there might be to many threads assigned
	uint validIndex = tid.y < inputBuffer[0].nonHolePixelCount;
	uint processPixel = validIndex;

	if(processPixel > 0){

		float2 coord = holePixelCoord(tid.x);

		//Process pixel only if there are enough neighbours already mapped and it is not mapped itself
		//use uint instead of bool
//		uint mappable = min(AreEightNeighboursMapped(coord), AreFiveByFiveNeighboursMapped(coord) );
//		uint mappable = min(AreColorNeighboursMapped(coord), AreFiveByFiveNeighboursMapped(coord) );
//		uint mappable = AreColorNeighboursMapped(coord);
//		uint mappable = AreFourNeighboursMapped(coord);
		uint mappable = AreEightNeighboursMapped(coord);
		processPixel =  max(0,mappable - hasPixelBeenMapped(tid.x));

		// check if enough neighbours are mapped
		if(processPixel > 0){
			float2 target = nonHolePixelCoord(tid.y);
			float weightedColorError;
			float weightedSpatialError;
//			float error = totalError(coord, target, weightedColorError,weightedSpatialError, tid, true);
			float error = computeColorError(coord, target, tid, true);

			//USE COLOR ERROR ONLY FOR INIT
			initError[tid.y] = float3(error,0,0);//, weightedColorError, weightedSpatialError);

//			if(tid.x == 6 ){
//				setErrorBufferData(tid.y+1, float4(error, weightedColorError, weightedSpatialError,1));
//			}

		}
	}

	//wait for all threads to write to initError array
	GroupMemoryBarrierWithGroupSync();

	//SELECT MIN ERROR
	uint indexOffset;
	if(INIT_RESOLUTION >= 16)
	{
		//1st iteration 256->128
		//Get the index indicating the middle of the initError array
		indexOffset = 128;// = 256/2
		selectMin(indexOffset, tid);
		GroupMemoryBarrierWithGroupSync();

		//2nd iteration 128->64
		//Get the index indicating the middle of the initError array
		indexOffset = indexOffset / 2;
		selectMin(indexOffset, tid);
		GroupMemoryBarrierWithGroupSync();
	}

	//1st iteration 64->32
	//Get the index indicating the middle of the initError array
	indexOffset = 32; // =64/2
	selectMin(indexOffset, tid);
	GroupMemoryBarrierWithGroupSync();

	//2nd iteration 32->16
	//Get the index indicating the middle of the initError array
	indexOffset = indexOffset / 2;
	selectMin(indexOffset, tid);
	GroupMemoryBarrierWithGroupSync();

	//3rd iteration 16->8
	//Get the index indicating the middle of the initError array
	indexOffset = indexOffset / 2;
	selectMin(indexOffset, tid);
	GroupMemoryBarrierWithGroupSync();

	//4th iteration 8->4
	//Get the index indicating the middle of the initError array
	indexOffset = indexOffset / 2;
	selectMin(indexOffset, tid);
	GroupMemoryBarrierWithGroupSync();

	//5th iteration 4->2
	//Get the index indicating the middle of the initError array
	indexOffset = indexOffset / 2;
	selectMin(indexOffset, tid);
	GroupMemoryBarrierWithGroupSync();

	//6th iteration 2->1
	//Get the index indicating the middle of the initError array
	indexOffset = indexOffset / 2;
	selectMin(indexOffset, tid);
	GroupMemoryBarrierWithGroupSync();

	//finally set coordinate to mapping

	if(tid.y == 0){
		float2 holeCoord = holePixelCoord(tid.x);
		if(processPixel > 0)
		{
			float2 bestTarget = nonHolePixelCoord(bestTargetIndex[0]);
			mapSetNew(holeCoord, float4(bestTarget,1,1) );

			//ARGB-format
			setErrorTextureData(holeCoord, float4(initError[0], 0));

			setOutputData(tid.x, 1);

//			if(tid.x==3)
//			{
//				setErrorBufferData(0, float4(bestTarget, bestTargetIndex[0],1));
////				mapSetNew(holeCoord, float4(0,0,1,1) );
//			}

		}else if (hasPixelBeenMapped(tid.x)>0){
			//If the pixel is already mapped, or not changed, update the new map
			float4 oldMapping = mapGetOld(holeCoord);
			mapSetNew(holeCoord, oldMapping );

		}
		else{
			//This will mark it as unmapped, because the initError is set to a very large value
			setErrorTextureData(holeCoord, float4(initError[0], 0));
		}

	}
}
[numthreads(1,INIT_RESOLUTION*INIT_RESOLUTION,1)] 
void InitHolePixelsMapping8(uint3 tid : SV_DispatchThreadID)
{
	 InitHolePixelsMapping (tid);
}

[numthreads(1,INIT_RESOLUTION*INIT_RESOLUTION,1)] 
void InitHolePixelsMapping16(uint3 tid : SV_DispatchThreadID)
{
	 InitHolePixelsMapping (tid);
}
float2 OutOfHoleBest(float2 source, float2 proposedMapping, uint id)
{
	float2 neighbours[8];
	EightNeighbours(source, neighbours);
	float bestError = 999; 
	float2 bestNeighbour;
	float colorError, spatialError;
	float2 mappedNeighbour;

	for(uint i=0; i<8; i++)
	{
		mappedNeighbour = mapGetOld(neighbours[i]).xy + randomTex(id.x).xy;
		float error = totalError(source, mappedNeighbour, colorError, spatialError, uint3(id,0,0), false);
		if(error<bestError)
		{
			bestError = error;
			bestNeighbour = mappedNeighbour;
		}
	}
	return bestNeighbour;
}
float2 OutOfHoleForce(float2 source, float2 proposedMapping, uint id)
{
	//TODO: THIS IS FOR TESTING ONLY
	float2 direction = proposedMapping - inputBuffer[0].centerOfMass ;
	direction = direction / (length(direction)*length(direction));
	return proposedMapping + direction;
}


float2 OutOfHoleRandom(float2 source, float2 proposedMapping, uint id)
{
	float2 neighbours[8];
	EightNeighbours(source, neighbours);

	float2 randomVector ;
	float4 randomNeighbourMapped = float4(proposedMapping,0,0);
	float3 rand;
	uint index = 0;

	// Try a arandom neighbour plus a random offset of length 1 until you get a valid mapping or more than a certain number of iterations have passed.
	// Then it was just bad luck. But the loop MUST terminate after a fixed number of iterations, otherwise this is a problem for the compiler
	while(IsValidMapping(randomNeighbourMapped) < 1  && index < 10)
	{
		rand = randomTex(id.x+index);
		randomVector = rand.xy;
		randomNeighbourMapped =  mapGetOld(neighbours[uint(rand.z)]);
		index = index + 1;
	}

//	float4 randomNeighbourMapped = mapGetOld(randomNeighbour);
	float2 escapedMapping = randomNeighbourMapped.xy + randomVector;
//	setErrorBufferData(id, float4(id+index, randomTex(id+index)));
	return escapedMapping;
}

[numthreads(1,1,1)] 
void RefineMapping(uint3 tid : SV_DispatchThreadID)
{
	uint id = tid.x;
	float2 source = holePixelCoord(id);
	float2 oldMapping = mapGetOldNonInterpolated(source).xy;

	//compute directions
	bool invalid;
	float2 spatialDirection = computeSpatialDirection(source, oldMapping, uint3(id,0,0), false, invalid);
	float2 colorDirection = computeColorDirection(source, oldMapping, uint3(id,0,0));

	//scale them
	float2 scaledSpatialDirection = spatialDirection * inputBuffer[0].spatialStepSize * inputBuffer[0].spatialErrorWeight;
	colorDirection = colorDirection * inputBuffer[0].colorStepSize * inputBuffer[0].colorErrorWeight;
	float2 totalDirection = scaledSpatialDirection  + colorDirection ;
//	setErrorBufferData(id,float4(scaledSpatialDirection,colorDirection));
	float2 newMapping = oldMapping + totalDirection;

	//If mapped into the hole, get out of there
	if(isInHole(newMapping))
	{
		
		newMapping = OutOfHoleRandom(source, newMapping, id);
	}

	//Clamp if outside valid range
	newMapping.x = min(max(newMapping.x,0.0), inputBuffer[0].currentWidth-1.0);
	newMapping.y = min(max(newMapping.y,0.0), inputBuffer[0].currentHeight-1.0);

	//update error texture
	float spatialError = length(spatialDirection);
	float colorError = computeColorError(source, newMapping.xy, uint3(id,0,0), false);
	float totalError = colorError * inputBuffer[0].colorErrorWeight + spatialError * inputBuffer[0].spatialErrorWeight;

	setErrorTextureData(source, float4(totalError, colorError* inputBuffer[0].colorErrorWeight, spatialError * inputBuffer[0].spatialErrorWeight, 0));
	setUpdateTextureData(source, length(totalDirection), length(colorDirection), length(scaledSpatialDirection) );

//	setErrorBufferData(id, float4( length(totalDirection), length(colorDirection) * inputBuffer[0].colorErrorWeight, length(scaledSpatialDirection) * inputBuffer[0].spatialErrorWeight, 0) );

	
	mapSetNew(source, float4(newMapping,1,1));
}
