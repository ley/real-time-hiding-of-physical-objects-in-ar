﻿Shader "Hidden/mapped_inpainting"
{
	Properties
	{
		_BkgrTex ("BackgroundTexture", 2D) = "white" {}
		_Mask ("MaskTexture", 2D) = "white" {}
		_ObjectTex ("ObjectTexture", 2D) = "white" {}
		_MappingTex ("MappingTexture", 2D) = "white" {}
		map_width ("Map width", Int) = 8
		map_height ("Map height", Int) = 8
		real_map_width ("Actual map width", Int) = 8
		real_map_height ("Actual map height", Int) = 8
		render_virtual ("Render virtual object", Int) = 1
		mipmap_level("Mipmap level", Int) = 0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM 
			#pragma vertex vert
			#pragma fragment frag			
			
			#include "UnityCG.cginc"

			struct Input
			{
				float4 vertex : POSITION;
				float2 uv_tex: TEXCOORD0;
			};

			struct v2f
			{
				float2 uv_tex : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _BkgrTex;

			//The following color<->object relations exist in the mask
			//Black: background
			//red: tracked object
			//green: virtual object
			sampler2D _Mask;
			sampler2D _ObjectTex;
			//Contains the mapping for each pixel to the background texture encoded into the x and y coordinates as normalizesd texture coordinates.
			sampler2D _MappingTex;

			float4 _Mask_ST;
			float4 _BkgrTex_ST;
			float4 _ObjectTex_ST;
			float4 _MappingTex_ST;

			int map_width;
			int map_height;
			int real_map_width;
			int real_map_height;
			int render_virtual;
			int mipmap_level;
						
			v2f vert (Input v)
			{
				v2f o;
				o.uv_tex = v.uv_tex;
				o.vertex = UnityObjectToClipPos(v.vertex);
//				o.vertex = v.vertex;
				return o;
			}

//			float4 bilinear_lookup(float2 coord, width, height)
//			{
//				float x = coord.x*width;
//				float y = coord.y*height;
//				float x_floor = floor(x)/width;
//				float x_ceil = ceil(x)/width;
//				float y_floor = floor(y)/height;
//				float y_ceil = ceil(y)/height;
//
//				float4 col_top_left = tex2D(_MappingTex, float2(x_floor, y_ceil));
//				float4 col_top_right = tex2D(_MappingTex, float2(x_ceil, y_ceil));
//				float4 col_bottom_left = tex2D(_MappingTex, float2(x_floor, y_floor));
//				float4 col_bottom_right = tex2D(_MappingTex, float2(x_ceil, y_floor));
//				float dx = x-x_floor;
//				float dy = y-y_floor;
//				float4 col = (dy)*((1-dx)*col_top_left+dx*col_top_right) + (1-dy)*((1-dx)*col_bottom_left + dx*col_bottom_right);
//				return col;
//			}

			fixed4 frag (v2f i) : SV_Target
			{
				//Correct the mapping lookup, so that the corners of the textures are mapped to the center of the mapping's corner pixels.
				//This allows for interpolation at the edges of the image.
				//Also the mapping texture is padded with 1 row/column at each side
				float2 corrected_uv = float2(i.uv_tex.x/real_map_width*(map_width-1) + 1.5/real_map_width, i.uv_tex.y/real_map_height*(map_height-1) + 1.5/real_map_height);
				// get lookup coordinates from the mapping texture

				float4 tex_coord = tex2D(_MappingTex, corrected_uv);

//				float x = 10;
//				float y = 10;
//				if(tex_coord.x == x && tex_coord.y == y)
////				if(tex_coord.x > x-1 && tex_coord.x < x+1 && tex_coord.y > y-1 && tex_coord.y < y+1 )
//				{
//					return fixed4(0,0,1,1);
//				}
////

				//also render the virtual object
				if(render_virtual == 1)
				{
					fixed4 col_object = tex2D(_ObjectTex, i.uv_tex);

					//if the pixel is part of the virtual object, render this
					if(col_object.a > 0)
					{
						//return fixed4(1,0,1,1);;
						return col_object;
					}
				}

				//if the pixel is not mapped, return red
				if(tex_coord.w < 1.0)
				{
					
					return fixed4(1,0,1,1);
				}

				if(tex_coord.z == 0)
				{
					return tex2D(_BkgrTex, i.uv_tex);
				}
			
				// Pixel could still be mapped into the hole due to interpolation, handle that case by taking the closest pixel
				float4 mask_value = tex2Dlod(_Mask, float4((tex_coord.x+0.5)/map_width, (tex_coord.y+0.5)/map_height, 0, mipmap_level));
				if(mask_value.r>0)
				{
//					return fixed4(0,0,1,1);
					corrected_uv = float2(round(i.uv_tex.x*(map_width-1))/real_map_width + 1.5/real_map_width, round(i.uv_tex.y*(map_height-1))/real_map_height + 1.5/real_map_height);
					tex_coord = tex2D(_MappingTex, float2(corrected_uv) );
					//tex_coord = float4(round(tex_coord.x), round(tex_coord.y),0,1);
				}


				fixed4 col = tex2Dlod(_BkgrTex, float4((tex_coord.x+0.5)/map_width, (tex_coord.y+0.5)/map_height, 0, mipmap_level)); // float4((tex_coord.x+0.5)/(tex_width+1), (tex_coord.y+1)/(tex_height+1)
				return col;
			}
			ENDCG
		}
	}
}
