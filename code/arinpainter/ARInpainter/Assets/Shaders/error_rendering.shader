﻿Shader "Hidden/error_rendering"
{
	Properties
	{
		_ErrorTex ("ErrorTex", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert Lambert
			#pragma fragment frag Lambert
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _ErrorTex;
			float4 _ErrorTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _ErrorTex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				float4 col = tex2D(_ErrorTex, i.uv);
				if(col.r > 1000.0)
				{
					return fixed4(1,1,1,0.3);
				}
				//return col.rgba;
				return fixed4(col.rgb,1);
			}
			ENDCG
		}
	}
}
