﻿Shader "Hidden/inpaint_prep_virtual"
{
	Properties
	{
	}
	SubShader
	{
		Tags { "RenderType"="MaskVirtual" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// render a green if the pixel is part of the virtual object
				return fixed4(0,1,0,1);
			}
			ENDCG
		}
	}
}
