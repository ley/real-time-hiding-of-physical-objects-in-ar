﻿Shader "Hidden/solid_inpainting"
{
	Properties
	{
		_BkgrTex ("BackgroundTexture", 2D) = "white" {}
		_Mask ("MaskTexture", 2D) = "white" {}
		_ObjectTex ("ObjectTexture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM 
			#pragma vertex vert
			#pragma fragment frag			
			
			#include "UnityCG.cginc"

			struct Input
			{
				float4 vertex : POSITION;
				float2 uv_tex: TEXCOORD0;
			};

			struct v2f
			{
				float2 uv_tex : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _BkgrTex;

			//The following color<->object relations exist in the mask
			//Black: background
			//red: tracked object
			//green: virtual object
			sampler2D _Mask;
			sampler2D _ObjectTex;

			float4 _Mask_ST;
			float4 _BkgrTex_ST;
			float4 _ObjectTex_ST;
						
			v2f vert (Input v)
			{
				v2f o;
				o.uv_tex = v.uv_tex;
				o.vertex = UnityObjectToClipPos(v.vertex);
				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				//also render the virtual object
				fixed4 col_object = tex2D(_ObjectTex, i.uv_tex);
				float4 mask_value = tex2D(_Mask, i.uv_tex);

				if(mask_value.r > 0.0)
				{
					return fixed4(0.0,0.5,1.0,1.0);
				}

				//if the pixel is part of the virtual object, render this
				if(col_object.g > 0)
				{
					//return fixed4(1,0,1,1);;
					return col_object;
				}

				return tex2D(_BkgrTex, i.uv_tex);
			}
			ENDCG
		}
	}
}
