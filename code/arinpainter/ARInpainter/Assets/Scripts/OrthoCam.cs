﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

namespace ARInpainter{
	public class OrthoCam : MonoBehaviour, InpaintingCamera {

		public GameObject RenderQuad;
		public MainRendering MainRendering;
		//private RenderTexture BackgroundRenderTexture;
		private RenderTexture MaskRenderTexture;
		private RenderTexture ObjectRenderTexture;
		public ComputeShader MappingShader;
		public ComputeShader InitNormalShader;
		public ComputeShader ReInitMappingShader;
		public Shader MappedInpaintingShader;
		public Shader SolidInpaintingShader;
		public Shader ErrorShader;
		public Shader ShowMappingShader;
		public Shader BackgroundShader;
		public Shader UpdateShader;

		private Texture2D tempMaskTexture;
		private Texture2D tempBackgroundTexture;

		private Inpainter inpainter;

		void Start()
		{
			Init ();
		}

		// Use this for initialization
		void Init ()
		{

//			QualitySettings.vSyncCount = 0;
//			Application.targetFrameRate = 2;

			var camera = this.GetComponent<Camera> ();
			Debug.AssertFormat (camera != null, "Ortho Camera not initialized.");

			var shaders = new Dictionary<Configuration.RenderMode.Mode, Shader>(){
				{Configuration.RenderMode.Mode.Mapped,this.MappedInpaintingShader},
				{Configuration.RenderMode.Mode.Solid,this.SolidInpaintingShader},
				{Configuration.RenderMode.Mode.Error,this.ErrorShader},
				{Configuration.RenderMode.Mode.Mapping,this.ShowMappingShader},
				{Configuration.RenderMode.Mode.Background, this.BackgroundShader},
				{Configuration.RenderMode.Mode.Update, this.UpdateShader}
			};

			//Set render textures to proper size
			var width =  MainRendering.CanvasPixelWidth();
			var height = MainRendering.CanvasPixelHeight ();

			//Create Mask and Object RenderTexture
			this.CreateRenderTexture(width: width, height: height);

			//Deal with different screen sizes
			float quadRatio = (float)(1.0*width/height);
			//camera.aspect = quadRatio;
			RenderQuad.transform.localScale = new Vector3(1.3f*camera.aspect*quadRatio, 1.3f*camera.aspect, 1);

			//Create and start inpainter
			this.inpainter = new Inpainter (
				mappingShader: MappingShader,
				initNormalShader: InitNormalShader,
				reInitMappingShader: ReInitMappingShader,
				inpaintingCamera: this,
				shaders: shaders,
				textureWidth: width,
				textureHeight: height
			);

			inpainter.Start ();


		}

		private void CreateRenderTexture(int width, int height)
		{
			this.MaskRenderTexture = new RenderTexture(width: width, height: height, depth: 0, format: RenderTextureFormat.ARGBFloat);
			//this.MaskRenderTexture.enableRandomWrite = true;
			this.MaskRenderTexture.useMipMap = true;
			this.MaskRenderTexture.filterMode = FilterMode.Point;
			this.MaskRenderTexture.wrapMode = TextureWrapMode.Clamp;
			this.MaskRenderTexture.Create ();

			this.ObjectRenderTexture = new RenderTexture(width: width, height: height, depth: 24, format: RenderTextureFormat.ARGB32);
			//this.ObjectRenderTexture.enableRandomWrite = true;
			this.ObjectRenderTexture.useMipMap = false;
			this.ObjectRenderTexture.filterMode = FilterMode.Point;
			this.ObjectRenderTexture.wrapMode = TextureWrapMode.Clamp;
			this.ObjectRenderTexture.Create ();
		}

		void OnPreRender (){

			//TIME IT
			this.inpainter.timeIt.StartTotal();
			this.inpainter.timeIt.StartSetupFrame ();

			MainRendering.CreateMask (MaskRenderTexture);
			MainRendering.CreateVirtualObject (ObjectRenderTexture);
			MainRendering.setupCameraForNormalRendering();

			//TIME IT
			this.inpainter.timeIt.EndSetupFrame();

			inpainter.OnPreRender ();

		}

		void OnPostRender(){
			this.inpainter.timeIt.EndTotal ();
			this.inpainter.LogTimeIt ();
		}
			
		public Texture getBackgroundRenderTexture (){
			return this.MainRendering.BackgroundRenderTexture;
		}
		public Texture getMaskRenderTexture (){
			return this.MaskRenderTexture;
		}
		public Texture2D getMaskTexture (){
			if (this.tempMaskTexture == null || this.tempMaskTexture.width != MaskRenderTexture.width || tempMaskTexture.height != MaskRenderTexture.height)
			{
				this.tempMaskTexture = new Texture2D (width: MaskRenderTexture.width, height: MaskRenderTexture.height, format: Configuration.Mask.TextureFormat, mipmap: MaskRenderTexture.useMipMap);
			}
			Common.renderTextureToTexture2D (renderTexture: this.MaskRenderTexture, targetTexture: ref this.tempMaskTexture);
			return this.tempMaskTexture;
		}

		public Texture2D getBackgroundTexture (){
			if (this.tempBackgroundTexture == null || this.tempBackgroundTexture.width != this.MainRendering.BackgroundRenderTexture.width || tempBackgroundTexture.height != this.MainRendering.BackgroundRenderTexture.height)
			{
				this.tempBackgroundTexture = new Texture2D (width: this.MainRendering.BackgroundRenderTexture.width, height: this.MainRendering.BackgroundRenderTexture.height, format: TextureFormat.ARGB32, mipmap: this.MainRendering.BackgroundRenderTexture.useMipMap);
			}
			Common.renderTextureToTexture2D (renderTexture: this.MainRendering.BackgroundRenderTexture, targetTexture: ref this.tempBackgroundTexture);
			return this.tempBackgroundTexture;
		}

		public Texture getObjectRenderTexture (){
			return this.ObjectRenderTexture;
		}
		public void setInpaintingShader(Shader shader){
			this.RenderQuad.GetComponent<Renderer> ().material.shader = shader;
		}
		public Material getMaterial(){
			return RenderQuad.GetComponent<Renderer> ().material;
		}

	}
}
