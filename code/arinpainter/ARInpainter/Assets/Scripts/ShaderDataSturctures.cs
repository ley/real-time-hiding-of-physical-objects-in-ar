﻿using System;
using UnityEngine;

namespace ARInpainter
{
	struct InitInputData
	{
		public uint mipmapLevel;
		public uint nonHolePixelCount;
		public uint holePixelCount;
		public uint minNeighboursMappedCountFiveByFive;
		public uint minNeighboursMappedCountEight;
		public uint minNeighboursMappedCountColor;
		public uint currentWidth;
		public uint currentHeight;
		public uint realMapWidth;
		public uint realMapHeight;
		public float maxSpatialComponentDist;
		public float colorErrorWeight;
		public float spatialErrorWeight;
		public float typicalColorError;
		public float colorStepSize;
		public float spatialStepSize;
		public float normalPixelColorWeight;
		public Vector2 centerOfMass;

		public int strideSize(){
			return 10*sizeof(uint) + 9*sizeof(float);
		}
	}

	struct InitOutputData
	{
		public uint isPixelsMapped;
	}

	struct ReInitInputData
	{
		public uint newRealMapWidth;
		public uint newRealMapHeight;
		public uint oldRealMapWidth;
		public uint oldRealMapHeight;
		public uint newMapWidth;
		public uint newMapHeight;
		public uint oldMapWidth;
		public uint oldMapHeight;
	}

}