﻿using System;
using UnityEngine;

namespace ARInpainter
{
	public class Common
	{
		public static void renderTextureToTexture2D(RenderTexture renderTexture, ref Texture2D targetTexture){
			Debug.AssertFormat(renderTexture != null && renderTexture.width > 0 && renderTexture.height > 0, "Invalid render texture");
			RenderTexture.active = renderTexture;
			targetTexture.ReadPixels (new Rect (0, 0, renderTexture.width, renderTexture.height), 0, 0);
			targetTexture.Apply ();
			//return targetTexture;
		}
	}
}

