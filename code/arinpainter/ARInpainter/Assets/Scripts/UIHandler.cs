﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace ARInpainter{
	public class UIHandler : MonoBehaviour  {

		public Dropdown shaderDropdown;
		public Dropdown initResolutionDropdown;
		public Toggle renderVirtualObjectToggle;
		public Toggle interpolateToggle;
		public Toggle punchHoleToggle;
		public Slider alphaSlider;
		public Text alphaLabel;
		public Slider iterationSlider;
		public Text iterationLabel;
		public Slider levelCountSlider;
		public Text levelCountLabel;

		public delegate void OnInterpolationChangeDelegate();
		public static OnInterpolationChangeDelegate OnInterpolationChange;
		public delegate void OnInitResolutionChangeDelegate();
		public static OnInitResolutionChangeDelegate OnInitResolutionChange;
		public delegate void OnAlphaChangeDelegate();
		public static OnAlphaChangeDelegate OnAlphaChange;

		void Start(){
			initShaderDropdown ();
			initInitResolutionDropdown ();
			initVirtualObjectToggle ();
			initInterpolateToggle ();
			initPunchHoleToggle ();
			initAlphaSlider();
			initIterationSlider ();
			initLevelCountSlider ();
		}

		public void ShaderModeDropdownChanged(){
			int selectedIndex = shaderDropdown.value;
			var renderMode = (Configuration.RenderMode.Mode) selectedIndex;
			Configuration.RenderMode.CurrentRenderMode = renderMode;
		}

		public void InitResolutionDropdownChanged(){
			int selectedIndex = initResolutionDropdown.value;
			var resolution = (Configuration.ComputeShaders.Initialization.InitialResolution) Configuration.ComputeShaders.Initialization.InitResolutionLabels[selectedIndex];
			Configuration.ComputeShaders.Initialization.StartResolution = resolution;
			if (OnInitResolutionChange != null)
			{
				OnInitResolutionChange.Invoke ();
			}
		}

		private void initShaderDropdown(){
			//Add listener
			shaderDropdown.onValueChanged.AddListener (delegate{this.ShaderModeDropdownChanged();});

			//set options
			List<Dropdown.OptionData> options = new List<Dropdown.OptionData> ();
			foreach(var label in Configuration.RenderMode.ShaderModeLabels){
				options.Add(new Dropdown.OptionData(label));
			}
			shaderDropdown.options = options;
			shaderDropdown.value = (int)Configuration.RenderMode.CurrentRenderMode;
		}

		private void initInitResolutionDropdown(){
			//Add listener
			initResolutionDropdown.onValueChanged.AddListener (delegate{this.InitResolutionDropdownChanged();});

			//set options
			List<Dropdown.OptionData> options = new List<Dropdown.OptionData> ();
			int selectedOption = 0; 
			int i = 0;
			foreach(var label in Configuration.ComputeShaders.Initialization.InitResolutionLabels){
				options.Add(new Dropdown.OptionData(""+label));
				if (label == (int)Configuration.ComputeShaders.Initialization.StartResolution) {
					selectedOption = i;
				}
				i++;
			}
			initResolutionDropdown.options = options;
			initResolutionDropdown.value = selectedOption;
		}

		private void initVirtualObjectToggle(){
			this.renderVirtualObjectToggle.onValueChanged.AddListener(delegate {this.OnVirtualObjectToggleChanged();});
			this.renderVirtualObjectToggle.isOn = Configuration.RenderMode.RenderVirtualObject;
		}
		private void initInterpolateToggle(){
			this.interpolateToggle.onValueChanged.AddListener(delegate {this.OnInterpolateToggleChanged();});
			this.interpolateToggle.isOn = Configuration.RenderMode.InterpolateMapping;
		}
		private void initPunchHoleToggle(){
			this.punchHoleToggle.onValueChanged.AddListener(delegate{this.onPunchHoleToggleChanged();});
			this.punchHoleToggle.isOn = Configuration.RenderMode.PunchHole;
		}

		private void initAlphaSlider(){
			this.alphaSlider.value = Configuration.Inpainting.Weights.ColorErrorWeight;
			this.alphaLabel.text = ""+Configuration.Inpainting.Weights.ColorErrorWeight;
			this.alphaSlider.onValueChanged.AddListener (delegate {
				this.OnAlphaSliderChanged ();
			});
		}

		private void initIterationSlider(){
			this.iterationSlider.value = Configuration.Inpainting.NumberOfRefinementIterations;
			this.iterationLabel.text = ""+Configuration.Inpainting.NumberOfRefinementIterations;
			this.iterationSlider.onValueChanged.AddListener (delegate {
				this.OnIterationSliderChanged ();
			});
		}

		private void initLevelCountSlider(){
			this.levelCountSlider.value = Configuration.Inpainting.NumberOfMipmapLevelsToProcess;
			this.levelCountLabel.text = ""+Configuration.Inpainting.NumberOfMipmapLevelsToProcess;
			this.levelCountSlider.onValueChanged.AddListener (delegate {
				this.OnLevelCountSliderChanged();
			});
		}

		private void OnAlphaSliderChanged(){
			float alpha = this.alphaSlider.value;
			this.alphaLabel.text = "" + alpha;
			Configuration.Inpainting.Weights.ColorErrorWeight = alpha;
			//Debug.Log (Configuration.Inpainting.Weights.ColorErrorWeight + "," + Configuration.Inpainting.Weights.SpatialErrorWeight);
		}

		private void OnIterationSliderChanged(){
			int iterationCount =(int) this.iterationSlider.value;
			this.iterationLabel.text = "" + iterationCount;
			Configuration.Inpainting.NumberOfRefinementIterations = iterationCount;
		}
		private void OnLevelCountSliderChanged(){
			int levelCount = (int)this.levelCountSlider.value;
			this.levelCountLabel.text = "" + levelCount;
			Configuration.Inpainting.NumberOfMipmapLevelsToProcess = levelCount;
		}

		private void OnVirtualObjectToggleChanged(){
			Configuration.RenderMode.RenderVirtualObject = this.renderVirtualObjectToggle.isOn;
		}

		private void OnInterpolateToggleChanged(){
			Configuration.RenderMode.InterpolateMapping = this.interpolateToggle.isOn;
			if (OnInterpolationChange != null)
			{
				OnInterpolationChange.Invoke ();
			}
		}
		private void onPunchHoleToggleChanged()
		{
			Configuration.RenderMode.PunchHole = this.punchHoleToggle.isOn;
		}

	}
}