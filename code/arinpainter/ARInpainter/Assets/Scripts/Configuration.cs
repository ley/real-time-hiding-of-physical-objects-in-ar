﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace ARInpainter{

	/**
	 * Contains all global configurations
	 */
	public class Configuration : ScriptableObject {

		public struct Files{
			public static string SceneName = "mill";
			public static int startFrame = 1;
			public static int width = 1024;//1024;//256;//128;//2048;//
			public static int height = 1024;//1024;//256;//128;//2048;//
		}


		public struct Mask{
			public static RenderTextureFormat RenderTextureFormat = RenderTextureFormat.ARGB32;
			public static TextureFormat TextureFormat = TextureFormat.ARGB32;
		}

		public struct RenderMode{
			public enum Mode{
				Solid=0,
				Mapped=1,
				Error=2,
				Mapping=3,
				Background=4,
				Update=5
			}
			public static string[] ShaderModeLabels = new string[6]{ "Solid", "Mapped", "Error","Mapping","Background","Update"};
			public static Mode CurrentRenderMode = Mode.Mapped;
			public static bool RenderVirtualObject = false;
			public static bool SingleFrameRendering = true;
			public static bool InitOnly = false;
			public static bool InterpolateMapping = false;
			public static bool PunchHole = false;
			public static bool FixedHole = true;
		}

		public struct ComputeShaders{
			public struct Initialization{
				public enum InitialResolution{
					Side8=8,
					Side16=16,
				}
				public static int[] InitResolutionLabels = new int[2]{8,16};
				public static InitialResolution StartResolution = InitialResolution.Side16;
				public static uint MinMappedNeighboursCount5x5 = 10;
				public static uint MinMappedNeighboursCount8 = 4;
				public static uint MinMappedNeighboursCountColor = 5;
			}
		}

		public struct Inpainting{
			public struct Weights{
				public static float ColorErrorWeight = 0.93f;
				public static float SpatialErrorWeight{ get { return (1 - ColorErrorWeight); } }
				public static float NormalPixelColorWeight = 20;
			}
			public struct StepSizes{
				public static float ColorStepSize = 1f;
				public static float SpatialStepSize = 1/4f;
			}

			public static float MaxSpatialComponentDist = 4f;
			public static int NumberOfRefinementIterations = 30;
			public static int NumberOfMipmapLevelsToProcess = 2;
		}

		public struct RenderTextures{
			public static int DefaultWidth = 1024;
			public static int DefaultHeight = 1024;
		}
	}
}