﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using System.IO;

namespace ARInpainter{
	public class MainRendering : MonoBehaviour {

		//public MaskCreator maskCreator;
		public Material MaskVirtualObjectMaterial;
		public Material FinalVirtualObjectMaterial;
		public GameObject VirtualObject;
		public GameObject BackgroundPlane;
		public GameObject HoleQuad;

		private RenderTexture backgroundRenderTexture;
		public RenderTexture BackgroundRenderTexture {
			get {
				return this.backgroundRenderTexture;
			}
		}
		private new Camera camera;

		// Use this for initialization
		void Start () {

			this.camera = this.GetComponent<Camera> ();
			//HACK: TODO: The BackgroundPlane's dimension is manually set in the Unity editor.
			// To solve this properly, the camera ratio has to be known, this is only possible once Vuforia has started the camera...
			var heightFactor = BackgroundPlane.transform.localScale.z / BackgroundPlane.transform.localScale.x;
				
			Debug.AssertFormat (this.camera != null, "Helper Camera not initialized.");
			this.backgroundRenderTexture = camera.targetTexture;
			this.backgroundRenderTexture.height = (int) (Configuration.RenderTextures.DefaultHeight * heightFactor);
			this.backgroundRenderTexture.width = Configuration.RenderTextures.DefaultWidth;
		}

		public int CanvasPixelWidth()
		{
			return Configuration.RenderTextures.DefaultWidth;
		}

		public int CanvasPixelHeight()
		{
			var heightFactor = BackgroundPlane.transform.localScale.z / BackgroundPlane.transform.localScale.x;
			return (int) (Configuration.RenderTextures.DefaultHeight * heightFactor);
		}

		/*void saveTexture(Texture2D texture, string toPath){
			Debug.AssertFormat (texture != null && texture.width > 0 && texture.height > 0, "Invalid texture");
			var bytes = texture.EncodeToPNG ();
			var fileStream = File.Open (path: toPath+".png", mode: FileMode.Create);
			var binary= new BinaryWriter(fileStream);
			binary.Write(bytes);
			fileStream.Close();
		}*/

		/*Texture2D renderTextureToTexture2D(RenderTexture renderTexture){
			Debug.AssertFormat(renderTexture != null && renderTexture.width > 0 && renderTexture.height > 0, "Invalid render texture");
			Texture2D texture = new Texture2D(width: renderTexture.width, height: renderTexture.height, format: Configuration.Mask.TextureFormat, mipmap: false);
			texture.ReadPixels (new Rect (0, 0, renderTexture.width, renderTexture.height), 0, 0);
			texture.Apply ();
			return texture;
		}*/

		/*private Texture2D getCameraTexture(){
			Texture2D cameraTexture = (Texture2D)VuforiaRenderer.Instance.VideoBackgroundTexture;
			if (cameraTexture == null || cameraTexture.width <= 0 || cameraTexture.height <= 0) {
				Debug.LogWarning ("Camera not ready");
				return null;
			}
			return cameraTexture;
		}*/
	
		public void CreateVirtualObject(RenderTexture renderTexture){
			camera.targetTexture = renderTexture;

			camera.clearFlags = CameraClearFlags.SolidColor;
			camera.backgroundColor = new Color (0,0,0,0);

			VirtualObject.GetComponentInChildren<Renderer> ().material = FinalVirtualObjectMaterial;

			var layerMask = LayerMask.GetMask("VirtualLayer");
			if (layerMask == 0) {
				Debug.LogError ("VirtualLayer Mask not found");
			}
			camera.cullingMask = layerMask;
			camera.Render ();

			Debug.Assert (renderTexture.width > 0 && renderTexture.height > 0);
		}

		public void setupCameraForNormalRendering()
		{
			camera.targetTexture = this.backgroundRenderTexture;
			camera.clearFlags = CameraClearFlags.SolidColor;
			camera.backgroundColor = new Color (0,0,0,0);

			VirtualObject.GetComponentInChildren<Renderer> ().material = FinalVirtualObjectMaterial;
			var layerMask = LayerMask.GetMask("BackgroundLayer") ;
			
			// punch a red hole into the background where the inpainting area is. Don't do so when only showing the background
			if (Configuration.RenderMode.CurrentRenderMode != Configuration.RenderMode.Mode.Background && Configuration.RenderMode.PunchHole) {
				layerMask = layerMask | LayerMask.GetMask("TrackedLayer");
			}
			if (layerMask == 0) {
				Debug.LogError ("BackgroundLayer Mask not found");
			}
			camera.cullingMask = layerMask;
		}

		/***
			 * Creates a mask texture of pixels that need to be inpainted
			 * This is done by first rendering all tracked objects with value 1 and then all virtual objects with value 0. The render texture is cleared with 0.
			 * The pixels with 1 will be element of the tracked\virtual object pixel-set.
			 ***/
		public void CreateMask(RenderTexture renderTexture){
			
			//Turn of the HoleQuad when tracking target is found
			this.HoleQuad.SetActive (!TargetBehaviour.TargetFound);

			camera.targetTexture = renderTexture;

			//0. Clear with 0
			camera.clearFlags = CameraClearFlags.SolidColor;
			camera.backgroundColor = new Color (0,0,0,0);

			//1. Render virtual object with 0
			//Set shader for virutal Object
			VirtualObject.GetComponentInChildren<Renderer>().material = MaskVirtualObjectMaterial;

			//Get virtual layer, so all virtual
			var virtualLayerMask = LayerMask.GetMask ("VirtualLayer");
			if (virtualLayerMask == 0) {
				Debug.LogError ("VirtualLayer Mask not found");
			}
			camera.cullingMask = virtualLayerMask;
			camera.Render();

			//Don't clear
			camera.clearFlags = CameraClearFlags.Nothing;

			//2. render tracked Object with 1
			var trackedLayerMask = LayerMask.GetMask ("TrackedLayer");
			if (trackedLayerMask == 0) {
				Debug.LogError ("TrackedLayer Mask not found");
			}
			camera.cullingMask = trackedLayerMask;
			camera.Render ();

			Debug.Assert (renderTexture.width > 0 && renderTexture.height > 0);

		}
	}
}