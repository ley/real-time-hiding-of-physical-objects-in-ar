﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
namespace ARInpainter{
	public class FrameImporter{

		public string imagePath = "../../../benchmark_imgs/"+Configuration.Files.SceneName;
		public string imageFormatPostfix = "png";//"raw";
		public string backgroundImageName = "background";
		public string maskImageName = "mask";

		private FileInfo[] maskFileInfos;
		private FileInfo[] backgroundFileInfos;

		//Some hard-coded vales. must be the same as when recorded. Are part of the filename
		public int width = 1024;
		public int height = 1024;
		public int frameRate = 20;

		//Keeps track of the current frame index
		public int frameIndex = 0;

		public FrameImporter(){
			this.width = Configuration.Files.width;
			this.height = Configuration.Files.height;
			this.frameIndex = Configuration.Files.startFrame;
		}

		public void loadFrameNames(){
			DirectoryInfo dirInfo = new DirectoryInfo(imagePath);
			maskFileInfos = dirInfo.GetFiles("*"+maskImageName+"*."+imageFormatPostfix);
			backgroundFileInfos = dirInfo.GetFiles("*"+backgroundImageName+"*."+imageFormatPostfix);

			//Make sure files were found
			Debug.AssertFormat(maskFileInfos.Length > 0 && backgroundFileInfos.Length > 0, "No mask or background frames found");

			//Sort files by name. The timestamp will be the only difference
			Array.Sort(maskFileInfos, ((x, y) => String.Compare(x.Name, y.Name)));
			Array.Sort(backgroundFileInfos, ((x, y) => String.Compare(x.Name, y.Name)));
		}

		private Texture2D loadTexture(FileInfo textureFileInfo){
			if (textureFileInfo.Extension == ".png") {
				return this.loadPNGTexture (textureFileInfo);
			} else if (textureFileInfo.Extension == ".raw") {
				return this.loadBinaryTexture (textureFileInfo);
			} else {
				throw new Exception ("No valid filetype for loading image.");
			}
		}

		private Texture2D loadBinaryTexture(FileInfo textureFileInfo){
			Texture2D targetTexture = new Texture2D(this.width, this.height, Configuration.Mask.TextureFormat, true);
			targetTexture.filterMode = FilterMode.Bilinear;
			//read binary data and convert load into Texture2D object
			using (BinaryReader binaryReader = new BinaryReader (textureFileInfo.OpenRead())) {
				int length = (int) binaryReader.BaseStream.Length;
				var data = binaryReader.ReadBytes (length);
				targetTexture.LoadRawTextureData (data);
				targetTexture.Apply ();
				//Debug.Log ("loaded: " + textureFileInfo.Name);
			}
			return targetTexture;
		}

		private Texture2D loadPNGTexture(FileInfo pngInfo){
			Texture2D targetTexture = new Texture2D(this.width, this.height,  Configuration.Mask.TextureFormat, true);
			targetTexture.filterMode = FilterMode.Bilinear;
			//read binary data and convert load into Texture2D object
			using (BinaryReader binaryReader = new BinaryReader (pngInfo.OpenRead())) {
				int length = (int) binaryReader.BaseStream.Length;
				var data = binaryReader.ReadBytes (length);
				targetTexture.LoadImage(data);
				targetTexture.Apply ();
				//Debug.Log ("loaded: " + textureFileInfo.Name);
			}
			return targetTexture;
		}

		//update frameIndex if there are still new frames available. Otherwise just keep last index
		public void moveToNextFrame(){
			frameIndex = frameIndex < backgroundFileInfos.Length - 1 ? frameIndex + 1 : frameIndex;
		}

		public bool hasNext(){
			Debug.AssertFormat (backgroundFileInfos != null, "FrameImporter.loadFrameNames() not called?");
			return backgroundFileInfos != null && this.frameIndex < backgroundFileInfos.Length - 1;
		}


		//Loads the frame's mask texture at given frameIndex
		public Texture2D loadFrameMask(int? frameIndex=null){
			var currentFrameIndex = frameIndex.GetValueOrDefault (this.frameIndex);
			Debug.AssertFormat (maskFileInfos.Length > currentFrameIndex, "Invalid index " + currentFrameIndex + " when loading mask.");
			return loadTexture (maskFileInfos [currentFrameIndex]);
		}

		//Loads the frame's background texture at given frameIndex
		public Texture2D loadFrameBackground(int? frameIndex=null){
			var currentFrameIndex = frameIndex.GetValueOrDefault (this.frameIndex);
			Debug.AssertFormat (backgroundFileInfos.Length > currentFrameIndex, "Invalid index " + currentFrameIndex + " when loading background.");
			return loadTexture (backgroundFileInfos [currentFrameIndex]);
		}

		public string backgroundFileName(int? frameIndex=null, bool withFileExtension=false){
			var currentFrameIndex = frameIndex.GetValueOrDefault (this.frameIndex);
			Debug.AssertFormat (backgroundFileInfos.Length > currentFrameIndex, "Invalid index " + currentFrameIndex + " when getting background file name.");
			var name = backgroundFileInfos [currentFrameIndex].Name;
			if (withFileExtension) {
				return name;
			} else {
				return Path.GetFileNameWithoutExtension (name);
			}
			
		}

		public string maskFileName(int? frameIndex=null, bool withFileExtension=false){
			var currentFrameIndex = frameIndex.GetValueOrDefault (this.frameIndex);
			Debug.AssertFormat (backgroundFileInfos.Length > currentFrameIndex, "Invalid index " + currentFrameIndex + " when getting mask file name.");
			var name = maskFileInfos [currentFrameIndex].Name;
			if (withFileExtension) {
				return name;
			} else {
				return Path.GetFileNameWithoutExtension (name);
			}
		}
	}
}