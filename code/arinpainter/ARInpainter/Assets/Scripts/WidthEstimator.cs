﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ARInpainter{
	public class WidthEstimator : MonoBehaviour {
		public Camera ProjectionCamera;

		/**
		 * Estimate of projected objects width in pixels  
		 */
		public int WidthEstimate(){

			//Get the mesh size
			var size = this.gameObject.GetComponent<Renderer> ().bounds.size;
			var screenSize = ProjectionCamera.WorldToScreenPoint (size);
			var projSize = ProjectionCamera.projectionMatrix.MultiplyVector (screenSize);
//			var sizeVector = new Vector4 (size.x, size.y, size.z, 0);
//			//transform to world -> camera -> screen
//			var parentSize = this.transform.localToWorldMatrix.MultiplyVector (sizeVector); 
//			var worldSize = this.transform.parent.localToWorldMatrix.MultiplyVector (parentSize);
//			var cameraSize = ProjectionCamera.worldToCameraMatrix.MultiplyVector (worldSize);
//			var screenSize = ProjectionCamera.projectionMatrix.MultiplyVector (cameraSize);
			return 0;
		}
	}
}
