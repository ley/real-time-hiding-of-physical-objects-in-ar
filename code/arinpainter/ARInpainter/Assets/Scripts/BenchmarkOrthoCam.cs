﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

namespace ARInpainter{
	public class BenchmarkOrthoCam : MonoBehaviour, InpaintingCamera {
		public GameObject RenderQuad;
		public ComputeShader MappingShader;
		public ComputeShader InitNormalShader;
		public Shader MappedInpaintingShader;
		public ComputeShader ReInitMappingShader;
		public Shader SolidInpaintingShader;
		public Shader ErrorShader;
		public Shader ShowMappingShader;
		public Shader BackgroundShader;
		public Shader UpdateShader;

		private Inpainter inpainter;
		private FrameImporter frameImporter;
		private int frameRate = 20;

		private Texture2D lastLoadedMaskTexture;
		private int lastLoadedMaskIndex = -1;

		private Texture2D lastLoadedBackgroundTexture;
		private int lastLoadedBackgroundIndex = -1;

		// Use this for initialization
		void Start () {
			this.frameImporter = new FrameImporter ();
			this.frameImporter.frameRate = this.frameRate;
			this.frameImporter.loadFrameNames ();

			var scale = new Vector3 (1.0f * frameImporter.width / frameImporter.height, 1, 1);
			//this.RenderQuad.gameObject.transform.localScale = scale;
			this.gameObject.transform.localScale = scale;

			QualitySettings.vSyncCount = 0;  // VSync must be disabled
			Application.targetFrameRate = this.frameImporter.frameRate;


			var shaders = new Dictionary<Configuration.RenderMode.Mode, Shader>(){
				{Configuration.RenderMode.Mode.Mapped,this.MappedInpaintingShader},
				{Configuration.RenderMode.Mode.Solid,this.SolidInpaintingShader},
				{Configuration.RenderMode.Mode.Error,this.ErrorShader},
				{Configuration.RenderMode.Mode.Mapping,this.ShowMappingShader},
				{Configuration.RenderMode.Mode.Background, this.BackgroundShader},
				{Configuration.RenderMode.Mode.Update, this.UpdateShader}
			};
			//Create and start inpainter
			this.inpainter = new Inpainter (
				mappingShader: MappingShader,
				initNormalShader: InitNormalShader,
				reInitMappingShader: ReInitMappingShader,
				inpaintingCamera: this,
				shaders: shaders,
				textureWidth: frameImporter.width,
				textureHeight: frameImporter.height
			);


			inpainter.Start ();
		}

		// Update is called once per frame
		void Update () {
			
		}
			
		void OnPreRender (){

			//TIME IT
			this.inpainter.timeIt.StartTotal();

			inpainter.OnPreRender ();

			//if only one image is used for rendering, don't move to next frame
			if (!Configuration.RenderMode.SingleFrameRendering) {
				frameImporter.moveToNextFrame ();
			}
		}

		void OnPostRender(){
			this.inpainter.timeIt.EndTotal ();
			this.inpainter.LogTimeIt ();
		}

		public Texture2D getMaskTexture(){
			if (frameImporter.frameIndex != this.lastLoadedMaskIndex) {
				this.lastLoadedMaskTexture = frameImporter.loadFrameMask ();
				this.lastLoadedMaskIndex = frameImporter.frameIndex;
			}
			return this.lastLoadedMaskTexture;
		}

		public Texture2D getBackgroundTexture(){
			if (frameImporter.frameIndex != this.lastLoadedBackgroundIndex) {
				this.lastLoadedBackgroundTexture = frameImporter.loadFrameBackground ();
				this.lastLoadedBackgroundIndex = frameImporter.frameIndex;
			}
			return this.lastLoadedBackgroundTexture;
		}

		public Texture getBackgroundRenderTexture (){
			return getBackgroundTexture ();
		}

		public Texture getMaskRenderTexture (){
			return this.getMaskTexture();
		}
			
		public Texture getObjectRenderTexture (){
			//Set any texture with content as ObjectTexture. If this is not done, it won't work
			return getMaskTexture();
		}
		public void setInpaintingShader(Shader shader){
			this.RenderQuad.GetComponent<Renderer> ().material.shader = shader;
		}
		public Material getMaterial(){
			return RenderQuad.GetComponent<Renderer> ().material;
		}
	}
}
