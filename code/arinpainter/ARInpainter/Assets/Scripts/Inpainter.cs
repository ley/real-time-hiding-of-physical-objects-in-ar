﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using System.Threading;

namespace ARInpainter{
	using Coord = Vector2;
	struct Size{
		public Size(int width, int height){this.width = width; this.height=height;}
		public int width;
		public int height;
	}
	public class Inpainter: ScriptableObject {

		private RenderTexture[] MappingRenderTextures;
		private RenderTexture ErrorRenderTexture;
		private RenderTexture UpdateRenderTexture;
//		private RenderTexture HoleCoordinatesRenderTexture;
//		private RenderTexture NonHoleCoordinatesRenderTexture;
		private RenderTexture HoleNonHoleCoordinatesRenderTexture;
		private RenderTexture RandomTexture;
//		private Texture2D BackgroundLevelTexture;
		private ComputeShader mappingShader;
		private ComputeShader initNormalShader;
		private ComputeShader reInitMappingShader;
		private InpaintingCamera inpaintingCamera;
		private Size textureSize;
		private int mipmapLevel = 0;
		private Size currentSize;
		private Dictionary<Configuration.RenderMode.Mode,Shader> shaders;

		//Coordinates of hole and non-hole pixels
		private IList<Coord> holePixelCoords;
		private IList<Coord> nonHolePixelCoords;
		private Vector3[] randomVectors;

		private bool initialized = false;
		private int lastMappedTextureIndex = 0;
		private RenderTexture OldMapping {
			get{ return this.MappingRenderTextures [this.lastMappedTextureIndex]; }
		}
		private RenderTexture NewMapping {
			get{ return this.MappingRenderTextures [this.NewMappingIndex]; }
		}
		private int NewMappingIndex {
			get {
				return (this.lastMappedTextureIndex + 1) % 2;
			}
		}

		public TimeIt timeIt = new TimeIt();
		private int maxResolutionHolePixelCount;
		private int maxResolutionTotalPixelCount;

		/*The render mode used right now*/
		private Configuration.RenderMode.Mode currentRenderMode = Configuration.RenderMode.CurrentRenderMode;
		/*The render mode that should be used now*/
		private Configuration.RenderMode.Mode desiredRenderMode {
			get
			{
				if (this.initialized)
				{
					return Configuration.RenderMode.CurrentRenderMode;
				}
				else
				{
					return Configuration.RenderMode.Mode.Background;
				}
			}
		}

		public Inpainter(ComputeShader mappingShader, ComputeShader initNormalShader, ComputeShader reInitMappingShader, InpaintingCamera inpaintingCamera, Dictionary<Configuration.RenderMode.Mode,Shader> shaders, int textureWidth, int textureHeight)
		{
			this.mappingShader = mappingShader;
			this.initNormalShader = initNormalShader;
			this.reInitMappingShader = reInitMappingShader;
			this.inpaintingCamera = inpaintingCamera;
			this.shaders = shaders;
			this.textureSize = new Size (width: textureWidth, height: textureHeight);
		}

		public void Start(){
			if (!SystemInfo.supportsComputeShaders) {
				throw new MissingComponentException ("Compute Shaders not supported");
			}
			if (!SystemInfo.SupportsTextureFormat(TextureFormat.RGBAFloat)) {
				throw new MissingComponentException ("RGBAFloat format not supported");
			}

			//Listen to changes in tracking state
			TargetBehaviour.OnTargetVisibileChanged += this.OnTargetVisibileChanged;

			//Listen to changes in interpolation mode
			UIHandler.OnInterpolationChange += this.OnChangeInterpolationMode;
			UIHandler.OnInitResolutionChange += this.OnInitResolutionChanged;

			Setup ();
		}

		private void Setup()
		{

			//FIRST set mipmap level
			this.mipmapLevel = initMipmapLevelForSize(initSize: (int) Configuration.ComputeShaders.Initialization.StartResolution, imageWidth: this.textureSize.width , imageHeight:this.textureSize.height);

//			//SECOND set currentWidth and currentSize based on mipmap level
//			this.BackgroundLevelTexture = ComputeCurrentSizeAndCreateBackgroundRenderTexture(this.mipmapLevel);

			//SECOND set currentSize based on mipmap level
			//get size of mask at mipmapLevel
			this.currentSize = computeCurrentSize (mipmapLevel: this.mipmapLevel);


			//THEN initialize all the other textures
			//Create texture for error visalization
			this.createErrorTextures (width: this.currentSize.width, height: this.currentSize.height);

			//Create Textures for mapping
			this.MappingRenderTextures = this.createMappingRenderTextures (width: this.currentSize.width, height: this.currentSize.height, bitdepth: 128);//128bit = 4 color channels * 32 bit/float for RGBAFloat

			computeHoleAndNonHoleCoordinates();

			//Create Textures containing the coordinates of the hole/non-hole pixels
			this.CreateCoordinateTextures(width: this.currentSize.width, height: this.currentSize.height);

			this.createUpdateTextures(width: this.currentSize.width, height: this.currentSize.height);

			computeHoleAndNonHoleCoordinates();

			//Create and init random texture
			this.createRandomTexture();
			this.initRandomTexture ();
		}



		private void Init()
		{
			
			//Don't initialize more than once
			if (!this.initialized)
			{
				//TIME IT
				this.timeIt.StartInit();

				//get hole and non-hole coordinates from mask
				computeHoleAndNonHoleCoordinates();

				//Initialize
				var success = this.InitMapping();

				ChangeRenderMode ();

				this.initialized = success;

				//TIME IT
				this.timeIt.EndInit();
			}
		}



		public void OnTargetVisibileChanged(bool targetFound)
		{
			if (!targetFound)
			{
				//If the target was lost, go back to lowest resolution
				this.HardReset ();
			}

		}

		private void Reset()
		{
			this.initialized = false;
		}
		private void HardReset()
		{
			this.Setup ();
			this.Reset ();
		}

		public void OnInitResolutionChanged()
		{
			this.HardReset ();
		}

		private Size computeCurrentSize(int mipmapLevel){
			//Copy pixels from RenderTexture to regular texture so they can be accessed
			var maskTexture = this.inpaintingCamera.getMaskTexture();
			//pixels as 1D array left->right, button->up, row-by-row
			Color[] pixels = maskTexture.GetPixels(miplevel: mipmapLevel);

			double pixelCount = (double)(maskTexture.width * maskTexture.height);
			int maskHeight = (int)Math.Sqrt ((double)pixels.Length/ pixelCount * maskTexture.height * maskTexture.height);
			int maskWidth = pixels.Length / maskHeight;

			return new Size (width: maskWidth, height: maskHeight);
		}
			

		private void computeHoleAndNonHoleCoordinates()
		{
			this.holePixelCoords = new List<Coord>();
			this.nonHolePixelCoords = new List<Coord>();

			this.pixelsCoords (holePixelCoords: this.holePixelCoords, nonHolePixelCoords: this.nonHolePixelCoords);
		}

		/*private Texture2D ComputeCurrentSizeAndCreateBackgroundRenderTexture(int mipmapLevel)
		{
			this.inpaintingCamera.getBackgroundRenderTexture().mi
			var backgroundTexture = this.inpaintingCamera.getBackgroundTexture ();
			Color32[] backgroundPixelData = backgroundTexture.GetPixels32 (miplevel: mipmapLevel);
			double pixelCount = (double)(backgroundTexture.width * backgroundTexture.height);
			int backgroundHeight = (int)Math.Sqrt ((double)backgroundPixelData.Length / pixelCount * backgroundTexture.height * backgroundTexture.height);
			int backgroundWidth = backgroundPixelData.Length / backgroundHeight;

			this.currentWidth = backgroundWidth;
			this.currentHeight = backgroundHeight;

			//Create background texture
			Texture2D backgroundLevelTexture = new Texture2D (backgroundWidth, backgroundHeight);
			backgroundLevelTexture.filterMode = FilterMode.Bilinear;

			//populate with data
			backgroundLevelTexture.SetPixels32 (backgroundPixelData);
			backgroundLevelTexture.Apply ();
			return backgroundLevelTexture;
		}

		private void UpdateBackgroundLevelTexture(){
			Color32[] backgroundPixelData = this.inpaintingCamera.getBackgroundTexture ().GetPixels32 (miplevel: mipmapLevel);
			this.BackgroundLevelTexture.SetPixels32 (backgroundPixelData);
			this.BackgroundLevelTexture.Apply ();
		}*/



		private RenderTexture[] createMappingRenderTextures(int width, int height, int bitdepth){

			var MappingTextures = new RenderTexture[2];
			for (int i = 0; i < 2; ++i) {
				/*
				 * Make the RenderTexture 2 pixels larget than necessary. This will result in a padding row on all sides.
				 * These mappings are set as invalid. This way, any overshooting lookup coordinates are clamped to the last, invalid, pixel.
				*/
				MappingTextures[i] = new RenderTexture (width+2, height+2, bitdepth);
				MappingTextures[i].enableRandomWrite = true;
				MappingTextures[i].format = RenderTextureFormat.ARGBFloat;
				MappingTextures[i].depth = 0;
				MappingTextures[i].useMipMap = false;
				MappingTextures[i].filterMode = Configuration.RenderMode.InterpolateMapping ? FilterMode.Bilinear : FilterMode.Point;
				MappingTextures[i].wrapMode = TextureWrapMode.Clamp;
				MappingTextures [i].antiAliasing = 1;
				MappingTextures [i].anisoLevel = 0;
				MappingTextures[i].Create ();
			}
			return MappingTextures;
		}

		public void OnChangeInterpolationMode()
		{
			this.ErrorRenderTexture.filterMode = Configuration.RenderMode.InterpolateMapping ? FilterMode.Bilinear : FilterMode.Point;
			this.OldMapping.filterMode = Configuration.RenderMode.InterpolateMapping ? FilterMode.Bilinear : FilterMode.Point;
			this.NewMapping.filterMode = Configuration.RenderMode.InterpolateMapping ? FilterMode.Bilinear : FilterMode.Point;
			this.UpdateRenderTexture.filterMode = Configuration.RenderMode.InterpolateMapping ? FilterMode.Bilinear : FilterMode.Point;
		}

		private void createErrorTextures(int width, int height)
		{
			this.ErrorRenderTexture = new RenderTexture (width, height, 128);//RenderTexture (width, height, bitdepth);
			this.ErrorRenderTexture.enableRandomWrite = true;
			this.ErrorRenderTexture.format = RenderTextureFormat.ARGBFloat;
			this.ErrorRenderTexture.depth = 0;
			this.ErrorRenderTexture.useMipMap = false;
			this.ErrorRenderTexture.filterMode = Configuration.RenderMode.InterpolateMapping ? FilterMode.Bilinear : FilterMode.Point;
			this.ErrorRenderTexture.Create ();
		}

		private void createUpdateTextures(int width, int height)
		{
			this.UpdateRenderTexture = new RenderTexture (width, height, 128);//RenderTexture (width, height, bitdepth);
			this.UpdateRenderTexture.enableRandomWrite = true;
			this.UpdateRenderTexture.format = RenderTextureFormat.ARGBFloat;
			this.UpdateRenderTexture.depth = 0;
			this.UpdateRenderTexture.useMipMap = false;
			this.UpdateRenderTexture.filterMode = Configuration.RenderMode.InterpolateMapping ? FilterMode.Bilinear : FilterMode.Point;
			this.UpdateRenderTexture.Create ();
		}
		/**
		 * Create Render textures of dimensiont 1xtotal-image-resolution that contain the coordinates of the hole/non-hole pixsels.
		 * It has to be done this way, because OpenGLES3 can not bind enough buffers.
		*/
		private void CreateCoordinateTextures(int width, int height){

			int size = width * height;//Math.Max (this.holePixelCoords.Count, this.nonHolePixelCoords.Count);

//			this.NonHoleCoordinatesRenderTexture = new RenderTexture (size, 1, 128);
//			this.NonHoleCoordinatesRenderTexture.enableRandomWrite = true;
//			this.NonHoleCoordinatesRenderTexture.format = RenderTextureFormat.ARGBFloat; // ARGBFloat is supported by all required devices, RGFloat is not
//			this.NonHoleCoordinatesRenderTexture.depth = 0;
//			this.NonHoleCoordinatesRenderTexture.useMipMap = false;
//			this.NonHoleCoordinatesRenderTexture.filterMode = FilterMode.Point;
//			this.NonHoleCoordinatesRenderTexture.Create ();
//
//			this.HoleCoordinatesRenderTexture = new RenderTexture (size, 1, 128);
//			this.HoleCoordinatesRenderTexture.enableRandomWrite = true;
//			this.HoleCoordinatesRenderTexture.format = RenderTextureFormat.ARGBFloat;
//			this.HoleCoordinatesRenderTexture.depth = 0;
//			this.HoleCoordinatesRenderTexture.useMipMap = false;
//			this.HoleCoordinatesRenderTexture.filterMode = FilterMode.Point;
//			this.HoleCoordinatesRenderTexture.Create ();
//
//
			this.HoleNonHoleCoordinatesRenderTexture = new RenderTexture (size, 1, 128);
			this.HoleNonHoleCoordinatesRenderTexture.enableRandomWrite = true;
			this.HoleNonHoleCoordinatesRenderTexture.format = RenderTextureFormat.ARGBFloat; // ARGBFloat is supported by all required devices, RGFloat is not
			this.HoleNonHoleCoordinatesRenderTexture.depth = 0;
			this.HoleNonHoleCoordinatesRenderTexture.useMipMap = false;
			this.HoleNonHoleCoordinatesRenderTexture.filterMode = FilterMode.Point;
			this.HoleNonHoleCoordinatesRenderTexture.Create ();
		}

		private void createRandomTexture()
		{
			int randomNumberCount = this.holePixelCoords.Count + 11;;
			this.RandomTexture = new RenderTexture(width: randomNumberCount, height: 1, depth: 128);
			this.RandomTexture.useMipMap = false;
			this.RandomTexture.enableRandomWrite = true;
			this.RandomTexture.filterMode = FilterMode.Point;
//			this.RandomTexture.depth = 0;
			this.RandomTexture.Create();

			UnityEngine.Random.InitState (42);//(int)(Time.time*42));
			this.randomVectors = new Vector3[randomNumberCount];
			for(int i=0; i<randomNumberCount;i++){
				var randomIndex = (int) Math.Min(Math.Floor(UnityEngine.Random.value*8),7);
				var randomVector = UnityEngine.Random.insideUnitCircle;
				randomVectors[i] = new Vector3(randomVector.x, randomVector.y, randomIndex);
			}
		}


		private void MoveToNextLevel()
		{
			//Release old textures
			this.ErrorRenderTexture.Release ();
			this.UpdateRenderTexture.Release ();
//			this.HoleCoordinatesRenderTexture.Release ();
//			this.NonHoleCoordinatesRenderTexture.Release ();
			this.HoleNonHoleCoordinatesRenderTexture.Release();
			this.NewMapping.Release ();
			this.RandomTexture.Release ();
			Destroy(this.ErrorRenderTexture);
			Destroy (this.UpdateRenderTexture);
//			Destroy (this.HoleCoordinatesRenderTexture);
//			Destroy (this.NonHoleCoordinatesRenderTexture);
			Destroy(this.HoleNonHoleCoordinatesRenderTexture);
			Destroy (this.NewMapping);
			Destroy (this.RandomTexture);

			//Set new mipmap level
			this.mipmapLevel = this.mipmapLevel == 0 ? this.mipmapLevel : mipmapLevel - 1;

			//get size of mask at mipmapLevel
			this.currentSize = computeCurrentSize (mipmapLevel: this.mipmapLevel);

			//Create texture for error visalization
			this.createErrorTextures (width: this.currentSize.width, height: this.currentSize.height);

			//Create Textures for mapping
			//keep a reference to old mapping

			this.NewMapping.Release();
			RenderTexture oldMappingTexture = this.OldMapping;
			this.MappingRenderTextures = this.createMappingRenderTextures (width: this.currentSize.width, height: this.currentSize.height, bitdepth: 128);//128bit = 4 color channels * 32 bit/float for RGBAFloat
			

			//get hole and non-hole coordinates from mask
			computeHoleAndNonHoleCoordinates();

			//Create Textures containing the coordinates of the hole/non-hole pixels
			this.CreateCoordinateTextures(width: this.currentSize.width, height: this.currentSize.height);

			this.createUpdateTextures(width: this.currentSize.width, height: this.currentSize.height);

			
			//TODO: Handle case of no hole pixels

			this.createRandomTexture();
			this.initRandomTexture ();



			//initialize new mapping textures with 0s
			//Create buffers 
			ComputeBuffer holePixelBuffer = new ComputeBuffer (holePixelCoords.Count, sizeof(float) * 2);
			ComputeBuffer nonHolePixelBuffer = new ComputeBuffer (nonHolePixelCoords.Count, sizeof(float) * 2);
			holePixelBuffer.SetData (holePixelCoords.ToArray ());
			nonHolePixelBuffer.SetData (nonHolePixelCoords.ToArray ());

			//Generate input data
			var inputData = generateInputData ();
			//Create buffers
			var strideSize = inputData [0].strideSize ();
			ComputeBuffer inputBuffer = new ComputeBuffer (inputData.Length, strideSize);
			inputBuffer.SetData (inputData);

			// INIT Textures
			this.initNormalPixels(nonHolePixelBuffer: nonHolePixelBuffer, inputBuffer: inputBuffer);
			this.initCoordTextures (nonHolePixelBuffer: nonHolePixelBuffer, holePixelBuffer: holePixelBuffer);

			//Initialize using old mapping
			this.TransfereMapping(oldMappingTexture: oldMappingTexture, newMappingTextures: this.MappingRenderTextures, holePixelBuffer: holePixelBuffer);
			oldMappingTexture.Release ();
			Destroy(oldMappingTexture);
			//Reset mapping index
			this.lastMappedTextureIndex = 0;

			//release buffers
			holePixelBuffer.Release ();
			nonHolePixelBuffer.Release ();
			inputBuffer.Release ();

			//Refine
		}

		private void TransfereMapping(RenderTexture oldMappingTexture, RenderTexture[] newMappingTextures, ComputeBuffer holePixelBuffer)
		{
			var inputData = new ReInitInputData[1];
			inputData [0] = new ReInitInputData () {
				newRealMapWidth = (uint)newMappingTextures[0].width,
				newRealMapHeight = (uint)newMappingTextures[0].height,
				oldRealMapWidth = (uint) oldMappingTexture.width,
				oldRealMapHeight = (uint) oldMappingTexture.height,
				newMapWidth = (uint) this.currentSize.width,
				newMapHeight = (uint) this.currentSize.height,
				oldMapWidth = (uint) oldMappingTexture.width - 2,
				oldMapHeight = (uint) oldMappingTexture.height -2
			};
			var inputBuffer = new ComputeBuffer (count: 1, stride: sizeof(uint) * 8);
			inputBuffer.SetData (inputData);

			ComputeBuffer errorBuffer = new ComputeBuffer ( this.holePixelCoords.Count, sizeof(float) * 4);


			int reInitMappingTexturesKernel = reInitMappingShader.FindKernel ("ReInitMapping");
			reInitMappingShader.SetTexture (kernelIndex: reInitMappingTexturesKernel, name: "NewMapping1", texture: newMappingTextures[0]);
			reInitMappingShader.SetTexture (kernelIndex: reInitMappingTexturesKernel, name: "NewMapping2", texture: newMappingTextures[1]);
			reInitMappingShader.SetTexture (kernelIndex: reInitMappingTexturesKernel, name: "OldMapping", texture: oldMappingTexture);
			reInitMappingShader.SetBuffer (kernelIndex: reInitMappingTexturesKernel, name: "inputBuffer", buffer: inputBuffer);
			reInitMappingShader.SetBuffer(kernelIndex: reInitMappingTexturesKernel, name: "errorBuffer", buffer: errorBuffer);
			reInitMappingShader.SetBuffer (kernelIndex: reInitMappingTexturesKernel, name: "holePixelCoordBuffer", buffer: holePixelBuffer);

			// 8.0 is not dependent on the resolution, but the threadgroup size
			reInitMappingShader.Dispatch (reInitMappingTexturesKernel, this.holePixelCoords.Count, 1, 1);

			Vector4[] errorData = new Vector4[holePixelCoords.Count];
			errorBuffer.GetData(errorData);

			inputBuffer.Release ();
			errorBuffer.Release ();
		}

		private bool InitMapping()
		{
			//If there are no hole pixel, it means, the mask is not created properly yet. Reset
			if (holePixelCoords.Count == 0) {
				return false;
			}

			//Create buffers 
			ComputeBuffer holePixelBuffer = new ComputeBuffer (holePixelCoords.Count, sizeof(float) * 2);
			ComputeBuffer nonHolePixelBuffer = new ComputeBuffer (nonHolePixelCoords.Count, sizeof(float) * 2);
			holePixelBuffer.SetData (holePixelCoords.ToArray ());
			nonHolePixelBuffer.SetData (nonHolePixelCoords.ToArray ());

			//Generate input data
			var inputData = generateInputData ();
			//Create buffers
			var strideSize = inputData [0].strideSize ();
			ComputeBuffer inputBuffer = new ComputeBuffer (inputData.Length, strideSize);
			inputBuffer.SetData (inputData);

			// INIT Textures
			this.initNormalPixels(nonHolePixelBuffer: nonHolePixelBuffer, inputBuffer: inputBuffer);
			this.initCoordTextures (nonHolePixelBuffer: nonHolePixelBuffer, holePixelBuffer: holePixelBuffer);
			this.initHolePixels (inputBuffer: inputBuffer);

			//release buffers
			holePixelBuffer.Release ();
			nonHolePixelBuffer.Release ();
			inputBuffer.Release ();

			return true;
		}

		private void initNormalPixels(ComputeBuffer nonHolePixelBuffer, ComputeBuffer inputBuffer)
		{
			

			//INIT ALL PIXELS WITH 0 FIRST
			int initMappingTexturesKernel = initNormalShader.FindKernel ("InitMappingTextures");
			initNormalShader.SetTexture (kernelIndex: initMappingTexturesKernel, name: "NewMapping", texture: this.NewMapping);
			initNormalShader.SetTexture (kernelIndex: initMappingTexturesKernel, name: "OldMapping", texture: this.OldMapping);
			initNormalShader.SetTexture (kernelIndex: initMappingTexturesKernel, name: "ErrorTexture", texture: this.ErrorRenderTexture);

			// 8.0 is not dependent on the resolution, but the threadgroup size
			initNormalShader.Dispatch (initMappingTexturesKernel, (int)Math.Ceiling (MappingRenderTextures [0].width / 8.0), (int)Math.Ceiling (MappingRenderTextures [0].height / 8.0), 1);

			//INIT NORMAL PIXELS
			int initNormalKernelHandle = initNormalShader.FindKernel ("InitNormalPixelsMapping");
			initNormalShader.SetTexture (kernelIndex: initNormalKernelHandle, name: "NewMapping", texture: this.NewMapping);
			initNormalShader.SetTexture (kernelIndex: initNormalKernelHandle, name: "OldMapping", texture: this.OldMapping);
			initNormalShader.SetBuffer (kernelIndex: initNormalKernelHandle, name: "nonHolePixelCoordBuffer", buffer: nonHolePixelBuffer);
			initNormalShader.SetBuffer (kernelIndex: initNormalKernelHandle, name: "inputBuffer", buffer: inputBuffer);

			initNormalShader.Dispatch (initNormalKernelHandle, nonHolePixelCoords.Count, 1, 1);
		}

		private void initCoordTextures(ComputeBuffer nonHolePixelBuffer, ComputeBuffer holePixelBuffer)
		{
			//INIT COORD TEXTURES
			int initCoordTexturesKernel = initNormalShader.FindKernel ("InitCoordTextures");
//			initNormalShader.SetTexture (kernelIndex: initCoordTexturesKernel, name: "NonHoleCoordinatesRenderTexture", texture: this.NonHoleCoordinatesRenderTexture);
//			initNormalShader.SetTexture (kernelIndex: initCoordTexturesKernel, name: "HoleCoordinatesRenderTexture", texture: this.HoleCoordinatesRenderTexture);
			initNormalShader.SetTexture (kernelIndex: initCoordTexturesKernel, name: "HoleNonHoleCoordinatesRenderTexture", texture: this.HoleNonHoleCoordinatesRenderTexture);
			initNormalShader.SetBuffer (kernelIndex: initCoordTexturesKernel, name: "nonHolePixelCoordBuffer", buffer: nonHolePixelBuffer);
			initNormalShader.SetBuffer (kernelIndex: initCoordTexturesKernel, name: "holePixelCoordBuffer", buffer: holePixelBuffer);


			initNormalShader.Dispatch (initCoordTexturesKernel, Math.Max (nonHolePixelCoords.Count, holePixelCoords.Count), 1, 1);


		}
		private void initRandomTexture()
		{
			var randomBuffer = new ComputeBuffer (count: this.randomVectors.Length, stride: sizeof(float) * 3);
			randomBuffer.SetData (this.randomVectors);

			var errorBuffer = new ComputeBuffer (count:  this.randomVectors.Length, stride: sizeof(float) * 4);

			int initRandomTexturesKernel = initNormalShader.FindKernel ("InitRandomTextures");
			initNormalShader.SetTexture (kernelIndex: initRandomTexturesKernel, name: "RandomTexture", texture: this.RandomTexture);
			initNormalShader.SetBuffer (kernelIndex: initRandomTexturesKernel, name: "randomBuffer", buffer: randomBuffer);
			initNormalShader.SetBuffer(kernelIndex: initRandomTexturesKernel, name: "errorBuffer", buffer: errorBuffer);
			initNormalShader.Dispatch (initRandomTexturesKernel, this.randomVectors.Length, 1, 1);

			Vector4[] errorData = new Vector4[ this.randomVectors.Length];
			errorBuffer.GetData(errorData);

			randomBuffer.Dispose ();
			errorBuffer.Dispose ();

		}

		private void initHolePixels(ComputeBuffer inputBuffer)
		{
			//Initialize output data to not mapped
			InitOutputData[] outputData = new InitOutputData[holePixelCoords.Count];
			for (int i = 0; i < outputData.Count (); i++) {
				outputData [i] = new InitOutputData ();
				outputData [i].isPixelsMapped = 0;
			}

			ComputeBuffer outputBuffer = new ComputeBuffer (count: outputData.Count (), stride: sizeof(uint));
			outputBuffer.SetData (outputData);
			ComputeBuffer errorBuffer = new ComputeBuffer (nonHolePixelCoords.Count+1, sizeof(float) * 4);

			//INIT HOLE PIXELS
			var shaderName = "InitHolePixelsMapping"+(int)Configuration.ComputeShaders.Initialization.StartResolution;
			int initHoleKernelHandle = mappingShader.FindKernel (shaderName);

			//var holePixelCoordTexture = SetCoordDataToTexture (holePixelCoords);

			//bind buffer
			mappingShader.SetBuffer(kernelIndex: initHoleKernelHandle, name: "inputBuffer", buffer: inputBuffer);
			mappingShader.SetBuffer(kernelIndex: initHoleKernelHandle, name: "outputBuffer", buffer: outputBuffer);
			mappingShader.SetBuffer(kernelIndex: initHoleKernelHandle, name: "errorBuffer", buffer: errorBuffer);

			//Set textures
			mappingShader.SetTexture (kernelIndex: initHoleKernelHandle, name: "ErrorTexture", texture: this.ErrorRenderTexture);
			mappingShader.SetTexture (kernelIndex: initHoleKernelHandle, name: "Background", texture: this.inpaintingCamera.getBackgroundRenderTexture ());// this.BackgroundLevelTexture);
//			mappingShader.SetTexture (kernelIndex: initHoleKernelHandle, name: "holePixelCoordTexture", texture: this.HoleCoordinatesRenderTexture);
//			mappingShader.SetTexture (kernelIndex: initHoleKernelHandle, name: "nonHolePixelCoordTexture", texture: this.NonHoleCoordinatesRenderTexture);
			mappingShader.SetTexture (kernelIndex: initHoleKernelHandle, name: "HoleNonHoleCoordinatesRenderTexture", texture: this.HoleNonHoleCoordinatesRenderTexture);
			mappingShader.SetTexture (kernelIndex: initHoleKernelHandle, name: "Mask", texture: this.inpaintingCamera.getMaskRenderTexture());

			//iterate until all pixels are mapped
			bool allPixelsMapped = false;
			int index = 0;

			while (!allPixelsMapped && index<10)
			{
				//TIME IT
				this.timeIt.StartInitIter();

				// Need to set textures inside loop, as they are swapped every iteration
				mappingShader.SetTexture (kernelIndex: initHoleKernelHandle, name: "NewMapping", texture: this.NewMapping);
				mappingShader.SetTexture (kernelIndex: initHoleKernelHandle, name: "OldMapping", texture: this.OldMapping);
				mappingShader.Dispatch (initHoleKernelHandle, holePixelCoords.Count, 1, 1);

				int[] isPixelMappedData = new int[holePixelCoords.Count];
				outputBuffer.GetData(isPixelMappedData);
				//If all entries are true, all pixels are mapped
				allPixelsMapped = isPixelMappedData.Aggregate ((result, mapped) => result + mapped) == holePixelCoords.Count();
				Vector4[] errorData = new Vector4[nonHolePixelCoords.Count+1];
				errorBuffer.GetData(errorData);

				//TIME IT
				this.timeIt.EndInitIter(iter: index);

				index++;
				lastMappedTextureIndex = (lastMappedTextureIndex + 1) % 2;
			}

			outputBuffer.Release ();
			errorBuffer.Release ();
		}
		private InitInputData[] generateInputData()
		{
			InitInputData[] inputData = new InitInputData[1];
			inputData [0] = new InitInputData ();
			inputData [0].mipmapLevel = (uint)this.mipmapLevel;
			inputData [0].holePixelCount = (uint) holePixelCoords.Count;
			inputData [0].nonHolePixelCount = (uint) nonHolePixelCoords.Count;
			inputData [0].minNeighboursMappedCountFiveByFive = Configuration.ComputeShaders.Initialization.MinMappedNeighboursCount5x5;
			inputData [0].minNeighboursMappedCountEight = Configuration.ComputeShaders.Initialization.MinMappedNeighboursCount8;
			inputData [0].minNeighboursMappedCountColor = Configuration.ComputeShaders.Initialization.MinMappedNeighboursCountColor;
			inputData [0].colorErrorWeight = Configuration.Inpainting.Weights.ColorErrorWeight;
			inputData [0].spatialErrorWeight = Configuration.Inpainting.Weights.SpatialErrorWeight;
			inputData [0].typicalColorError = computeTypicalColorError ();
			inputData [0].maxSpatialComponentDist = Configuration.Inpainting.MaxSpatialComponentDist;
			inputData[0].currentWidth = (uint)this.currentSize.width;
			inputData[0].currentHeight = (uint)this.currentSize.height;
			inputData[0].realMapWidth = (uint) this.OldMapping.width;
			inputData[0].realMapHeight = (uint) this.OldMapping.height;
			inputData [0].colorStepSize = Configuration.Inpainting.StepSizes.ColorStepSize;
			inputData [0].spatialStepSize = Configuration.Inpainting.StepSizes.SpatialStepSize;
			inputData [0].normalPixelColorWeight = Configuration.Inpainting.Weights.NormalPixelColorWeight;
			//TODO: THIS IS FOR TESTING ONLY!
			inputData [0].centerOfMass = new Vector2 (0.712890625f * this.currentSize.width, (0.3359375f) * this.currentSize.height);

			return inputData;
		}

		private void RefineMapping ()
		{
			int refinementKernelHandle = mappingShader.FindKernel ("RefineMapping");

			//Generate input data
			var inputData = generateInputData();

			//Create buffers
			ComputeBuffer buffer = new ComputeBuffer(inputData.Length, inputData[0].strideSize());
			buffer.SetData(inputData);

			var outputBuffer = new ComputeBuffer (count: 1, stride: sizeof(uint));
			var errorBuffer = new ComputeBuffer (count: this.holePixelCoords.Count, stride: sizeof(float) * 4);
			//Bind buffers
			mappingShader.SetBuffer(kernelIndex: refinementKernelHandle, name: "inputBuffer", buffer: buffer);

			//Also bind unused buffers to avoid undefined behaviour
			mappingShader.SetBuffer(kernelIndex: refinementKernelHandle, name: "outputBuffer", buffer: outputBuffer );
			mappingShader.SetBuffer(kernelIndex: refinementKernelHandle, name: "errorBuffer", buffer: errorBuffer);

			//These two textures remain the same throughout a frames duration and are initialized when the mapping is initialized
			mappingShader.SetTexture (kernelIndex: refinementKernelHandle, name: "HoleNonHoleCoordinatesRenderTexture", texture: this.HoleNonHoleCoordinatesRenderTexture);
//			mappingShader.SetTexture (kernelIndex: refinementKernelHandle, name: "holePixelCoordTexture", texture: this.HoleCoordinatesRenderTexture);
//			mappingShader.SetTexture (kernelIndex: refinementKernelHandle, name: "nonHolePixelCoordTexture", texture: this.NonHoleCoordinatesRenderTexture);
			mappingShader.SetTexture (kernelIndex: refinementKernelHandle, name: "ErrorTexture", texture: this.ErrorRenderTexture);
			mappingShader.SetTexture (kernelIndex: refinementKernelHandle, name: "UpdateTexture", texture: this.UpdateRenderTexture);
			mappingShader.SetTexture (kernelIndex: refinementKernelHandle, name: "Background", texture: this.inpaintingCamera.getBackgroundRenderTexture());
			mappingShader.SetTexture (kernelIndex: refinementKernelHandle, name: "RandomTexture", texture: this.RandomTexture);
			mappingShader.SetTexture (kernelIndex: refinementKernelHandle, name: "Mask", texture: this.inpaintingCamera.getMaskRenderTexture());

			var iterCounter = 0;
			for (int iterIndex = 0; iterIndex < Configuration.Inpainting.NumberOfRefinementIterations; iterIndex++)
			{
				//TIME IT
				this.timeIt.StartOptimizationIter();

				// Need to set textures inside loop, as they are swapped every iteration
				mappingShader.SetTexture (kernelIndex: refinementKernelHandle, name: "NewMapping", texture: this.NewMapping);
				mappingShader.SetTexture (kernelIndex: refinementKernelHandle, name: "OldMapping", texture: this.OldMapping);

				mappingShader.Dispatch (refinementKernelHandle, this.holePixelCoords.Count, 1, 1);

				Vector4[] errorData = new Vector4[this.holePixelCoords.Count];
				errorBuffer.GetData(errorData);
				var inHole = errorData.Aggregate ((result, mapped) => result + mapped).w > 0;

				//TIME IT
				this.timeIt.EndOptimizationIter(level: this.mipmapLevel);

				lastMappedTextureIndex = (lastMappedTextureIndex + 1) % 2;
				iterCounter++;
			}

			errorBuffer.Release ();
			outputBuffer.Release ();
			buffer.Release ();

		}

		private Texture2D SetCoordDataToTexture(IList<Coord> data)
		{
			Texture2D coordDataTexture = new Texture2D (width: 4, height: 1, format: TextureFormat.RGBAFloat, mipmap: false);
			//RenderTexture coordDataRenderTexture = new RenderTexture (width: data.Count, height: 1, format: RenderTextureFormat.RGFloat, mipmap: false);

			byte[] dataArray = new byte[16 * 4];
			convertToByte(5).CopyTo(dataArray, 0);
			convertToByte(3).CopyTo(dataArray, 4);
			convertToByte(10).CopyTo(dataArray, 8);
			convertToByte(88).CopyTo(dataArray, 12);

			convertToByte(99).CopyTo(dataArray, 16);
			convertToByte(100).CopyTo(dataArray, 20);
			convertToByte(101).CopyTo(dataArray, 24);
			convertToByte(111).CopyTo(dataArray, 28);

			convertToByte(1).CopyTo(dataArray, 32);
			convertToByte(2).CopyTo(dataArray, 36);
			convertToByte(3).CopyTo(dataArray, 40);
			convertToByte(4).CopyTo(dataArray, 44);

			convertToByte(5).CopyTo(dataArray, 48);
			convertToByte(6).CopyTo(dataArray, 52);
			convertToByte(7).CopyTo(dataArray, 56);
			convertToByte(8).CopyTo(dataArray, 60);

			coordDataTexture.LoadRawTextureData (dataArray);
//			coordDataTexture.SetPixels32
			coordDataTexture.Apply();
			return coordDataTexture;
		}
		private byte[] convertToByte(float value)
		{

			byte[] bytes = BitConverter.GetBytes(value);
			if (!BitConverter.IsLittleEndian)
			{
				Array.Reverse (bytes);
			}
			return bytes;
		}

		private void ChangeRenderMode()
		{
			//Set new render mode if it is supported and initialize shader and so on
			if (this.desiredRenderMode != this.currentRenderMode)
			{
				if (!shaders.ContainsKey (this.desiredRenderMode)) {
					Debug.LogError ("RenderMode " + this.desiredRenderMode.ToString () + " not supported. Set a shader for this mode.");
					return;
				}
				this.currentRenderMode = this.desiredRenderMode;
				OnChangeRenderMode ();
			}
		}

		private void OnChangeRenderMode()
		{
			//Set textures to shader (using the name defined in the shader programm, not the unity name)
			var material = inpaintingCamera.getMaterial();
			material.SetTexture(name:"_BkgrTex", value: inpaintingCamera.getBackgroundRenderTexture());
			//material.SetTexture(name:"_BkgrTex", value: this.ColorErrorRenderTexture);
			material.SetTexture(name:"_ObjectTex", value: inpaintingCamera.getObjectRenderTexture());
			material.SetTexture(name:"_Mask", value: inpaintingCamera.getMaskRenderTexture());

			if (!shaders.ContainsKey (this.currentRenderMode )) {
				Debug.LogError ("RenderMode " + this.currentRenderMode .ToString () + " not supported. Set a shader for this mode.");
				return;
			}
			material.shader = shaders [this.currentRenderMode ];
		}

		private void MainLoop()
		{
			//TIME IT
			this.timeIt.StartOptimization ();
			this.timeIt.StartLevelOptimization();

			//Do the first refinement based on initialization
			RefineMapping ();

			//TIME IT
			this.timeIt.EndLevelOptimization(level: this.mipmapLevel);

			//Iterate mipmap levels down to zero or the predefined number of iterations
			int levelCount = 0;
			while (levelCount < Configuration.Inpainting.NumberOfMipmapLevelsToProcess && this.mipmapLevel > 0) {
				//TIME IT
				this.timeIt.StartLevelOptimization();

				//Move to next lower mipmap level by using the mapping of the previous level as initialization
				MoveToNextLevel ();

				//Based on that refine the mapping
				RefineMapping ();

				//TIME IT
				this.timeIt.EndLevelOptimization(level: this.mipmapLevel);

				levelCount++;
			}
			//TIME IT
			this.timeIt.EndOptimization();
			this.maxResolutionHolePixelCount = this.holePixelCoords.Count;
			this.maxResolutionTotalPixelCount = this.currentSize.width * this.currentSize.height;
		}

		public void OnPreRender (){
			//Don't render, if target is not visible
			if (!this.initialized) {
				Init ();

			}
			//this.initialized might have changed after the Init() statement
			if(this.initialized){
				
				if (!Configuration.RenderMode.InitOnly) {
					MainLoop ();
				}
			}


			//Change to desired render mode
			ChangeRenderMode ();

			switch (this.currentRenderMode) {
			case Configuration.RenderMode.Mode.Mapped:
				renderMapped ();
				break;
			case Configuration.RenderMode.Mode.Solid:
				renderSolid ();
				break;
			case Configuration.RenderMode.Mode.Error:
				renderErrors ();
				break;
			case Configuration.RenderMode.Mode.Mapping:
				renderMapping ();
				break;
			case Configuration.RenderMode.Mode.Background:
				renderBackground ();
				break;
			case Configuration.RenderMode.Mode.Update:
				renderUpdate ();
				break;
			}
			HardReset ();
		}

		private void renderBackground()
		{

		}

		private void renderSolid(){

		}

		private void renderErrors()
		{
			var material = inpaintingCamera.getMaterial();
			material.SetTexture (name: "_ErrorTex", value: this.ErrorRenderTexture);
		}

		private void renderUpdate()
		{
			var material = inpaintingCamera.getMaterial();
			material.SetTexture (name: "_UpdateTex", value: this.UpdateRenderTexture);
		}

		private void renderMapping()
		{
			var material = inpaintingCamera.getMaterial();
			material.SetTexture (name: "_MappingTex", value: this.OldMapping);
			material.SetInt (name: "map_width", value: currentSize.width );
			material.SetInt (name: "map_height", value: currentSize.height);
			material.SetInt (name: "real_map_width", value: this.OldMapping.width );
			material.SetInt (name: "real_map_height", value: this.OldMapping.height );
		}

		private void renderMapped(){
			var material = inpaintingCamera.getMaterial();
			//Use OldMapping. At that point the index has already been increased and the last created mapping is the OldMapping
			material.SetTexture (name: "_MappingTex", value: this.OldMapping);
			material.SetInt (name: "map_width", value: currentSize.width );
			material.SetInt (name: "map_height", value: currentSize.height);
			material.SetInt (name: "real_map_width", value: MappingRenderTextures[0].width );
			material.SetInt (name: "real_map_height", value: MappingRenderTextures[0].height );
			material.SetInt (name: "render_virtual", value: Configuration.RenderMode.RenderVirtualObject ? 1: 0);
			material.SetInt (name: "mipmap_level", value: this.mipmapLevel);
		}
			
		private float sizeAtMipmapLevel(int size, int mipmapLevel)
		{
			return (float)( size / Math.Pow (2, mipmapLevel));
		}

		private int initMipmapLevelForSize(int initSize, int imageWidth, int imageHeight)
		{
			var size = Math.Max (imageWidth, imageHeight);
			Debug.AssertFormat(initSize <= size, "Size of mipmap level is larger than total size");
			return (int)Math.Ceiling(Math.Log (size / initSize, 2));
		}

		private void pixelsCoords(IList<Coord> holePixelCoords, IList<Coord> nonHolePixelCoords)
		{

			//Copy pixels from RenderTexture to regular texture so they can be accessed
			var maskTexture = this.inpaintingCamera.getMaskTexture();
			//pixels as 1D array left->right, button->up, row-by-row
			IList<Color> pixels = new List<Color>(maskTexture.GetPixels(miplevel: this.mipmapLevel));

			double pixelCount = (double)(maskTexture.width * maskTexture.height);
			int maskHeight = (int)Math.Sqrt ((double)pixels.Count / pixelCount * maskTexture.height * maskTexture.height);
			int maskWidth = pixels.Count / maskHeight;

			//Iterate all pixels, compute their index and put them into the correct set
			for (int pixelIndex = 0; pixelIndex < pixels.Count; pixelIndex++)
			{
				int x = pixelIndex % maskWidth;
				int y = (int)(1.0 * pixelIndex / maskWidth);
				var coord = new Coord (x: x, y: y);

				// If pixel is red, it is part of the hole
				if (pixels [pixelIndex].r > 0) {
					holePixelCoords.Add (coord);
				} else {
					//else it is not part of the hole
					nonHolePixelCoords.Add (coord);
				}
			}
		}

		private float computeTypicalColorError()
		{
			//Copy pixels from RenderTexture to regular texture so they can be accessed
			var backgroundTexture = this.inpaintingCamera.getBackgroundTexture();
			float widthFloat = this.sizeAtMipmapLevel (size: backgroundTexture.width, mipmapLevel: this.mipmapLevel);
			int width = (int)widthFloat;
			//pixels as 1D array left->right, button->up, row-by-row
			IList<Color> pixels = new List<Color> ( backgroundTexture.GetPixels (miplevel: this.mipmapLevel));

			var avgR = pixels.Average (col => col.r);
			var avgG = pixels.Average (col => col.g);
			var avgB = pixels.Average (col => col.b);

			float sumR = 0;
			float sumG = 0;
			float sumB = 0;
			int n = pixels.Count;
			for (int i = 0; i < n; i++) {
				var diffR = pixels [i].r - avgR;
				var diffG = pixels [i].g - avgG;
				var diffB = pixels [i].b - avgB;

				sumR = sumR + diffR * diffR;
				sumG = sumG + diffG * diffG;
				sumB = sumB + diffB * diffB;
			}
			var stdR = Math.Sqrt (sumR / (n ));
			var stdG = Math.Sqrt (sumG / (n ));
			var stdB = Math.Sqrt (sumB / (n ));


			double result = Math.Sqrt(stdR*stdR + stdG*stdG + stdB*stdB);
			//normalize error to be 1 at max 1.7320508075688772 = norm([1,1,1]) = sqrt(3)
//			result = result / 1.7320508075688772; 
			if (result == 0) {
				return 1f;
			}
			return (float)result;
		}

		public void LogTimeIt(){
			this.timeIt.Log (holePixelCount: this.maxResolutionHolePixelCount, totalPixelCount: this.maxResolutionTotalPixelCount);
		}

	}

	public interface InpaintingCamera{
		Material getMaterial ();
		Texture getBackgroundRenderTexture ();
		Texture2D getBackgroundTexture ();
		Texture getMaskRenderTexture ();
		Texture2D getMaskTexture();
		Texture getObjectRenderTexture ();
	}
}
