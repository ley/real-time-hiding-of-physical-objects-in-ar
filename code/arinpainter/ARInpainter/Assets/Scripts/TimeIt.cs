﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace ARInpainter
{
	public class TimeIt
	{
		private IList<long> InitTimes = new List<long> ();
		private IList<IList<long>> InitIterTimes = new List<IList<long>>();
		private IDictionary<int, IList<long>> OptimizationIterTimes = new Dictionary<int, IList<long>> ();
		private IDictionary<int, IList<long>> OptimizationLevelTimes = new Dictionary<int, IList<long>> ();
		private IList<long> OptimizationTimes = new List<long>();
		private IList<long> SetupFrameTimes = new List<long> ();
		private IList<long> TotalTimes = new List<long> ();

		private DateTime startInit;
		private DateTime startInitIter;
		private DateTime startLevelOptimization;
		private DateTime startOptimizationIter;
		private DateTime startOptimizaiton;
		private DateTime startFrameSetup;
		private DateTime startTotal;

		private bool first {
			get {
				return frameCount < 10;
			}
		}

		private int frameCount = 0;

		public TimeIt ()
		{
			
		}

		public void StartTotal(){
			if (first) {
				return;
			}
			this.startTotal = DateTime.Now;
		}
		public void EndTotal(){
			if (first){
				frameCount++;
				return;
			}
			this.TotalTimes.Add((long)(DateTime.Now - this.startTotal).TotalMilliseconds);
		}

		public void StartInit(){
			if (first) {
				return;
			}
			this.startInit = DateTime.Now;
		}

		public void EndInit(){
			if (first) {
				return;
			}
			this.InitTimes.Add ((long)(DateTime.Now - this.startInit).TotalMilliseconds);
		}

		public void StartInitIter(){
			if (first) {
				return;
			}
			this.startInitIter = DateTime.Now;
		}

		public void EndInitIter(int iter){
			if (first) {
				return;
			}
			if (this.InitIterTimes.Count <= iter) {
				this.InitIterTimes.Add (new List<long> ());
			}
			this.InitIterTimes [iter].Add ((long)(DateTime.Now - this.startInitIter).TotalMilliseconds );
		}

		public void StartLevelOptimization(){
			if (first) {
				return;
			}
			this.startLevelOptimization = DateTime.Now;
		}
		public void EndLevelOptimization(int level){
			if (first) {
				return;
			}
			if (!this.OptimizationLevelTimes.ContainsKey(level)) {
				this.OptimizationLevelTimes[level] = new List<long> ();
			}
			this.OptimizationLevelTimes [level].Add ((long)(DateTime.Now - this.startLevelOptimization).TotalMilliseconds );
		}

		public void StartOptimizationIter(){
			if (first) {
				return;
			}
			this.startOptimizationIter = DateTime.Now;
		}
		public void EndOptimizationIter(int level){
			if (first) {
				return;
			}
			if(!this.OptimizationIterTimes.ContainsKey(level)){
				this.OptimizationIterTimes[level] = new List<long>();
			}
			this.OptimizationIterTimes[level].Add((long)(DateTime.Now - this.startOptimizationIter).TotalMilliseconds);
		}

		public void StartOptimization()
		{
			if (first) {
				return;
			}
			this.startOptimizaiton = DateTime.Now;
		}
		public void EndOptimization(){
			if (first) {
				return;
			}
			this.OptimizationTimes.Add ((long)(DateTime.Now - this.startOptimizaiton).TotalMilliseconds);
		}

		public void StartSetupFrame(){
			if (first) {
				return;
			}
			this.startFrameSetup = DateTime.Now;
		}
		public void EndSetupFrame(){
			if (first) {
				return;
			}
			this.SetupFrameTimes.Add ((long)(DateTime.Now - this.startFrameSetup).TotalMilliseconds);
		}

		public void Log(int holePixelCount, int totalPixelCount)
		{
			if (first) {
				return;
			}

			var format = "{0:N2} ({1}/{2}/{3:N2})";

			var holePixelRatio = (double)(holePixelCount) / (double)(totalPixelCount);

			var totalAvg = this.TotalTimes.Average ();
			var totalMin = this.TotalTimes.Min ();
			var totalMax = this.TotalTimes.Max ();
			var totalStd = Std(this.TotalTimes);

			var totalInitAvg = this.InitTimes.Average ();
			var totalInitMin = this.InitTimes.Min ();
			var totalInitMax = this.InitTimes.Max ();
			var totalInitStd = Std(this.InitTimes);


			var totalOptAvg = this.OptimizationTimes.Average ();
			var totalOptMin = this.OptimizationTimes.Min();
			var totalOptMax = this.OptimizationTimes.Max ();
			var totalOptStd = Std (this.OptimizationTimes);


			var setupStr = "-";
			if (this.SetupFrameTimes.Count >= 1) {
				var setupAvg = this.SetupFrameTimes.Average ();
				var setupMin = this.SetupFrameTimes.Min ();
				var setupMax = this.SetupFrameTimes.Max ();
				var setupStd = Std (this.SetupFrameTimes);
				setupStr = String.Format (format, setupAvg, (setupMin == 0 ? "<1" : "" + setupMin), setupMax, setupStd);
			}



			var firstLevel = this.OptimizationLevelTimes.Keys.Max ();
			var lastLevel = this.OptimizationLevelTimes.Keys.Min ();
			var levelsString = "";
			for (int level = firstLevel; level >= lastLevel; level--) {
				var levelOptAvg = this.OptimizationLevelTimes [level].Average ();
				var levelOptMin = this.OptimizationLevelTimes [level].Min ();
				var levelOptMax = this.OptimizationLevelTimes [level].Max ();
				var levelOptStd = Std (this.OptimizationLevelTimes [level]);
				levelsString = levelsString + " & " + String.Format ("{0:N2}", levelOptAvg);// String.Format (format, levelOptAvg, levelOptMin, levelOptMax, levelOptStd);
			}

			string pixelRatioStr = String.Format("{0:N3}",holePixelRatio);

			var totalStr = String.Format (format, totalAvg, (totalMin == 0 ? "<1" : "" + totalMin), totalMax, totalStd);
			var initTotalStr = String.Format (format, totalInitAvg, (totalInitMin == 0 ? "<1" : ""+totalInitMin), totalInitMax, totalInitStd);
			var optTotalStr = String.Format (format, totalOptAvg, (totalOptMin == 0 ? "<1" : ""+totalOptMin), totalOptMax, totalOptStd);


			string textShort = String.Format ("{0} & {1} & {2} & {3} & {4}", pixelRatioStr, totalStr, setupStr, initTotalStr, optTotalStr);
			string textLevels = optTotalStr + levelsString;
			Debug.Log (textShort);
			Debug.Log (textLevels);

		}

		private double Std(IEnumerable<long> values)
		{   
			double ret = 0;
			if (values.Count() > 0) 
			{      
				//Compute the Average      
				double avg = values.Average();
				//Perform the Sum of (value-avg)_2_2      
				double sum = values.Sum(d => Math.Pow(d - avg, 2));
				//Put it all together      
				ret = Math.Sqrt((sum) / (values.Count()-1));   
			}   
			return ret;
		}

	}


}

