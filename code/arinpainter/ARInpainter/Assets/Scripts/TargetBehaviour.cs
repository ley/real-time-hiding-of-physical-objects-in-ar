﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

namespace ARInpainter{
	public class TargetBehaviour : MonoBehaviour,ITrackableEventHandler{
		
		public delegate void OnTargetVisibileChangedDelegate(bool found);
		public static OnTargetVisibileChangedDelegate OnTargetVisibileChanged;
		public static bool TargetFound = false;

		private TrackableBehaviour mTrackableBehaviour;

		void Start()
		{
			mTrackableBehaviour = GetComponent<TrackableBehaviour>();
			if (mTrackableBehaviour)
			{
				mTrackableBehaviour.RegisterTrackableEventHandler(this);
			}
		}


		public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
		{
			if (newStatus == TrackableBehaviour.Status.DETECTED ||
			    newStatus == TrackableBehaviour.Status.TRACKED ||
			    newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED) {
				TargetFound = true;
			} else {
				TargetFound = false;
			}
			if (OnTargetVisibileChanged != null)
			{
				OnTargetVisibileChanged.Invoke (TargetFound);
			}
		}   		

	}
}