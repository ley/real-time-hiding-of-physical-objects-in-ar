﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class Converter : MonoBehaviour {

	private FrameImporter frameImporter;

	// Use this for initialization
	void Start () {
		this.frameImporter = new FrameImporter ();
		this.frameImporter.loadFrameNames ();
		while(frameImporter.hasNext()){
			this.convertFrame ();
			frameImporter.moveToNextFrame ();
		}
		Debug.Log ("Done converting!");
	}

	private void convertFrame(){
		var mask = this.frameImporter.loadFrameMask ();
		var background = this.frameImporter.loadFrameBackground ();

		//Store mask
		var maskData = mask.EncodeToPNG ();
		var maskName = this.frameImporter.imagePath+"/"+frameImporter.maskFileName ()+".png";
		var maskFileStream = File.Open (path: maskName, mode: FileMode.Create);
		var maskBinary = new BinaryWriter(maskFileStream);
		Debug.Log ("Mask image file path: "+maskName);
		maskBinary.Write(maskData);
		maskFileStream.Close();

		//Store mask
		var backgroundData = background.EncodeToPNG ();
		var backgroundName = this.frameImporter.imagePath+"/"+frameImporter.backgroundFileName ()+".png";
		var backgroundFileStream = File.Open (path: backgroundName, mode: FileMode.Create);
		var backgroundBinary = new BinaryWriter(backgroundFileStream);
		Debug.Log ("background image file path: "+backgroundName);
		backgroundBinary.Write(backgroundData);
		backgroundFileStream.Close();
	}

	
	// Update is called once per frame
	void Update () {
		
	}
}
