﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Recorder{
	public class Rendering : MonoBehaviour {

		public GameObject VirtualObject;
		public Material MaskVirtualObjectMaterial;
		public GameObject CenterOfMass;

		private new Camera camera;
		private RenderTexture defaultRenderTexture;
		private Material defaultMaterial;

		// Use this for initialization
		void Start () {
			this.camera = this.GetComponent<Camera> ();
			Debug.AssertFormat (this.camera != null, "Helper Camera not initialized.");
			this.defaultRenderTexture = camera.targetTexture;
		}

		public void setupCameraForNormalRendering()
		{
			camera.targetTexture = this.defaultRenderTexture;
			camera.clearFlags = CameraClearFlags.SolidColor;
			camera.backgroundColor = new Color (0,0,0,0);

			VirtualObject.GetComponent<Renderer> ().material = this.defaultMaterial;

			var layerMask = LayerMask.GetMask("Background");
			if (layerMask == 0) {
				Debug.LogError ("Background Layer Mask not found");
			}
			camera.cullingMask = layerMask;
		}

		/***
		 * Creates a mask texture of pixels that need to be inpainted
		 * This is done by first rendering all tracked objects with value 1 and then all virtual objects with value 0. The render texture is cleared with 0.
		 * The pixels with 1 will be element of the tracked\virtual object pixel-set.
		 ***/
		public void CreateMask(RenderTexture renderTexture){
			camera.targetTexture = renderTexture;

			//0. Clear with 0
			camera.clearFlags = CameraClearFlags.SolidColor;
			camera.backgroundColor = new Color (0,0,0,0);

			//Set shader for virutal Object
			VirtualObject.GetComponent<Renderer>().material = MaskVirtualObjectMaterial;

			//1. render tracked Object with 1
			var trackedLayerMask = LayerMask.GetMask ("Mask");
			if (trackedLayerMask == 0) {
				Debug.LogError ("Mask Layer Mask not found");
			}
			camera.cullingMask = trackedLayerMask;
			camera.Render ();
		}


		public Vector2 projectCenterOfMass(){
			var worldPoint = this.CenterOfMass.transform.position;
			var screenPoint = this.camera.WorldToScreenPoint(worldPoint);
			return screenPoint;
		}
	}
}