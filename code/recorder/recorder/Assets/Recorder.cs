﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;

namespace Recorder{
	public class Recorder : MonoBehaviour {

		public GameObject RenderQuad;
		public Rendering Rendering;
		public RenderTexture BackgroundRenderTexture;
		public RenderTexture MaskRenderTexture;

		public GameObject RecordButton;

		private string startRecording = "Record";
		private string stopRecording = "Stop";
		private Color defaultColor = new Color (1f, 0.5f, 0.5f);
		private Color recordingColor = new Color (0.8f, 0.8f, 0.8f);

		private Texture2D tempBackgroundTexture;
		private Texture2D tempMaskTexture;

		private string folderName = "../../../benchmark_imgs";
		private string metadataFilename = "additional_data";
		private string backgroundImageName = "background";
		private string maskImageName = "mask";
		private string fileFormatPostfix = "raw";
		private string centerOfMassLabel = "center_of_mass";

		private int frameRate = 20;
		private int width;
		private int height;

		private bool saveOutput = true;

//		private Dictionary<string,byte[]> maskFrames = new Dictionary<string,byte[]> (); 
//		private Dictionary<string,byte[]> backgroundFrames = new Dictionary<string,byte[]> (); 


		/*Indicating whether currently recording or not*/
		private bool recording = false;

		void Start(){
			QualitySettings.vSyncCount = 0;  // VSync must be disabled
			Application.targetFrameRate = this.frameRate;

			this.width = BackgroundRenderTexture.width;
			this.height = BackgroundRenderTexture.height;

			this.UpdateButton ();
			Directory.CreateDirectory (folderName);
		}
		void OnPreRender (){
			Rendering.CreateMask (MaskRenderTexture);
			Rendering.setupCameraForNormalRendering();
		}
		void Update(){
			this.processFrame ();

			//Debug.Log (this.Rendering.projectCenterOfMass ());
		}
			
		/***
		 * Start or stop recording
		 */
		public void RecordButtonPressed(){
			this.recording = !this.recording;
			this.UpdateButton ();
			if (!recording) {
				this.stoppedRecording ();
			}
		}

		private void stoppedRecording(){
//			foreach(var maskEntry in this.maskFrames){
//				//saveTexture (maskEntry.Value, maskEntry.Key);
//
//			}
//			foreach(var backgroundEntry in this.backgroundFrames){
//				//saveTexture (backgroundEntry.Value, backgroundEntry.Key);
//				storeFrame (backgroundEntry.Value, backgroundEntry.Key);
//			}
//
//			maskFrames = new Dictionary<string,byte[]> (); 
//			backgroundFrames = new Dictionary<string,byte[]> (); 
		}

		private void UpdateButton(){
			RecordButton.GetComponentInChildren<Text> ().text = this.recording ? this.stopRecording : this.startRecording;
			this.RecordButton.GetComponent<Image> ().color =  this.recording ? this.recordingColor : this.defaultColor;
		}

		private void processFrame(){
			//Only proceed if recording
			if (!recording) {
				return;
			}

			//How to save RenderTexture? readPixel is expensive...
			if (tempMaskTexture == null) {
				this.tempMaskTexture = new Texture2D(width: MaskRenderTexture.width, height: MaskRenderTexture.height, format: TextureFormat.ARGB32, mipmap: false);
			}
			if (tempBackgroundTexture == null) {
				this.tempBackgroundTexture = new Texture2D(width: BackgroundRenderTexture.width, height: BackgroundRenderTexture.height, format: TextureFormat.ARGB32, mipmap: false);
			}

			//this.tempBackgroundTexture = new Texture2D(width: BackgroundRenderTexture.width, height: BackgroundRenderTexture.height, format: TextureFormat.RGB24, mipmap: false);
			//this.tempMaskTexture = new Texture2D(width: MaskRenderTexture.width, height: MaskRenderTexture.height, format: TextureFormat.RGB24, mipmap: false);

			renderTextureToTexture2D (this.BackgroundRenderTexture, ref this.tempBackgroundTexture);
			renderTextureToTexture2D (this.MaskRenderTexture, ref this.tempMaskTexture);

//			var timestamp = getTimestamp ();
			var time = DateTime.Now;

			var backgroundName = createFileName (time: time, name: backgroundImageName, width: width, height: height, framerate: frameRate);
			var maskName = createFileName (time: time, name: maskImageName, width: width, height: height, framerate: frameRate);

			if (this.saveOutput) {
				saveTextureBinary (this.tempBackgroundTexture, backgroundName);
				saveTextureBinary (this.tempMaskTexture, maskName);
			}

//			storeInMemory (this.maskFrames, this.tempMaskTexture, "mask_" + getTimestamp ());
//			storeInMemory (this.backgroundFrames, this.tempBackgroundTexture,  "background_" + getTimestamp ());

			//saveTexture(tempMaskTexture, "mask_"+ getTimestamp() );
			//saveTexture(tempBackgroundTexture, "background_"+ getTimestamp() );

			//store projected center of mass
			var centerOfMass = this.Rendering.projectCenterOfMass ();
			if (this.saveOutput) {
				this.saveCenterOfMassToFile (centerOfMass, time);
			}

		}

		private void storeInMemory( Dictionary<string,byte[]> dict, Texture2D texture, string fileName){
			Debug.AssertFormat (texture != null && texture.width > 0 && texture.height > 0, "Invalid texture");
			//var bytes = texture.EncodeToEXR(Texture2D.EXRFlags.None);
			var bytes = texture.EncodeToPNG();
			//dict [fileName] = texture;
			dict [fileName] = bytes;
		}

		private void saveTexture(Texture2D texture, string toPath){
			Debug.AssertFormat (texture != null && texture.width > 0 && texture.height > 0, "Invalid texture");
			//var bytes = texture.EncodeToEXR(Texture2D.EXRFlags.None);
			var bytes = texture.EncodeToPNG();
			var fileStream = File.Open (path: this.folderName+"/"+toPath+".png", mode: FileMode.Create);
			var binary = new BinaryWriter(fileStream);
			binary.Write(bytes);
			fileStream.Close();
		}

		private void saveTextureBinary(Texture2D texture, string toPath){
			Debug.AssertFormat (texture != null && texture.width > 0 && texture.height > 0, "Invalid texture");
			//var bytes = texture.EncodeToEXR(Texture2D.EXRFlags.None);
			var bytes = texture.GetRawTextureData();
			var fileStream = File.Open (path: this.folderName+"/"+toPath+".raw", mode: FileMode.Create);
			var binary = new BinaryWriter(fileStream);
			binary.Write(bytes);
			fileStream.Close();
		}

		private void storeFrame(byte[] bytes,  string toPath){
			var fileStream = File.Open (path: this.folderName+"/"+toPath+".png", mode: FileMode.Create);
			var binary = new BinaryWriter(fileStream);
			Debug.Log ("Image file path: "+this.folderName);
			binary.Write(bytes);
			fileStream.Close();
		}

		private void renderTextureToTexture2D(RenderTexture renderTexture, ref Texture2D targetTexture){
			Debug.AssertFormat(renderTexture != null && renderTexture.width > 0 && renderTexture.height > 0, "Invalid render texture");
			RenderTexture.active = renderTexture;
			targetTexture.ReadPixels (new Rect (0, 0, renderTexture.width, renderTexture.height), 0, 0);
			targetTexture.Apply ();
			//return targetTexture;
		}

		private void saveCenterOfMassToFile(Vector2 centerOfMass, DateTime time){
			var data = centerOfMass.ToString ();
			this.saveMetaDataToFile(label: this.centerOfMassLabel, time: time, data: data);
		}

		private void saveMetaDataToFile(string label, DateTime time, string data){
			var filename = this.folderName + "/" + this.metadataFilename + ".csv";
			var stamp = timestamp (time);
			using (StreamWriter file = new StreamWriter(filename, true))
			{
				file.WriteLine(string.Format("{0};{1};{2}", stamp, label, data));
			}
		}

//		private String getTimestamp(){
//			return DateTime.Now.ToString ("MM_dd_HH_mm_ss_ffffff");
//		}
		private string createFileName(DateTime time, string name, int width, int height, int framerate){
			return string.Format("{0}_{1}_{2}_{3}_{4}",name, timestamp(time), width, height, framerate);
		}

		private string timestamp(DateTime time){
			return time.ToString ("MM_dd_HH_mm_ss_ffffff");
		}

	}
}

