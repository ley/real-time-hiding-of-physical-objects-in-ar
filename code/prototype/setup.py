# from distutils.core import setup
# from Cython.Build import cythonize
#
# setup(
#     name = "My hello app",
#     ext_modules = cythonize('**.pyx'),  # accepts a glob pattern
# )

from setuptools import setup, find_packages, Extension
from Cython.Distutils import build_ext
from Cython.Build import cythonize
from glob import glob
import os.path as path
import numpy

source_files = glob('./*.pyx')
print(source_files)

ext_modules = cythonize('./*pyx', include_path=[numpy.get_include()], annotate=True,  compiler_directives=dict(always_allow_keywords=True))
# ext_modules=[ Extension(path.splitext(path.basename(source_file))[0], [source_file], cython_directives={"annontate": True, 'always_allow_keywords':True}, include_dirs=[numpy.get_include()]) for source_file in source_files]


setup(name='arinpainter',
      packages=find_packages(),
      cmdclass = {'build_ext': build_ext},
      ext_modules = ext_modules,
      include_dirs=[numpy.get_include()]
     )