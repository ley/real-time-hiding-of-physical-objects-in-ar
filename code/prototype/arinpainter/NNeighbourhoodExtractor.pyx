from arinpainter.AbstractNeighbourExtractor import AbstractNeighbourExtractor
from arinpainter.ARLogging import log
import numpy as np
cimport numpy as np

class NNeighbourhoodExtractor(AbstractNeighbourExtractor):

    def __init__(self, size):
        if size < 3:
            log('Invalid neighbourhood size {}. Must be at least 3'.format(size))
        if not size % 2 == 1:
            log('Invalid neighbourhood size {}. Only odd number neighbourhood size allowed.'.format(size))
        self.size = size
        self.half_size = size // 2
        self.neighbourhood = np.indices((self.size, self.size)).transpose((1, 2, 0)) - np.array([self.half_size,self.half_size]).astype(np.double)

    def get_neighbours(self, double y, double x, int map_size, bint as_patch=False):
        neighbours, _ = self.get_neighbours_and_directions(y=y, x=x, map_size=map_size, as_patch=as_patch)
        return neighbours

    def get_neighbours_and_directions(self, double y, double x, int map_size, bint as_patch=False):
        # create neighbourhood with invalid values
        neighbours = self.neighbourhood + np.array([y,x])

        # remove all negative values
        neighbours[np.any(neighbours < [0,0], axis=2)] = [-1,-1]
        neighbours[np.any(neighbours > [map_size - 1 ,map_size - 1], axis=2)] = [-1,-1]
        neighbours[self.half_size, self.half_size] = [-1,-1]

        if as_patch:
            return neighbours, self.neighbourhood
        else:
            # If indices are non negative, it's a valid neighbour
            valid_neighbours_indices = np.all(neighbours >= [0, 0], axis=2)
            return neighbours[valid_neighbours_indices], self.neighbourhood[valid_neighbours_indices]

    def get_neighbourhood(self, y, x, map_size):
        _, directions = self.get_neighbours_and_directions(y=y, x=x, map_size=map_size, as_patch=False)
        return directions