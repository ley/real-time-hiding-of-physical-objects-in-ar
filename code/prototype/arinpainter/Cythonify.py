import sys,os
import pyximport
import numpy as np
from arinpainter.ARLogging import log
import traceback
include = np.get_include()
#include = include+':/usr/local/lib/python3.6/site-packages/numpy/core/include/'
include = include + ';C:/Users/nick/AppData/Local/Programs/Python/Python36/Lib/site-packages/numpy/core/include/'#':C:\\Users\\nick\\AppData\\Local\\Programs\\Python\\Python36\\Lib\\site-packages\\numpy\\core\\include\\'
#os.environ['CFLAGS'] = '"-I/usr/local/lib/python3.6/site-packages/numpy/core/include/"'
#os.environ['CFLAGS'] = '"-I "C:/Users/nick/AppData/Local/Programs/Python/Python36/Lib/site-packages/numpy/core/include/""'

pyximport.install(setup_args={'include_dirs': include})
from arinpainter.AlgoMaster import AlgoMaster
from arinpainter.Config import config,OutOfHoleProcedure
from arinpainter.ARLogging import log

def normal_run():
    try:
        master = AlgoMaster()
        master.init_data()
        master.run()
    except Exception as e:
        log('ERROR: '+str(e))
        log(traceback.format_exc())
        raise e

def scripted_run(frame_index):
    procedures = [OutOfHoleProcedure.NEIGHBOUR_RANDOM, OutOfHoleProcedure.FORCE, OutOfHoleProcedure.NEIGHBOUR_SMALLEST_ERROR]#
    for run_index in range(len(procedures)):
        config.out_of_hole_procedure = procedures[run_index]
        normal_run()

if __name__ == '__main__':
    # config.set_test()
    # config.exhaustive_search = True
    # frame_index = 0
    # # scene_name = 'window'
    # # scene_name = 'building'
    # # scene_name = 'lausanne'
    # scene_name = 'trash'
    # # scene_name = 'office_desk'
    # # scene_name = 'white_board'
    #
    # config.imagePath = '../../../benchmark_imgs/' + scene_name
    # config.initial_frame_index = frame_index
    #
    # if config.exhaustive_search:
    #     config.stop_at_resolution = 128
    #     normal_run()
    # else:
    #     config.stop_at_resolution = 512
    #     scripted_run( frame_index=frame_index)
    normal_run()

# import pickle
# with open('../../../plots/window_date080618_114549_NEIGHBOUR_SMALLEST_ERROR/mapping_lvl7.pkl', 'rb') as infile:
#     map = pickle.load(infile)
#     i=0