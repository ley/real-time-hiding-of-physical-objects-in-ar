#!python
#cython: language_level=3

import numpy as np
cimport numpy as np
from arinpainter.Map import Map
from arinpainter.Config import config
from arinpainter.ARLogging import log
from copy import deepcopy
import re

class DataProvider:

    def __init__(self, size):
        self.masks = []
        self.backgrounds = []
        self.maps = []
        self.center_of_mass = None
        self.size = size
        self.start_level = 0

    def add_level(self, mask, np.ndarray[dtype=np.uint8_t, ndim=3] background):
        if not len(self.masks) == len(self.backgrounds):
            log('Inconsistent DataProvider state')
            return

        self.backgrounds.append(background)
        self.masks.append(mask)

        return len(self.masks) - 1

    def create_maps(self, size):
        #create two maps
        self.maps.append(Map(side=size))
        self.maps.append(Map(side=size))

    # def previous_levels_last_map(self, level):
    #     # If this is the first level, there is no lower level, so all mappings are to the 'single' pixel
    #     if level == 0:
    #         return np.array([0,0])
    #     else:
    #         #if the maps were made consistent properly, it does not matter which map is returned
    #         return self.maps[level-1][0]

    def make_maps_consistent(self, best_map):
        """
        Copy the best map into both maps
        :return:
        """
        self.maps[0] = best_map
        self.maps[1] = deepcopy(best_map)

    def set_mipmap_level(self, level):
        for map in self.maps:
            map.lookup_level = level

    def add_metadata(self, metadata_lines):
        for stamp, label, value in metadata_lines:
             if label == config.center_of_mass_label:
                x,y = self.parse_string_pair(value)
                y = self.size - y
                self.center_of_mass = np.array([y,x])

    def parse_string_pair(self, line):
        matched = re.match('\D+(\d+\.?\d*)\D+(\d+\.?\d*)\D*', line)
        return np.array([float(matched.group(1)), float(matched.group(2))])

    def center_of_mass_at_level(self, level):
        return self.center_of_mass / 2**level

    @property
    def levels(self):
        return len(self.masks)