from arinpainter.Config import config
#set  different backend, so plots are not shown, but still created and saved
if not config.show_images:
    import matplotlib
    matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.lines as mlines
import matplotlib.gridspec as gridspec
import numpy as np
import scipy.misc

from matplotlib.widgets import Button
from arinpainter.analysis.DataCollector import DataCollector
from arinpainter.ARLogging import log

from datetime import datetime
import os

class ImageDisplay():

    def __init__(self, spatial_neighbourhood_extractor):
        self.mapping_function = None
        self.figure = plt.figure(figsize=(12, 12), dpi=80,)
        self.grid_shape = (8,5)
        self.old_mapping = None
        self.new_mapping = None
        self.center_of_mass = None
        self.imgplot = None
        self.map_plot = None
        self.paused = False
        self.size = 0
        self.last_event = None
        self.iteration = None
        self.trajectory = None
        self.spatial_target = None
        self.color_target = None
        self.neighbour_mapping = None
        self.mapping = None
        self.spatial_neighbourhood_extractor = spatial_neighbourhood_extractor


    # def create_figure(self, number_of_subplots):
    #     # rows = int(math.ceil(math.sqrt(number_of_subplots)))
    #     # cols = int(number_of_subplots / rows)
    #     # self.plot_specs = gridspec.GridSpec(nrows=1, ncols=1, left=0.05, right=0.48, wspace=0.05)
    #     self.figure = plt.figure()
    def update_frame(self, image, center_of_mass, map, background, level, iteration, mipmap_iteration):
        self.iteration = iteration
        # if the plot is not ready yet, create it
        if self.imgplot == None:
            self.add_image(image=image, center_of_mass=center_of_mass, map=map, background=background, level=level)
            # update mapping
            self.remove_mapping()
            if config.show_mapping:
                self.draw_mapping(iteration=iteration)
        # else:
        # plt.pause(99999)
        # else just update
        map_image = self.map_to_image(map=map, level=level)

        # set new extent
        self.imgplot.set_extent([0, image.shape[0], image.shape[1], 0])
        self.map_plot.set_extent([0, map_image.shape[0], map_image.shape[1], 0])
        self.background_plot.set_extent([0, background.shape[0], background.shape[1], 0])

        self.map_plot.set_data(map_image)
        self.imgplot.set_data(image)
        self.background_plot.set_data(background)

        #update axis
        self.image_axis.set_xlim([0,image.shape[0]])
        self.image_axis.set_ylim([image.shape[1], 0])

        self.map_axis.set_xlim([0, map_image.shape[0]])
        self.map_axis.set_ylim([map_image.shape[1], 0])

        self.background_axis.set_xlim([0, background.shape[0]])
        self.background_axis.set_ylim([background.shape[1], 0])

        # Label plots
        self.map_axis.set_title('mapping')
        self.background_axis.set_title('true background')

        self.size = image.shape[0]

        # draw center of mass
        self.draw_center_of_mass(center_of_mass=center_of_mass)

        # update error images
        self.update_error_images(iteration=iteration)

        # update plots
        self.update_plots(iteration=iteration, mipmap_iteration=mipmap_iteration)

        # update mapping
        self.remove_mapping()
        if config.show_mapping:
            self.draw_mapping(iteration=iteration)

        # show image
        plt.draw()
        plt.pause(.1)

        # udpate interactive mapping using last cursor position
        if not self.last_event == None:
            self.onclick(self.last_event)

    def map_to_image(self, map, level):
        size = map.size_at_level(level=level)
        image = np.zeros(shape=(size,size,4), dtype=np.int)

        for y in range(size):
            for x in range(size):
                raw_value = map.total_lookup(y=y, x=x, lookup_level=level)
                raw_value = raw_value / size * 255
                value = np.array([int(raw_value[0]), int(raw_value[1]), 0, 255])
                image[y,x] = value

        return image

    def add_map(self, map, level):
        map_image = self.map_to_image(map=map, level=level)
        self.map_axis = plt.subplot2grid(self.grid_shape, (1, 2), rowspan=2, colspan=1)
        self.map_plot = plt.imshow(map_image, extent=[0, map_image.shape[0], map_image.shape[1], 0])

    def add_background(self, background):
        self.background_axis = plt.subplot2grid(self.grid_shape, (3, 2), rowspan=2, colspan=1)
        self.background_plot = plt.imshow(background, extent=[0, background.shape[0], background.shape[1], 0])

    def error_to_image(self, error, iteration, color_channel=0):
        # normalize error
        norm_error = error[iteration,:,:] / np.max(error)

        image = np.zeros((norm_error.shape[0], norm_error.shape[1],4))
        image[:,:,3] = 1.0
        image[:,:,color_channel] = norm_error
        # image[:,:,1] = 1.0 - error

        return image

    def add_error_maps(self):

        # color error
        color_errors = DataCollector.instance.collection.color_error
        color_error_image = self.error_to_image(color_errors, color_channel=2, iteration=0)
        self.color_error_image_axis = plt.subplot2grid(self.grid_shape, (1, 3), rowspan=2, colspan=1)
        self.color_error_image_plot = plt.imshow(color_error_image, extent=[0, color_error_image.shape[0], color_error_image.shape[1], 0])

        # spatial error
        spatial_errors = DataCollector.instance.collection.spatial_error
        spatial_error_image = self.error_to_image(spatial_errors, iteration=0)
        self.spatial_error_image_axis = plt.subplot2grid(self.grid_shape, (3, 3), rowspan=2, colspan=1)
        self.spatial_error_image_plot = plt.imshow(spatial_error_image, extent=[0, spatial_error_image.shape[0], spatial_error_image.shape[1], 0])

    def add_image(self, image, map, level, background, center_of_mass):

        # buttons
        self.add_buttons()

        #connect event handler
        self.figure.canvas.mpl_connect('motion_notify_event', self.onclick) #motion_notify_event, button_press_event

        # create plots
        self.image_axis = plt.subplot2grid(self.grid_shape,(1,0),rowspan=4, colspan=2)
        # self.image_axis = self.figure.add_subplot(self.img_grid[0])
        self.imgplot = plt.imshow(image)#, extent=[0, image.shape[0], image.shape[1], 0])

        # draw center of mass
        self.draw_center_of_mass(center_of_mass=center_of_mass)

        # add map
        self.add_map(map=map, level=level)

        # add background
        self.add_background(background=background)

        # create plots
        self.add_plots()

        # creat error images
        self.add_error_maps()

        # labels and so on
        self.label_axis =  plt.subplot2grid(shape=self.grid_shape,loc=(0,0),rowspan=1, colspan=4)
        self.label_axis.set_axis_off()
        # legend
        self.create_legend()

        # annotation
        self.label = self.label_axis.annotate('Map', xy=(0.0, 0.0))

        # show image
        plt.tight_layout()
        plt.show(block=False)
        plt.pause(.1)

    def update_error_images(self, iteration):
        if iteration is None:
            return

        color_errors = DataCollector.instance.collection.color_error
        spatial_errors = DataCollector.instance.collection.spatial_error
        color_error_image = self.error_to_image(color_errors, iteration=iteration, color_channel=2)
        spatial_error_image = self.error_to_image(spatial_errors, iteration=iteration)

        # set extent
        self.color_error_image_plot.set_extent([0, color_error_image.shape[0], color_error_image.shape[1], 0])
        self.spatial_error_image_plot.set_extent([0, spatial_error_image.shape[0], spatial_error_image.shape[1], 0])

        # set new data
        self.color_error_image_plot.set_data(color_error_image)
        self.spatial_error_image_plot.set_data(spatial_error_image)

        #update axis
        self.color_error_image_axis.set_xlim([0,color_error_image.shape[0]])
        self.color_error_image_axis.set_ylim([color_error_image.shape[1], 0])

        self.spatial_error_image_axis.set_xlim([0, spatial_error_image.shape[0]])
        self.spatial_error_image_axis.set_ylim([spatial_error_image.shape[1], 0])

        # label plots
        self.color_error_image_axis.set_title('color error')
        self.spatial_error_image_axis.set_title('spatial error')

    def update_plots(self, iteration, mipmap_iteration):
        if iteration is None:
            return
        self.update_norm_plot(iteration=iteration,mipmap_iteration=mipmap_iteration)
        self.update_error_plot(iteration=iteration)
        self.update_in_hole_plot(iteration=iteration)

    def update_in_hole_plot(self, iteration):
        self.in_hole_plot_axis.clear()
        in_hole = DataCollector.instance.collection.mapped_into_hole[:iteration+1,:,:]
        in_hole = np.sum(in_hole, axis=0)
        # log(in_hole)
        counts = []
        max_count = int(np.max(in_hole))
        count_points = range(1,max(3,max_count), max(1, int(max_count/10)))
        for i in count_points:
            counts.append( np.count_nonzero(in_hole >= i) )

        self.in_hole_plot_axis.bar(count_points, counts)

        self.in_hole_plot_axis.set_title('same pixel mapped into hole')

    def update_error_plot(self, iteration):
        self.spatial_error_plot_axis.clear()
        self.color_error_plot_axis.clear()
        self.total_error_plot_axis.clear()

        valid_pixels = (DataCollector.instance.collection.inside_hole[iteration+1,:,:] == 1)

        color_errors = DataCollector.instance.collection.color_error[:iteration+1][:,valid_pixels]
        spatial_errors = DataCollector.instance.collection.spatial_error[:iteration+1][:,valid_pixels]
        total_errors = DataCollector.instance.collection.total_error[:iteration+1][:,valid_pixels]
        # i=0
        # color_errors = color_errors.reshape((color_errors.shape[0], color_errors.shape[1]*color_errors.shape[2]))
        # spatial_errors = spatial_errors.reshape((spatial_errors.shape[0], spatial_errors.shape[1] * spatial_errors.shape[2]))
        # hole_errors = hole_errors.reshape((hole_errors.shape[0], hole_errors.shape[1] * hole_errors.shape[2]))

        color_error = np.mean(color_errors, axis=1)
        spatial_error = np.mean(spatial_errors, axis=1)
        total_error = np.mean(total_errors, axis=1)

        self.color_error_plot_axis.plot(color_error, c='blue')
        self.spatial_error_plot_axis.plot(spatial_error, c='red')
        self.total_error_plot_axis.plot(total_error, c='red')

        self.color_error_plot_axis.set_title('color error')
        self.spatial_error_plot_axis.set_title('spatial error')
        self.total_error_plot_axis.set_title('total error')

        # blue_patch = mpatches.Patch(color='blue', label='color error')
        # red_patch = mpatches.Patch(color='red', label='spatial error')
        # self.error_plot_axis.legend(handles=[blue_patch, red_patch])


    def update_norm_plot(self, iteration, mipmap_iteration):
        self.norm_plot_axis.clear()

        # get gradients, compute norm and reshape
        color_gradient = DataCollector.instance.collection.color_gradient[iteration,:,:,:]
        color_gradient_norm = np.linalg.norm(color_gradient, axis=2)
        color_gradient_norm = color_gradient_norm.reshape((color_gradient_norm.shape[0]*color_gradient_norm.shape[1]))

        # scale by weight
        color_gradient_norm = color_gradient_norm * config.color_direction_weight(iteration=iteration, mipmap_iteration=mipmap_iteration)

        spatial_gradient = DataCollector.instance.collection.spatial_gradient[iteration, :, :, :]
        spatial_gradient_norm = np.linalg.norm(spatial_gradient, axis=2)
        spatial_gradient_norm = spatial_gradient_norm.reshape((spatial_gradient_norm.shape[0] * spatial_gradient_norm.shape[1]))

        # scale by weight
        spatial_gradient_norm = spatial_gradient_norm * config.spatial_direction_weight(iteration=iteration, mipmap_iteration=mipmap_iteration)

        # plot only elements with at least one non zero norm
        valid_elements = (spatial_gradient_norm != 0) + (color_gradient_norm != 0)

        spatial_gradient_norm = spatial_gradient_norm[valid_elements]
        color_gradient_norm = color_gradient_norm[valid_elements]

        self.norm_plot_axis.scatter(color_gradient_norm, spatial_gradient_norm, s=5, alpha=0.5)

        # self.norm_plot_axis.set_xlim([0, config.abs_threshold])
        # self.norm_plot_axis.set_ylim([0, config.abs_threshold])

        self.norm_plot_axis.set_xlabel('color')
        self.norm_plot_axis.set_ylabel('spatial')
        self.norm_plot_axis.set_title('norm of gradients [px]')


    def add_plots(self):
        #  gradient norm plot
        self.norm_plot_axis = plt.subplot2grid(self.grid_shape,(5,0),rowspan=2)

        self.color_error_plot_axis = plt.subplot2grid(self.grid_shape, (5, 1), rowspan=2)
        self.spatial_error_plot_axis = plt.subplot2grid(self.grid_shape, (5, 2), rowspan=2)
        self.total_error_plot_axis = plt.subplot2grid(self.grid_shape, (5, 3), rowspan=2)
        self.in_hole_plot_axis = plt.subplot2grid(self.grid_shape, (0, 4), rowspan=1)

    def remove_markers(self):
        # remove old highligh if exists
        if not self.old_mapping is None:
            self.old_mapping.remove()
            self.old_mapping = None

        if not self.new_mapping is None:
            self.new_mapping.remove()
            self.new_mapping = None

        if not self.trajectory is None:
            for line in self.trajectory:
                line.remove()
            self.trajectory = None

        if not self.spatial_target is None:
            self.spatial_target.remove()
            self.spatial_target = None

        if not self.color_target is None:
            self.color_target.remove()
            self.color_target = None

        if not self.neighbour_mapping is None:
            self.neighbour_mapping.remove()
            self.neighbour_mapping = None

    def remove_mapping(self):
        if not self.mapping is None:
            for map in self.mapping:
                map.remove()
            self.mapping = None

    def show_new_markers(self,y,x,iteration):
        if config.collect_data and not self.iteration is None:
            # highlight pixel
            new_mapping = DataCollector.instance.collection.new_mapping[iteration, y, x]
            self.new_mapping = self.image_axis.scatter(new_mapping[1]+0.5, new_mapping[0]+0.5, color='red', s=4)

            # old_mapping = DataCollector.instance.collection.old_mapping[iteration, y, x]
            # self.old_mapping = self.image_axis.scatter(old_mapping[1]+0.5, old_mapping[0]+0.5, color='yellow', s=4)

            self.draw_trajectory(y=y, x=x, iteration=iteration)

            spatial_target = DataCollector.instance.collection.spatial_target[iteration, y, x]
            self.spatial_target = self.image_axis.scatter(spatial_target[1]+0.5, spatial_target[0]+0.5, color='blue', s=10)

            color_target = DataCollector.instance.collection.color_target[iteration, y, x]
            self.color_target = self.image_axis.scatter(color_target[1] + 0.5, color_target[0] + 0.5, color='darkblue', s=10)

            self.show_neighbours_mapping(y=y, x=x, iteration=iteration)

    def show_neighbours_mapping(self,y,x, iteration):
            map_size = DataCollector.instance.collection.old_mapping.shape[1]


            # neighbours_mapping = []
            # if x >= 1:
            #     neighbours_mapping.append(DataCollector.instance.collection.new_mapping[iteration, y, x - 1])
            # if x < map_size - 1.5:
            #     neighbours_mapping.append(DataCollector.instance.collection.new_mapping[iteration, y, x + 1])
            # if y >= 1:
            #     neighbours_mapping.append(DataCollector.instance.collection.new_mapping[iteration, y - 1, x])
            # if y < map_size - 1.5:
            #     neighbours_mapping.append(DataCollector.instance.collection.new_mapping[iteration, y + 1, x])
            #
            neighbours = self.spatial_neighbourhood_extractor.get_neighbours(y=y, x=x, map_size=map_size)
            neighbour_indices = self.spatial_neighbourhood_extractor.indices_for_array_access(indices=neighbours)
            neighbours_mapping = DataCollector.instance.collection.new_mapping[iteration][neighbour_indices]
            self.neighbour_mapping = self.image_axis.scatter(neighbours_mapping[:,1] + 0.5, neighbours_mapping[:,0] + 0.5, color='yellow',
                                                        s=10)


    def draw_trajectory(self, y,x, iteration):
        if not iteration > 2:
            return
        points = DataCollector.instance.collection.new_mapping[:iteration, y, x]
        points = np.transpose(points)
        self.trajectory = self.image_axis.plot(points[1]+0.5, points[0]+0.5, c='red')

    def draw_mapping(self, iteration):
        size = DataCollector.instance.collection.new_mapping.shape[1]
        points = np.indices((size, size)).transpose((1, 2, 0))
        points = points.reshape((size**2,2))
        mapped_points = DataCollector.instance.collection.new_mapping[iteration, :, :]
        mapped_points = mapped_points.reshape((size ** 2, 2))
        mapping = np.column_stack((points, mapped_points))
        valid_mapping = np.any((np.abs(mapped_points - points)) > np.array([0,0]), axis=1)
        mapping = mapping[valid_mapping,:]
        mapping = np.transpose(mapping)
        self.mapping = self.image_axis.plot(mapping[(1,3),:] + 0.5, mapping[(0,2),:] + 0.5, c='green')
        self.mapping_dots = self.image_axis.scatter(mapping[1,:]+ 0.5, mapping[0,:]+ 0.5, c='green',s=10)

    def onclick(self,event):
        self.last_event = event

        self.remove_markers()

        #update canvas with new mapping info
        if not event.xdata == None:
            y = int(event.ydata)
            x = int(event.xdata)
            if not (0 <= y < self.size and 0 <= x < self.size):
                return
            mapped = self.mapping_function(y=y, x=x)

            # avoid highlights outside image
            if not (0 <= mapped[0] < self.size and 0 <= mapped[1] < self.size):
                return

            # update label
            text = 'mapping x,y={:.2f},{:.2f}'.format(mapped[1], mapped[0])
            self.label.update({'text': text})

            self.show_new_markers(y=y, x=x, iteration=self.iteration)

            # update canvas
            self.figure.canvas.draw()

    def add_buttons(self):
        pause_axis = plt.axes([0.05, 0.02, 0.1, 0.05])
        self.pause_button = Button(pause_axis, 'Pause')
        self.pause_button.connect_event(event='button_press_event', callback=self.pause)

    def pause(self,event):
        self.paused = not self.paused
        if self.paused:
            plt.pause(100000)
        else:
            plt.gcf().canvas.stop_event_loop()


    def create_legend(self):
        # legend
        red_patch = mpatches.Patch(color='red', label='Mapping')
        green_patch = mpatches.Patch(color='green', label='center of mass')
        darkblue_patch = mpatches.Patch(color='darkblue', label='color target')
        blue_patch = mpatches.Patch(color='blue', label='spatial target')

        self.label_axis.legend(handles=[red_patch, green_patch, darkblue_patch, blue_patch])


    def draw_center_of_mass(self, center_of_mass):
        if not self.center_of_mass == None:
            self.center_of_mass.remove()
        self.center_of_mass = self.image_axis.scatter(center_of_mass[1], center_of_mass[0], color='green', s=50)

    def save_plot(self, filename):
        path = config.out_dir_name+'/'+filename
        if config.debug_verbose:
            log('saving image to '+path)
        plt.savefig(path)

    def save_frame(self, filename, frame):
        scipy.misc.imsave(config.out_dir_name+'/'+filename+'.png', frame)



# if __name__ == '__main__':
    # from arinpainter.ImageImporter import ImageImporter
    #
    # importer = ImageImporter()
    # importer.read_filenames()
    # image = importer.read_background(0)
    #
    # display = ImageDisplay(mapping_function=lambda y,x:(y,x))
    #
    # display.add_image(image)


