from abc import ABCMeta, abstractmethod
import numpy as np
cimport numpy as np


class AbstractNeighbourExtractor(metaclass=ABCMeta):

    # def get_neighbourhood_color(self, int y, int x, map, background, color_lookup):
    #     """
    #     Gets all neighbours' colors
    #     For edge or corner pixels only the valid neighbours are considered
    #     :param y: coordinate for which to get the neighbours info
    #     :param x: coordinate for which to get the neighbours info
    #     :param map: The map based on which the neighbours mapping and color should be determined
    #     :param background: The image based on which the neighbours colour is determined
    #     :return: contains all neighbours colors,  np.array of dimension (#neighbours,4)
    #     """
    #
    #     cdef np.ndarray[dtype=np.double_t, ndim=2] neighbour_mappping = self.get_neighbourhood_mapping(y=y, x=x, map=map)
    #     cdef np.ndarray[dtype=np.int_t, ndim=2] neighbour_colors = np.zeros(shape=(neighbour_mappping.shape[0],4), dtype=np.int)
    #     for y,x in neighbour_mappping:
    #         neighbour_colors[y,x,:] = color_lookup(background=background, y=y, x=x)
    #     return neighbour_colors


    # def get_neighbourhood_average_color(self, y, x, map, background):
    #     """
    #     Computes the average color of the neighbouring pixel. The averaging scheme is implementation depended.
    #     :param y: coordinate for which to get the neighbours info
    #     :param x: coordinate for which to get the neighbours info
    #     :param map: The map based on which the neighbours' colors should be determined
    #     :param background: The image based on which the neighbours colour is determined
    #     :return: average_color np.array of dimension (4,)
    #     """
    #     colors = self.get_neighbourhood_color(y=y, x=x, map=map, background=background)
    #     average_color = np.mean(colors, axis=0)
    #     return average_color

    def get_neighbourhood_mapping(self, double y, double x, map):
        """
        Return all neighbours mapping.
        :param y: coordinate for which to get the neighbours info
        :param x: coordinate for which to get the neighbours info
        :param map: The map based on which the neighbours mapping should be determined
        :return: the mapping of each neighbour, np.array of dimension (#neighbours,2)
        """
        cdef np.ndarray[dtype=np.double_t, ndim=2] neighbours = self.get_neighbours(y=y, x=x, map_size=map.size_at_level())
        indices = self.indices_for_array_access(neighbours)
        cdef np.ndarray[dtype=np.double_t, ndim=2] mapping = map[indices]

        return mapping

    def indices_for_array_access(self, np.ndarray[ndim=2, dtype=np.double_t] indices):
        """
        Turns a list of coordinates into two lists of coordinate components. Also rounds to integer values
        :param indices: list of coordinates shape=(n,2)
        :return: coordinate component lists shape=(2,n)
        """
        int_indices = np.round(indices).astype(dtype=np.int)
        int_indices = list(np.transpose(int_indices))
        return int_indices

    # def get_properly_mapped_neighbours(self,y,x,map, mask):
    #     """
    #     Get all neighbours that are not mapped into the hole
    #     :param y: coordinate for which to get the neighbours info
    #     :param x: coordinate for which to get the neighbours info
    #     :param map: The map based on which the neighbours mapping should be determined
    #     :param mask: The image mask to determine whether the mapping is proper (not into hole)
    #     :return: np.array (n by 2) containing the neighbour coordinates
    #     """
    #     # get neighbours
    #     neighbours = self.get_neighbours(y=y,x=x, map_size=map.size_at_level(), as_patch=True)
    #
    #     color = -np.ones(neighbours.shape)
    #     for y in range(neighbours.shape[0]):
    #         for x in range(neighbours.shape[1]):
    #             (n_y, n_x) = neighbours[y,x]
    #             if n_x>=0 and n_y>=0:
    #                 mapped_y, mapped_x = map[n_y,n_x]
    #
    #                 color[y,x] = background[mapped_y, mapped_x]
    #
    #     # get the mappings mask value
    #     mask_values = mask[mapping]
    #
    #     # filter neighbours according to mask value
    #     proper_neighbours = neighbours[mask_values == 0,:]
    #
    #     return proper_neighbours

    @abstractmethod
    def get_neighbours(self, double y, double x, int map_size, bint as_patch=False):
        """
        Get all existing neighbours
        :param y: coordinate for which to get the neighbours info
        :param x: coordinate for which to get the neighbours info
        :return: np.array (n by 2) containing the neighbour coordinates
        """
    @abstractmethod
    def get_neighbourhood(self, y, x, map_size):
        """

        :param y:
        :param x:
        :param map_size:
        :return:
        """


