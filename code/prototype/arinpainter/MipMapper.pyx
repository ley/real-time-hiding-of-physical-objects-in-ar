#!python
#cython: language_level=3
import numpy as np
cimport numpy as np
from arinpainter.DataProvider import DataProvider
from arinpainter.ARLogging import log
from collections import namedtuple
import math

class MipMapper:
    limit_size = 2

    def compute_mipmaps(self, mask, np.ndarray[dtype=np.uint8_t, ndim=3] background):
        #pre-conditions
        if not mask.shape[0] == background.shape[0] == mask.shape[1] == background.shape[1]:
            log('Only square images of equal size allowed')
            return
        if not mask.shape[0] % 2 == 0:
            log('Only power-of-two size images allowed')
            return

        data_provider = DataProvider(size=mask.shape[0])
        data_provider.add_level(mask=mask, background=background)
        for mipmap_data in self.mipmaps(mask=mask, background=background):
            data_provider.add_level(mask=mipmap_data.mask, background=mipmap_data.background)

        data_provider.create_maps(size=mask.shape[0])

        return data_provider

    def mipmaps(self, background, mask):
        cdef int level = 1
        cdef int width = mask.shape[0]
        mipmap_data = namedtuple('mipmap_data',['mask','background'])
        # MipMap until only 2x2 pixels
        while width > self.limit_size:
            mask = self.mipmap_mask(mask)
            background = self.mipmap_background(background)

            width = mask.shape[0]
            level = level + 1

            yield mipmap_data(mask=mask, background=background)

    def mipmap_background(self, np.ndarray[dtype=np.uint8_t, ndim=3] image):
        cdef np.ndarray[dtype=np.uint8_t, ndim=3] padded_image = np.pad(image , pad_width=((0,1),(0,1),(0,0)), mode='constant')
        cdef np.ndarray[dtype=np.double_t, ndim=3] sum_image = np.zeros((padded_image.shape[0], padded_image.shape[1],padded_image.shape[2]), dtype=np.double)
        sum_image[:,:,:] = padded_image[:,:,:]
        sum_image[1:,:,:] += padded_image[:-1,:,:]
        sum_image[:, 1:, :] += padded_image[:, :-1, :]
        sum_image[1:, 1:, :] += padded_image[:-1, :-1, :]
        sum_image = sum_image // 4

        cdef np.ndarray[dtype=np.uint8_t, ndim=3] mipped = np.zeros((image.shape[0]//2, image.shape[1]//2, image.shape[2]), dtype=np.uint8)
        mipped[:,:,:] = sum_image[1::2,1::2,:]
        return mipped

    def mipmap_mask(self, image):
        padded_image = np.pad(image , pad_width=((0,1),(0,1)), mode='constant')
        sum_image = np.zeros(padded_image.shape, dtype='int32')
        sum_image[:,:] = padded_image[:,:]
        sum_image[1:,:] += padded_image[:-1,:]
        sum_image[:, 1:] += padded_image[:, :-1]
        sum_image[1:, 1:] += padded_image[:-1, :-1]
        sum_image = np.ceil(sum_image / 4)

        mipped = np.zeros((image.shape[0]//2, image.shape[1]//2), dtype=np.uint8)
        mipped[:,:] = sum_image[1::2,1::2]
        return mipped

if __name__ == '__main__':
    from arinpainter.ImageImporter import ImageImporter
    import matplotlib.pyplot as plot

    importer = ImageImporter()
    importer.read_filenames()
    (background, mask) = importer.read_frame(index=0)

    mapper = MipMapper()
    mipped = mapper.mipmap_it(background)
    plot.imshow(mipped)
    plot.show()