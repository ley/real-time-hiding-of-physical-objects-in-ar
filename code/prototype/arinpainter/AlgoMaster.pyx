#!python
#cython: language_level=3
from arinpainter.Inpainter import Inpainter
from arinpainter.ImageImporter import ImageImporter
from arinpainter.MipMapper import MipMapper
from arinpainter.Config import config
from arinpainter.ImageDisplay import ImageDisplay
from arinpainter.analysis.DataCollector import *
from arinpainter.ARLogging import log,time_ellapse_formated
from copy import deepcopy
cimport numpy as np
from time import time
import math

class AlgoMaster:


    def __init__(self):
        config.set_out_dir_name()
        config.save_config_file()
        log(config.string_represent())
        self.data_provider = None
        self.start_level = 0
        if config.collect_data:
            self.data_collector = DataCollector.create_instance()

    def run(self):
        start_time = time()
        next_level = self.data_provider.levels - 1
        is_first_level = True
        #for level_index in range(self.data_provider.levels-1,0,-1):
        while next_level >= 0 and self.data_provider.maps[0].size_at_level(level=next_level) <= config.stop_at_resolution:
            if config.master_verbose:
                log('Entering mipmap level '+str(next_level))
            next_level, is_first_level, last_inpainter = self.process_level(level_index=next_level,first_level=is_first_level)
        if config.save_mapping:
            last_inpainter.save_mapping()
        end_time = time()
        log('DONE in '+time_ellapse_formated(start_time=start_time, end_time=end_time))


    def init_data(self):
        # load frame
        if config.master_verbose:
            log('loading images')
        importer = ImageImporter()
        importer.read_filenames()
        cdef np.ndarray[dtype=np.uint8_t, ndim=3] background
        (background, mask) = importer.read_frame(index=config.initial_frame_index)
        self.true_background = deepcopy(background)
        background = self.cut_hole_into_background(background=background, mask=mask)

        if config.master_verbose:
            log('Create mipmaps')
        mipper = MipMapper()
        self.data_provider = mipper.compute_mipmaps(mask=mask, background=background)
        importer.read_meta_data()
        meta_line = importer.get_metadata(config.initial_frame_index)
        self.data_provider.add_metadata(meta_line)

    def process_level(self, level_index, first_level):
        """
        Processes one level and returns the index of the next level to process
        :param level_index:
        :return:
        """
        inpainter = Inpainter(data_provider=self.data_provider, mipmap_level=level_index)
        self.display = ImageDisplay(spatial_neighbourhood_extractor=inpainter.spatial_neighbour_extractor)

        if config.collect_data:
            DataCollector.instance.next_level(level=level_index, size=inpainter.old_map.size_at_level())

        if not config.exhaustive_search and not inpainter.should_start():
            return level_index - 2 , True, None
        elif config.exhaustive_search and not inpainter.should_start():
            start_level = int(math.log2(self.data_provider.size // config.stop_at_resolution))
            log('Do exhaustive, start at level {} with resolution {}'.format(start_level, self.data_provider.maps[0].size_at_level(start_level)))
            return start_level, True, None
        if config.master_verbose:
            log('Start mipmap level ' + str(level_index))

        start_time = time()

        if first_level:
            self.data_provider.start_level = level_index
            inpainter.initialize_map()
            inpainter.finalize()

            if config.save_plots:
                self.show_frame(inpainter=inpainter, iteration=0, save_frame=True)
                iteration = inpainter.iteration_index
                filename = 'init_colorwieght{:.0f}_spatialweight{:.0f}'.format(inpainter.color_direction_weight*100, inpainter.spatial_direction_weight*100)
                self.display.save_plot(filename)
            else:
                self.show_frame(inpainter=inpainter, iteration=0)

            # return level_index - 1, False

        else:
            inpainter.re_initialize_map()

        # self.show_frame(inpainter=inpainter, iteration=0)

        while not inpainter.done():
            if config.master_verbose:
                log('Iteration '+str(inpainter.iteration_index)+' started. Image size '+str(self.data_provider.backgrounds[level_index].shape))
            iter_start_time = time()

            inpainter.do_iteration()
            iter_end_time = time()

            log('Iteration '+str(inpainter.iteration_index-1)+' ended in '+time_ellapse_formated(start_time=iter_start_time, end_time=iter_end_time))

            if config.save_plots and config.exhaustive_search:
                filename = 'lvl{}_iter{}'.format(level_index,iteration)
                self.show_frame(inpainter=inpainter, iteration=0, save_frame=True, frame_filename=filename)
                iteration = inpainter.iteration_index

                self.display.save_plot('plot_'+filename)

            if config.show_intermediate_frames:
                self.show_frame(inpainter=inpainter, iteration=inpainter.iteration_index-1)

        inpainter.finalize()

        if config.save_plots:
            self.show_frame(inpainter=inpainter, iteration=inpainter.iteration_index-1, save_frame=True)
            iteration = inpainter.iteration_index
            filename = 'lvl{}_iter{}_colorwieght{:.0f}_spatialweight{:.0f}'.format(level_index,iteration, inpainter.color_direction_weight*100, inpainter.spatial_direction_weight*100)
            self.display.save_plot(filename)
        else:
            self.show_frame(inpainter=inpainter, iteration=inpainter.iteration_index-1)



        end_time = time()
        log('Picked mapping of iteration {}'.format(inpainter.best_map_iteration))
        log('Completed mipmap level ' + str(level_index)+' in '+time_ellapse_formated(start_time=start_time, end_time=end_time))


        return level_index - 1, False, inpainter

    def cut_hole_into_background(self, background, mask):
        # cut hole into background
        background[:, :, 0][mask > 0] = 255  # set color to 255,0,0
        background[:, :, 1:3][mask > 0] = 0  # set color to 255,0,0
        background[:, :, 3][mask > 0] = 255  # set alpha to max
        return background

    def show_frame(self, inpainter, iteration, save_frame=False, frame_filename=None):
        # create frame from mapping
        frame = inpainter.create_new_frame()

        if save_frame and config.save_plots:
            frame_filename = frame_filename if not frame_filename == None else 'lvl'+str(inpainter.mipmap_level)
            self.display.save_frame(filename=frame_filename, frame=frame)

        #update mapping function based on last map
        mapping = lambda y,x: inpainter.old_map[y,x]
        self.display.mapping_function = mapping

        # update center of mass
        center_of_mass = inpainter.data_provider.center_of_mass_at_level(level=inpainter.mipmap_level)

        # set new frame
        self.display.update_frame(image=frame, center_of_mass=center_of_mass, map=inpainter.old_map, background=self.true_background, level=inpainter.mipmap_level, iteration=iteration, mipmap_iteration=inpainter.mipmap_iteration)

if __name__ == '__main__':
    master = AlgoMaster()
    master.init_data()
    master.run()