from arinpainter.NNeighbourhoodExtractor import NNeighbourhoodExtractor
from arinpainter.analysis.DataCollector import DataCollector, DataCollection
from arinpainter.ARLogging import log
cimport numpy as np
import numpy as np
import pickle
from copy import deepcopy
from random import randint
np.import_array()

from arinpainter.Config import config, OutOfHoleProcedure

cdef class Inpainter:
    cdef public:
        object color_neighbour_extractor, spatial_neighbour_extractor, data_provider, best_map
        int mipmap_level, iteration_index, error_not_smaller_for_iterations, best_map_iteration
        double smallest_error, relevant_color_distance

    # cdef int mipmap_level
    # cdef object data_provider
    # # cpdef object color_neighbour_extractor
    # # cpdef object spatial_neighbour_extractor
    # cdef int iteration_index

    def __init__(self, data_provider, mipmap_level, color_neighbour_extractor=None, spatial_neighbour_extractor=None):
        if color_neighbour_extractor is None:
            color_neighbour_extractor = NNeighbourhoodExtractor(size=5)
        if spatial_neighbour_extractor is None:
            spatial_neighbour_extractor = NNeighbourhoodExtractor(size=3)
        self.mipmap_level = mipmap_level
        self.data_provider = data_provider
        self.data_provider.set_mipmap_level(level=mipmap_level)
        self.color_neighbour_extractor = color_neighbour_extractor
        self.spatial_neighbour_extractor = spatial_neighbour_extractor
        self.iteration_index = 0
        self.best_map_iteration = -1
        self.smallest_error = 99999999
        self.error_not_smaller_for_iterations = 0
        self.relevant_color_distance = 1

    @property
    def mask(self):
        return self.data_provider.masks[self.mipmap_level]

    @property
    def background(self):
        return self.data_provider.backgrounds[self.mipmap_level]

    @property
    def old_map(self):
        return self.data_provider.maps[self.old_map_index]

    @property
    def new_map(self):
        return self.data_provider.maps[self.new_map_index]

    @property
    def new_map_index(self):
        return (self.iteration_index + 1) % 2

    @property
    def old_map_index(self):
        return self.iteration_index % 2
    @property
    def mipmap_iteration(self):
        return self.data_provider.start_level - self.mipmap_level
    @property
    def color_direction_weight(self):
        return config.color_direction_weight(iteration=self.iteration_index, mipmap_iteration=self.mipmap_iteration)
    @property
    def spatial_direction_weight(self):
        return config.spatial_direction_weight(iteration=self.iteration_index, mipmap_iteration=self.mipmap_iteration)
    @property
    def color_pixel_step_size(self):
        return config.color_pixel_step_size(iteration=self.iteration_index, mipmap_iteration=self.mipmap_iteration)
    @property
    def spatial_pixel_step_size(self):
        return config.spatial_pixel_step_size(iteration=self.iteration_index, mipmap_iteration=self.mipmap_iteration)
    @property
    def min_iteration(self):
        return config.min_iteration( mipmap_iteration=self.mipmap_iteration)

    # cpdef inpaint_frame(self):
    #     """
    #     Inpaints a frame for given frame_index. If no index is specified, a default is used
    #     :return:
    #     """
    #
    #     self.initialize_map()
    #
    #     # iterate until done
    #     while not self.done():
    #         if config.master_verbose:
    #             log('iteration ' + str(self.iteration_index)+ ' started. at level '+str(self.mipmap_level))
    #         self.do_iteration()


    cpdef do_iteration(self):
        # for each pixel: compute search directions based on the spatial and color cost function and update the map
        cdef np.ndarray[dtype=np.double_t, ndim=1] color_direction, spatial_direction
        for (y,x) in self.hole_pixels():
            color_direction = self.compute_color_direction(y=y, x=x)
            spatial_direction = self.compute_spatial_direction(y=y, x=x)
            self.update_map(color_direction=color_direction, spatial_direction=spatial_direction, y=y, x=x)
            # log('processed pixel '+str(x)+','+str(y))

        # complete iteration
        self.iteration_completed()

    cpdef np.ndarray[dtype=np.double_t, ndim=1] compute_color_direction(self, int y, int x):

        # get mapping
        cdef double mapped_y,mapped_x
        mapped_y,mapped_x = self.old_map[y,x]

        # compute errors on neighbouring positions for finite difference
        cdef double diff = 0.51
        cdef double mapped_y_min = mapped_y - diff
        cdef double mapped_x_min = mapped_x - diff
        cdef double mapped_y_max = mapped_y + diff
        cdef double mapped_x_max = mapped_x + diff

        cdef double error_top, error_bottom, error_left, error_right
        try:
            error_top = self.compute_color_error(y=y, x=x, mapped_y=mapped_y_min, mapped_x=mapped_x)
        except:
            error_top = 1
        try:
            error_bottom = self.compute_color_error(y=y, x=x, mapped_y=mapped_y_max, mapped_x=mapped_x)
        except:
            error_bottom = 1
        try:
            error_left = self.compute_color_error(y=y, x=x, mapped_y=mapped_y, mapped_x=mapped_x_min)
        except:
            error_left = 1
        try:
            error_right = self.compute_color_error(y=y, x=x, mapped_y=mapped_y, mapped_x=mapped_x_max)
        except:
            error_right = 1
        # log(mapped_y, mapped_x)
        # log('amapped ', mapped_y_min, mapped_y_max, mapped_x_min, mapped_x_max)
        # log(error_top, error_bottom, error_left, error_right)

        # compute finite difference
        cdef double gradient_y = error_bottom - error_top
        cdef double gradient_x = error_right - error_left

        cdef np.ndarray[dtype=np.double_t, ndim=1] direction = -np.array([gradient_y, gradient_x], dtype=np.double)
        #sanity check
        if not np.linalg.norm(direction) > 0:
            if config.debug_verbose:
                log('color direction norm is zero')
            return np.array([0,0], dtype=np.double)

        # move by pixel_step_size pixels
        direction = direction * self.color_pixel_step_size

        # clamp direction
        # direction = self.normalize_vector(direction)

        if config.collect_data:
            DataCollector.instance.collection.color_target[self.iteration_index,y,x] = np.array([mapped_y,mapped_x]) + direction

        return direction

    cpdef np.double_t compute_color_error(self,double y,double x, double mapped_y, double mapped_x):
        # precondition

        if  not 0 <= y <= self.old_map.size_at_level() - 1\
            or not 0 <= x <= self.old_map.size_at_level() - 1\
            or not 0 <= mapped_y <= self.old_map.size_at_level() - 1\
            or not 0 <= mapped_x <= self.old_map.size_at_level() - 1:
            return 1

        if config.boundary_checks:
            try:
                assert 0 <= x
                assert self.round(x) < self.old_map.size_at_level()
                assert 0 <= y
                assert self.round(y) < self.old_map.size_at_level()
                assert 0 <= mapped_x
                assert self.round(mapped_x) < self.old_map.size_at_level()
                assert 0 <= mapped_y
                assert self.round(mapped_y) < self.old_map.size_at_level()
            except:
                raise Exception('invalid coordinates')


        # get properly mapped neighbours and their colour
        cdef np.ndarray[dtype=np.int_t, ndim=3] patch = self.get_patch(y=y, x=x)

        #get mapped coordinates patch
        cdef np.ndarray[dtype=np.int_t, ndim=3] mapped_patch = self.get_patch(y=mapped_y, x= mapped_x)

        # compute mask for pixels that are valid in both patches
        #The red channel is positive -> valid color
        valid = patch[:,:,0]>=0
        valid_mapped = mapped_patch[:,:,0]>=0

        # component-wise and
        valid = valid * valid_mapped

        #compute norm of differences
        norm_diff = np.linalg.norm(patch[:,:,0:2] - mapped_patch[:,:,0:2],ord=2, axis=2)

        # normalize by max color difference value
        norm_diff = norm_diff / 441.6729559300637 #  510.0 # 510.0 = np.linalg.norm(np.array([255,255,255,255])) 441.6729559300637 = np.linalg.norm(np.array([255,255,255]))

        # filter valid values: Where both patches have a color value
        cdef double[:] valid_norm_diff = norm_diff[valid]

        cdef double total_error = np.sum(valid_norm_diff)

        # incase of no overlap, return a very high error
        if valid_norm_diff.shape[0] == 0:
            return 1

        #normalize by number of valid pixels
        total_error = total_error / valid_norm_diff.shape[0]

        # normalize by relevant color error (avg or max, depending on implementation) This should avoid having very small errors in uniform images
        if config.regularize_with_relevant_color_distance:
            total_error = total_error / self.relevant_color_distance

        return total_error

    cpdef np.ndarray[dtype=np.int_t, ndim=3] get_patch(self,double y,double  x):
        # precondition
        if config.boundary_checks:
            assert 0 <= x < self.old_map.size_at_level()
            assert 0 <= y < self.old_map.size_at_level()

        # get neighbours
        cdef int size = self.old_map.size_at_level()
        cdef double[:,:,:] neighbours = self.color_neighbour_extractor.get_neighbours(y=y, x=x, map_size=self.old_map.size_at_level(), as_patch=True)

        # for each neighbour: get mapping and then color
        cdef np.ndarray[dtype=np.int_t, ndim=3] color = -np.ones((neighbours.shape[0],neighbours.shape[1],4), dtype=np.int)
        cdef double n_y, n_x, mapped_y, mapped_x, y_offset, x_offset
        for i_y in range(neighbours.shape[0]):
            for i_x in range(neighbours.shape[1]):

                (n_y, n_x) = neighbours[i_y, i_x]
                # only process valid neighbours
                if n_x >= 0 and n_y >= 0:
                    # compensate for discrete map lookup
                    y_offset = n_y - self.round(n_y)
                    x_offset = n_x - self.round(n_x)


                    # get mapping
                    mapped_y, mapped_x = self.old_map[self.round(n_y), self.round(n_x)]
                    # mapped_y = mapped_y + y_offset
                    # mapped_x = mapped_x + x_offset

                    # only consider value, if its not in hole
                    if not self.mask[self.round(mapped_y), self.round(mapped_x)] > 0:
                        #get mapped color
                        color[i_y, i_x] = color_lookup(background=self.background, y=mapped_y, x= mapped_x)
        # log(y, x, self.round(y), self.round(x), color)
        return color

    cpdef int round(self, double value):
        return int(round(value))

    # def get_colors_via_map(self, coordinates):
    #     """
    #     Get the color of one or more pixels via their mapping
    #     :param coordinates: Coordinates to lookup color for
    #     :return: np.array shape=(#coordinates, 4)
    #     """
    #     if coordinates.shape[0] == 0:
    #         return np.array([])
    #     indices = self.color_neighbour_extractor.indices_for_array_access(coordinates)
    #     mapped = self.old_map[indices]
    #     mapped_indices = self.color_neighbour_extractor.indices_for_array_access(mapped)
    #     colors = self.background[mapped_indices]
    #     return colors
    cpdef double spatial_error(self,int y, int x, double mapped_y, double mapped_x, use_only_hole_pixels_for_spatial=None, hole_pixel_list=None):
        cdef np.ndarray[dtype=np.double_t, ndim=1] target_coord, direction
        target_coord, direction = self.spatial_target(y=y, x=x, mapped_y=mapped_y, mapped_x=mapped_x, use_only_hole_pixels_for_spatial=use_only_hole_pixels_for_spatial, hole_pixel_list=hole_pixel_list)
        return np.linalg.norm(direction)

    cpdef spatial_target(self, int y, int x, double mapped_y, double mapped_x, use_only_hole_pixels_for_spatial=None, hole_pixel_list=None):
        if use_only_hole_pixels_for_spatial is None:
            use_only_hole_pixels_for_spatial = config.use_only_hole_pixels_for_spatial_gradient
        if hole_pixel_list is None:
            hole_pixel_list = self.hole_pixels()

        cdef np.ndarray[dtype=np.double_t, ndim=2] mapping
        cdef np.ndarray[dtype=np.double_t, ndim=2] neighbourhood
        if use_only_hole_pixels_for_spatial:
            neighbours = self.spatial_neighbour_extractor.get_neighbours(y=y, x=x, map_size=self.old_map.size_at_level())
            neighbourhood = self.spatial_neighbour_extractor.get_neighbourhood(y=y, x=x, map_size=self.old_map.size_at_level())

            # log(hole_pixel_list)
            valid_neighbour_indices = [(neighbours[i,0],neighbours[i,1]) in hole_pixel_list for i in range(neighbours.shape[0])]
            # log('valid_neighbour_indices {}'.format(valid_neighbour_indices))
            # if no valid neighbours are found, use all
            if np.any(valid_neighbour_indices):
                valid_neighbours = neighbours[valid_neighbour_indices]
                # log('f')
                # log(neighbourhood)
                # log(valid_neighbour_indices)
                # log('b')
                neighbourhood = neighbourhood[valid_neighbour_indices]

                indices = self.spatial_neighbour_extractor.indices_for_array_access(valid_neighbours)
                # log(indices)
                mapping = self.old_map[indices]
            else:
                mapping = self.spatial_neighbour_extractor.get_neighbourhood_mapping(y=y, x=x, map=self.old_map)
                neighbourhood = self.spatial_neighbour_extractor.get_neighbourhood(y=y, x=x, map_size=self.old_map.size_at_level())
        else:
            mapping = self.spatial_neighbour_extractor.get_neighbourhood_mapping(y=y, x=x, map=self.old_map)
            neighbourhood = self.spatial_neighbour_extractor.get_neighbourhood(y=y, x=x, map_size=self.old_map.size_at_level())


        # compute distance to pixel's last mapping
        cdef np.ndarray[dtype=np.double_t, ndim=2] old_mapping = np.repeat(np.array([[mapped_y, mapped_x]]), repeats=mapping.shape[0], axis=0)
        cdef np.ndarray[dtype=np.double_t, ndim=2] directions = mapping - old_mapping

        # clamp the directions
        cdef np.ndarray[dtype=np.double_t, ndim=2] clampped_directions = self.clamp_rows(directions)
        # log(old_mapping)
        # log(mapping)
        # log(directions)
        # log(clampped_directions)

        # compute norm of directsion
        directions_norm = np.linalg.norm(directions, axis=1)
        # log(directions_norm)

        # # if norm is 0 replace it with 1 and replace the resulting component_vector later
        zero_norms = directions_norm[:] == 0
        # log(zero_norms)
        directions_norm[zero_norms] = 1

        # log(directions_norm)



        # rescale directions by (norm -1)/norm to enforce a distance of exactly one to the new mapping
        # compute distance to patch center
        distance_to_center = np.linalg.norm(neighbourhood, axis=1)
        # log(neighbourhood)
        # log(distance_to_center)
        # log(directions_norm)
        component_vectors = (directions_norm - distance_to_center)[:, np.newaxis] * directions / directions_norm[:, np.newaxis]
        component_vectors[zero_norms] = [1,0]
        # log(component_vectors)
        cdef np.ndarray[dtype=np.double_t, ndim=1] direction = np.mean(component_vectors, axis=0, dtype=np.double)

        # cdef np.ndarray[dtype=np.double_t, ndim=1] direction = np.zeros((2), dtype=np.double)
        # cdef i = 0
        # for i in range(directions.shape[0]):
        #     component_vector = (np.linalg.norm(directions[i]) - 1) * directions[i] / np.linalg.norm(directions[i])
        #     direction += component_vector
        #     log(component_vector)
        # direction = direction / directions.shape[0]


        # some checks
        cdef np.ndarray[dtype=np.double_t, ndim=1] target_coord = self.old_map[y, x] + direction
        if not 0 <= target_coord[0] < self.mask.shape[0] or not 0 <= target_coord[1] < self.mask.shape[1]:
            log('target coordinate out of bounds')

        if not np.linalg.norm(direction) > 0.0:
            if config.debug_verbose:
                log('Norm of spatial direction is zero')
            return np.array([mapped_y, mapped_x], dtype=np.double), np.array([0, 0],dtype=np.double)

        return target_coord, direction

    cpdef np.ndarray[dtype=np.double_t, ndim=1] compute_spatial_direction(self, int y, int x):
        cdef double mapped_y, mapped_x
        cdef np.ndarray[dtype=np.double_t, ndim=1] target_coord, direction
        (mapped_y, mapped_x) = self.old_map[y,x]
        target_coord, direction = self.spatial_target(y=y, x=x, mapped_y=mapped_y, mapped_x=mapped_x)

        if config.collect_data:
            DataCollector.instance.collection.spatial_target[self.iteration_index,y,x] = target_coord

        # limit length
        direction = self.clamp_vector(direction)

        # move by pixel_step_size pixels
        direction = direction * self.spatial_pixel_step_size
        # log(np.linalg.norm(direction))
        return direction

    cpdef double in_hole_error(self, double mapped_y, double mapped_x):
        cdef np.ndarray[dtype=np.double_t, ndim=1] direction = self.distance_to_center_of_mass(mapped_y=mapped_y,mapped_x=mapped_x)
        cdef double dir_normal = np.linalg.norm(direction)
        return dir_normal

    cpdef double total_error(self,int y, int x, double mapped_y, double mapped_x, use_only_hole_pixels_for_spatial=None):
        cdef double color_error = self.compute_color_error(y=y, x=x, mapped_y=mapped_y, mapped_x=mapped_x)
        cdef double spatial_error = self.spatial_error(y=y, x=x, mapped_y=mapped_y, mapped_x=mapped_x, use_only_hole_pixels_for_spatial=use_only_hole_pixels_for_spatial)
        cdef double in_hole_error = self.in_hole_error(mapped_y=mapped_y, mapped_x=mapped_x)
        cdef double total_error = self.color_direction_weight * color_error + self.spatial_direction_weight * spatial_error + in_hole_error * config.hole_weight
        return total_error

    cpdef np.ndarray[dtype=np.double_t, ndim=1] distance_to_center_of_mass(self, double mapped_y, double mapped_x):
        mapped_y_int = self.round(mapped_y)
        mapped_x_int = self.round(mapped_x)

        #if old mapping is not in hole, dont do anything
        if self.mask[mapped_y_int,mapped_x_int] == 0:
            return np.array([0,0], dtype=np.double)

        #push mapping out of hole
        cdef np.ndarray[dtype=np.double_t, ndim=1] direction = np.array([mapped_y, mapped_x], dtype=np.double) - self.data_provider.center_of_mass_at_level(level=self.mipmap_level)
        return direction

    cpdef np.ndarray[dtype=np.double_t, ndim=1] compute_out_of_hole_direction(self,double mapped_y, double mapped_x):
        cdef np.ndarray[dtype=np.double_t, ndim=1] direction = self.distance_to_center_of_mass(mapped_y=mapped_y,mapped_x=mapped_x)
        cdef double dir_normal = np.linalg.norm(direction)

        if not dir_normal > 0.0001:
            if config.debug_verbose:
                log('Norm of out-of-hole direction is zero')
            return np.array([0.0, 0.0], dtype=np.double)

        # normalize direction by inverse normal
        direction = direction / dir_normal**1.5

        # scale by some scalar
        direction = direction * config.out_of_hole_step_size

        #limit length
        direction = self.clamp_vector(direction, threshold=config.out_of_hole_threshold)


        return direction

    # cpdef out_of_hole_random(self,y,x, mapped_y, mapped_x):
    #     mapped_y_int = self.round(mapped_y)
    #     mapped_x_int = self.round(mapped_x)
    #
    #     # if old mapping is not in hole, dont do anything
    #     if self.mask[mapped_y_int, mapped_x_int] == 0:
    #         return np.array([mapped_y, mapped_x])
    #
    #     # (new_y, new_x) = self.random_lookup(y=y, x=x, mapped_y=mapped_y, mapped_x=mapped_x)
    #     best_match = self.random_lookup(y=y, x=x, mapped_y=mapped_y, mapped_x=mapped_x)
    #     return best_match
    #
    # cpdef random_lookup(self,y,x, mapped_y, mapped_x):
    #     random_points = np.random.multivariate_normal(mean=(mapped_y, mapped_x), cov=np.identity(2) * config.random_radius * self.old_map.size_at_level(), size=(config.number_of_lookups))
    #     random_points[random_points < 0] = 0
    #     random_points[random_points > self.old_map.size_at_level()-1] = self.old_map.size_at_level()-1
    #     valid = self.mask[self.color_neighbour_extractor.indices_for_array_access(indices=random_points)] == 0
    #     random_points = random_points[valid]
    #     best_error = self.total_error(y=y, x=x, mapped_y=mapped_y, mapped_x=mapped_x)
    #     best_y = mapped_y
    #     best_x = mapped_x
    #     for rand_y,rand_x in random_points:
    #         try:
    #             error = self.total_error(y=y, x=x, mapped_y=rand_y, mapped_x=rand_x)
    #         except Exception:
    #             error = 9999999
    #         if error < best_error:
    #             best_y = rand_y
    #             best_x = rand_x
    #             best_error = error
    #     return np.array([best_y, best_x])

    cpdef np.ndarray[dtype=np.double_t, ndim=1] out_of_hole_based_on_neighbour_random(self, int y, int x, double mapped_y, double mapped_x):
        # return np.array([mapped_y, mapped_x], dtype=np.double)
        mapped_y_int = self.round(mapped_y)
        mapped_x_int = self.round(mapped_x)

         #if old mapping is not in hole, dont do anything
        if self.mask[mapped_y_int,mapped_x_int] == 0:
            return np.array([mapped_y, mapped_x], dtype=np.double)

        cdef np.ndarray[dtype=np.double_t, ndim=2] neighbours_mapped = self.spatial_neighbour_extractor.get_neighbourhood_mapping(y=y, x=x, map=self.old_map)

        #get a rendom neighbour
        cdef int random_neighbour_index = randint(0,neighbours_mapped.shape[0]-1)
        cdef np.ndarray[dtype=np.double_t, ndim=1] random_neighbour_mapping = neighbours_mapped[random_neighbour_index,:]

        #random direction of length 1
        cdef np.ndarray[dtype=np.double_t, ndim=1] random_direction = np.random.rand(2)
        random_direction = random_direction / np.linalg.norm(random_direction)
        # log(random_direction)

        cdef np.ndarray[dtype=np.double_t, ndim=1] mapping = random_neighbour_mapping + random_direction
        # log(mapping)
        return mapping

    cpdef np.ndarray[dtype=np.double_t, ndim=1] out_of_hole_based_on_neighbour(self, int y, int x, double mapped_y, double mapped_x):
        cdef np.ndarray[dtype=np.double_t, ndim=1] offset

        # return np.array([mapped_y, mapped_x], dtype=np.double)
        mapped_y_int = self.round(mapped_y)
        mapped_x_int = self.round(mapped_x)

         #if old mapping is not in hole, dont do anything
        if self.mask[mapped_y_int,mapped_x_int] == 0:
            return np.array([mapped_y, mapped_x], dtype=np.double)

        cdef np.ndarray[dtype=np.double_t, ndim=2] neighbours_mapped = self.spatial_neighbour_extractor.get_neighbourhood_mapping(y=y, x=x, map=self.old_map)

        cdef np.ndarray[dtype=np.double_t, ndim=2] normal_pixel_array = np.array(self.normal_pixels(), dtype=np.double)

        cdef np.ndarray[dtype=np.double_t, ndim=1] distance, best_distance
        cdef double best_y, best_x, distance_norm
        cdef double best_distance_norm = 0.0

        cdef double error, min_error
        min_error = 9999
        for n_y, n_x in neighbours_mapped:
            if config.out_of_hole_procedure == OutOfHoleProcedure.NEIGHBOUR_FURTHEST_FROM_OLD_MAPPING \
            or config.out_of_hole_procedure == OutOfHoleProcedure.NEIGHBOUR_CLOSEST_TO_TO_OLD_MAPPING:
                # compute distance to old mapping
                distance = np.array([n_y, n_x], dtype=np.double) - np.array([mapped_y, mapped_x], dtype=np.double)

            elif config.out_of_hole_procedure == OutOfHoleProcedure.NEIGHBOUR_FURTHEST_FROM_HOLE\
                or config.out_of_hole_procedure == OutOfHoleProcedure.NEIGHBOUR_SMALLEST_ERROR:
                # compute distance to center
                distance = np.array([n_y, n_x], dtype=np.double) - self.data_provider.center_of_mass_at_level(level=self.mipmap_level)

            distance_norm = np.linalg.norm(distance)
            offset = distance / distance_norm * 0.5

            if config.out_of_hole_procedure == OutOfHoleProcedure.NEIGHBOUR_SMALLEST_ERROR:
                error = self.total_error(y=y, x=x, mapped_y=n_y+offset[0], mapped_x=n_x+offset[1])
                # error = self.compute_color_error(y=y, x=x, mapped_y=n_y, mapped_x=n_x)

            if config.out_of_hole_procedure == OutOfHoleProcedure.NEIGHBOUR_FURTHEST_FROM_OLD_MAPPING \
                or config.out_of_hole_procedure == OutOfHoleProcedure.NEIGHBOUR_FURTHEST_FROM_HOLE:
                # get neighbour furthest away
                if distance_norm > best_distance_norm:
                    best_distance_norm = distance_norm
                    best_distance = distance
                    best_y = n_y
                    best_x = n_x
            elif config.out_of_hole_procedure == OutOfHoleProcedure.NEIGHBOUR_CLOSEST_TO_TO_OLD_MAPPING:
                # get neighbour closest
                if distance_norm < best_distance_norm:
                    best_distance_norm = distance_norm
                    best_distance = distance
                    best_y = n_y
                    best_x = n_x
            elif config.out_of_hole_procedure == OutOfHoleProcedure.NEIGHBOUR_SMALLEST_ERROR:
                # use the neighbour with smallest error
                if error < min_error:
                    min_error = error
                    best_y = n_y
                    best_x = n_x
                    best_distance_norm = distance_norm
                    best_distance = distance

        # if no mapping was found, return old value
        if best_distance_norm == 0:
            return np.array([mapped_y, mapped_x], dtype=np.double)

        # move a little further than found value
        offset = best_distance / best_distance_norm

        cdef np.ndarray[dtype=np.double_t, ndim=1] new_mapping = np.array([best_y, best_x], dtype=np.double) + offset

        # log('old mapping: {},{}'.format(mapped_y, mapped_x))
        # log('found mapping: {},{}'.format(best_y, best_x))
        # log('offset: {},{}'.format(offset[0], offset[1]))
        # log('new mapping: {},{}\n'.format(new_mapping[0], new_mapping[1]))
        # log('problem')
        return new_mapping


    cpdef np.ndarray[dtype=np.double_t, ndim=1] clamp_vector(self, np.ndarray[dtype=np.double_t, ndim=1] vector, threshold=None):
        if threshold is None:
            threshold = config.abs_threshold
        cdef double normal = np.linalg.norm(vector, ord=2)
        if normal == 0:
            return vector
        cdef double normalizer = min(1, threshold / normal)
        cdef np.ndarray[dtype=np.double_t, ndim=1] result_vector = vector * normalizer
        return result_vector


    cpdef clamp_rows(self, matrix):
        if matrix.shape[0] == 1:
            return np.array([self.clamp_vector(matrix[0], threshold=config.spatial_component_threshold)])

        normal = np.linalg.norm(matrix, axis=1)
        # avoid division by zero
        non_zero_indices = np.nonzero(normal)
        normalizer = np.zeros((matrix.shape[0]))

        # replace all elements that are larger than one. equivalent to min(1, scaled_threshold / normal)
        threshold = config.spatial_component_threshold
        normalizer[non_zero_indices] = threshold / normal[non_zero_indices]
        normalizer[normalizer > 1] = 1
        matrix[non_zero_indices,:] = matrix[non_zero_indices,:] * normalizer[non_zero_indices,np.newaxis]
        return matrix

    cpdef np.ndarray[dtype=np.double_t, ndim=1] exhaustive_search(self,double y,double x, double mapped_y, double mapped_x):
        cdef np.ndarray[dtype=np.double_t, ndim=2] np_pixels
        pixels = self.normal_pixels()
        np_pixels = np.array(pixels, dtype=np.double)
        cpdef int i, best_i;
        cpdef double error, best_error;
        best_error = 99999;

        cpdef int x_int,y_int
        y_int = self.round(y)
        x_int = self.round(x)
        # Do exhaustive search over all normal pixels and pick the one with smallest error
        for i in range(np_pixels.shape[0]):
            error = self.total_error(y=y_int, x=x_int, mapped_y=np_pixels[i,0], mapped_x=np_pixels[i,1])
            if error < best_error:
                best_i = i
                best_error = error
        # print(np_pixels[i,:])
        return np_pixels[best_i,:]

    cpdef update_map(self, color_direction, spatial_direction, int y, int x):
        cdef np.ndarray[dtype=np.double_t, ndim=1] direction, mixed_direction, new_direction, old_value, new_value,remapped_value

        old_value = self.old_map[y,x]
        if config.exhaustive_search:
            new_value = self.exhaustive_search(y=y,x=x, mapped_y=old_value[0], mapped_x=old_value[1])
        else:
            old_color_error = self.compute_color_error(y=y,x=x, mapped_y=old_value[0], mapped_x=old_value[1])
            old_spatial_error = self.spatial_error(y=y,x=x, mapped_y=old_value[0], mapped_x=old_value[1])
            old_total_error = self.total_error(y=y,x=x, mapped_y=old_value[0], mapped_x=old_value[1])

            mixed_direction = self.spatial_direction_weight*spatial_direction + self.color_direction_weight*color_direction
            direction = mixed_direction

            # If new value is not inside valid range, clamp it
            direction = self.clamp_if_outside(y=old_value[0], x=old_value[1], direction=direction)
            out_of_hole_before = not np.all(direction == mixed_direction)
            if config.out_of_hole_procedure == OutOfHoleProcedure.FORCE:
                # If the new value is inside the hole, push it outwards
                new_value = old_value + direction
                out_of_hole_direction = self.compute_out_of_hole_direction(mapped_y=new_value[0], mapped_x=new_value[1])
                mapped_into_hole = np.linalg.norm(out_of_hole_direction) > 0.0
                direction = direction + out_of_hole_direction

                # If new value is not inside valid range, clamp it
                new_direction = self.clamp_if_outside(y=old_value[0], x=old_value[1], direction=direction)

                direction = new_direction

                # limit length
                direction = self.clamp_vector(direction)

                # apply direction to get new mapping
                new_value = old_value + direction

            elif config.out_of_hole_procedure == OutOfHoleProcedure.NEIGHBOUR_FURTHEST_FROM_OLD_MAPPING \
                    or config.out_of_hole_procedure == OutOfHoleProcedure.NEIGHBOUR_FURTHEST_FROM_HOLE \
                    or config.out_of_hole_procedure == OutOfHoleProcedure.NEIGHBOUR_CLOSEST_TO_TO_OLD_MAPPING \
                    or config.out_of_hole_procedure == OutOfHoleProcedure.NEIGHBOUR_SMALLEST_ERROR \
                    or config.out_of_hole_procedure == OutOfHoleProcedure.NEIGHBOUR_RANDOM:
                # apply direction to get new mapping
                new_value = old_value + direction

                if config.out_of_hole_procedure == OutOfHoleProcedure.NEIGHBOUR_RANDOM:
                    remapped_value = self.out_of_hole_based_on_neighbour_random(y=y, x=x, mapped_y=new_value[0], mapped_x=new_value[1])
                else:
                    remapped_value = self.out_of_hole_based_on_neighbour(y=y, x=x, mapped_y=new_value[0], mapped_x=new_value[1])
                mapped_into_hole = not np.all(remapped_value == new_value)
                new_value = remapped_value
                new_value = self.remap_if_outside(mapped_y=new_value[0], mapped_x=new_value[1])


            # randomly try to find a better match, if its inside hole
            elif config.out_of_hole_procedure == OutOfHoleProcedure.RANDOM:
                new_value = old_value + direction
                new_value = self.out_of_hole_random(y=y, x=x, mapped_y=new_value[0], mapped_x=new_value[1])

                out_of_hole_direction = -np.ones((2))
                out_of_hole_after = False

        # by how much are we off from the ideal (under robust distance function) position
        new_color_error = self.compute_color_error(y=y, x=x, mapped_y=new_value[0], mapped_x=new_value[1])
        new_spatial_error = self.spatial_error(y=y, x=x, mapped_y=new_value[0], mapped_x=new_value[1])
        new_total_error = self.total_error(y=y, x=x, mapped_y=new_value[0], mapped_x=new_value[1])

        # if new_total_error > old_total_error * (1+config.error_tolernce):
        #     new_value = old_value
        #     new_total_error = old_total_error
        #     new_color_error = old_color_error
        #     new_spatial_error = old_spatial_error
        #
        #     if config.debug_verbose:
        #         log('no improvement, dont update')

        if config.collect_data:
            if not config.exhaustive_search:
                DataCollector.instance.collection.spatial_gradient[self.iteration_index,y,x] = spatial_direction
                DataCollector.instance.collection.color_gradient[self.iteration_index, y, x] = color_direction
                DataCollector.instance.collection.mixed_gradient[self.iteration_index, y, x] = mixed_direction
                # DataCollector.instance.collection.out_of_hole_vector[self.iteration_index, y, x] = out_of_hole_direction
                DataCollector.instance.collection.final_vector[self.iteration_index, y, x] = direction

                # DataCollector.instance.collection.out_of_bounds_before_hole[self.iteration_index, y, x] = out_of_hole_before
                DataCollector.instance.collection.out_of_bounds_after_hole[self.iteration_index, y, x] = out_of_hole_after
                DataCollector.instance.collection.inside_hole[self.iteration_index, y, x] = self.mask[y,x] > 0
                DataCollector.instance.collection.mapped_into_hole[self.iteration_index, y, x] = 1 if mapped_into_hole else 0

            DataCollector.instance.collection.old_mapping[self.iteration_index, y, x] = old_value
            DataCollector.instance.collection.new_mapping[self.iteration_index, y, x] = new_value

            DataCollector.instance.collection.total_error[self.iteration_index, y, x] = new_total_error
            DataCollector.instance.collection.color_error[self.iteration_index, y, x] = new_color_error
            DataCollector.instance.collection.spatial_error[self.iteration_index, y, x] = new_spatial_error
            DataCollector.instance.collection.out_of_hole_error[self.iteration_index, y, x] = self.in_hole_error(mapped_y=new_value[0], mapped_x=new_value[1])


        self.new_map[y,x] = new_value

    cpdef np.ndarray[dtype=np.double_t, ndim=1] clamp_if_outside(self,double y, double x, np.ndarray[dtype=np.double_t, ndim=1] direction):
        cdef np.ndarray[dtype=np.double_t, ndim=1] clamped_point = self.remap_if_outside(mapped_y=y+direction[0], mapped_x=x+direction[1])
        cdef np.ndarray[dtype=np.double_t, ndim=1] to_new_point = np.array([clamped_point[0]-y, clamped_point[1]-x], dtype=np.double)
        return to_new_point

    cpdef np.ndarray[dtype=np.double_t, ndim=1] remap_if_outside(self, double mapped_y, double mapped_x):
        size = self.new_map.size_at_level()

        # If new value is not inside valid range, clamp it
        if not np.all(np.array([0.0,0.0]) <= np.array([mapped_y, mapped_x])) or mapped_y > size-1 or mapped_x > size-1:
            if config.debug_verbose:
                log('Mapping out of bounds. clamping')

            new_y = min(max(mapped_y, 0), size-1.0)
            new_x = min(max(mapped_x, 0), size-1.0)

            return np.array([new_y, new_x], dtype=np.double)
        else:
            return np.array([mapped_y, mapped_x], dtype=np.double)

    cpdef re_initialize_map(self):
        self.data_provider.set_mipmap_level(level=self.mipmap_level)

        self.relevant_color_distance = self.compute_relevant_color_distance()

        # for (y,x) in self.hole_pixels():
            # normal_pixel_indices = self.normal_pixels()
            # random_index = random_func(low=0, high=normal_pixel_indices.shape[0])
            # random_mapping = normal_pixel_indices[random_index,:]

            # self.old_map[self.mipmap_level,y,x] = np.array([0,1])
            # log('init pixel '+str(x)+','+str(y))
        if config.collect_data:
            DataCollector.instance.collection.inside_hole[:,:,:] = np.ones((config.iterations+1, self.old_map.size_at_level(),self.old_map.size_at_level()))

        for (y,x) in self.normal_pixels():
            self.old_map[y,x] = np.array([y,x])
            self.new_map[y,x] = np.array([y,x])

            if config.collect_data:
                DataCollector.instance.collection.old_mapping[:,y,x] = np.array([y,x])
                DataCollector.instance.collection.new_mapping[:,y,x] = np.array([y,x])
                DataCollector.instance.collection.inside_hole[:,y,x] = 0


        return

    def initialize_map(self,iteration_completed=lambda map, not_mapped: None):
        """
        Initializes the map by exhaustively searching the best match for each unmapped pixel. The matching is based on the neighbours average color.
        Each iteration picks the pixels with enough neighbours and searches for the best match. This is done until all pixels are mapped
        :return:
        """
        self.relevant_color_distance = self.compute_relevant_color_distance()

        remove_immediately = False

        # all pixels in the not_mapped array are to be processed
        not_mapped = [[y,x] for (y,x) in self.hole_pixels()]
        mapped_pixels = []
        # do exhausive search for best mapping
        while len(not_mapped) > 0:
            map = self.old_map
            newly_mapped = []

            # for all pixels left to be mapped
            for (y,x) in not_mapped:
                new_mapping = self.search_mapping_by_color(y=y, x=x, map=map, not_mapped=not_mapped, mapped_pixels=mapped_pixels)

                # If a mapping was found, set it and remove the point from not_mapped
                if not np.all(new_mapping is None):

                    map[y,x] = new_mapping

                    if config.collect_data:
                        DataCollector.instance.collection.old_mapping[0,y,x] = new_mapping
                        DataCollector.instance.collection.new_mapping[0,y,x] = new_mapping
                        DataCollector.instance.collection.inside_hole[:,y,x] = 1
                        log('mapped {},{}'.format(y,x))

                    if remove_immediately:
                        not_mapped.remove([y, x])
                        mapped_pixels.append((y,x))
                    else:
                        newly_mapped.append([y,x])
                        mapped_pixels.append((y,x))


            # remove all points that were mapped during the last iteration
            if not remove_immediately:
                not_mapped = [[y,x] for [y,x] in not_mapped if not [y,x] in newly_mapped]
                # mapped_pixels = mapped_pixels + newly_mapped
                # log('newlymapped {}'.format(newly_mapped))
                # log(mapped_pixels)

            # Complete iteration. Used mostly for testing
            iteration_completed(map=map, not_mapped=not_mapped)

        self.best_map = deepcopy(self.old_map)

        if config.collect_data:
            for (y,x) in self.normal_pixels():
                DataCollector.instance.collection.old_mapping[:,y,x] = np.array([y,x])
                DataCollector.instance.collection.new_mapping[:,y,x] = np.array([y,x])


    cpdef search_mapping_by_color(self, y, x, map, not_mapped, mapped_pixels):
        """
        Search all mapped pixels for the best mapping based on the neighbours average color
        :return: [y,x] the best match, or None, if not enough neighbours are found
        """

        cdef np.ndarray[dtype=np.double_t, ndim=1] best_match
        cdef np.ndarray[dtype=np.uint8_t, ndim=2] colors
        cdef np.ndarray[dtype=np.double_t, ndim=1] average_color

        # get neighbours that are mapped already <-> not in not_mapped list
        neighbours = self.spatial_neighbour_extractor.get_neighbours(y=y, x=x, map_size=map.size_at_level())
        valid_neighbours = [[y,x] for [y,x] in neighbours if not [y,x] in not_mapped]

        # there are at least a given number of already mapped neighbours
        if len(valid_neighbours) >= config.number_of_mapped_neighbours_required:

            # Get neighbours average color
            indices = list(np.transpose(valid_neighbours))
            neighbours_mapping = map[indices]
            mapped_indices = self.color_neighbour_extractor.indices_for_array_access(neighbours_mapping)
            colors = self.background[mapped_indices]
            average_color = np.mean(colors, axis=0)

            # Get best matching pixel for average color
            best_match = self.find_best_match(y=y, x=x, color=average_color, neighbours_mapping=neighbours_mapping, mapped_pixels = mapped_pixels)

            return best_match
        else:
            return None

    cpdef find_best_match(self, y,x, color, neighbours_mapping, mapped_pixels):
        """
        Find the best matching pixel in the background for given color
        :param color: np.array(RGBA)
        :return: coordinates of best match
        """
        cdef np.ndarray[dtype=np.double_t, ndim=1] min_diff_coord
        cdef np.ndarray[dtype=np.double_t, ndim=2] np_pixels
        pixels = self.normal_pixels()
        np_pixels = np.array(pixels, dtype=np.double)
        errors = []

        # Compute color distance
        indices = list(np.transpose(pixels))
        pixel_colors = self.background[indices]
        colors = np.repeat([color], len(pixels),0)
        diff = pixel_colors - colors# pixel_colors - colors# pixel_colors[:,:2] - colors[:,:2]
        color_error = np.linalg.norm( diff[:,:2], ord=2, axis=1)/441.6729559300637

        # regularize by relevant color distance
        if config.regularize_with_relevant_color_distance:
            color_error = color_error / self.relevant_color_distance

        # log(color_error)
        for i in range(len(pixels)):
            ny = pixels[i,0]
            nx = pixels[i,1]
            # log(y,x,ny,nx)
            # log(pixels)
            if config.init_with_spatial:
                spatial_error = self.spatial_error(y=y, x=x, mapped_y=ny, mapped_x=nx, use_only_hole_pixels_for_spatial=config.use_only_hole_pixels_for_spatial_init, hole_pixel_list=mapped_pixels)
                # log('spatial {}'.format(self.spatial_direction_weight * spatial_error))
                # log('color {}\n'.format(self.color_direction_weight * color_error[i]))
                error = self.color_direction_weight * color_error[i] + self.spatial_direction_weight * spatial_error
            else:
                error = color_error
            # color_error = self.compute_color_error(y=y, x=x, mapped_y=ny, mapped_x=nx)
            # spatial_error = self.spatial_error(y=y, x=x, mapped_y=ny, mapped_x=nx)
            errors.append(error)

        min_diff_index = np.argmin(errors)
        # log(errors)
        # log(min_diff_index)
        min_diff_coord = np_pixels[min_diff_index,:]
        return min_diff_coord


        # indices = list(np.transpose(pixels))
        # pixel_colors = self.background[indices]
        #
        # colors = np.repeat([color], len(pixels),0)
        # diff = pixel_colors - colors# pixel_colors - colors# pixel_colors[:,:2] - colors[:,:2]
        # diff_norm = np.linalg.norm( diff, ord=2, axis=1)/510.

        # if not config.init_with_spatial:
        #     min_diff_index = np.argmin(diff_norm)
        #     min_diff_coord = np_pixels[min_diff_index,:]
        #     return min_diff_coord
        # else:
        #     norm_neighbours_mapping = self.normalize_rows(matrix=np.array(neighbours_mapping))
        #     avg_mapping = np.mean(norm_neighbours_mapping, axis=0)
        #     avg_mappings = np.repeat(np.array([avg_mapping]), repeats=len(pixel_colors), axis=0)
        #     mapping_distance = np.linalg.norm(avg_mappings - pixels, axis=1)
        #     mapping_distance[mapping_distance>config.abs_threshold] = config.abs_threshold
        #     total_errors = self.color_direction_weight*diff_norm + self.spatial_direction_weight * mapping_distance
        #
        #     min_diff_index = np.argmin(total_errors)
        #     log(total_errors)
        #     log(min_diff_index)
        #     min_diff_coord = np_pixels[min_diff_index,:]
        #     return min_diff_coord

    cpdef double compute_relevant_color_distance(self):
        size = self.background.shape[0]
        pixels = self.background.reshape((size*size, 4))[:,:3]
        pixel_sum = np.linalg.norm(pixels, axis=1)
        sorted_indices = np.argsort(pixel_sum, axis=0)
        # max_index = sorted_indices[int(size*0.1)]
        # min_index = sorted_indices[int(size*0.9)]
        # pixel_diff = pixels[max_index,:]-pixels[min_index,:]
        pixel_diff = np.std(pixels, axis=0)
        cdef double diff_norm = np.linalg.norm(pixel_diff) / 441.6729559300637 * 2
        # log(diff_norm)
        return diff_norm

    cpdef create_new_frame(self):
        """
        Creates a new image for displaying based on map with index map_index
        :param map_index: Specifies which map matrix to use to resolve the image. default is the last computed map
        :return: np.array image of new frame
        """

        width = self.background.shape[1]
        height = self.background.shape[0]
        frame = np.zeros(self.background.shape, dtype=np.uint8)

        # for each pixel: lookup the mapping index from the map matrix and then lookup the color value from the background image at this index
        for y in range(height):
            for x in range(width):
                index = self.old_map[y,x]
               # if map_x < 0 or map_y < 0 or

                frame[y,x,:] = color_lookup(background=self.background, y=index[0], x=index[1]) if config.interpolate_final_images else self.background[self.round(index[0]), self.round(index[1])]

        return frame

    cpdef hole_pixels(self):
        non_zero_indices = np.nonzero(self.mask)
        pixels = [(non_zero_indices[0][i],non_zero_indices[1][i]) for i in range(non_zero_indices[0].shape[0])]
        return pixels

    cpdef normal_pixels(self):
        (zero_y, zero_x) = np.where(self.mask == 0)
        zero_indices = np.transpose(np.vstack((zero_y, zero_x,)))
        return zero_indices

    cpdef should_start(self):
        start = len(self.hole_pixels()) > config.min_hole_pixels
        start = start and len(self.normal_pixels()) > config.min_normal_pixels
        return start

    cpdef done(self):
        min_iterations = self.iteration_index > self.min_iteration
        max_iterations_reached = self.iteration_index == config.iterations
        error_increasing = self.error_not_smaller_for_iterations >= config.max_increasing_error_iterations
        normal_done = (max_iterations_reached or error_increasing) and min_iterations
        exhaustive_done = self.iteration_index == config.exhaustive_iterations
        return normal_done if not config.exhaustive_search else exhaustive_done

    cpdef iteration_completed(self):

        # check convergence criteria
        if self.iteration_index >= self.min_iteration:
            total_errors = DataCollector.instance.collection.total_error[self.iteration_index,:,:]
            total_error = np.sum(total_errors)

            if total_error < self.smallest_error:
                self.best_map = deepcopy(self.new_map)
                self.smallest_error = total_error
                self.error_not_smaller_for_iterations = 0
                self.best_map_iteration = self.iteration_index
            else:
                self.error_not_smaller_for_iterations += 1

        self.iteration_index = self.iteration_index + 1

    cpdef finalize(self):
        self.data_provider.make_maps_consistent(best_map = self.best_map)

    cpdef save_mapping(self):
        with open(config.out_dir_name+'/mapping_lvl'+str(self.mipmap_level)+'.pkl', 'wb') as outfile:
            pickle.dump(self.new_map, outfile, pickle.HIGHEST_PROTOCOL)

cdef np.ndarray[dtype=np.uint8_t, ndim=1] color_lookup( background, double y, double x):
    cdef int size = background.shape[0]


    cdef int y_low = np.floor(y)
    cdef bint y_overflow = y_low >= size - 1

    cdef int x_low = np.floor(x)
    cdef bint x_overflow = x_low >= size - 1

    cdef int y_high = y_low if y_overflow else y_low + 1

    cdef int x_high = x_low if x_overflow else x_low + 1

    cdef double y_low_weight = 1.0 - (y - y_low)
    cdef double y_high_weight = 0.0 if y_overflow else 1.0 - (y_high - y)
    cdef double x_low_weight = 1.0 - (x - x_low)
    cdef double x_high_weight = 0.0 if x_overflow else 1.0 - (x_high - x)

    # log(y_low, y_high, x_low, x_high)

    # cdef np.ndarray[dtype=np.double_t, ndim=1] top_color =
    cdef np.ndarray[dtype=np.double_t, ndim=1] top_color = x_low_weight * background[y_low,x_low,:] + x_high_weight * background[y_low, x_high,:] # left top + right top
    cdef np.ndarray[dtype=np.double_t, ndim=1] bottom_color = x_low_weight * background[y_high, x_low,:] + x_high_weight * background[y_high, x_high,:] # bottom left + bottom right
    cdef np.ndarray[dtype=np.double_t, ndim=1] color = y_low_weight * top_color + y_high_weight * bottom_color
    # log(color, top_color)
    cdef np.ndarray[dtype=np.uint8_t, ndim=1] int_color = np.round(color).astype(dtype=np.uint8)

    return int_color

# if __name__ == '__main__':
    # # Sample code for testing and demo
    # import matplotlib.pyplot as plt
    # from arinpainter.ImageImporter import ImageImporter
    # from arinpainter.AlgoMaster import AlgoMaster
    # from arinpainter.DataProvider import DataProvider
    #
    # # load frame
    # importer = ImageImporter()
    # importer.read_filenames()
    # (background, mask) = importer.read_frame(index=1)
    #
    # #make background pixels inside the hole red for better visibility
    # algoMaster = AlgoMaster()
    # background = algoMaster.cut_hole_into_background(background=background, mask=mask)
    #
    # data_provider = DataProvider()
    # level = data_provider.add_level(mask=mask, background=background)
    # data_provider.create_maps(size=mask.shape[0])
    # inpainter = Inpainter(data_provider=data_provider,mipmap_level=level)
    #
    # # plt.imshow(inpainter.mask, cmap='gray')
    # plt.imshow(inpainter.background)
    # plt.imshow(inpainter.maps[0], cmap='gray')

    # inpainter.inpaint_frame()  # inpaint one frame

    # frame = inpainter.create_new_frame()

    # plt.imshow(frame)
    # plt.show()

