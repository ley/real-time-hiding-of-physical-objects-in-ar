from arinpainter.AbstractNeighbourExtractor import AbstractNeighbourExtractor
import numpy as np
cimport numpy as np

class FourNeighbourExtractor(AbstractNeighbourExtractor):

    def get_neighbours_and_direction(self, double y, double x, int map_size, bint as_patch=False):
        cdef np.ndarray[dtype=np.double_t, ndim=3] neighbour = -np.ones((3, 3, 2), dtype=np.double)
        cdef np.ndarray[dtype=np.double_t, ndim=3] direction = np.zeros((3, 3, 2), dtype=np.double)
        if x >= 1:
            neighbour[1, 0] = [y, x - 1]
            direction[1, 0] = [0, -1]
        if x < map_size - 1.5:
            neighbour[1, 2] = [y, x + 1]
            direction[1, 2] = [0, 1]
        if y >= 1:
            neighbour[0, 1] = [y - 1, x]
            direction[0, 1] = [-1, 0]
        if y < map_size - 1.5:
            neighbour[2, 1] = [y + 1, x]
            direction[2, 1] = [1, 0]

        if as_patch:
            return neighbour, direction
        else:
            # If indices are non negative, it's a valid neighbour
            valid_neighbours_indices = np.all(neighbour >= [0, 0], axis=2)
            return neighbour[valid_neighbours_indices], direction[valid_neighbours_indices]

    # def neighbour_count(self, y,x, map):
    #     count = 0
    #     count = count + 1 if x > 0 else 0
    #     count = count + 1 if x < map.size_at_level()- 1 else 0
    #     count = count + 1 if y > 0 else 0
    #     count = count + 1 if y < map.size_at_level() - 1 else 0
    #
    #     return count

    def get_neighbours(self, double y, double x, int map_size, bint as_patch=False):
        neighbours, _ = self.get_neighbours_and_direction(y=y, x=x, map_size=map_size, as_patch=as_patch)
        return neighbours

    def get_neighbourhood(self, y, x, map_size):
        _, directions = self.get_neighbours_and_direction(y=y, x=x, map_size=map_size, as_patch=False)
        return directions