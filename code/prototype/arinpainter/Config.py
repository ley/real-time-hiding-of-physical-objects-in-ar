from enum import Enum
from datetime import datetime
import os, sys, pickle

class OutOfHoleProcedure(Enum):
    FORCE = 1
    NEIGHBOUR_FURTHEST_FROM_HOLE = 2
    NEIGHBOUR_FURTHEST_FROM_OLD_MAPPING = 3
    NEIGHBOUR_CLOSEST_TO_TO_OLD_MAPPING = 4
    NEIGHBOUR_SMALLEST_ERROR = 5
    RANDOM = 6
    NEIGHBOUR_RANDOM = 7

class config:
    # verbose
    debug_verbose = False
    master_verbose = True

    # checks
    boundary_checks = True

    #collect data
    collect_data = True # MUST BE true by now

    # show frames
    show_images = True
    show_intermediate_frames = show_images and False

    show_mapping = False

    save_anything = True
    save_plots = save_anything and True
    save_mapping = save_anything and True
    save_config = save_anything and True
    log_to_file = save_anything and True

    # inpainting parameters

    iterations = 30
    exhaustive_iterations = 50
    min_iterations = [15,10,7,5]
    max_increasing_error_iterations = 3
    stop_at_resolution = 1024


    interpolate_final_images = False

    init_with_spatial = True

    use_only_hole_pixels_for_spatial_gradient = True
    use_only_hole_pixels_for_spatial_init = True

    regularize_with_relevant_color_distance = True

    # If a certain number of pixels is not updated in an iteration, it is considered converged
    no_update_threshold = 0.8

    min_hole_pixels = 1
    min_normal_pixels = 3
    color_direction_weights = [ 0.8]# , 0.82, 0.84, 0.86, 0.88, 0.9, 0.92, 0.94, 0.96] #


    spatial_direction_weights = lambda i,l :1 - config.color_direction_weight(i,l)
    hole_weight = 0

    # gradient step size
    spatial_pixel_step_sizes = [0.25]
    color_pixel_step_sizes = [1]
    out_of_hole_step_size = 1

    abs_threshold = 4
    spatial_component_threshold = 4

    exhaustive_search = False
    # Out of hole procedure
    out_of_hole_procedure = OutOfHoleProcedure.NEIGHBOUR_RANDOM

    # FORCE- style
    out_of_hole_threshold = 1

    # Random lookup
    number_of_lookups = 100
    # The 'radius' of the gaussian distribution relative to the image size
    random_radius = 0.5

    # initialization
    number_of_mapped_neighbours_required = 3

    # meta-data import
    center_of_mass_label = "center_of_mass";

    # file location
    # Path of directory containing images relative to module location
    imagePath = '../../../benchmark_imgs/desk_ar'
    # imagePath = '../../../benchmark_imgs'
    # office_desk
    # home_desk
    # window
    initial_frame_index = 2

    save_plot_dir = '../../../plots/'

    imageFormatPostfix = 'png'
    backgroundImageName = 'background'
    maskImageName = 'mask'
    metadata_file_name = 'additional_data.csv'

    logfile_name = 'log.txt'

    @classmethod
    def color_direction_weight(cls, iteration, mipmap_iteration):
        i = min(len(config.color_direction_weights)-1, mipmap_iteration)
        return config.color_direction_weights[i]

    @classmethod
    def spatial_direction_weight(cls, iteration, mipmap_iteration):
        return config.spatial_direction_weights(iteration, mipmap_iteration)

    @classmethod
    def color_pixel_step_size(cls, iteration, mipmap_iteration):
        i = min(len(config.color_pixel_step_sizes) - 1, mipmap_iteration)
        return config.color_pixel_step_sizes[i]

    @classmethod
    def spatial_pixel_step_size(cls, iteration, mipmap_iteration):
        i = min(len(config.spatial_pixel_step_sizes) - 1, mipmap_iteration)
        return config.spatial_pixel_step_sizes[i]

    @classmethod
    def min_iteration(cls, mipmap_iteration):
        i = min(len(config.min_iterations) - 1, mipmap_iteration)
        return config.min_iterations[i]

    @classmethod
    def set_out_dir_name(self):
        if config.save_plots or config.save_mapping or config.log_to_file or config.save_config:
            self.start_date_time = datetime.now()
            out_hole = 'EXHAUSTIVE' if config.exhaustive_search else str(self.out_of_hole_procedure).split('.')[-1]
            self.out_dir_name = '{base}{scene_name}_frame{frame_index}_date{date}_{out_of_hole}'.format(
                base=self.save_plot_dir,
                scene_name=self.imagePath.split('/')[-1],
                frame_index=self.initial_frame_index,
                date=self.start_date_time.strftime('%d%m%y_%H%M%S'),
                out_of_hole=out_hole
            )
            if not os.path.exists(self.out_dir_name):
                os.mkdir(self.out_dir_name)

    @classmethod
    def save_config_file(cls):
        if config.save_config:
            with open(config.out_dir_name + '/config.pkl', 'wb') as outfile:
                pickle.dump(config, outfile, pickle.HIGHEST_PROTOCOL)

    @classmethod
    def string_represent(cls):
        string_rep = '----------CONFIG----------\n'
        di = vars(config)
        for (key, value) in di.items():
            string_rep = string_rep + key +': '+str(value)+'\n'
        string_rep = string_rep+'-----------------------------\n'
        return string_rep

    @classmethod
    def set_test(self):
        config.iterations = 2
        config.min_iterations = [1]
        config.stop_at_resolution = 16
        config.save_plots = False
        config.save_config = False
        config.save_mapping = False
        config.log_to_file = False

#TODO: fix logging

if __name__ == '__main__':
    print(config.color_direction_weight(0))
    OutOfHoleProcedure.FORCE
    #
    #     # @classmethod
    #     # def spatial_direction_weight(i):
    #     #     i = min(len(config.spatial_direction_weights)-1, i)
    #     #     return  config.spatial_direction_weights[i]