import glob
import imageio
import csv
import numpy as np
from arinpainter.Config import config
from arinpainter.ARLogging import log

class ImageImporter:
    """
    Read frames (background and mask image) from the file system
    """

    def __init__(self):
        self.frame_index = 0  # current frame index. It's used to load the corresponding background and mask image

        self.mask_filenames = []
        self.background_filenames = []


    def read_frame(self, index = None):
        """
        Read a single frame
        :param index: Read frame with given index, or if none is specified self.frame_index is used. (0 by default)
        :return:
        """
        if index == None:
            index = self.frame_index

        # Make sure the filenames have been read
        if len(self.mask_filenames) == 0 or len(self.background_filenames) == 0:
            log('Mask or background filenames not read yet?')

        # make sure the index is valic
        if not 0 <= index < len(self.background_filenames):
            log('Invalid frame index '+str(index))

        mask = self.read_mask(index=index)
        background = self.read_background(index=index)

        return (background,mask)


    def read_filenames(self):
        """
        Gets all filenames as sorted list from the imagePath directory. And sets them to the corresponding class properties.
        :return:
        """
        mask_path_regex = config.imagePath + '/' + config.maskImageName +'*.png'
        background_path_regex = config.imagePath + '/' + config.backgroundImageName +'*.png'
        mask_files = glob.glob(mask_path_regex)
        background_files = glob.glob(background_path_regex)
        mask_files.sort()
        background_files.sort()

        if len(mask_files) == 0 or len(background_files) == 0:
            log('Mask or background files could not be found in '+config.imagePath)
        else:
            self.mask_filenames = mask_files
            self.background_filenames = background_files

    def read_mask(self, index):
        file_path = self.mask_filenames[index]
        mask = self.read_image(file_path)
        mask_red = mask[:,:,0]
        return mask_red

    def read_background(self, index):
        file_path = self.background_filenames[index]
        image = self.read_image(file_path)
        # image = image.astype(dtype=np.int8)
        return image

    def read_image(self, image_name):
        image = imageio.imread(uri=image_name)
        return image

    def read_meta_data(self):
        name =  config.imagePath + '/' + config.metadata_file_name
        with open(file=name, mode='r') as file:
            metadata_reader = csv.reader(file, delimiter=';')
            self.metadata = {}
            for line in metadata_reader:
                if len(line)>0:
                    timestamp = line[0]
                    if not timestamp in self.metadata.keys():
                        self.metadata[timestamp] = []
                    self.metadata[timestamp].append(line)


    def get_metadata(self, index):
        values = list(self.metadata.values())
        values.sort()
        return values[index]



if __name__ == '__main__':
    # Sample code for testing and demo
    import matplotlib.pyplot as plt

    #load image
    importer = ImageImporter() #Create ImageImporter
    importer.read_filenames() #Load the filenames from the filesystem, order them and check whether they are valid
    (background, mask) = importer.read_frame() #read one single frame. Since nothing is specified, its the first frame. use importer.frame_index = 10 to change the current frame or specify an index when calling read_frame()

    importer.read_meta_data()
    meta = importer.get_metadata(0)

    #show image
    plt.imshow(background)
    plt.show()
