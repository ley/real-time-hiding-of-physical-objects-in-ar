import numpy as np
from math import log2
from arinpainter.Config import config
from arinpainter.ARLogging import log

class Map():
    def __init__(self, side):
        if side == 0 or log2(side) % 1 > 0:
            raise Exception('Only power of two size allowed')
        self.maps = []
        self.create_maps(side=side)
        self.size = side
        #Set the lookup level to the lowest resolution
        self.lookup_level = self.levels - 1

    def create_maps(self, side):
        size = side
        while size > 1:
            level_map = self.create_level_map(size)
            self.maps.append(level_map)
            size = int(size / 2)

    def create_level_map(self, side):
        #indices_mat = np.indices((width, height)).transpose((1, 2, 0))  # create matrix containing indices: indices_mat[i,i] = [i,j] forall i,j
        repeats = side // 2
        template = np.indices((2,2), dtype=np.double).transpose((1, 2, 0))
        level_map = np.tile(template, (repeats,repeats,1))
        return level_map

    @property
    def levels(self):
        return len(self.maps)

    def total_lookup(self, y, x, lookup_level):
        total_mapping = np.zeros((2), dtype=np.float)
        for level in range(lookup_level,self.levels):
            level_diff = level - lookup_level
            mapping = self[level, y, x]
            total_mapping = total_mapping + mapping * (2**level_diff)
            x = x//2
            y = y//2

        if config.boundary_checks:
            try:
                self.check_bounds(y=y, x=x, level=self.lookup_level)
            except:
                log('Tried to get invalid mapping y,x={:.2f},{:.2f}'.format(total_mapping[0], total_mapping[1]))


        return total_mapping

    def set_total_mapping(self,y,x,level, new_value):
        #if at smallest resolution, just set the value
        if level == self.levels-1:
            self.maps[self.levels-1][y,x] = new_value
        else:
            #set the difference between the total mapping of the higher level and the new value
            # look up total at lower level (using lover level coordinates, thats why x,y//2) but consider the value at this level, so the result * 2 (transforming it one level down
            higher_total = self.total_lookup(y=y//2,x=x//2, lookup_level=level+1) * 2
            diff = new_value - higher_total
            self.maps[level][y,x] = diff

        if config.boundary_checks:
            total_mapping = self.total_lookup(y,x, lookup_level=level)
            if np.linalg.norm(total_mapping - new_value, ord=1) > 0.001:
                log('Total value was not set properly')
                try:
                    self.check_bounds(y=total_mapping[0], x=total_mapping[1], level=self.lookup_level)
                except:
                    log('Total value is outside range')
    def __getitem__(self, item):
        """
        selective or total lookup of map.
        The map is hierarchical. A lower level only contains the difference between the total mapping and the next larger level. So the total mapping is a (scaled) sum of all maps of higher level.
        The indexed lookup using three coordinates (level,y,x) will return the map's value at map-level 'level' only. So only this levels contribution.
        The indexed lookup using two components(y,x) will return the total mapping from self.lookup_level to the maximum level. This is what's typically used.
        :param item: either (level,y,x) for partial lookup, or (y,x) for total lookup
        :return:
        """
        if len(item) == 2:
            y,x = item
            # slicing
            if isinstance(item, list):
                return np.array([self.__getitem__((y_i, x_i)) for y_i, x_i in zip(y, x)], dtype=np.double)
            else:
                # check for invalid indices when debugging
                if config.boundary_checks:
                    try:
                        self.check_bounds(y=y, x=x, level=self.lookup_level)
                    except:
                        log('Invalid index when accessing map: y,x: {},{}'.format(y,x))

                value = self.total_lookup(y=y, x=x, lookup_level=self.lookup_level)

                # check for invalid stored values
                if config.boundary_checks:
                    try:
                        self.check_bounds(y=value[0], x=value[1], level=self.lookup_level)
                    except:
                        log('Invalid value retrieved from map y,x={:.2f},{:.2f}'.format(value[0],value[1]))
                return value

        elif len(item) == 3:
            (level,y,x) = item
            return self.maps[level][int(round(y)), int(round(x))]
        else:
            raise Exception('Invalid number of elements in item')

    def __setitem__(self, key, value):
        """
        Works the same way as __getitem__
        :param key:
        :param value:
        :return:
        """

        if len(key) == 2:
            y,x = key
            if config.boundary_checks:
                assert int(y) - y == 0
                assert int(x) - x == 0
                try:
                    self.check_bounds(value[0],value[1], self.lookup_level)
                except:
                    log('Tried to set invalid mapping y,x={:.2f},{:.2f}'.format(value[0],value[1]))
            self.set_total_mapping(y=y,x=x,level=self.lookup_level,new_value=value)
        elif len(key) == 3:
            (level, y, x) = key
            if config.boundary_checks:
                assert int(y) - y == 0
                assert int(x) - x == 0
            self.maps[level][y,x] = value
        else:
            raise Exception('Invalid number of elements in item')

    def __eq__(self, other):
        is_equal = self.levels == other.levels
        for level in range(self.levels):
            is_equal = is_equal and np.all(self.maps[level] == other.maps[level])
        return is_equal

    def size_at_level(self, level=None):
        if level == None:
            level = self.lookup_level
        return self.size // 2**level

    def check_bounds(self,y,x, level):
        size = self.size_at_level()
        level_size = self.size_at_level(level)
        assert size == level_size
        assert size == self.maps[level].shape[0]
        assert size == self.maps[level].shape[1]
        assert y >= 0
        assert x >= 0
        assert y <= size - 1
        assert x <= size - 1