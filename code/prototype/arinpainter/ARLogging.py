from arinpainter.Config import config
from datetime import timedelta

def log(text):
    print(text)
    if config.log_to_file:
        with open(config.out_dir_name+'/'+config.logfile_name, 'a') as logfile:
            logfile.write(text+'\n')

def time_ellapse_formated(start_time, end_time):
    return str(timedelta(seconds=end_time - start_time))