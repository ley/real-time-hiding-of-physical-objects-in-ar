
import pyximport;
import numpy as np
include = np.get_include()
pyximport.install(setup_args={'include_dirs': include})
from arinpainter.AlgoMaster import AlgoMaster

from arinpainter.Config import config

if __name__ == '__main__':
    # synth
    config.initial_frame_index = 5
    config.save_plots = False

    config.imagePath = '../../../../benchmark_imgs/synth'
    config.save_plot_dir = '../../../../plots/examples/{}_'.format(config.initial_frame_index)

    master = AlgoMaster()
    master.init_data()
    master.run()