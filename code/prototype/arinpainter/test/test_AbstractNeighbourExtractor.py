
import pyximport;
import numpy as np
include = np.get_include()
pyximport.install(setup_args={'include_dirs': include})

from unittest import TestCase
import numpy as np
from arinpainter.AbstractNeighbourExtractor import AbstractNeighbourExtractor
from arinpainter.Map import Map

class TestAbstractNeighbourExtractor(TestCase):

# #get_neighbourhood_colors
#     def test_get_neighbourhood_color(self):
#         true_colors = np.array([[255,0,255,255],[0, 255, 0, 255], [0, 255, 0, 255],[0, 0, 255, 255]])
#         colors = self.extractor.get_neighbourhood_color(y=1,x=1, map=self.map, background=self.background)
#         self.assertTrue(np.all(true_colors == colors))
#
#     def test_get_neighbourhood_color_mapped(self):
#         map = np.array([
#             [[0, 0], [0, 2], [2, 2]],
#             [[2, 2], [1, 1], [2, 2]],
#             [[2, 0], [0, 2], [2, 2]]
#         ])
#         self.map.maps = [map]
#         true_colors = np.array([[255,0,255,255], [0, 0, 0, 255], [0, 0, 0, 255],  [255,0,255,255]])
#         colors = self.extractor.get_neighbourhood_color(y=1, x=1, map=self.map, background=self.background)
#         self.assertTrue(np.all(true_colors == colors))

# #compute_4_neighbourhood_average_color
#     def test_get_neighbourhood_average_color(self):
#         true_avg_color = np.array([63.75,127.5,127.5,255])
#         avg_color = self.extractor.get_neighbourhood_average_color(y=1,x=1, map=self.map, background=self.background)
#         self.assertTrue(np.all(avg_color == true_avg_color))
#
#     def test_get_neighbourhood_average_color_mapped(self):
#         map = np.array([
#             [[0, 0], [0, 2], [2, 2]],
#             [[2, 2], [1, 1], [2, 2]],
#             [[2, 0], [0, 2], [2, 2]]
#         ])
#         self.map.maps = [map]
#         true_avg_color = np.array([127.5,0,127.5,255])
#         avg_color = self.extractor.get_neighbourhood_average_color(y=1, x=1, map=self.map, background=self.background)
#         self.assertTrue(np.all(avg_color == true_avg_color))
#
#     def test_get_neighbourhood_average_color_corner(self):
#         true_avg_color = np.array([127.5,127.5,127.5,255])
#         avg_color = self.extractor.get_neighbourhood_average_color(y=0, x=0, map=self.map, background=self.background)
#         self.assertTrue(np.all(avg_color == true_avg_color))
#
#     def test_get_neighbourhood_average_color_edge(self):
#         true_avg_color = np.array([0,85,85,255])
#         avg_color = self.extractor.get_neighbourhood_average_color(y=1, x=0, map=self.map, background=self.background)
#         self.assertTrue(np.all(avg_color == true_avg_color))

#get_neighbourhood_mapping
    def test_get_neighbourhood_mapping(self):
        # test at (y,x)=(1,1)
        mapping = self.extractor.get_neighbourhood_mapping(y=1.0,x=1.0, map=self.map)
        true_mapping = np.array([[0,1],[1,0],[1,2],[2,1]], dtype=np.int)
        self.assertTrue(np.all(mapping == true_mapping))

        # test at (y,x)=(0,0)
        true_mapping = np.array([[0,1],[1,0]], dtype=np.double)
        mapping = self.extractor.get_neighbourhood_mapping(y=0, x=0, map=self.map)
        self.assertTrue(np.all(mapping == true_mapping))

        # test at (y,x)=(3,1)
        true_mapping = np.array([[2,1],[3,0],[3,2]], dtype=np.double)
        mapping = self.extractor.get_neighbourhood_mapping(y=3, x=1, map=self.map)
        self.assertTrue(np.all(mapping == true_mapping))


# indices_for_array_access
    def test_indices_for_array_access(self):
        indices = np.array([[1.5,0.1],[0.1,1.1],[2,0.9],[9,11]])
        int_indices = self.extractor.indices_for_array_access(indices=indices)
        true_indices = np.array([[2,0,2,9],[0,1,1,11]])
        self.assertTrue(np.all(int_indices == true_indices))

# #get_properly_mapped_neighbours
#     def test_get_properly_mapped_neighbours(self):
#         # trivial case. Should be the same as get_neighbours
#         true_neighbours = np.array([[0,1],[1,0]])
#         proper_neighbours = self.extractor.get_properly_mapped_neighbours(y=0,x=0,map=self.map, mask=self.mask)
#         self.assertTrue(np.all(proper_neighbours == true_neighbours))
#
#         # one neighbour is mapped to hole, simple
#         true_neighbours = np.array([[0,1],[1, 0], [1, 2]])
#         proper_neighbours = self.extractor.get_properly_mapped_neighbours(y=1, x=1, map=self.map, mask=self.mask)
#         self.assertTrue(np.all(proper_neighbours == true_neighbours))
#
#         # all neighbours are mapped to hole, simple
#         true_neighbours = np.array([])
#         proper_neighbours = self.extractor.get_properly_mapped_neighbours(y=2, x=1, map=self.map, mask=self.mask)
#         self.assertTrue(proper_neighbours.shape == (0,2))
#
#     def test_get_properly_mapped_neighbours_mapped(self):
#         test_map = np.array([
#             [[0, 0], [0, 0], [0, 2], [0, 3]],
#             [[0, 0], [1, 1], [0, 0], [1, 3]],
#             [[1, 1], [0, 0], [1, 1], [2, 3]],
#             [[3, 0], [1, 1], [3, 2], [3, 3]]
#         ])
#         self.map.maps = [test_map]
#
#         self.mask = np.array([
#             [0, 0, 0, 0],
#             [0, 1, 0, 0],
#             [1, 1, 1, 0],
#             [1, 1, 1, 0],
#         ])
#
#         # trivial case. all neighbours mapped to same, not in hole
#         true_neighbours = np.array([[0, 1],[1, 0], [1, 2], [2,1]])
#         proper_neighbours = self.extractor.get_properly_mapped_neighbours(y=1, x=1, map=self.map, mask=self.mask)
#         self.assertTrue(np.all(proper_neighbours == true_neighbours))
#
#         # all neighbours are mapped to same pixel inside hole
#         proper_neighbours = self.extractor.get_properly_mapped_neighbours(y=2, x=1, map=self.map, mask=self.mask)
#         self.assertTrue(proper_neighbours.shape == (0, 2))
#
#         # two neighbours are mapped to different pixel inside hole
#         test_map = np.array([
#             [[0, 0], [0, 0], [0, 2], [0, 3]],
#             [[1, 0], [1, 1], [2, 2], [1, 3]],
#             [[1, 1], [2, 1], [1, 1], [2, 3]],
#             [[3, 0], [1, 1], [3, 2], [3, 3]]
#         ])
#         self.map.maps = [test_map]
#         true_neighbours = np.array([[0, 1],[1, 0]])
#         proper_neighbours = self.extractor.get_properly_mapped_neighbours(y=1, x=1, map=self.map, mask=self.mask)
#         self.assertTrue(np.all(proper_neighbours == true_neighbours))

# set up
    def setUp(self):

        class MockExtractor(AbstractNeighbourExtractor):
            def get_neighbours(self, y, x, map_size, as_patch=False):
                neighbour = -np.ones((3,3,2), dtype=np.double)
                if x > 0:
                    neighbour[1,0]= [y, x - 1]
                if x < map_size - 1:
                    neighbour[1,2] = [y, x + 1]
                if y > 0:
                    neighbour[0,1] = [y - 1, x]
                if y < map_size - 1:
                    neighbour[2,1] = [y + 1, x]

                if as_patch:
                    return neighbour
                else:
                    # If indices are non negative, it's a valid neighbour
                    valid_neighbours_indices = np.all(neighbour >= [0, 0], axis=2)
                    return neighbour[valid_neighbours_indices]

        self.extractor = MockExtractor()

        test_map = np.array([
            [[0, 0], [0, 1], [0, 2],[0,3]],
            [[1, 0], [1, 1], [1, 2],[1,3]],
            [[2, 0], [2, 1], [2, 2],[2,3]],
            [[3, 0], [3, 1], [3, 2], [3, 3]]
        ], dtype=np.double)

        self.mask = np.array([
            [0, 0, 0, 0],
            [0, 1, 0, 0],
            [1, 1, 1, 0],
            [1, 1, 1, 0],
        ])

        self.map = Map(side=4)
        self.map.lookup_level = 0
        self.map.maps = [test_map]

        self.background = np.array([
            [[0, 0, 0, 255], [255, 0, 255, 255], [255, 0, 255, 255]],
            [[0, 255, 0, 255], [0, 255, 0, 255], [0, 255, 0, 255]],
            [[0, 0, 255, 255], [0, 0, 255, 255], [0, 0, 0, 255]]
        ])