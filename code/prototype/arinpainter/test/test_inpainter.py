import pyximport;
import numpy as np
include = np.get_include()
pyximport.install(setup_args={'include_dirs': include})

from unittest import TestCase
from arinpainter.Inpainter import Inpainter
from arinpainter. DataProvider import DataProvider
from arinpainter.Map import Map
from arinpainter.Config import config
from arinpainter.FourNeighbourExtractor import FourNeighbourExtractor
import math
from unittest.mock import Mock
from copy import deepcopy


class TestInpainter(TestCase):

#init
    def test_init(self):
        self.assertEqual(self.inpainter.data_provider, self.data_provider)
        self.assertTrue(np.all(self.inpainter.mask == self.data_provider.masks[0]))
        self.assertTrue(np.all(self.inpainter.background == self.data_provider.backgrounds[0]))


#compute spatial direction
    def test_compute_spatial_direction_zero_direction(self):
        direction = self.inpainter.compute_spatial_direction(y=1, x=1)
        true_direction = np.array([0,0])
        self.assertTrue(np.all(direction == true_direction))

    def test_compute_spatial_direction_all_the_same(self):
        config.abs_threshold = 2
        config.spatial_component_threshold = 2

        self.inpainter.data_provider.maps[0].maps[0] = np.array([
            [[0, 0], [2, 2], [0, 2], [0,3]],
            [[2, 2], [1, 1], [2, 2], [1,3]],
            [[2, 0], [2, 2], [2, 2], [2,3]],
            [[3, 0], [3, 1], [3, 2], [3, 3]]
        ])
        direction = self.inpainter.compute_spatial_direction(y=1, x=1)
        true_direction = np.array([1, 1]) / math.sqrt(2) * (math.sqrt(2) - 1)
        self.assertTrue(np.all(direction == true_direction))

    def test_compute_spatial_direction_mapped_unnormed(self):
        #config
        config.abs_threshold = 999
        config.spatial_component_threshold = 999

        map = np.array([
            [[0, 0], [0, 4], [0, 2], [0,3]],
            [[4, 2], [0, 0], [4, 4], [1,3]],
            [[2, 0], [0, 2], [2, 2], [2,3]],
            [[3, 0], [3, 1], [3, 2], [3, 3]]
        ])
        self.inpainter.data_provider.maps[0].maps[0] = map
        direction = self.inpainter.compute_spatial_direction(y=1, x=1)
        true_direction = np.array([2,3])
        self.assertTrue(np.all(direction == true_direction))


    def test_compute_spatial_direction_mapped_normalized_gradient(self):
        # config
        config.abs_threshold = 1
        config.spatial_component_threshold = 999

        map = np.array([
            [[0, 0], [0, 4], [0, 2], [0, 3]],
            [[4, 2], [0, 0], [4, 4], [1, 3]],
            [[2, 0], [0, 2], [2, 2], [2, 3]],
            [[3, 0], [3, 1], [3, 2], [3, 3]]
        ])
        self.inpainter.data_provider.maps[0].maps[0] = map
        direction = self.inpainter.compute_spatial_direction(y=1, x=1)
        true_direction = np.array([2, 3]) / np.linalg.norm(np.array([2, 3]))
        self.assertTrue(np.all(direction == true_direction))

    def test_compute_spatial_direction_mapped_normalized_components(self):
        # config
        config.abs_threshold = 999
        config.spatial_component_threshold = 2

        map = np.array([
            [[0, 0], [0, 4], [0, 2], [0, 3]],
            [[4, 2], [0, 0], [4, 4], [1, 3]],
            [[2, 0], [0, 2], [2, 2], [2, 3]],
            [[3, 0], [3, 1], [3, 2], [3, 3]]
        ])
        self.inpainter.data_provider.maps[0].maps[0] = map
        direction = self.inpainter.compute_spatial_direction(y=1, x=1)
        true_direction = np.array([4, 2]) / np.linalg.norm(np.array([4, 2])) * 2
        true_direction = true_direction + np.array([4, 4]) / np.linalg.norm(np.array([4, 4])) * 2
        true_direction = true_direction + np.array([0,2])
        true_direction = true_direction + np.array([0, 2])
        true_direction = true_direction / 4
        self.assertTrue(np.all(direction == true_direction))

    def test_compute_spatial_direction_different_mappings(self):
        map = np.array([
            [[0, 0], [0, 2], [0, 2], [0, 3]],
            [[2, 2], [1, 1], [2, 2], [1, 3]],
            [[2, 0], [0, 2], [2, 2], [2, 3]],
            [[3, 0], [3, 1], [3, 2], [3, 3]]
        ])
        self.inpainter.data_provider.maps[0].maps[0] = map
        direction = self.inpainter.compute_spatial_direction(y=1, x=1)
        true_direction = np.array([0 ,1]) / config.spatial_pixel_step_size
        self.assertTrue(np.all(direction == true_direction))

    def test_compute_spatial_direction_edge(self):
        map = np.array([
            [[0, 0], [0, 2], [2, 2], [0, 3]],
            [[2, 2], [1, 1], [2, 2], [1, 3]],
            [[2, 0], [0, 2], [2, 2], [2, 3]],
            [[3, 0], [3, 1], [3, 2], [3, 3]]
        ])
        self.inpainter.data_provider.maps[0].maps[0] = map
        direction = self.inpainter.compute_spatial_direction(y=0, x=1)
        true_direction = np.array([1, -1]) /math.sqrt(2)
        self.assertTrue(np.all(direction == true_direction))

#compute_color_direction
    def test_compute_color_direction_simple(self):
        b = np.array([0,0,0,0])
        w = np.array([255,255,255,255])
        self.inpainter.data_provider.backgrounds[0] = np.array([
            [w, w, w, w, w, b, b, b],
            [w, w, w, w, w, b, b, b],
            [w, w, w, w, w, b, b, b],
            [w, w, w, w, w, b, b, b],
            [w, w, w, w, w, b, b, b],
            [w, w, w, w, w, b, b, b],
            [b, b, b, b, b, b, b, b],
            [b, b, b, b, b, b, b, b]
        ])
        self.inpainter.data_provider.masks[0] = np.array([
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0]
        ])
        test_map = np.array([
            [[0, 0], [0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [0, 6], [0, 7]],
            [[1, 0], [1, 1], [1, 2], [1, 3], [1, 4], [1, 5], [1, 6], [1, 7]],
            [[2, 0], [2, 1], [2, 2], [2, 3], [2, 4], [2, 5], [2, 6], [2, 7]],
            [[3, 0], [3, 1], [3, 2], [3, 3], [3, 4], [3, 5], [3, 6], [3, 7]],
            [[4, 0], [4, 1], [4, 2], [4, 3], [4, 4], [4, 5], [4, 6], [4, 7]],
            [[5, 0], [5, 1], [5, 2], [5, 3], [5, 4], [5, 5], [5, 6], [5, 7]],
            [[6, 0], [6, 1], [6, 2], [6, 3], [6, 4], [6, 5], [6, 6], [6, 7]],
            [[7, 0], [7, 1], [7, 2], [7, 3], [7, 4], [7, 5], [7, 6], [7, 7]]
        ], dtype=np.double)
        self.data_provider.maps[0].maps = [test_map]
        self.data_provider.maps[0].size_at_level = Mock(return_value=8)

        # 1. step
        # should point towards left
        self.data_provider.maps[0].maps[0][2, 2] = [2, 5]
        true_direction = np.array([0,-0.5])*config.color_pixel_step_size
        direction = self.inpainter.compute_color_direction(y=2,x=2)
        self.assertTrue(np.all(direction == true_direction))

        # 2. step
        self.data_provider.maps[0].maps[0][2,2] = [2, 4.5]
        # should point towards left
        true_direction = np.array([0, -0.5]) * config.color_pixel_step_size
        direction = self.inpainter.compute_color_direction(y=2, x=2)
        self.assertTrue(np.all(direction == true_direction))

        # 3. step
        self.data_provider.maps[0].maps[0][2, 2] = [2, 4]
        # should point towards left
        true_direction = np.array([0, -0.25]) * config.color_pixel_step_size
        direction = self.inpainter.compute_color_direction(y=2, x=2)
        self.assertTrue(np.all(direction == true_direction))

        # 4. step
        self.data_provider.maps[0].maps[0][2, 2] = [2, 3.75]
        # has arrived within best proximity due to rounding
        true_direction = np.array([0, -0.25]) * config.color_pixel_step_size
        direction = self.inpainter.compute_color_direction(y=2, x=2)
        self.assertTrue(np.all(direction == true_direction))

        # 5. step
        self.data_provider.maps[0].maps[0][2, 2] = [2, 3.5]
        # has arrived within best proximity due to rounding
        true_direction = np.array([0, -0.25]) * config.color_pixel_step_size
        direction = self.inpainter.compute_color_direction(y=2, x=2)
        self.assertTrue(np.all(direction == true_direction))

        # 6. step
        self.data_provider.maps[0].maps[0][2, 2] = [2, 3.25]
        # has arrived within best proximity due to rounding
        true_direction = np.array([0, -0.25]) * config.color_pixel_step_size
        direction = self.inpainter.compute_color_direction(y=2, x=2)
        self.assertTrue(np.all(direction == true_direction))

        # 7. step
        self.data_provider.maps[0].maps[0][2, 2] = [2, 3]
        # has arrived within best proximity due to rounding
        true_direction = np.array([0, 0]) * config.color_pixel_step_size
        direction = self.inpainter.compute_color_direction(y=2, x=2)
        self.assertTrue(np.all(direction == true_direction))


    def test_compute_color_direction_simple_mirror(self):
        b = np.array([0, 0, 0, 0])
        w = np.array([255, 255, 255, 255])
        self.inpainter.data_provider.backgrounds[0] = np.array([
            [b, b, b, b, b, b, b, b],
            [b, b, b, b, b, b, b, b],
            [b, b, b, w, w, w, w, w],
            [b, b, b, w, w, w, w, w],
            [b, b, b, w, w, w, w, w],
            [b, b, b, w, w, w, w, w],
            [b, b, b, w, w, w, w, w],
            [b, b, b, w, w, w, w, w]

        ])
        self.inpainter.data_provider.masks[0] = np.array([
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0]
        ])
        test_map = np.array([
            [[0, 0], [0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [0, 6], [0, 7]],
            [[1, 0], [1, 1], [1, 2], [1, 3], [1, 4], [1, 5], [1, 6], [1, 7]],
            [[2, 0], [2, 1], [2, 2], [2, 3], [2, 4], [2, 5], [2, 6], [2, 7]],
            [[3, 0], [3, 1], [3, 2], [3, 3], [3, 4], [3, 5], [3, 6], [3, 7]],
            [[4, 0], [4, 1], [4, 2], [4, 3], [4, 4], [4, 5], [4, 6], [4, 7]],
            [[5, 0], [5, 1], [5, 2], [5, 3], [5, 4], [5, 5], [5, 6], [5, 7]],
            [[6, 0], [6, 1], [6, 2], [6, 3], [6, 4], [6, 5], [6, 6], [6, 7]],
            [[7, 0], [7, 1], [7, 2], [7, 3], [7, 4], [7, 5], [7, 6], [7, 7]]
        ], dtype=np.double)
        self.data_provider.maps[0].maps = [test_map]
        self.data_provider.maps[0].size_at_level = Mock(return_value=8)

        # 1. step
        # should point towards right
        self.data_provider.maps[0].maps[0][5, 5] = [5, 2]
        true_direction = np.array([0, 0.25])
        direction = self.inpainter.compute_color_direction(y=5, x=5)
        self.assertTrue(np.all(direction == true_direction))

        # 2. step
        # should point towards right
        self.data_provider.maps[0].maps[0][5, 5] = [5, 2.25]
        true_direction = np.array([0, 0.5])
        direction = self.inpainter.compute_color_direction(y=5, x=5)
        self.assertTrue(np.all(direction == true_direction))

        # 3. step
        # should point towards right
        self.data_provider.maps[0].maps[0][5, 5] = [5, 2.75]
        true_direction = np.array([0, 0.5])
        direction = self.inpainter.compute_color_direction(y=5, x=5)
        self.assertTrue(np.all(direction == true_direction))

        # 4. step
        # should point towards right
        self.data_provider.maps[0].maps[0][5, 5] = [5, 3.25]
        true_direction = np.array([0, 0.25])
        direction = self.inpainter.compute_color_direction(y=5, x=5)
        self.assertTrue(np.all(direction == true_direction))

        # 5. step
        # should point towards right
        self.data_provider.maps[0].maps[0][5, 5] = [5, 3.5]
        true_direction = np.array([0, 0.25])
        direction = self.inpainter.compute_color_direction(y=5, x=5)
        self.assertTrue(np.all(direction == true_direction))

        # 6. step
        # should point towards right
        self.data_provider.maps[0].maps[0][5, 5] = [5, 3.75]
        true_direction = np.array([0, 0.25])
        direction = self.inpainter.compute_color_direction(y=5, x=5)
        self.assertTrue(np.all(direction == true_direction))

        # 7. step
        # should point towards right
        self.data_provider.maps[0].maps[0][5, 5] = [5, 4]
        true_direction = np.array([0, 0.25])
        direction = self.inpainter.compute_color_direction(y=5, x=5)
        self.assertTrue(np.all(direction == true_direction))

        # 8. step
        self.data_provider.maps[0].maps[0][5, 5] = [5, 4.25]
        true_direction = np.array([0, 0])
        direction = self.inpainter.compute_color_direction(y=5, x=5)
        self.assertTrue(np.all(direction == true_direction))

    # compute_color_direction
    def test_compute_color_direction_diagonal(self):
        b = np.array([0, 0, 0, 0])
        w = np.array([255, 255, 255, 255])
        self.inpainter.data_provider.backgrounds[0] = np.array([
            [w, b, b, b, b, b, b, b],
            [w, w, b, b, b, b, b, b],
            [w, w, w, b, b, b, b, b],
            [w, w, w, w, b, b, b, b],
            [w, w, w, w, w, b, b, b],
            [w, w, w, w, w, w, b, b],
            [w, w, w, w, w, w, w, b]

        ])
        self.inpainter.data_provider.masks[0] = np.array([
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0]
        ])
        test_map = np.array([
            [[0, 0], [0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [0, 6], [0, 7]],
            [[1, 0], [1, 1], [1, 2], [1, 3], [1, 4], [1, 5], [1, 6], [1, 7]],
            [[2, 0], [2, 1], [2, 2], [2, 3], [2, 4], [2, 5], [2, 6], [2, 7]],
            [[3, 0], [3, 1], [3, 2], [3, 3], [3, 4], [3, 5], [3, 6], [3, 7]],
            [[4, 0], [4, 1], [4, 2], [4, 3], [4, 4], [4, 5], [4, 6], [4, 7]],
            [[5, 0], [5, 1], [5, 2], [5, 3], [5, 4], [5, 5], [5, 6], [5, 7]],
            [[6, 0], [6, 1], [6, 2], [6, 3], [6, 4], [6, 5], [6, 6], [6, 7]],
            [[7, 0], [7, 1], [7, 2], [7, 3], [7, 4], [7, 5], [7, 6], [7, 7]]
        ], dtype=np.double)
        self.data_provider.maps[0].maps = [test_map]
        self.data_provider.maps[0].size_at_level = Mock(return_value=8)

        # 1. step
        self.data_provider.maps[0].maps[0][4, 2] = [2, 4]
        true_direction = np.array([0.5, -0.5]) * config.color_pixel_step_size
        direction = self.inpainter.compute_color_direction(y=4, x=2)
        self.assertTrue(np.all(direction == true_direction))

        # 2. step
        self.data_provider.maps[0].maps[0][4, 2] = [2.5, 3.5]
        true_direction = np.array([0.5, -0.5]) * config.color_pixel_step_size
        direction = self.inpainter.compute_color_direction(y=4, x=2)
        self.assertTrue(np.all(direction == true_direction))

        # 3. step
        self.data_provider.maps[0].maps[0][4, 2] = [3, 3]
        true_direction = np.array([0.5, -0.5]) * config.color_pixel_step_size
        direction = self.inpainter.compute_color_direction(y=4, x=2)
        self.assertTrue(np.all(direction == true_direction))

        # 4. step
        self.data_provider.maps[0].maps[0][4, 2] = [3.5, 2.5]
        true_direction = np.array([0.5, -0.5]) * config.color_pixel_step_size
        direction = self.inpainter.compute_color_direction(y=4, x=2)
        self.assertTrue(np.all(direction == true_direction))

        # 5. step
        self.data_provider.maps[0].maps[0][4, 2] = [4, 2]
        true_direction = np.array([0, 0]) * config.color_pixel_step_size
        direction = self.inpainter.compute_color_direction(y=4, x=2)
        self.assertTrue(np.all(direction == true_direction))

    def test_compute_color_direction_hole(self):
        b = np.array([0, 0, 0, 0])
        w = np.array([255, 255, 255, 255])
        self.inpainter.data_provider.backgrounds[0] = np.array([
            [b, b, b, b, w, w, w, w],
            [b, b, b, b, w, w, w, w],
            [b, b, b, b, w, w, w, w],
            [b, b, b, b, w, w, w, w],
            [b, b, b, b, w, w, w, w],
            [b, b, b, b, w, w, w, w],
            [b, b, b, b, w, w, w, w],
            [b, b, b, b, w, w, w, w]

        ])
        self.inpainter.data_provider.masks[0] = np.ones((8,8))
        test_map = np.array([
            [[0, 0], [0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [0, 6], [0, 7]],
            [[1, 0], [1, 1], [1, 2], [1, 3], [1, 4], [1, 5], [1, 6], [1, 7]],
            [[2, 0], [2, 1], [2, 2], [2, 3], [2, 4], [2, 5], [2, 6], [2, 7]],
            [[3, 0], [3, 1], [3, 2], [3, 3], [3, 4], [3, 5], [3, 6], [3, 7]],
            [[4, 0], [4, 1], [4, 2], [4, 3], [4, 4], [4, 5], [4, 6], [4, 7]],
            [[5, 0], [5, 1], [5, 2], [5, 3], [5, 4], [5, 5], [5, 6], [5, 7]],
            [[6, 0], [6, 1], [6, 2], [6, 3], [6, 4], [6, 5], [6, 6], [6, 7]],
            [[7, 0], [7, 1], [7, 2], [7, 3], [7, 4], [7, 5], [7, 6], [7, 7]]
        ])
        self.data_provider.maps[0].maps = [test_map]
        self.data_provider.maps[0].size_at_level = Mock(return_value=8)

        # 1. step
        # should not move, no valid mappings
        self.data_provider.maps[0].maps[0][2, 2] = [2, 4]
        true_direction = np.array([0, 0]) * config.color_pixel_step_size
        direction = self.inpainter.compute_color_direction(y=2, x=2)
        self.assertTrue(np.all(direction == true_direction))


    def test_compute_color_direction_edge(self):
        w = np.array([255,255,255,255])
        b = np.array([0,0,0,0])

        self.data_provider.mask = np.zeros((4,4))
        self.inpainter.data_provider.backgrounds[0] = np.array([
            [w, b, b, b],
            [w, b, b, b],
            [w, b, b, b],
            [w, b, b, b],

        ])
        true_direction = np.array([0,0])
        direction = self.inpainter.compute_color_direction(y=1,x=0)
        self.assertTrue(np.all(direction == true_direction))

    def test_compute_color_direction_not_unique(self):
        #if the direction is not unique, the order of the neighbours matters
        self.inpainter.data_provider.backgrounds[0] = np.array([
            [[0, 0, 0, 255], [0, 0, 0, 255], [0, 0, 0, 255]],
            [[0, 0, 0, 255], [0, 0, 0, 255], [0, 0, 0, 255]],
            [[0, 0, 0, 255],  [0, 0, 0, 255], [0, 0, 0, 255]]

        ])
        # best match is left neighbour, its mapping is 1,0
        true_direction = np.array([0,-1])/config.color_pixel_step_size
        direction = self.inpainter.compute_color_direction(y=1,x=1)
        self.assertTrue(np.all(direction == true_direction))

    def test_compute_color_direction_mapped(self):
        self.inpainter.data_provider.backgrounds[0] = np.array([
            [[0, 0, 0, 255], [0, 0, 0, 255], [0, 0, 0, 255]],
            [[0, 0, 0, 255], [0, 0, 0, 255], [0, 0, 0, 255]],
            [[0, 0, 0, 255],  [0, 0, 0, 255], [0, 0, 0, 255]]

        ])
        self.inpainter.data_provider.maps[0].maps[0] = np.array([
            [[0, 0], [0, 2], [2, 2]],
            [[2, 2], [1, 1], [2, 2]],
            [[2, 0], [0, 2], [2, 2]]
        ])
        # self.inpainter.data_provider.maps[0] = (map,map)
        #best match is left neighbour, its mapping is 2,2
        true_direction = np.array([1,1]) * config.color_pixel_step_size
        direction = self.inpainter.compute_color_direction(y=1,x=1)
        self.assertTrue(np.all(direction == true_direction))

    def test_compute_color_direction_mapped_2(self):
        self.inpainter.data_provider.backgrounds[0] = np.array([
            [[0, 0, 0, 255], [0, 0, 0, 255], [255, 255, 0, 255]],
            [[0, 0, 0, 255], [0, 0, 0, 255], [255, 0, 255, 255]],
            [[0, 0, 0, 255],  [0, 0, 0, 255], [200, 200, 200, 255]]

        ])
        self.inpainter.data_provider.maps[0].maps[0] = np.array([
            [[0, 0], [1, 2], [2, 2]],
            [[0, 0], [1, 1], [2, 2]],
            [[2, 0], [0, 2], [2, 2]]
        ])
        #best match is right neighbour, its mapping is 2,2
        true_direction = np.array([1,1]) * config.color_pixel_step_size
        direction = self.inpainter.compute_color_direction(y=1,x=1)
        self.assertTrue(np.all(direction == true_direction))

#compute_out_of_hole_direction
    def test_compute_out_of_hole_direction(self):
        self.data_provider.center_of_mass = np.array([1,1])
        dir = self.inpainter.compute_out_of_hole_direction(0,0)
        true_dir = np.array([0,0])
        self.assertTrue(np.all(dir == true_dir))

        dir = self.inpainter.compute_out_of_hole_direction(1, 1)
        true_dir = np.array([0.0, 0])*config.out_of_hole_step_size
        self.assertTrue(np.all(dir == true_dir))

        dir = self.inpainter.compute_out_of_hole_direction(2, 1)
        true_dir = np.array([1, 0])*config.out_of_hole_step_size
        self.assertTrue(np.all(dir == true_dir))

        dir = self.inpainter.compute_out_of_hole_direction(1, 2)
        true_dir = np.array([0, 1])*config.out_of_hole_step_size
        self.assertTrue(np.all(dir == true_dir))

# get_patch
    def test_get_patch(self):

        self.data_provider.masks[0] = np.zeros((4,4))
        # simple case
        true_patch = -np.ones((3,3,4))
        true_patch[0,1] = [255, 0, 255, 255]
        true_patch[2, 1] = [0, 0, 255, 255]
        true_patch[1, 0] = [0, 255, 0, 255]
        true_patch[1, 2] = [0, 255, 0, 255]

        patch = self.inpainter.get_patch(y=1, x=1)
        self.assertTrue(np.all(patch == true_patch))

        # edge case
        true_patch = -np.ones((3, 3, 4))
        true_patch[2, 1] = [0, 255, 0, 255]
        true_patch[1, 0] = [0, 0, 0, 255]
        true_patch[1, 2] = [255, 0,255, 255]

        patch = self.inpainter.get_patch(y=0, x=1)
        self.assertTrue(np.all(patch == true_patch))

        # corner case
        true_patch = -np.ones((3, 3, 4))
        true_patch[2, 1] = [0, 255, 0, 255]
        true_patch[1, 2] = [255, 0, 255, 255]

        patch = self.inpainter.get_patch(y=0, x=0)
        self.assertTrue(np.all(patch == true_patch))


    def test_get_patch_hole(self):
        self.data_provider.masks[0] = np.array([
            [1, 1, 1, 0],
            [1, 1, 1, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0]
        ])

        # simple case
        true_patch = -np.ones((3, 3, 4))
        true_patch[2, 1] = [0, 0, 255, 255]

    def test_get_patch_mapped(self):

        self.data_provider.masks[0] = np.zeros((4, 4))
        self.data_provider.masks[0][2,1] = 1
        test_map = np.array([
            [[0, 0], [2, 1], [0, 2], [0, 3]],
            [[0, 0], [0, 1], [0, 2], [1, 3]],
            [[2, 0], [2, 1], [2, 0], [2, 3]],
            [[3, 0], [3, 1], [3, 2], [3, 3]]
        ])
        self.data_provider.maps[0].maps = [test_map]

        # simple case
        true_patch = -np.ones((3, 3, 4))
        # true_patch[0, 1] = [0, 0, 255, 255] inside hole
        # true_patch[2, 1] = [0, 0, 255, 255] inside hole
        true_patch[1, 0] = [0, 0, 0, 255]
        true_patch[1, 2] = [255,0, 255, 255]

        patch = self.inpainter.get_patch(y=1, x=1)
        self.assertTrue(np.all(patch == true_patch))

        # edge case
        true_patch = -np.ones((3, 3, 4))
        true_patch[2, 1] = [255, 0, 255, 255]
        true_patch[1, 0] = [0, 0, 0, 255]
        true_patch[1, 2] = [255, 0, 255, 255]

        patch = self.inpainter.get_patch(y=0, x=1)
        self.assertTrue(np.all(patch == true_patch))

        # corner case
        true_patch = -np.ones((3, 3, 4))
        true_patch[2, 1] = [0, 0, 0, 255]
        #true_patch[1, 2] = [0, 0, 255, 255]  inside hole

        patch = self.inpainter.get_patch(y=0, x=0)
        self.assertTrue(np.all(patch == true_patch))

# compute_color_error
    def test_compute_color_error_trivial(self):

        #trivial case. Mapped to itself. Error must be 0
        error = self.inpainter.compute_color_error(y=1,x=1, mapped_y=1, mapped_x=1)
        self.assertEqual(error, 0)

    def test_compute_color_error_no_overlap(self):
        # background = np.array([
        #     [[0, 0, 0, 255], [255, 0, 255, 255], [255, 0, 255, 255], [0, 0, 0, 0]],
        #     [[0, 255, 0, 255], [0, 255, 0, 255], [0, 255, 0, 255], [0, 0, 0, 0]],
        #     [[0, 0, 255, 255], [0, 0, 255, 255], [0, 0, 0, 255], [0, 0, 0, 0]],
        #     [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
        #
        # ])
        test_map = np.array([
            [[3, 3], [0, 1], [0, 2], [0, 3]],
            [[1, 0], [1, 1], [1, 2], [1, 3]],
            [[2, 0], [2, 1], [2, 1], [2, 3]],
            [[3, 0], [3, 1], [3, 2], [3, 3]]
        ])
        self.data_provider.maps[0].maps = [test_map]

        #no overlap -> max error
        error = self.inpainter.compute_color_error(y=0, x=0, mapped_y=3, mapped_x=3)
        self.assertEqual(error, 1)

    def test_compute_color_error_normal(self):
        # background = np.array([
        #     [[0, 0, 0, 255], [255, 0, 255, 255], [255, 0, 255, 255], [0, 0, 0, 0]],
        #     [[0, 255, 0, 255], [0, 255, 0, 255], [0, 255, 0, 255], [0, 0, 0, 0]],
        #     [[0, 0, 255, 255], [0, 0, 255, 255], [0, 0, 0, 255], [0, 0, 0, 0]],
        #     [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
        #
        # ])
        test_map = np.array([
            [[0, 0], [0, 1], [0, 2], [0, 3]],
            [[1, 0], [2, 2], [1, 2], [1, 3]],
            [[2, 0], [2, 1], [2, 1], [2, 3]],
            [[3, 0], [3, 1], [3, 2], [3, 3]]
        ])
        self.inpainter.data_provider.masks[0] = np.zeros((4,4))
        # Used to compute error
        # first_patch = np.array([
        #     [1, 0, 1, 1],
        #     [0, 1, 0, 1],
        #     [0, 1, 0, 1],
        #     [0, 0, 1, 1]
        # ])*255
        # second_patch = np.array([
        #     [0, 1, 0, 1],
        #     [0, 0, 1, 1],
        #     [0, 0, 0, 0],
        #     [0, 0, 0, 0]
        # ])*255
        # diff = np.linalg.norm(first_patch - second_patch, ord=2, axis=1)/510
        # error = np.mean(diff)

        self.data_provider.maps[0].maps = [test_map]

        #no overlap -> max error
        error = self.inpainter.compute_color_error(y=1, x=1, mapped_y=2, mapped_x=2)
        self.assertEqual(error, 0.7468364368360203)

    def test_compute_color_error_hole(self):
        self.inpainter.data_provider.masks[0] = np.ones((4, 4))
        error = self.inpainter.compute_color_error(y=1, x=1, mapped_y=2, mapped_x=2)
        # must be 1, because its in hole
        self.assertEqual(error, 1)

    def test_compute_color_error_mapped(self):
        # background = np.array([
        #     [[0, 0, 0, 255], [255, 0, 255, 255], [255, 0, 255, 255], [0, 0, 0, 0]],
        #     [[0, 255, 0, 255], [0, 255, 0, 255], [0, 255, 0, 255], [0, 0, 0, 0]],
        #     [[0, 0, 255, 255], [0, 0, 255, 255], [0, 0, 0, 255], [0, 0, 0, 0]],
        #     [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
        #
        # ])
        test_map = np.array([
            [[0, 0], [0, 0], [0, 2], [0, 3]],
            [[0, 0], [2, 2], [0, 1], [1, 3]],
            [[2, 0], [2, 0], [2, 1], [2, 3]],
            [[3, 0], [3, 1], [3, 2], [3, 3]]
        ])
        # # Used to compute error
        # first_patch = np.array([
        #     [0, 0, 0, 1],
        #     [0, 0, 0, 1],
        #     [1, 0, 1, 1],
        #     [0, 0, 1, 1]
        # ])*255
        # second_patch = np.array([
        #     [1, 0, 1, 1],
        #     [0, 0, 1, 1],
        #     [0, 0, 0, 0],
        #     [0, 0, 0, 0]
        # ])*255
        # diff = np.linalg.norm(first_patch - second_patch, ord=2, axis=1)/510
        # error = np.mean(diff)

        self.data_provider.maps[0].maps = [test_map]

        #no overlap -> max error
        error = self.inpainter.compute_color_error(y=1, x=1, mapped_y=2,mapped_x=2)
        self.assertEqual(error, 0.6950597415393834)


    def test_compute_color_error_invalid(self):

        # invalid mapping
        self.assertRaises(Exception, self.inpainter.compute_color_error,y=1, x=1, mapped_y=1, mapped_x=-1)
        self.assertRaises(Exception, self.inpainter.compute_color_error, y=-1, x=1, mapped_y=1, mapped_x=1)
        self.assertRaises(Exception, self.inpainter.compute_color_error, y=1000, x=1, mapped_y=1, mapped_x=1)
        self.assertRaises(Exception, self.inpainter.compute_color_error, y=1, x=1, mapped_y=1000, mapped_x=1)

    #re_init_map
    def test_re_initialize_map(self):
        test_map = np.array([
            [[1, 0], [1, 1], [0, 2], [0, 3]],
            [[1, 0], [1, 1], [1, 2], [1, 3]],
            [[2, 0], [2, 1], [2, 2], [2, 3]],
            [[3, 1], [3, 1], [3, 2], [3, 3]]
        ])
        self.data_provider.maps[0].maps = [test_map]
        self.inpainter.re_initialize_map()
        map = self.inpainter.old_map
        self.assertTrue(np.all(map[0,0] == [0,0]))
        self.assertTrue(np.all(map[0,1] == [0,1]))
        self.assertTrue(np.all(map[3,0] == [3,0]))

# initialize_map
    def test_initialize_map(self):
        # 1. Test one iteration without any change
        # 2. after 3 iterations, return a mapping value for one coordinate, then for the next in the next iteration
        # 3. Once all holes are fille (after 6 iterations) the algorithm stops

        search = lambda x, y, map, not_mapped: None

        class MockInpainter(Inpainter):
            def __init__(self, data_provider, mipmap_level, color_neighbour_extractor=None, spatial_neighbour_extractor=None):
                super().__init__(data_provider=data_provider, mipmap_level=mipmap_level, color_neighbour_extractor=color_neighbour_extractor, spatial_neighbour_extractor=spatial_neighbour_extractor)
            # def search_mapping_by_color(self, y, x, map, not_mapped):
            #     return search(x,y,map,not_mapped)

        self.inpainter = MockInpainter(data_provider=self.data_provider, mipmap_level=0, color_neighbour_extractor=FourNeighbourExtractor(), spatial_neighbour_extractor=FourNeighbourExtractor() )


        self.inpainter.search_mapping_by_color = Mock(side_effect=search)

        true_map = deepcopy(self.inpainter.old_map)
        true_not_mapped = [[1,1],[1,2],[2,1]]

        self.iteration = 0

        def iteration_completed(true_map, true_not_mapped, map, not_mapped):
            if self.iteration < 3:
                self.assertTrue(np.all(map == true_map))
                self.assertTrue(np.all(not_mapped == true_not_mapped))
            if self.iteration == 2:
                self.assertTrue(np.all(map[1, 1] == [1, 1]))
                search = lambda y, x, map, not_mapped: [0,0] if [y,x] == [1,1] else None
                self.inpainter.search_mapping_by_color = Mock(side_effect=search)
            if self.iteration == 3:
                self.assertTrue(np.all(map[1,1] == [0,0]))
                self.assertTrue(np.all(map[1, 2] == [1,2]))
                self.assertTrue(np.all(map[2, 1] == [2,1]))
                self.assertTrue(np.all(not_mapped == [[1,2],[2,1]]))

                def search (y,x, map, not_mapped):
                    if [y, x] == [1, 2]:
                        return [0, 0]
                    else:
                        return None
                self.inpainter.search_mapping_by_color = Mock(side_effect=search)
            if self.iteration == 4:
                self.assertTrue(np.all(map[1,1] == [0,0]))
                m = map[1, 2]
                self.assertTrue(np.all(map[1, 2] == [0,0]))
                self.assertTrue(np.all(map[2, 1] == [2,1]))
                self.assertTrue(np.all(not_mapped == [[2,1]]))

                search = lambda y, x, map, not_mapped: [0, 0] if [y, x] == [2, 1] else None
                self.inpainter.search_mapping_by_color = Mock(side_effect=search)
            if self.iteration == 5:
                self.assertTrue(np.all(map[1,1] == [0,0]))
                self.assertTrue(np.all(map[1, 2] == [0,0]))
                self.assertTrue(np.all(map[2, 1] == [0,0]))
                self.assertTrue(np.all(not_mapped == []))

            self.iteration = self.iteration + 1

        self.inpainter.initialize_map(iteration_completed=lambda map, not_mapped: iteration_completed(true_map=true_map,true_not_mapped=true_not_mapped, map=map, not_mapped=not_mapped))

        self.assertEqual(self.iteration, 6)

        del self.iteration

# search_mapping_by_color
#     def test_search_mapping_by_color(self):
#         true_avg_color = [127.5,127.5,127.5,255]
#         return_match = [0,0]
#         def find_value(y,x, color, neighbours_mapping):
#             self.assertTrue(np.all(true_avg_color == color))
#             return return_match
#
#         mock = Mock()
#         mock.side_effect = find_value
#         self.inpainter.find_best_match= mock
#
#         #setup config
#         config.number_of_mapped_neighbours_required = 2
#         config.init_with_spatial = False
#
#         # simple test
#         new_match = self.inpainter.search_mapping_by_color(y=1,x=1,map=self.inpainter.old_map,not_mapped=[[1,1],[1,2],[2,1]])
#         self.assertTrue(np.all(new_match == return_match))
#
#         # another simple test
#         true_avg_color = [0, 0, 85, 170]
#         return_match = [3, 0]
#         new_match = self.inpainter.search_mapping_by_color(y=2, x=1, map=self.inpainter.old_map, not_mapped=[[1, 1], [1, 2], [2, 1]])
#         self.assertTrue(np.all(new_match == return_match))
#
#         # invalid neighbourhood
#         new_match = self.inpainter.search_mapping_by_color(y=1, x=1, map=self.inpainter.old_map, not_mapped=[[1, 1], [1, 2], [2, 1],[0,1]])
#         self.assertIsNone(new_match)



# find_best_match_for_color
    def test_find_best_match_for_color(self):
        # Only use color initialization
        config.init_with_spatial = False

        background = np.array([
            [[0, 0, 0, 255], [10, 10, 10, 255], [0,0,0,0],[0,0,0,0]],
            [[20, 20, 20, 255], [31, 31, 31, 0], [0,0,0,0],[0,0,0,0]],
            [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
            [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
        ])
        self.data_provider.backgrounds = [background]

        color = [10,10,10,255]
        true_match = [0,1]
        matched = self.inpainter.find_best_match(y=0,x=0, color=color, neighbours_mapping=[])
        self.assertTrue(np.all(matched == true_match))

        color = [20, 20, 20, 200]
        true_match = [1, 0]
        matched = self.inpainter.find_best_match(y=0,x=0, color=color, neighbours_mapping=[])
        self.assertTrue(np.all(matched == true_match))

        color = [25, 25, 25, 200]
        true_match = [1, 0]
        matched = self.inpainter.find_best_match(y=0,x=0, color=color, neighbours_mapping=[])
        self.assertTrue(np.all(matched == true_match))

        # first pixel with very low alpha
        color = [20, 20, 20, 0]
        true_match = [0, 2]
        matched = self.inpainter.find_best_match(y=0,x=0, color=color, neighbours_mapping=[])
        self.assertTrue(np.all(matched == true_match))


#hole pixels
    def test_hole_pixels(self):
        hole_pixels = list(self.inpainter.hole_pixels())
        true_hole_pixels = [(1,1),(1,2),(2,1)]
        self.assertTrue(np.all(hole_pixels==true_hole_pixels))

#normal pixels
    def test_normal_pixels(self):
        normal_pixels = self.inpainter.normal_pixels()
        true_normal_pixels = np.array([[0,0],[0,1],[0,2],[0,3],[1,0],[1,3],[2,0],[2,2],[2,3],[3, 0], [3, 1], [3, 2], [3, 3]])
        self.assertTrue(np.all(normal_pixels == true_normal_pixels))
#finalize
    def test_finalize(self):
        map1 = self.inpainter.data_provider.maps[0]
        map2 = deepcopy(map1)
        self.inpainter.data_provider.maps = [map1, map2]

        changed_value = np.array([100,100])
        self.inpainter.old_map[0,0] = changed_value

        self.assertFalse(np.all(self.inpainter.new_map[0,0] == changed_value ))
        self.assertTrue(np.all(self.inpainter.old_map[0,0] == changed_value ))

        self.inpainter.finalize()

        self.assertTrue(np.all(self.inpainter.new_map[0, 0] == changed_value))
        self.assertTrue(np.all(self.inpainter.old_map[0, 0] == changed_value))
        self.assertTrue(np.all(self.inpainter.new_map[0, 1] == self.inpainter.old_map[0, 1]))
        self.assertTrue(np.all(self.inpainter.new_map[2, 1] == self.inpainter.old_map[2, 1]))

#normalize_vectore
    def test_normalize_vector(self):
        config.abs_threshold = 4
        vector = np.array([1,0], dtype=np.double)
        true_vector = np.array([1,0])
        normalized = self.inpainter.clamp_vector(vector=vector)
        self.assertTrue(np.all(normalized == true_vector))


        vector = np.array([1, 10], dtype=np.double)
        true_vector = np.array([1, 10]) / np.linalg.norm(np.array([1, 10]))*4
        normalized = self.inpainter.clamp_vector(vector=vector)
        self.assertTrue(np.all(normalized == true_vector))

#normalize_rows
    def test_normalize_rows(self):

        # config
        config.spatial_component_threshold = 2

        # single pair test
        mat = np.array([[1, 0]], dtype=np.double)
        true_mat = np.array([[1, 0]])
        norm_mat = self.inpainter.clamp_rows(mat)
        self.assertTrue(np.all(norm_mat == true_mat))

        # simple zero-norm case
        mat = np.array([[0,0]], dtype=np.double)
        true_mat = np.array([[0, 0]])
        norm_mat = self.inpainter.clamp_rows(mat)
        self.assertTrue(np.all(norm_mat == true_mat))

        #advanced zero-norm case
        mat = np.array([[1,0],[2,0],[1,1],[0,0]], dtype=np.double)
        true_mat = np.array([[1,0],[2,0],[1,1],[0,0]])
        norm_mat = self.inpainter.clamp_rows(mat)
        self.assertTrue(np.all(norm_mat == true_mat))

        # simple cut-norm case
        mat = np.array([[4, 0]], dtype=np.double)
        true_mat = np.array([[2, 0]])
        norm_mat = self.inpainter.clamp_rows(mat)
        self.assertTrue(np.all(norm_mat == true_mat))

        # advanced cut norm case
        mat = np.array([[1, 0], [2, 0], [100, 0], [4, 4]], dtype=np.double)
        true_mat = np.array([[1, 0], [2, 0], [2, 0], [math.sqrt(2), math.sqrt(2)]])
        norm_mat = self.inpainter.clamp_rows(mat)
        self.assertTrue(np.all(np.abs(norm_mat - true_mat) < 0.001))

#clamp_if_outside
    def test_clamp_if_outside(self):
        # positive y direction
        clamped_dir = self.inpainter.clamp_if_outside(y=0,x=0, direction=[10,0])
        self.assertTrue(np.all(clamped_dir == [3,0]))

        clamped_dir = self.inpainter.clamp_if_outside(y=1, x=1, direction=[10,0])
        self.assertTrue(np.all(clamped_dir == [2, 0]))

        #negative y direction
        clamped_dir = self.inpainter.clamp_if_outside(y=-0, x=0, direction=[-10,0])
        self.assertTrue(np.all(clamped_dir == [0, 0]))

        clamped_dir = self.inpainter.clamp_if_outside(y=1, x=1, direction=[-10, 0])
        self.assertTrue(np.all(clamped_dir == [-1, 0]))


        # positive x direction
        clamped_dir = self.inpainter.clamp_if_outside(y=0, x=0, direction=[0, 10])
        self.assertTrue(np.all(clamped_dir == [0, 3]))

        clamped_dir = self.inpainter.clamp_if_outside(y=1, x=1, direction=[0, 10])
        self.assertTrue(np.all(clamped_dir == [0, 2]))

        # negative x direction
        clamped_dir = self.inpainter.clamp_if_outside(y=-0, x=0, direction=[0, -10])
        self.assertTrue(np.all(clamped_dir == [0, 0]))

        clamped_dir = self.inpainter.clamp_if_outside(y=1, x=1, direction=[0, -10])
        self.assertTrue(np.all(clamped_dir == [0, -1]))


        # positive yx direction
        clamped_dir = self.inpainter.clamp_if_outside(y=0, x=0, direction=[10, 10])
        self.assertTrue(np.all(clamped_dir == [3, 3]))

        clamped_dir = self.inpainter.clamp_if_outside(y=1, x=1, direction=[10, 10])
        self.assertTrue(np.all(clamped_dir == [2, 2]))

        # negative x direction
        clamped_dir = self.inpainter.clamp_if_outside(y=-0, x=0, direction=[-10, -10])
        self.assertTrue(np.all(clamped_dir == [0, 0]))

        clamped_dir = self.inpainter.clamp_if_outside(y=1, x=1, direction=[-10, -10])
        self.assertTrue(np.all(clamped_dir == [-1, -1]))

        # positive yx direction
        clamped_dir = self.inpainter.clamp_if_outside(y=0, x=0, direction=[-10, 10])
        self.assertTrue(np.all(clamped_dir == [0, 3]))

        clamped_dir = self.inpainter.clamp_if_outside(y=1, x=1, direction=[-10, 10])
        self.assertTrue(np.all(clamped_dir == [-1, 2]))

        # negative x direction
        clamped_dir = self.inpainter.clamp_if_outside(y=-0, x=0, direction=[10, -10])
        self.assertTrue(np.all(clamped_dir == [3, 0]))

        clamped_dir = self.inpainter.clamp_if_outside(y=1, x=1, direction=[10, -10])
        self.assertTrue(np.all(clamped_dir == [2, -1]))

# set up
    def setUp(self):
        test_map = np.array([
            [[0, 0], [0, 1], [0, 2], [0, 3]],
            [[1, 0], [1, 1], [1, 2], [1, 3]],
            [[2, 0], [2, 1], [2, 2], [2, 3]],
            [[3, 0], [3, 1], [3, 2], [3, 3]]
        ])

        mask = np.array([
            [0,0,0,0],
            [0,1,1,0],
            [0,1,0,0],
            [0,0,0,0]
        ])
        background = np.array([
            [[0,0,0,255], [255,0,255,255], [255,0,255,255], [0,0,0,0]],
            [[0, 255, 0, 255], [0, 255, 0, 255], [0, 255, 0, 255], [0,0,0,0]],
            [[0, 0, 255, 255], [0, 0, 255, 255], [0, 0, 0, 255], [0,0,0,0]],
            [[0,0,0,0], [0,0,0,0], [0,0,0,0], [0,0,0,0]]

        ], dtype=np.uint8)

        self.data_provider = DataProvider(size=4)
        self.data_provider.add_level(mask=mask, background=background)
        map = Map(side=4)
        map.lookup_level = 0
        map.maps = [test_map]
        map2 = deepcopy(map)
        self.data_provider.maps = [map,map2]
        self.inpainter = Inpainter(data_provider=self.data_provider, mipmap_level=0, color_neighbour_extractor=FourNeighbourExtractor(), spatial_neighbour_extractor=FourNeighbourExtractor())

        config.color_pixel_step_size = 1.0
        config.spatial_pixel_step_size = 1.0
        config.out_of_hole_step_size = 1.0
        config.collect_data = False
