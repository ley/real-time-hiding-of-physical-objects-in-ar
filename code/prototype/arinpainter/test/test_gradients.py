import pyximport;
import numpy as np
include = np.get_include()
pyximport.install(setup_args={'include_dirs': include})

from unittest import TestCase
from arinpainter.Inpainter import Inpainter
from arinpainter.Map import Map
from arinpainter.DataProvider import DataProvider
from arinpainter.FourNeighbourExtractor import FourNeighbourExtractor
from arinpainter.Config import config

b = [0,0,0,0]
w = [255,255,255,255]

class TestGradients(TestCase):

    def test_check_converged_spatial(self):
        # don't limit the gradient
        config.abs_threshold = 99
        config.spatial_component_threshold = 99

        spatial_dir = self.inpainter.compute_spatial_direction(y=2, x=2)

        # The mapping should also be moved by 4,4
        self.assertTrue(np.all(spatial_dir == np.array([0, 0])))

    def test_checkerboard_color(self):
        # checker board
        self.background[::2,::2] = w

        # The color gradient is always zero on a checker board
        self.map1[2, 2] = [4, 4]
        color_dir = self.inpainter.compute_color_direction(y=2, x=2)
        self.assertTrue(np.all(color_dir == np.array([0,0])))


        # The color gradient is always zero on a checker board
        self.map1[2, 2] = [4, 3]
        color_dir = self.inpainter.compute_color_direction(y=2, x=2)
        self.assertTrue(np.all(color_dir == np.array([0, 0])))

    def test_checkerboard_spatial(self):
        # checker board
        self.background[::2, ::2] = w

        # offset all neighbours by an offset
        offset = [1,0]
        self.map1[1,2] += offset
        self.map1[3, 2] += offset
        self.map1[2, 1] += offset
        self.map1[2, 3] += offset

        # don't limit the gradient
        config.abs_threshold = 99
        config.spatial_component_threshold = 99

        spatial_dir = self.inpainter.compute_spatial_direction(y=2, x=2)

        # The mapping should also be moved by 4,4
        self.assertTrue(np.all(spatial_dir == np.array(offset)))

    def test_checkerboard_both(self):
        # checker board
        self.background[::2, ::2] = w

        # offset all neighbours by 4
        self.map1[1, 2] += [4, 4]
        self.map1[3, 2] += [4, 4]
        self.map1[2, 1] += [4, 4]
        self.map1[2, 3] += [4, 4]

        # don't limit the gradient
        config.norm_threshold_factor = 1

        # 0,0
        color_dir = self.inpainter.compute_color_direction(y=2, x=2)

        # 4,4
        spatial_dir = self.inpainter.compute_spatial_direction(y=2, x=2)


        # FIRST: normal weights
        # set weights
        config.color_direction_weight = 0.5
        config.spatial_direction_weight = 0.5
        self.inpainter.update_map(color_direction=color_dir, spatial_direction=spatial_dir, y=2, x=2)

        # the mapping should have moved by 4,4 * 0.5 = 2,2
        self.assertTrue(np.all(self.map2[2,2] == np.array([4,4])))


        # SECOND: biased weights
        # set weights
        config.color_direction_weight = 0.0
        config.spatial_direction_weight = 1.0
        self.inpainter.update_map(color_direction=color_dir, spatial_direction=spatial_dir, y=2, x=2)

        # the mapping should have moved by 4,4 * 1.0 = 4,4
        self.assertTrue(np.all(self.map2[2, 2] == np.array([6, 6])))

        # THIRD: step-size
        # set weights
        config.color_direction_weight = 0.0
        config.spatial_direction_weight = 1.0
        config.color_pixel_step_size = 2.
        config.spatial_pixel_step_size = 2.


        # 8,8
        spatial_dir = self.inpainter.compute_spatial_direction(y=2, x=2)

        # the mapping should have moved by 4,4 * 1.0 * 2.0 = 8,8
        self.inpainter.update_map(color_direction=color_dir, spatial_direction=spatial_dir, y=2, x=2)
        self.assertTrue(np.all(self.map2[2, 2] == np.array([10, 10])))

    def test_bi_color_color(self):
        # left half white, right half black
        self.background[:, :8] = w

        # The color gradient is always zero in a uniform area
        self.map1[2,2] = [4,4]
        color_dir = self.inpainter.compute_color_direction(y=2, x=2)
        self.assertTrue(np.all(color_dir == np.array([0, 0])))

        self.map1[2, 2] = [12, 12]
        color_dir = self.inpainter.compute_color_direction(y=2, x=2)
        self.assertTrue(np.all(color_dir == np.array([0, 0])))

        # at the edge of uniform areas, the gradient pulls towards the 'true' color
        self.map1[2, 2] = [9, 9]
        color_dir = self.inpainter.compute_color_direction(y=2, x=2)
        norm_dir = color_dir/np.linalg.norm(color_dir)
        self.assertTrue(np.all(norm_dir== np.array([0, -1])))

    def test_bi_color_spatial(self):
        # left half white, right half black
        self.background[:, :8] = w

        self.map1[2, 2] = [9, 9]

        # offset all neighbours by 10
        self.map1[1, 2] += [10, 10]
        self.map1[3, 2] += [10, 10]
        self.map1[2, 1] += [10, 10]
        self.map1[2, 3] += [10, 10]

        # don't limit the gradient
        config.norm_threshold_factor = 1.0
        config.spatial_direction_weight = 1.0

        # should pull towards 12,12
        spatial_dir = self.inpainter.compute_spatial_direction(y=2, x=2)

        # The mapping should also be moved by 12-9,12-9 = 3,3
        self.assertTrue(np.all(spatial_dir == np.array([3,3])))

    def test_bi_color_both(self):
        # left half white, right half black
        self.background[:, :8] = w

        self.map1[2, 2] = [9, 9]

        # offset all neighbours by 10
        self.map1[1, 2] += [10, 10]
        self.map1[3, 2] += [10, 10]
        self.map1[2, 1] += [10, 10]
        self.map1[2, 3] += [10, 10]

        # don't limit the gradient
        config.norm_threshold_factor = 1.0
        config.color_pixel_step_size = 1.0
        config.spatial_pixel_step_size = 1.0

        # should pull towards 12,12
        spatial_dir = self.inpainter.compute_spatial_direction(y=2, x=2)

        # The mapping should also be moved by (12-9,12-9) = 3,3
        self.assertTrue(np.all(spatial_dir == np.array([3,3])))

        # should pull left by 0.25
        color_dir = self.inpainter.compute_color_direction(y=2, x=2)
        self.assertTrue(np.all(color_dir == np.array([0, -0.25])))

    # FIRST: normal weights -> wrong direction
        # set weights
        config.color_direction_weight = 0.5
        config.spatial_direction_weight = 0.5
        self.inpainter.update_map(color_direction=color_dir, spatial_direction=spatial_dir, y=2, x=2)

        # the mapping should have moved by 0.5*(3,3) + 0.5*(0, -0.25)
        true_mapping = np.array([9, 9]) + np.array([3, 2.75])*0.5
        self.assertTrue(np.all(self.map2[2, 2] == true_mapping ))

    # SECOND: biased weights  -> still wrong direction
        # set weights
        config.color_direction_weight = 0.9
        config.spatial_direction_weight = 0.1
        self.inpainter.update_map(color_direction=color_dir, spatial_direction=spatial_dir, y=2, x=2)

        # the mapping should have moved by 0.1*(3,3) + 0.9*(0, -0.25) = (0.3, 0.07499)
        true_mapping = np.array([9, 9]) + np.array([0.3, 0.3 - 0.225])
        self.assertTrue(np.all(self.map2[2, 2] == true_mapping))


    # THIRD: increase step size -> still wrong direction
        config.color_direction_weight = 0.5
        config.spatial_direction_weight = 0.5
        config.color_pixel_step_size = 2
        config.spatial_pixel_step_size = 1

        # should pull towards 12,12
        spatial_dir = self.inpainter.compute_spatial_direction(y=2, x=2)

        # should pull left by 0.5
        color_dir = self.inpainter.compute_color_direction(y=2, x=2)

        self.inpainter.update_map(color_direction=color_dir, spatial_direction=spatial_dir, y=2, x=2)

        # the mapping should have moved by 0.5*(3,3) + 0.5*2*(0, -0.25) = (1.5, 1.25)
        true_mapping = np.array([9, 9]) + np.array([1.5,1.25])
        self.assertTrue(np.all(self.map2[2, 2] == true_mapping))


    # FOURTH: increase step size and weight -> right direction
        config.color_direction_weight = 0.9
        config.spatial_direction_weight = 0.1
        config.color_pixel_step_size = 2
        config.spatial_pixel_step_size = 1

        # should pull towards 12,12
        spatial_dir = self.inpainter.compute_spatial_direction(y=2, x=2)

        # should pull left by 0.5
        color_dir = self.inpainter.compute_color_direction(y=2, x=2)

        self.inpainter.update_map(color_direction=color_dir, spatial_direction=spatial_dir, y=2, x=2)

        # the mapping should have moved by 0.1*(3,3) + 0.9*2*(0, -0.25) = (0.3, -0.15)
        true_mapping = np.array([9, 9]) + np.array([0.3, -0.15])
        self.assertTrue(np.all(self.map2[2, 2] == true_mapping))


    # Iteration 2
        self.map1[2,2] = [9.3, 8.85]

        # should pull towards 12,12 by 2.7 , 3.15
        spatial_dir = self.inpainter.compute_spatial_direction(y=2, x=2)

        # should pull left by 0.5
        color_dir = self.inpainter.compute_color_direction(y=2, x=2)

        self.inpainter.update_map(color_direction=color_dir, spatial_direction=spatial_dir, y=2, x=2)

        # the mapping should have moved by 0.1*(2.7,3.15) + 0.9*2*(0, -0.25) = (0.27, -0.135)
        true_mapping = np.array([9.3, 8.85]) + np.array([0.27, -0.135])
        self.assertTrue(np.all(self.map2[2, 2] == true_mapping))

    # Iteration 3
        self.map1[2, 2] = [9.57, 8.615]

        # should pull towards 12,12 by 2.43 , 3.385
        spatial_dir = self.inpainter.compute_spatial_direction(y=2, x=2)

        # should pull left by 0.5
        color_dir = self.inpainter.compute_color_direction(y=2, x=2)

        self.inpainter.update_map(color_direction=color_dir, spatial_direction=spatial_dir, y=2, x=2)

        # the mapping should have moved by 0.1*(2.43, 3.385) + 0.9*2*(0, -0.25) = (0.243, -0.111499)
        true_mapping = np.array([9.57, 8.615]) + np.array([0.243, 0.3385-0.9*0.5])
        self.assertTrue(np.all(self.map2[2, 2] == true_mapping))


    def setUp(self):
        """
        Initializes an Inpainter with a black image, an empty mask and two default maps
        :return:
        """
        self.map1 = Map(side=16)
        self.map2 = Map(side=16)
        self.map1.lookup_level = 0
        self.map2.lookup_level = 0

        self.background = np.zeros((16,16,4), dtype=np.uint8)
        self.mask = np.zeros((16,16))

        self.data_provider = DataProvider(size=16)
        self.data_provider.add_level(mask=self.mask, background=self.background)

        self.data_provider.maps = [self.map1, self.map2]
        self.inpainter = Inpainter(data_provider=self.data_provider, mipmap_level=0,
                                   color_neighbour_extractor=FourNeighbourExtractor(),
                                   spatial_neighbour_extractor=FourNeighbourExtractor())

        config.collect_data = False
        config.spatial_pixel_step_size = 1.0
        config.color_pixel_step_size = 1.0
        config.spatial_direction_weight = 0.5
        config.color_direction_weight = 0.5
        config.norm_threshold_factor = 1.0


