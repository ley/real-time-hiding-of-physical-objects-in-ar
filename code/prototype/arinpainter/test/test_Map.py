from unittest import TestCase
import numpy as np
from arinpainter.Map import Map

class TestMap(TestCase):
    def test_init(self):
        map = Map(side=8)
        self.assertEqual(len(map.maps), 3)
        self.assertTrue(np.all(map.maps[2] == self.true_map_2))
        self.assertTrue(np.all(map.maps[1] == self.true_map_4))

        self.assertRaises(Exception, Map, 0)
        self.assertRaises(Exception, Map, 3)
        self.assertRaises(Exception, Map, 55)


#create_maps
    def test_create_maps(self):
        map = Map(side=1)
        map.create_maps(side=4)
        self.assertTrue(np.all(map.maps[1] == self.true_map_2))
        self.assertTrue(np.all(map.maps[0] == self.true_map_4))

#create_level_map
    def test_create_level_map(self):
        level_map = self.map.create_level_map(side=2)
        self.assertTrue(np.all(level_map == self.true_map_2))

        level_map = self.map.create_level_map(side=4)
        self.assertTrue(np.all(level_map == self.true_map_4))
#get_item
    def test_get_item(self):
        self.assertTrue(np.all(self.map[0,0,0] == np.array([0,0])))
        self.assertTrue(np.all(self.map[0, 0, 1] == np.array([0, 1])))
        self.assertTrue(np.all(self.map[0, 5, 0] == np.array([1, 0])))
        self.assertTrue(np.all(self.map[0, 4, 7] == np.array([0, 1])))

        self.assertTrue(np.all(self.map[1, 0, 0] == np.array([0, 0])))
        self.assertTrue(np.all(self.map[1, 3, 2] == np.array([1, 0])))

        self.assertTrue(np.all(self.map[2, 0, 0] == np.array([0, 0])))
        self.assertTrue(np.all(self.map[2, 1, 1] == np.array([1, 1])))

        # self.assertEqual(self.map.lookup_level, 2)
        # self.assertTrue(np.all(self.map[0, 0] == np.array([0, 0])))
        #
        #
        # self.map.lookup_level = 0
        # self.assertEqual(self.map.lookup_level, 0)
        # self.assertTrue(np.all(self.map[0, 0] == np.array([0, 0])))
        # self.assertTrue(np.all(self.map[0, 1] == np.array([0, 1])))
        # self.assertTrue(np.all(self.map[5, 0] == np.array([1, 0])))
        # self.assertTrue(np.all(self.map[4, 7] == np.array([0, 1])))

#total_lookup
    def test_total_lookup(self):
        #level 2
        mapping = self.map.total_lookup(y=0,x=0, lookup_level=2)
        true_mapping = np.array([0,0])
        self.assertTrue(np.all(mapping == true_mapping))

        mapping = self.map.total_lookup(y=1, x=1, lookup_level=2)
        true_mapping = np.array([1, 1])
        self.assertTrue(np.all(mapping == true_mapping))

        #level 1
        mapping = self.map.total_lookup(y=0, x=0, lookup_level=1)
        true_mapping = np.array([0, 0])
        self.assertTrue(np.all(mapping == true_mapping))

        mapping = self.map.total_lookup(y=1, x=1, lookup_level=1)
        true_mapping = np.array([1, 1])
        self.assertTrue(np.all(mapping == true_mapping))

        mapping = self.map.total_lookup(y=2, x=2, lookup_level=1)
        true_mapping = np.array([2, 2])
        self.assertTrue(np.all(mapping == true_mapping))

        mapping = self.map.total_lookup(y=2, x=3, lookup_level=1)
        true_mapping = np.array([2, 3])
        self.assertTrue(np.all(mapping == true_mapping))

        #level 0
        mapping = self.map.total_lookup(y=0, x=0, lookup_level=0)
        true_mapping = np.array([0, 0])
        self.assertTrue(np.all(mapping == true_mapping))

        mapping = self.map.total_lookup(y=0, x=1, lookup_level=0)
        true_mapping = np.array([0, 1])
        self.assertTrue(np.all(mapping == true_mapping))

        mapping = self.map.total_lookup(y=5, x=2, lookup_level=0)
        true_mapping = np.array([5, 2])
        self.assertTrue(np.all(mapping == true_mapping))

        mapping = self.map.total_lookup(y=7, x=0, lookup_level=0)
        true_mapping = np.array([7, 0])
        self.assertTrue(np.all(mapping == true_mapping))

    def test_total_lookup_mod(self):
        y = 0
        x = 0
        self.map.maps[2][y, x] = np.array([10, 10])
        self.assertTrue(np.all(self.map.total_lookup(y=y,x=x, lookup_level=0) == np.array([40,40]) ))
        self.assertTrue(np.all(self.map.total_lookup(y=y, x=x, lookup_level=1) == np.array([20, 20])))
        self.assertTrue(np.all(self.map.total_lookup(y=y, x=x, lookup_level=2) == np.array([10, 10])))

    def test_total_lookup_mod_2(self):
        y = 1
        x = 0
        self.map.maps[2][y, x] = np.array([11, 10])
        self.assertTrue(np.all(self.map.total_lookup(y=0, x=0, lookup_level=0) == np.array([0, 0])))
        self.assertTrue(np.all(self.map.total_lookup(y=y, x=x, lookup_level=0) == np.array([1, 0])))
        self.assertTrue(np.all(self.map.total_lookup(y=4, x=1, lookup_level=0) == np.array([44, 41])))
        self.assertTrue(np.all(self.map.total_lookup(y=4, x=x + 3, lookup_level=0) == np.array([44, 43])))
        self.assertTrue(np.all(self.map.total_lookup(y=3, x=0, lookup_level=1) == np.array([23, 20])))
        self.assertTrue(np.all(self.map.total_lookup(y=2, x=1, lookup_level=1) == np.array([22, 21])))
        self.assertTrue(np.all(self.map.total_lookup(y=y, x=x, lookup_level=2) == np.array([11, 10])))
#setitem
    def test_setitem(self):
        true_value = np.array([10,10])
        self.map[2,0,0] = true_value
        self.assertTrue(np.all(self.map.maps[2][0,0] == true_value))
        self.assertTrue(np.all(self.map.maps[0][0, 0] == np.zeros((2))))

        self.map[0, 1, 7] = true_value
        self.assertTrue(np.all(self.map.maps[0][1, 7] == true_value))

    def test_setitem_total_level2(self):
        self.assertEqual(self.map.lookup_level,2)
        true_value = np.array([9,9])
        self.map[0,0] = true_value
        self.assertTrue(np.all(self.map.maps[2][0,0] == true_value))
        self.assertTrue(np.all(self.map.maps[1][0, 0] == np.zeros(2)))

        self.map[1, 1] = true_value
        self.assertTrue(np.all(self.map.maps[2][1, 1] == true_value))
        self.assertTrue(np.all(self.map.maps[1][3, 3] == np.array([1,1])))

    def test_setitem_total_level1(self):
        self.map.lookup_level = 1
        self.assertEqual(self.map.lookup_level, 1)
        true_value = np.array([10, 10])
        self.map[0, 0] = true_value
        self.assertTrue(np.all(self.map.maps[2][0, 0] == np.zeros(2)))#unchanged
        self.assertTrue(np.all(self.map.maps[1][0, 0] == true_value))
        self.assertTrue(np.all(self.map.maps[0][0, 0] == np.zeros(2)))#unchanged

        self.map[2, 2] = true_value
        self.assertTrue(np.all(self.map.maps[2][0, 0] == np.zeros(2)))#unchanged
        self.assertTrue(np.all(self.map.maps[1][2, 2] == np.array([8,8])))
        self.assertTrue(np.all(self.map.maps[0][4, 4] == np.array([0, 0])))#unchanged

    def test_setitem_total_level0(self):
        self.map.lookup_level = 0
        self.assertEqual(self.map.lookup_level, 0)
        true_value = np.array([10, 10])
        self.map[0, 0] = true_value

        self.assertTrue(np.all(self.map.maps[2][0, 0] == np.zeros(2)))#unchanged
        self.assertTrue(np.all(self.map.maps[1][0, 0] == np.zeros(2)))#unchanged
        self.assertTrue(np.all(self.map.maps[0][0, 0] == true_value))

        self.map[6, 5] = true_value

        self.assertTrue(np.all(self.map.maps[2][1, 1] == np.array([1,1]) )) #unchanged
        self.assertTrue(np.all(self.map.maps[1][3, 2] == np.array([1,0]) ))#unchanged
        a = self.map.maps[0][6, 5]
        self.assertTrue(np.all(self.map.maps[0][6, 5] == np.array([4,6]) ))

    #levels
    def test_levels(self):
        map = Map(side=1)
        self.assertEqual(map.levels, 0)
        map = Map(side=16)
        self.assertEqual(map.levels, 4)

    def test_size_at_level(self):
        map = Map(side=16)
        self.assertEqual(map.size_at_level(0), 16)
        map.lookup_level = 0
        self.assertEqual(map.size_at_level(), 16)

        self.assertEqual(map.size_at_level(2), 4)
        map.lookup_level = 2
        self.assertEqual(map.size_at_level(), 4)
#setup
    def setUp(self):
        self.map = Map(side=8)
        self.true_map_2 = np.array([
            [[0,0], [0,1]],
            [[1, 0], [1, 1]]
        ])

        self.true_map_4 = np.array([
            [[0, 0], [0, 1], [0, 0], [0, 1]],
            [[1, 0], [1, 1], [1, 0], [1, 1]],
            [[0, 0], [0, 1], [0, 0], [0, 1]],
            [[1, 0], [1, 1], [1, 0], [1, 1]]
        ])