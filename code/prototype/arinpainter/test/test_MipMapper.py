from unittest import TestCase
import numpy as np
from arinpainter.MipMapper import MipMapper
class TestMipMapper(TestCase):



#mipmaps
    def test_mipmaps(self):
        self.mipper.limit_size = 1
        true_images = [self.true_level1, self.true_level2]
        true_masks = [self.true_mask_level1, self.true_mask_level2]
        index = 0
        for mipmap_data in self.mipper.mipmaps(mask=self.mask, background=self.image):
            self.assertTrue(np.all(mipmap_data.background == true_images[index]))
            self.assertTrue(np.all(mipmap_data.mask == true_masks[index]))
            index += 1

#mipmap_it
    def test_mipmap_mask(self):
        level1 = self.mipper.mipmap_mask(self.mask)

        self.assertTrue(np.all(level1 == self.true_mask_level1))

        level2 = self.mipper.mipmap_mask(level1)

        self.assertTrue(np.all(level2 == self.true_mask_level2))

    def test_mipmap_background(self):
        level1 = self.mipper.mipmap_background(self.image)

        self.assertTrue(np.all(level1 == self.true_level1))

        level2 = self.mipper.mipmap_background(level1)

        self.assertTrue(np.all(level2 == self.true_level2))

    def setUp(self):
        self.mipper = MipMapper()
        self.image = np.array([
            [[255,0,0,0],[255,0,0,0],[0,255,0,0],[0,255,0,0]],
            [[255, 0, 0, 0], [255, 0, 0, 0], [0, 255, 0, 0], [0, 255, 0, 0]],
            [[255, 0, 0, 0], [255, 0, 0, 0], [0, 255, 0, 150], [0, 255, 0, 50]],
            [[0, 0, 255, 0], [0, 0, 255, 0], [0, 255, 0, 150], [0, 255, 0, 50]],
        ], dtype=np.uint8)
        self.true_level1 = np.array([
            [[255,0,0,0],[0,255,0,0]],
            [[127,0,127,0],[0,255,0,100]]
        ])
        self.true_level2 = np.array([95,127,31,25])

        self.mask = np.array([
            [1, 1, 0, 0],
            [1, 1, 0, 0],
            [0, 0, 0, 0],
            [1, 1, 1, 0]
        ])
        self.true_mask_level1 = np.array([
            [1,0],
            [1,1]
        ])
        self.true_mask_level2 = np.array([1])