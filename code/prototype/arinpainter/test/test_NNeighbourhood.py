import pyximport;
import numpy as np
include = np.get_include()
pyximport.install(setup_args={'include_dirs': include})

from unittest import TestCase
from arinpainter.NNeighbourhoodExtractor import NNeighbourhoodExtractor

class TestNNeighbourhoodExtractor(TestCase):

    def test_create_3(self):
        extractor = NNeighbourhoodExtractor(size=3)
        true_neighbourhood = np.array([
            [[-1,-1], [-1,0], [-1, 1]],
            [[0, -1],[0, 0],[0, 1]],
            [[1, -1],[1, 0], [1,1]]
        ])

        self.assertTrue(np.all(extractor.neighbourhood == true_neighbourhood))
        self.assertEqual(extractor.size, 3)
        self.assertEqual(extractor.half_size, 1)

    def test_create_5(self):
        extractor = NNeighbourhoodExtractor(size=5)
        true_neighbourhood = np.array([
            [[-2, -2], [-2, -1], [-2, 0], [-2, 1], [-2, 2]],
            [[-1, -2],[-1, -1], [-1, 0], [-1, 1], [-1, 2]],
            [[0, -2], [0, -1], [0, 0], [0, 1], [0, 2]],
            [[1, -2],[1, -1], [1, 0], [1, 1], [1, 2]],
            [[2, -2], [2, -1], [2, 0], [2, 1], [2, 2]]
        ])
        self.assertTrue(np.all(extractor.neighbourhood == true_neighbourhood))
        self.assertEqual(extractor.size, 5)
        self.assertEqual(extractor.half_size, 2)

    def test_create_7(self):
        extractor = NNeighbourhoodExtractor(size=7)
        true_neighbourhood = np.array([
            [[-3, -3], [-3, -2], [-3, -1], [-3, 0], [-3, 1], [-3, 2], [-3, 3]],
            [[-2,-3], [-2, -2], [-2, -1], [-2, 0], [-2, 1], [-2, 2], [-2, 3]],
            [[-1,-3], [-1, -2],[-1, -1], [-1, 0], [-1, 1], [-1, 2], [-1, 3]],
            [[0,-3], [0, -2], [0, -1], [0, 0], [0, 1], [0, 2], [0, 3]],
            [[1,-3], [1, -2],[1, -1], [1, 0], [1, 1], [1, 2], [1, 3]],
            [[2,-3], [2, -2], [2, -1], [2, 0], [2, 1], [2, 2], [2, 3]],
            [[3, -3], [3, -2], [3, -1], [3, 0], [3, 1], [3, 2], [3, 3]]
        ])
        self.assertTrue(np.all(extractor.neighbourhood == true_neighbourhood))
        self.assertEqual(extractor.size, 7)
        self.assertEqual(extractor.half_size, 3)

    def test_simple_3(self):
        extractor = NNeighbourhoodExtractor(size=3)
        neighbours = extractor.get_neighbours(y=2, x=2, map_size=9, as_patch=True)
        true_neighbours = np.array([
            [[1, 1], [1, 2], [1, 3]],
            [[2, 1], [-1, -1], [2, 3]],
            [[3, 1], [3, 2], [3, 3]],
        ])
        self.assertTrue(np.all(neighbours == true_neighbours))

        neighbours = extractor.get_neighbours(y=2, x=2, map_size=9, as_patch=False)
        true_neighbours = np.array([
            [1, 1], [1, 2], [1, 3],[2, 1], [2, 3],[3, 1], [3, 2], [3, 3]
        ])
        self.assertTrue(np.all(neighbours == true_neighbours))

    def test_simple_5(self):
        extractor = NNeighbourhoodExtractor(size=5)
        neighbours = extractor.get_neighbours(y=2, x=2, map_size=9, as_patch=True)
        true_neighbours = np.array([
            [[0, 0], [0, 1], [0, 2], [0, 3], [0, 4]],
            [[1, 0], [1, 1], [1, 2], [1, 3], [1, 4]],
            [[2, 0], [2, 1], [-1, -1], [2, 3], [2, 4]],
            [[3, 0], [3, 1], [3, 2], [3, 3], [3, 4]],
            [[4, 0], [4, 1], [4, 2], [4, 3], [4, 4]],
        ])
        self.assertTrue(np.all(neighbours == true_neighbours))

        neighbours = extractor.get_neighbours(y=2, x=2, map_size=9, as_patch=False)
        true_neighbours = np.array([
            [0, 0], [0, 1], [0, 2], [0, 3], [0, 4],
            [1, 0], [1, 1], [1, 2], [1, 3], [1, 4],
            [2, 0], [2, 1], [2, 3], [2, 4],
            [3, 0], [3, 1], [3, 2], [3, 3], [3, 4],
            [4, 0], [4, 1], [4, 2], [4, 3], [4, 4],
        ])
        self.assertTrue(np.all(neighbours == true_neighbours))

    def test_edge_3(self):
        extractor = NNeighbourhoodExtractor(size=3)
        neighbours = extractor.get_neighbours(y=1, x=0, map_size=9, as_patch=True)
        true_neighbours = np.array([
            [[-1, -1], [0, 0], [0, 1]],
            [[-1, -1], [-1, -1], [1, 1]],
            [[-1, -1], [2, 0], [2, 1]],
        ])
        self.assertTrue(np.all(neighbours == true_neighbours))

        neighbours = extractor.get_neighbours(y=1, x=2, map_size=3, as_patch=True)
        true_neighbours = np.array([
            [[0, 1], [0, 2], [-1, -1]],
            [[1, 1], [-1, -1], [-1, -1]],
            [[2, 1], [2, 2], [-1, -1]],
        ])
        self.assertTrue(np.all(neighbours == true_neighbours))

        neighbours = extractor.get_neighbours(y=1, x=0, map_size=9, as_patch=False)
        true_neighbours = np.array([
            [0,0], [0,1],[1, 1], [2, 0], [2, 1]
        ])
        self.assertTrue(np.all(neighbours == true_neighbours))

        neighbours = extractor.get_neighbours(y=1, x=2, map_size=3, as_patch=False)
        true_neighbours = np.array([
            [0, 1], [0, 2], [1, 1], [2, 1], [2, 2]
        ])
        self.assertTrue(np.all(neighbours == true_neighbours))

    def test_edge_3_double(self):
        extractor = NNeighbourhoodExtractor(size=3)
        neighbours = extractor.get_neighbours(y=1.1, x=0.9, map_size=3, as_patch=True)
        true_neighbours = np.array([
            [[-1, -1], [0.1, 0.9], [0.1, 1.9]],
            [[-1, -1], [-1, -1], [1.1, 1.9]],
            [[-1, -1], [-1, -1], [-1, -1]],
        ])
        self.assertTrue(np.all(np.abs(neighbours - true_neighbours) < 0.0001))

        neighbours = extractor.get_neighbours(y=0.5, x=1.9, map_size=3, as_patch=True)
        true_neighbours = np.array([
            [[-1, -1], [-1, -1], [-1, -1]],
            [[0.5, 0.9], [-1, -1], [-1, -1]],
            [[1.5, 0.9], [1.5, 1.9], [-1, -1]],
        ])
        self.assertTrue(np.all(np.abs(neighbours - true_neighbours) < 0.0001))

    def test_corner_3(self):
        extractor = NNeighbourhoodExtractor(size=3)
        neighbours = extractor.get_neighbours(y=1, x=0, map_size=9, as_patch=True)
        true_neighbours = np.array([
            [[-1, -1], [0, 0], [0, 1]],
            [[-1, -1], [-1, -1], [1, 1]],
            [[-1, -1], [2, 0], [2, 1]],
        ])
        self.assertTrue(np.all(neighbours == true_neighbours))

        neighbours = extractor.get_neighbours(y=1, x=0, map_size=9, as_patch=False)
        true_neighbours = np.array([
            [0,0], [0,1],[1, 1], [2, 0], [2, 1]
        ])
        self.assertTrue(np.all(neighbours == true_neighbours))