from unittest import TestCase
import numpy as np
from arinpainter.NineNeighbourExtractor import NineNeighbourExtractor
from arinpainter.Map import Map

class TestNineNeighbourExtractor(TestCase):

# get neighbours
    def test_get_neighbours(self):
        neighbours = self.extractor.get_neighbours(y=0,x=0, map_size=self.map.size_at_level(level=0))
        true_neighbours = np.array([[0,1],[1,0],[1,1]])
        self.assertTrue(np.all(neighbours == true_neighbours))

        neighbours = self.extractor.get_neighbours(y=1, x=1, map_size=self.map.size_at_level(level=0))
        true_neighbours = np.array([[0,0],[0, 1],[0,2],[1,0],[1,2],[2, 0],[2,1],[2,2]])
        self.assertTrue(np.all(neighbours == true_neighbours))

        neighbours = self.extractor.get_neighbours(y=0, x=1, map_size=self.map.size_at_level(level=0))
        true_neighbours = np.array([[0, 0],[0, 2], [1,0],[1, 1],[1,2]])
        self.assertTrue(np.all(neighbours == true_neighbours))

    def test_get_neighbours_as_patch(self):
        neighbours = self.extractor.get_neighbours(y=0, x=0, map_size=self.map.size_at_level(level=0), as_patch=True)

        true_neighbours = -np.ones((3,3,2))
        true_neighbours[1,2] = [0,1]
        true_neighbours[2, 1] = [1, 0]
        true_neighbours[2, 2] = [1, 1]
        self.assertTrue(np.all(neighbours == true_neighbours))

        neighbours = self.extractor.get_neighbours(y=1, x=1, map_size=self.map.size_at_level(level=0), as_patch=True)
        true_neighbours = -np.ones((3, 3, 2))
        true_neighbours[0, 0] = [0, 0]
        true_neighbours[0, 1] = [0, 1]
        true_neighbours[0, 2] = [0, 2]
        true_neighbours[1, 0] = [1, 0]
        true_neighbours[1, 2] = [1, 2]
        true_neighbours[2, 0] = [2, 0]
        true_neighbours[2, 1] = [2, 1]
        true_neighbours[2, 2] = [2, 2]
        self.assertTrue(np.all(neighbours == true_neighbours))

        neighbours = self.extractor.get_neighbours(y=0, x=1, map_size=self.map.size_at_level(level=0), as_patch=True)

        true_neighbours = -np.ones((3, 3, 2))
        true_neighbours[1, 0] = [0, 0]
        true_neighbours[1, 2] = [0, 2]
        true_neighbours[2, 0] = [1, 0]
        true_neighbours[2, 1] = [1, 1]
        true_neighbours[2, 2] = [1, 2]
        self.assertTrue(np.all(neighbours == true_neighbours))

    # set up
    def setUp(self):

        self.extractor = NineNeighbourExtractor()

        self.map = Map(side=4)
        self.map.lookup_level = 0
