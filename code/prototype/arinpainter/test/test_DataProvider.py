from unittest import TestCase
import numpy as np
from arinpainter.DataProvider import DataProvider
from arinpainter.Map import Map

class TestDataProvider(TestCase):
#add_level
    def test_add_level(self):
        mask = np.zeros((4,4,4))
        background = np.ones((4,4,4), dtype=np.uint8)

        level = self.provider.add_level(mask=mask, background=background)

        self.assertEqual(level,0)
        self.assertTrue(np.all(self.provider.masks[0] == mask))
        self.assertTrue(np.all(self.provider.backgrounds[0] == background))
        self.assertEqual(self.provider.levels, 1)

        mask = np.zeros((2, 2,4))
        background = np.ones((2, 2, 4), dtype=np.uint8)
        level = self.provider.add_level(mask=mask, background=background)
        self.assertEqual(level, 1)

        self.assertTrue(np.all(self.provider.masks[1] == mask))
        self.assertTrue(np.all(self.provider.backgrounds[1] == background))
        self.assertEqual(self.provider.levels, 2)

#Test different ways of accessing a np.array through DataProvider. Is it really working with references in all these ways? Apparently yes!
    def test_access(self):
        map = np.array([0,0])
        self.provider.maps.append(map)
        self.provider.maps[0][0] = 1

        def get_map():
            return map

        true_map = np.array([1,0])
        self.assertTrue(np.all(self.provider.maps[0] == true_map))
        self.assertTrue(np.all(map == true_map))
        self.assertTrue(np.all(get_map() == true_map))

        get_map()[1] = 1
        true_map = np.array([1, 1])
        self.assertTrue(np.all(self.provider.maps[0] == true_map))
        self.assertTrue(np.all(map == true_map))
        self.assertTrue(np.all(get_map() == true_map))

        map[0] = 2
        true_map = np.array([2, 1])
        self.assertTrue(np.all(self.provider.maps[0] == true_map))
        self.assertTrue(np.all(map == true_map))
        self.assertTrue(np.all(get_map() == true_map))

#create_maps
    def test_create_maps_size_2(self):
        self.provider.create_maps(size=2)
        map = Map(side=2)
        self.assertTrue(np.all(self.provider.maps[0] == map))
        self.assertTrue(np.all(self.provider.maps[1] == map))

    def test_create_maps_size_4(self):
        self.provider.create_maps(size=4)
        map = Map(side=4)
        self.assertTrue(np.all(self.provider.maps[0] == map))
        self.assertTrue(np.all(self.provider.maps[1] == map))

# #previous_levels_last_map
#     def test_previous_levels_last_map(self):
#         self.provider.add_level(mask=np.zeros((4, 4, 4)), background=np.zeros((4, 4, 4)))
#         self.provider.add_level(mask=np.zeros((2, 2, 4)), background=np.zeros((2, 2, 4)))
#         true_map = np.array([
#             [[0, 0], [0, 1], [0, 2], [0, 3]],
#             [[1, 0], [1, 1], [1, 2], [1, 3]],
#             [[2, 0], [2, 1], [2, 2], [2, 3]],
#             [[3, 0], [3, 1], [3, 2], [3, 3]]
#         ])
#
#         last_map = self.provider.previous_levels_last_map(level=1)
#         self.assertTrue(np.all(last_map == true_map))
#
#         pass

#make_maps_consistent
    def test_make_maps_consistent(self):
        self.provider.create_maps(size=4)
        original_value = np.array([0,0])
        changed_value = np.array([100, 100])
        self.provider.maps[0][0,0] = changed_value

        self.assertTrue(np.all(self.provider.maps[1][0, 0] == original_value))
        self.assertTrue(np.all(self.provider.maps[0][0, 0] == changed_value))

        self.provider.make_maps_consistent(last_updated_map_index=0)

        self.assertTrue(np.all(self.provider.maps[1][0, 0] == changed_value))
        self.assertTrue(np.all(self.provider.maps[0][0, 0] == changed_value))

#parse_string_pair
    def test_parse_string_pair(self):
        line = "'(512.4', ' 581.2)'"
        parsed = self.provider.parse_string_pair(line=line)
        self.assertTrue(np.all(parsed == [512.4,581.2]))

        line = "'(512', ' 581.2)'"
        parsed = self.provider.parse_string_pair(line=line)
        self.assertTrue(np.all(parsed == [512, 581.2]))

        line = "'(512.4', ' 581)'"
        parsed = self.provider.parse_string_pair(line=line)
        self.assertTrue(np.all(parsed == [512.4, 581]))

        line = "'(512', ' 581)'"
        parsed = self.provider.parse_string_pair(line=line)
        self.assertTrue(np.all(parsed == [512, 581]))

#add_metadata
    def test_add_metadata(self):
        self.provider = DataProvider(size=1024)

        line = [['04_16_12_30_21_253369', 'center_of_mass', '(512.4, 581.2)']]
        self.provider.add_metadata(metadata_lines=line)
        self.assertTrue(np.all(self.provider.center_of_mass == [1024-581.2, 512.4]))

# center_of_mass_at_level
    def test_center_of_mass_at_level(self):
        self.provider.center_of_mass = np.array([128,256])

        com = self.provider.center_of_mass_at_level(level=0)
        self.assertTrue(np.all(com == [128,256]))

        com = self.provider.center_of_mass_at_level(level=1)
        self.assertTrue(np.all(com == [64, 128]))


#setup
    def setUp(self):
        self.provider = DataProvider(size=4)