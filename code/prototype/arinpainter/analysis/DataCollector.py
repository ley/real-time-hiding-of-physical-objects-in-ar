from arinpainter.Config import config
import numpy as np

class DataCollector:
    mipmap_level = 0
    data_collections = {}

    def next_level(self, level, size):
        self.mipmap_level = level
        self.data_collections[level] = DataCollection(size=size)

    @property
    def collection(self):
        return self.data_collections[self.mipmap_level]

    @classmethod
    def create_instance(cls):
        DataCollector.instance = DataCollector()

class DataCollection:

    def __init__(self, size):
        iterations = config.iterations + 1
        # The normalized spatial gradient for given pixel
        self.spatial_gradient = np.zeros((iterations,size,size, 2))

        # Color gradient after normalization
        self.color_gradient = np.zeros((iterations,size,size, 2))

        # The weighted sum of color and spatial gradient
        self.mixed_gradient = np.zeros((iterations,size,size, 2))

        # # Vector pushing out of the hole. 0,0 if not applicable
        # self.out_of_hole_vector = np.zeros((iterations,size,size, 2))

        # coordinate targeted by spatial gradient
        self.spatial_target = np.zeros((iterations, size, size, 2))

        # coordinate targeted by color gradient
        self.color_target = np.zeros((iterations, size, size, 2))

        # 1 if the mapping BEFORE applying push-out-of-hole was invalid/out of bounds. 0 elsewise
        self.out_of_bounds_before_hole = np.zeros((iterations,size,size))

        # 1 if the mapping AFTER applying push-out-of-hole was invalid/out of bounds. 0 elsewise
        self.out_of_bounds_after_hole = np.zeros((iterations,size,size))

        # The final mapping update vector
        self.final_vector = np.zeros((iterations,size,size, 2))

        # mapping BEFORE updating
        self.old_mapping = np.zeros((iterations,size,size, 2))

        # mapping AFTER updating
        self.new_mapping = np.zeros((iterations,size,size, 2))

        # Whether the pixel is part of the hole
        self.inside_hole = np.zeros((iterations,size,size))

        # Whether the pixel is mapped into the hole
        self.mapped_into_hole = np.zeros((iterations, size, size))

        # color error
        self.color_error = np.zeros((iterations, size, size))

        # spatial error
        self.spatial_error = np.zeros((iterations, size, size))

        # out-of-hole error
        self.out_of_hole_error = np.zeros((iterations, size, size))

        self.total_error = np.zeros((iterations, size, size))