# Prototype
This is python project is used to prototype and try ideas for the final inpainting algorithm.

## Data sources
No realtime video data is used, only image files are used. Each frame is represented by two images.
The background and the mask.
They must be .png images and have the same name, except for the prefix 'mask_' or 'background_', so they can be matched correctly.

e.g. 'background_03_21_10_27_41_293102_1024_790_20.png' and 'mask_03_21_10_27_41_293102_1024_790_20.png'

## Coding constraints
All code must simulate the GPUs behaviour, so limited memory access is possible.
Each iteration and pixel (or pixel block) must be handled independently or such that thex could easily be handled independently.

## Dependencies
 - python 3.6
 - SciPy 1.0.1
 - NumPy 1.11.2
 - matplotlib 2.2.2
 - Pillow 5.1.0
 - imageio 2.3.0


 If cythonify is not finding the numpy headers: https://gist.github.com/hannes-brt/757191