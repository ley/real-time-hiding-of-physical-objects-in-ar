___PRESENTATION___:
 
 _INTODUCTION_
	- Motivate object removal. name some applications
	- Show example: Bunny removal (overlay virtual bunny?)
	- Explain Inpainting
 
 _RELATED WORK_
	- Explain Inpainting and the two most common approaches:
		- Diffusion
		- Patch-based
	- Video inpainting:
		- Explain how video inpainting can use more images and should be consistent/coherent
	- PixMix:
		- Short intoduction of PixMix with cost function
		
 _OUR APPROACH_:
	- Explain gradinet descent changes made
	- Give overview of the algorithm (step by step images?)
	
 _RESULTS_:
	- Show some good results
	- Show impact of interpolation/alpha

 _CONCLUSION_:
	- We are satisfied: blabla

 _FUTURE_WORK_:
	- a selection from the report

QUESTIONS:
 - When live demo?
 - How much math? How to make it short? Images that help?
 - What is really cool about it/will make people pay attention?
	
	
___TODO___:
 - Formula of cost functions: robust norm
 - Out-of-hole slide: Re-include it, if the presentation is not too long already. At least mention it somwhere.
 