% set counter to n-1:
\setcounter{chapter}{1}

\chapter{Related Work}
	To identify the most promising approach to realize real-time object removal, we start with an overview of the past research in the field.
	First, we discuss various solutions to the image inpainting and extensions to video inpainting.
	We also briefly discuss the problem of quality assessment in the context of image inpainting.
	In the second part, we evaluate the presented solutions and identify the one most suitable for our purpose.
	Finally, we discuss the PixMix algorithm introduced by Herling and Broll \cite{pixmix2014}.
	
	\section{Image Inpainting}
		The inpainting problem deals with finding the colors of missing pixels in an image.
		An image $I$ is decomposed into two disjoint areas, one with known pixel values called the source area $S$ and one for which the pixel values are missing named target area $T$ or simply \emph{hole}.
		Parts of the image can be missing because of the image has been corrupted, or they are intentionally removed to omit a certain fragment from the image.
		
		A solution to this problem can only be qualified by a human observer and thus the notion of a \emph{correct solution} is not strictly applicable.
		Moreover, there are typically several ways to solve the problem that lead to a comparable quality.
		Therefore, the inpainting problem is ill-posed and we need to make additional presumptions in order to have a more concise guidance when searching for the best solution.
		
		The inpainting problem has its roots in texture synthesis, which deals with generating new texture patches that resemble some template image.
		A large number of algorithms to solve the inpainting problem have been proposed in recent years.
		These approaches can roughly be classified into two groups: The first group uses partial differential equations (PDE) to solve a diffusion equation, which describes the transport of the known pixel colors into the hole.
		The second identifies image patches from the source area and copies them into the target area to cover the hole.
		
		\subsection{Diffusion-based Algorithms}
			When defining a \emph{good} inpainting, a possible definition is to have a smooth color transition from the source area into the hole.
			This can be achieved using PDEs, an approach first used in image filtering \cite{Weickert,PeronaScaleSpace} with the goal of removing noise or upscaling images without losing high frequency details or introducing artifacts. 
			A second desirable property is that structures, like lines, should be extended from the border of the hole into the hole.
			Structures can be described as areas of constant color intensity and the direction of the smallest intensity change is called the \emph{isophote}.
			Anisotropic diffusion has been used by Bertalmio et al. \cite{Bertalmio:2000} to continue lines into the hole while preserving their orientation.
			Other approaches were inspired by PDEs used in physics to describe heat diffusion or from fluid-dynamics, for example the work of Bertalmio et al. \cite{NavierStokes}, which uses the \emph{Navier-Stokes} equation.
			
			While these approaches perform well on images with mostly uniform areas and structures, they cannot reconstruct texture. The diffusion suppresses high frequencies and leads to blurring.\TODO{Reference?}
			
%			\begin{itemize}
%%				\item Smoothness priors: Parametric models (example?) or PDEs (example?)
%%				\item Originating in image filtering: Diffusion technics. 
%%				\item Parametric models suitable fort continuing structures through hole: Lines, curves, avoid unconnected edges. Not good for texture tend to blur (Guillemot and Le Meur p.2 first paragraph)
%				\item Isophotes: Lines of constant intensity. [The discretized gradient vector gives the direction of largest spatial changes, while its 90[deg] rotation is the direction of smallest spatial changes,			hence, of the isophotes, \cite{Guillemot}]
%				\item Linear PDEs: Heat diffusion -> suppresses high frequencies, thus blurs
%				\item Navier-Stokes PDE from fluid-dynamics \cite{NavierStokes}. continues isophotes.
%%	 			\item THE inpainting paper: [prolong the isophote lines arriving at, while maintaining the angle of “arrival"] using PDEs \cite{Bertalmio:2000}.
%	 			\item In general: No texture created, blurry. Not good for large holes
%			\end{itemize}
		
		\subsection{Patch-based Algorithms} \label{related_work:patch_based}
			The following section is mostly based on \emph{A survey of the state-of-the-art in patch-based synthesis} by  Barnes and Zhangrnes \cite{barnes2017survey} which provides an excellent overview on the topic.
			
			Another approach to the inpainting problem uses the notion of pixel patches.
			A patch is a small texture sample, typically only a few pixels in width and height.
			This approach requires a solution to the inpainting problem to only use texture material present in the image and therefore only pixel patches from the source area to fill the hole.
			The aim is to capture the image's characteristic appearance and synthesize new texture that embodies these characteristics.
			This is achieved by copying patches from the source to the target area.
			
			Patch-based algorithms typically incorporate two steps: \emph{Patch matching} and \emph{pixel synthesis}.
			Patch matching is the process of finding good candidate patches to fill the hole with.
			This is typically done by looking for patches that minimize a suitable cost function.
%			Such a cost function that can be applied to patches or pixels.
			Greedy minimization strategies start copying patches into the hole starting at the the border of the hole and move inwards shell by shell.
			Once all target pixels are covered by a patch, the algorithm terminates.
			Iterative strategies start with an initial matching and then repeatedly keep looking for better patches.
			No matter what strategy is used, matching patches always includes searching for a good candidate.
			The naive approach is to perform an exhaustive search on all candidate pixels and pick the best. However, this is very slow and thus only feasible for small images.
			Other approaches like \emph{PatchMatch} \cite{patchmatch:barnes} by Barnes et al. randomly try patches and once a suitable candidate is found, this information is propagated to neighboring patches.
			Locality sensitive hashing applies binning on the patches using a hash function, which captures similarity of patches.
			Korman and Avidan \cite{cshashing:korman:avidan} use this strategy to speed up \emph{PatchMatch}.
			Another technique uses tree data structures. A dimensionality reduction is applied to the patches and these are then inserted into a space-partitioning data structure, for example a \emph{kd-tree}, which can be to perform a nearest neighbor lookup in the patch appearance space.\TODO{Reference?}
			
			The source of patches can be extended to more than one image.
			Using entire image collections can yield better patches than a single image.
			Hayes and Efros \cite{Hays:2007} used a collection of millions of photographs.
			These images were first grouped according to their scene semantic using the context-based place and object recognition system \cite{gist} proposed by Torralba et al.
			Finding a patch is composed of first finding the scene group, the input image belongs, and then looking for good patches within that group.
			
			Patch-based approaches are capable of reproducing texture accurately.
			The problem formulation using a cost function allows crafting a large variety of desired properties of the resulting inpainting.
			Patch-based approaches typically outperform diffusion-based techniques in most scenarios, especially when the hole size is more than a few pixels wide and texture becomes crucial.
			However, if a structure needs to be reproduced, which is not available as a patch in the patch search space, diffusion techniques can outperform them \cite{overview:Guillemot}.
					
		\subsection{Hybrid Methods}
			Bertalmio et al. \cite{SimultaneousBertalmio} proposed a hybrid technique that combines the strength of reconstruction texture of the patch-based approaches with the ability to continue structures into the hole of diffusion techniques.
			The image is split into a texture and a structure layer, which when overlaid result in the original image.
			A patch-based approach used for the texture layer and a diffuse for the structure layer. Both results are then recombined.
			
%		\subsection{PatchMatch}
%			PatchMatch paper, because PixMix is based on it and the k-NN approach is similar to the suggestion of keeping the N best mappings for a pixel mentioned in future work)
%			\cite{patchmatch:barnes}
%\TODO{write this?}

	\section{Video Inpainting}\label{related_work:video}
		Video inpainting is the extension of image inpainting onto a collection of subsequent video frames.
		In comparison to image inpainting, the temporal order of the frames can be used to guide the process.
		If this is done off-line, the entire collection of frames is available when searching for good patches.
		
		The work done by Granados et al. \cite{GranadosVideoInpainting} combines frames, which show a part of the background behind the object they want to remove.
		These frames are combined and the true background is restored.
		In a first step, objects with a similar scene depth are tracked across the frames by estimating the homographies describing the transformation from one frame to the next.
		They assume these objects to be mostly planar, which allows to get depth information without having to reconstruct the entire 3D scene.
		For each target pixel, they identify a frame that shows the background for this pixel, as it would be seen from the perspective of the image to be inpainted.
		
%				 - Align frames pairwise. Arrange scene in planes at different depth and find homographies to map planes. This way the different scene-objects at different depth can be mapped without having to reconstruct the entire 3D scene. Use Graph-Cuts for this somehow.
%			- For each hole-pixel find the closest (perspective and illumination) 'source frame' that shows the background for this pixel.
%		- Essentially copying known true background from other frames.
%		They state a processing time of up to four hours for processing 100 frames on a server using 64 logical processors. Since this approach is working off-line, the inpainting of any given frame can use past and future frames.
%		As an approximation the background is decomposed into planes and the 
		
		Another approach to video inpainting proposed by Hocking et al. is \emph{Guidefill} \cite{Guidefill}.
		They address the problem of 2D to 3D conversion of movies.
		To produce a stereoscopic experience, the scenes must be rendered from a second perspective.
		Since this is an ill-posed problem, areas without information from the original frames must be reconstructed.
		These are generally small gaps and using a diffusion-based technique that transports colors along guiding splines, similar to the isophotes, produces satisfying results.
		
		\emph{PixMix} \cite{pixmix2014} by  Herling and Broll is another approach, which introduces tracking of object contours for object removal in a video.
		Similar to \cite{GranadosVideoInpainting}, they compute the homographies describing the transformation of the object contours from one frame to the next.
		They operate on the mapping between source and target patches and forward a good mapping of patches to the next frames using the information from the object tracking.
		PixMix also introduces a novel image inpainting approach based on patch matching.
		This is discussed in depth in \rf{method:pixmix}.
		
			
	\section{Inpainting Quality Assessment}\label{related_work:quality}
		It is desirable to have a measure to asses the quality of a result produced by any inpainting pipeline.
		Not only would this make the comparison of results easier and less biased, but it could also be used as cost a function for an optimization approach to the inpainting problem.
		
		We could compare the error of the pixel colors of the reconstructed background using the original image as ground truth.
		However, when want to remove an object, we do not expect the reconstructed background to be similar to the ground truth.
		The ground truth contains the object we want to remove in the reconstructed area.
		We want this area to be similar to the background surrounding the object.
		
		Guillemot et al. describe the problem of quality assessment accurately:
		\begin{displayquote}
			"The quality assessment of inpainted images is another open and difficult issue, as no quantitative metrics exists. Fidelity metrics cannot be used given that, for most inpainting applications, the ground truth is in general unknown. One has to rely on a subjective assessment to evaluate whether the inpainted images are visually pleasing and physically plausible." \cite[p.~143]{Guillemot}
		\end{displayquote}
		
		Therefore, mostly the human perception is decisive for quality assessment.
%		Some attempts have been made to define a quality measure, for example by Trung et al. \cite{quality_assessment} who proposed a metric based on saliency maps.
%		Ultimately these methods cannot be verified by anything other than the human perception or the ground truth.

	 %---------------TRADE-OFF------------%
	\section{Approach and Rationale}
	There is a wide range of possible approaches to the inpainting problem.%, a first challenge is to identify one that fits best given all constraints mentioned in \rf{intro:constraints}.
	The following section provides a comparison of different classes of approaches, aiming to identify the one best fit to cope with the constraints discussed in \rf{intro:constraints}.
	
	If not explicitly mentioned, the approaches introduced have (to the best of our knowledge) never been implemented and are only discussed here as comparison to other proposed methods.
	
	For this discussion, we assume that the position of the camera with respect to the real physical object to be removed is given, as well as the area to be inpainted.
	For more on how this is done, see \rf{imp:unity:tracking}.
	
	\subsection{Modeled, Learned or Synthesized Background}
	An intuitive solution to the object removal problem is using knowledge of the true background. This can be acquired in various ways, as described next.
	\paragraph{Full prior knowledge}
	In case of the example of the museum exhibition mentioned in the introduction, a 3D scan of the exhibit hall can be made.
	The problem is then reduced to tracking the camera's position in the hall and rendering the missing areas using the scan.
	\footnote{A similar approach was used for a VR art exhibition \cite{mobiliar}. The halls were modeled and altered, allowing a fictional glance into an apocalyptic future of the exhibit hall.}
	
	\textbf{Pros}: Given the 3D scan and the spatial tracking, the inpainting itself is trivial.
	
	\textbf{Cons}: It can only deal with static content. Other visitors, a change of exhibits in the background and the light direction cannot be reproduced without changing the reconstructed scene.
	Furthermore creating an accurate scan can be very time-consuming.
	
	This violates the property of being general-purpose and is thus not an option for this project.
	
	\paragraph{Partial Prior Knowledge}
	Some static properties of the scene can be used to guide the inpainting process.
	
	One suggestion is to model the scene using approximating planar surfaces only.
	This and the knowledge of the relative position of the camera to the physical exhibit provides an estimate of the distance between the camera and the true background.
	Like a simplified SLAM\footnote{Simultaneous localization and mapping} procedure, the true color of the background can be deduced from previous frames.
	Using the depth information of the scene, the background pixels to be reconstructed can be identified in previous frames where the exact background was not occluded.
	Given enough frames, the entire background can be deduced.
	By discarding older frames, an adaption to changes to the scene is possible, as long as the depth of the background objects does not change.
	
	\textbf{Pros:} In the best case, the true background is reconstructed and after some time the inpainting can be robust and consistent across frames.
	Minor changes in the background can be handled.
	
	\textbf{Cons:} Background with dynamic depth, such as other museum visitors, will not be reconstructed at all, or as part of a background plane, depending on their degree of movement and the number of past frames available for reconstruction.
	
	With regards to the targeted ability to adapt to any setting, acquiring the depth information of the environment is not an option.
	This approach depends less on the accuracy of the scene model and its creation is far more simple than the full 3D scan mentioned in the approach discussed first.
	However, it still violates the general-purpose principle.
	
	\paragraph{Learned Background Without Prior Knowledge}
	Research in \emph{Computer Vision} has lead to a large number of methods capable of reconstructing 3D maps based on a collection of images.
	In the domain of real-time processing, SLAM algorithms are used to learn a sparse 3D representation of the environment \cite{dtam}.
	Once such a 3D representation is present, full knowledge of the backgrounds depth is available and the inpainting problem can be solved in the same manner as with full or partial prior knowledge.
	The quality of the SLAM algorithm, however, is highly dependent on a sufficiently large distribution of the camera position and orientation when acquiring the images.
	If this is not given, the reconstruction is distorted, incomplete or not successful at all.
	
	The method by Granados et al. \cite{GranadosVideoInpainting} discussed in \rf{related_work:video} makes use of the fact that the true background can be learned without having to estimate a dense depth representation.
	However the processing time is in the dimension of hours for a small number of frames and the full video, including future frames, is used for the inpainting of any frame in the video.
	
	\textbf{Pros:} The true background can be reconstructed without any prior knowledge. The result is consistent across frames.
	
	\textbf{Cons:} The background occluded by the object to be removed must be static for as long as the learned depth estimate or homographies are used. Also the background has to be fully visible at least in one frame.
	
	These limitations are in conflict with the desired property of being able to deal with any kind of scenario, including fast moving objects.
	
	In a museum exhibition, the visitor using a tablet computer to examine an augmented sculpture is not likely to move much sideways, but rather to rotate the tablet and move it back and forth to center the sculpture on the screen.
	These are conditions under which SLAM is likely to fail due to the narrow baseline distance, and unlikely to produce a frame showing the full background.
	
	Despite these limitations, the first approach, inspired by the SLAM problem, could significantly benefit from this scenario.
	This is because the sculpture is static and its relative position to the camera is known.
	
	\paragraph{Synthesized Background}
	The naive solution to video inpainting is to treat each frame as an independent image inpainting problem.
	No assumptions on the setting or the background must be made.
	Moreover, most image inpainting algorithms are not concerned with the concept of fore- and background at all.
	Discarding the third dimension entirely seems like a severe loss of potentially useful information.
	However, since the video is an ordered collection of images, it does not truly contain any information about the third spatial dimension, but much rather about the temporal dimension.
	As discussed, extracting the third spatial dimension is not trivial, so focusing on the temporal dimension instead is a reasonable way to exploit the full potential of the video data.
	
	Additionally to the video, the relative position of the camera to the sculpture is known.
	This provides additional spatial information, but not the depth of the background, which would simplify the problem considerably.
	Compared to the homographies used by Granados et al. \cite{GranadosVideoInpainting}, this tracking information is also of lesser quality. These homographies describe the relation of background objects across frames, where the tracking contains no information on the backgrounds geometry whatsoever.
	However, the tracking information is very similar to the frame to frame contour tracking used by Herling et al. in their PixMix approach \cite{pixmix2014}.
	For tracking the object to be removed, they first compute the homographies using motion detection on selected contour points and apply some refinement to generate the inpainting mask.
	These homographies are additionally used on multiple occasions throughout their video inpainting pipeline, most prominently when guiding the inpainting of the next frame.
	This will be discussed in more detail in \rf{method:pixmix:video}.
	
	\paragraph{Conclusion} 
	It appears useful to leverage the structure of the video as sequence of frames combined with the known spatial transformations of the object to be removed from one frame to the next.
	In this thesis we aim to find a representation that can use these transformations to transfer the inpainting from one frame to the next.
	PixMix \cite{pixmix2014} provides valuable insight in how to achieve this and will be discussed thoroughly in \rf{method:pixmix:video}.
	
	
	\subsection{Diffusion or Patches}
	As discussed in the previous chapter, the two major classes of inpainting algorithms are the ones based on diffusion and the ones based on copying patches.
	We evaluate which of the two is better for our purpose in the following section
	
	\paragraph{Qualitative Comparison}
	In their informative overview on a broad variety of inpainting algorithms, Guillemot and Le Meur \cite[p.~141]{overview:Guillemot} state:
	\begin{displayquote}
		"Diffusion introduces smoothing and blurring artifacts in the synthesized region. To recover the texture of the hole, examplar-based methods [...]\cite{LLE-LDNR}, methods using sparse representations [...] \cite{patchsparsity}, or solutions combining structure diffusion and examplar-based texture recovery as, e.g., [...] \cite{hybrid:bugeau} are more appropriate."
	\end{displayquote}
	They propose to use diffusion based algorithms only for small holes, whereas for medium to big sized holes exemplar-based or hybrid methods are preferred \cite[p.~140]{overview:Guillemot}.
	Their reference images for the case of object removal is reprinted in \rf{method:comparison}. All patch-based or hybrid approaches (\rf{method:comparison:c,method:comparison:d,method:comparison:e,method:comparison:f}) perform clearly better than pure diffusion (\rf{method:comparison:b}).
	
	\begin{figure}[h]
		\graphicspath{{images/method/comparison/}}
		\centering
		\begin{subfigure}{0.32\linewidth}
			\centering
			\includegraphics[width=\linewidth]{a}
			\fig{}
		\end{subfigure}
		\begin{subfigure}{0.32\linewidth}
			\centering
			\includegraphics[width=\linewidth]{b}
			\fig{}
			\label{method:comparison:b}
		\end{subfigure}
		\begin{subfigure}{0.32\linewidth}
			\centering
			\includegraphics[width=\linewidth]{c}
			\fig{}
			\label{method:comparison:c}
		\end{subfigure}
		%				\\ \vspace{2px}
		\begin{subfigure}{0.32\linewidth}
			\centering
			\includegraphics[width=\linewidth]{d}
			\fig{}
			\label{method:comparison:d}
		\end{subfigure}
		\begin{subfigure}{0.32\linewidth}
			\centering
			\includegraphics[width=\linewidth]{e}
			\fig{}
			\label{method:comparison:e}
		\end{subfigure}
		\begin{subfigure}{0.32\linewidth}
			\centering
			\includegraphics[width=\linewidth]{f}
			\fig{}
			\label{method:comparison:f}
		\end{subfigure}
		\caption[Comparison of different object removal techniques.]{Object removal application (a) mask and inpainting results with methods from different categories, (b) anisotropic diffusion
			\cite{Tschumperle2006}, (c) examplar-based with LLE \cite{LLE-LDNR}, (d) patch sparse representation \cite{patchsparsity}, (e) hybrid with one
			global energy minimization \cite{hybrid:bugeau}, and (f) patch offsets \cite{patchoffset:Fitzgibbon}. Reprinted from \cite{overview:Guillemot} (With images provided to Guillemot and Le Meur as courtesy by the respective authors and (a) courtesy of www.magazinehive.com)}
		\label{method:comparison}
	\end{figure}			
	
	Similarly in their evaluation, Vreja et al. \cite[p.~10]{vreja2014image} conclude:
	\begin{displayquote}
		"The tests have shown that inpainting algorithms involving diffusion operations perform well for structural features images but cannot successfully rebuild textures."
	\end{displayquote}
	
	Hybrid techniques as a combination of diffusion and patch-based approaches utilize the best of both worlds and are thus expected to be at least equally good as pure diffusion or patch-based approaches in any given scenario.
	However, as a consequence, they are inherently more complex and require more processing time.
	
	
	Patch matching is the major challenge with patch-based algorithms.
	In \rf{related_work:patch_based} a number of techniques have been discussed using random search, coherence, tree data structures or hashing or more efficiently a combination of these, such as PatchMatch \cite{patchmatch:barnes} and CSH \cite{cshashing:korman:avidan}.
	As algorithms making use of traversing trees and looping are inherently unfit to be executed on the GPU.
	We will discuss the technical details of the limitations of GPU computing in \rf{impl:gpu_limitation}.
	However, since the overall objective is to minimize a distance term, any other class of minimization strategies can be used if the problem can be formulated appropriately. \PERF
	
	Iterative or greedy strategies have been proposed for solving the patch matching problem, as stated in \rf{related_work:patch_based}.
	The algorithm has to be compatible with a large number of devices, which may drastically differ in their computational power.
	Thus, the algorithm has to be able to generate results within a computational budget that might be as low as a few hundred milliseconds on a tablet computer.
	Choosing an iterative approach allows us to adjust the number of iterations without leaving any pixels without inpainting, merely reducing the quality.
	This is preferable over a greedy approach that might not complete its computation within the computation budget.
	%			This is true, even if the greedy algorithm would outperform the iterative given enough processing time. \PERF
	%This also simplifies the development of such an algorithm, when initially the eventual performance can only be guessed. Being able to later reduce or increase the computational cost reduces the risk of missing the aim by a great distance.
	
	The patch size and density is  a final feature to be considered.
	While most strategies define a patch as a statically sized and shaped neighborhood of pixels, Herling and Broll propose a relaxation of the definition of a patch in their PixMix paper \cite{pixmix2014}.
	They match single pixels instead of entire patches, but use a cost function that encourages neighboring pixels to be mapped to patches.
	This relaxed definition of a patch can easily integrate rotation and scale invariance by simple modifications of the cost function.
	
	Additionally, the mapping information computed on a downscaled version of the image, can be reused at higher resolutions and further refined. 
	At low resolution, the neighborhood covers a large area, identifying low-frequency features, while at high-resolution finer details are considered.
	Furthermore, the mapping information computed for one frame can serve as the initialization for a next frame, if properly transformed.
	%{How is that better? Allows working at different resolutions == different patch-size, feature-size. Synthesis simple. Can easily be propagated to next frame, small translations have littler effect on inpainting (do they have larger effect when using patches?), rotation-invariance, using interpolation can get any degree of detail at any mipmap-level}
	
	To conclude, patch-based approaches outperform diffusion techniques in terms of quality and hybrid techniques in terms of performance.
	Whether the qualitative superiority of hybrid techniques leads to a significant improvement of the result in the context of this projects, can not be answered in this thesis.
	However, initially choosing a patch-based approach to start with does not exclude a hybrid extension, in case the performance permits it.
	
	An iterative approach helps to adapt the computational cost to the given budget.
	A relaxed definition of patches allows for efficient reuse across different resolutions and frames.
	
	\section{Inpainting problem as Optimization}\label{method:definition}
	Formally, inpainting aims at completing an image $I$ that is composed of a source region $S$ with known pixel values and a target region $T$ with missing pixel values.
	$I$ is the disjoint union of $T$ and $S$.
	$T$ is also referred to as \emph{hole}.
	Formally it can be described as the optimization problem of finding a mapping function $f: T \rightarrow S$ that globally minimizes the cost function $cost_{tot}(f) = \sum_{\forall p \in T} cost(p)$.
	Where $cost(p)$ analytically defines the quality of the mapping of an individual pixel $p$ with respect to a subset of the image's pixels.
	In the context of image inpainting, defining \emph{quality} is not straightforward, since it is hard to tell which inpainting is best.
	Commonly a resulting inpainting is rated by how easily the inpainted area can be noticed and whether its texture looks plausible and consistent with the known part of the image.
	As mentioned in \rf{related_work:quality}, some effort has been made towards automatic quality assessment, but the results cannot be easily used in order to define an adequate cost function.
	
	
	%----------PIXMIX-----------%	
	
	\section{The PixMix Algorithm}\label{method:pixmix}
	In comparison to the previous work in the field of real-time video inpainting, the PixMix paper \cite{pixmix2014} by Herling and Broll excels not only with visually appealing results, but by introducing an algorithm that only requires minor changes in order to be implemented on the GPU.
	Since the work discussed in this thesis is strongly based on PixMix, the following section will present an introduction to the aspects of PixMix relevant to the understanding of the the discussion of our approach in \rf{method:our_approach}.
	We will introduce the key concepts here, the entire algorithm adapted to this project's needs is presented later in \rf{method:algo}.
	
	\subsection{Cost function}
	Following the formal definition of the inpainting problem given in \rf{method:definition}, Herling and Broll suggest a cost function composed of multiple terms.
	Relevant to this discussion are the color $cost_{color}$ and spatial $cost_{spatial}$ error terms.
	\begin{equation}\label{pixmix:total_cost}
	cost(p) = \alpha ~ cost_{color}(p) + (1-\alpha)cost_{spatial}(p)
	\end{equation}
	$\alpha \in[0,1]$ is used to weight the two error terms. This weight factor allows to balance the impact of the two terms, to make sure that a single term cannot dominate the entire optimization.
	
	\paragraph{Spatial cost}
	While the mapping is defined by individual pixels and not using patches, the aim is still to reproduce texture by copying entire patches from the known part of the image.
	The spatial cost should enforce that all neighbors of $p$ are mapped to neighbors of $f(p)$.
	This encourages extensible patch-like structures without enforcing a static patch.
	If the mapping has a piecewise patch-like structure, texture from the source region $S$ can get reconstructed inside the hole.
	This is formally captured using the following definition of the spatial cost term:
	\begin{equation}\label{method:pixmix:spatial_cost:eq}
	cost_{spatial}(p) = \sum_{\overrightarrow{v} \in N_s} d_s[f(p) + \overrightarrow{v}, f(p + \overrightarrow{v})]  ~ \omega_s(\v),	
	\end{equation}
	where $N_s$ is a neighborhood and its elements $\v \in N_s$ are the offsets from the neighborhood's center.
	The shape of the neighborhood is discussed in \rf{imp:neighbourhood}.
	$d_s$  is a robust distance function. $\omega_s\in\mathbb{Q}$ is a weight parameter with $\sum_{\v \in N_s} \omega(\v) = 1$.
	The robust distance function and the rational for using it is discussed in \rf{method:our:other:robust_distance}.
	
	\paragraph{Color cost}\label{method:pixmix:color_cost}
	Regarding the color, two properties must hold in order to get credible inpainting:
	First, the seams of neighboring patches, represented as a neighborhood $N_c$ around the pixel, must not be visible.
	Second, structures must be continued through such a patch.
	This can be achieved by making sure that the neighborhood around $p$ looks similar to the neighborhood around $f(p)$.
	If structures are present in the neighborhood around $p$, a good match would be a part of the image that contains a similar structure.
	Since neighborhoods of neighboring pixels have a large overlap, they share most neighbors and have thus similar constraints imposed by their neighbors' colors.
	This is formalized by the following equation:
	\begin{equation}
	cost_{color}(p) = \sum_{\overrightarrow{v} \in N_c} d_c[i(p+\overrightarrow{v}), i(f(p) + \overrightarrow{v})] ~ \omega_c(p+\v),
	\end{equation}
	where $N_c$ is a neighborhood, $d_c$ a pixel intensity distance measure, $i(p)$ the color value of a pixel $p$ and $\omega_c$ equivalent to $\omega_s$.
	
	\subsection{Image inpainting}
	Herling and Broll introduce a solution to the image inpainting problem.
	Its relevant aspects are highlighted in the following section.
	
	Like most patch-based image inpainting algorithms, PixMix has two major stages:
	\begin{enumerate}
		\item Finding a good mapping
		\item Synthesizing the pixel colors for all pixels inside the hole
	\end{enumerate}
	While the second step only requires looking up the pixel color using the given mapping, the first step is more involved.
	It requires finding the mapping that synthesizes the lowest cost.
	The authors suggest an approach similar to the PatchMatch\cite{patchmatch:barnes} algorithm.
	Once the mapping function is initialized, a source pixel is mapped to a random target pixel. If this new mapping results in a lower cost then before, it is kept, otherwise the old mapping is preserved.
	If a better mapping is found, it gets propagated to the neighboring pixels.
	This is repeated until a sufficiently good mapping function $f$ is found.
	
	\paragraph{Image pyramid}
	In computer graphics, an image pyramid is a data structure that contains an image and scaled-down versions of this image with different resolutions.
	They are also called mipmaps and are useful if multiple resolutions of an image are needed.
	
	The optimization is applied on such an image pyramid ${L_0,...L_m}$, where the highest resolution level $L_0$ is the original image and the lower level scaled-down versions of the image.
	Starting at the lowest level $L_0$ the next higher level is created by downscaling by a factor of two, creating an image half the width and height of the previous level.
	The optimization of the mapping function $f$ starts at low resolution and once a sufficiently good candidate for $f$ is found, this is then forwarded to the next higher resolution level until the final level $L_0$ is reached.
	The benefit of using image pyramids is threefold: 
	First, the optimization is less likely to get stuck in a bad local minimum, because at low resolution the cost function with respect to $f$ has a smaller number of local minima.
	As a consequence, the total cost synthesized by the final mapping is lower.
	Second, structures of various frequencies are captured at the respective resolutions.
	The neighborhoods $N_c$ and $N_s$ cover a larger image area at a lower resolution and are thus sensitive to lower frequency structures.
	As the mapping gets passed to the next pyramid level, the structures implicitly remain present in the mapping function and are later reproduced during the image synthesis.
	Third, the convergence is sped up.
	A single iteration on the full resolution image is much more computationally expensive than on lower resolutions.
	Presenting a good initialization to the high-resolution levels can thus significantly speed up the convergence.
	
	\subsection{Video inpainting}\label{method:pixmix:video}
		Additional to static image inpainting, this section discusses the contributions of PixMix to video inpainting, as far as they are relevant to this thesis.
		
		The authors introduce a guided object contour selection and tracking across the video frames based on the video frames exclusively.
		They explicitly compute the homographies mapping the object contour from one frame to the next, whereas for this project the transformations are provided by image tracking.
		
		The video inpainting pipeline is presented as follows: The first frame is inpainted according to the method described above. The resulting mapping $f_n$ function is transformed to the next frame using the corresponding homography $H$ and then used as initialization for the optimization of the next frames mapping $f^{init}_{n+1}$.
		This transformation of the old mapping to the new frame is given by $f^{init}_{n+1} = H^{-1}f_{n}H$.
		The same optimization as before is then performed using this initialization.
		
		In addition, the authors suggest using a keyframe that acts as reference model.
		This reference model is used such that the inpainting of a sequence of frames remains coherent to the key frame.
		This is achieved adding another term to the cost function.
		\begin{equation}\label{method:pixmix:video:cost_term}
			cost'_{color}(p) =  \sum_{\v\in N_c} d'_c[i(p+\v),r(p+\v)] ~ \omega'_c(p+\v),
		\end{equation}
		where $r(p)$ is the pixel value of $p$ in the reference model, $d'_c$ and $\omega'_c$ distance and weight function as described for the color cost.
		This additional cost term forces a pixel to be similar to its corresponding pixel in the reference model, preventing drastically differing results for similar frames.
		$r(p)$ is determined by applying the backward transformation from the current frame to the key frame using the computed homographies.
			
