\chapter{Implementation}\label{imp}
	Implementing the algorithm derived in the last chapter was motivated by two major goals:
	First, to evaluate whether video inpainting in real-time on mobile devices is feasible using the described approach.
	Second, to study the algorithm's behavior and if necessary make changes.
	Studying the algorithm's correctness necessarily precedes the performance evaluation.
	Therefore, we first made a prototype implementation in Python and once we sufficiently understood the algorithm's behavior and results, the GPU implementation followed.
	However, since understanding the constraints on the implementation imposed by the GPU is essential, we first present the GPU implementation and discuss the Python prototype afterwards.
	
%-------------------------------------%
%             UNITY		
%-------------------------------------%
	\section{GPU Implementation}
		The requirements on the implementation of the \emph{AR} inpainting pipeline presented in \rf{intro:constraints} imposed some restrictions on the choice of technology.
		We have emphasized on the advantage of leveraging the GPU for achieving the desired performance in the previous chapters.
		In this section, we will discuss how the GPU can be exploited best.
		
		However, the GPU API landscape is by far not as unified as the one for CPUs , and hardware architecture, operation system and generation of the API are very diverse and highly specialized for specific applications.
		Even though general purpose GPU computation (GPGPU) has gained a lot more popularity in recent years, the corresponding technologies, such as \emph{OpenCL}, \emph{CUDA}, and \emph{C++ AMP}, require either a specific hardware architecture, operations system or driver.
		Original intended for graphics processing only, GPU computing comes with a rich diversity of rendering APIs, e.g. Direct3D, OpenGL, Metal and Vulkan to name a few.
		They all support the execution of shader programs and have extended their API to allow dispatching the execution of shader programs independent of the rendering pipeline and added support for read-write data structures.
		To support devices based on Android, iOS and Windows devices at least OpenGLES, Metal and DirectX must be supported.
		These APIs all come with their own shading language used to define the programs executed on the GPU.
		
		To avoid writing and maintaining three different code bases, cross-compilers exist that translate shader programs from one language to the other.
		The game engine \emph{Unity} has incorporated such cross-compilers, along with a unified interface to the different rendering pipelines.
%		To the developer, the platform the game gets deployed to is mostly transparent.
		These general purpose shader programs are called \emph{Compute Shader} in Unity and are implemented using \emph{Cg} which is syntactically mostly identical to \emph{HLSL}.
		We use the Unity game engine in its 2017.1.3f version.
		
		\TODO{Ein wort zu performance?}
		
		\subsection{Coding for GPUs}\label{impl:gpu_limitation}
			With Unity, we found a framework to support deploying GPGPU code on different operation systems and devices.
			But, the algorithm needs to be implemented in \emph{HLSL} and with standard shader programs.
			This results in some limitations we discuss in the following section.
			
			\paragraph{Loop Unrolling}
			Loops get unrolled when compiled.
			As the cross-compiler is not efficient, a lot of temporary variables are created.
			These are limited, so loops must be kept short and have only a few iterations (we used at most loops with 24 iterations).
			This is particularly problematic for nested loops.
			Another problem with unrolling loops is that the number of iterations must be known at compile-time, branching out based on dynamic input is thus not possible.
			
			\paragraph{Branching}
			When a shader program gets dispatched, a thread-group size is defined by the user.
			Thread-groups are a number of threads that get executed together. They can be forced to synchronize and share memory.
			A thread-group is executed in a \emph{single instruction multiple data} fashion, which means that all threads are executing the same instruction simultaneously.
			This is in particular important for branching, as it creates multiple execution paths.
			Branching paths get serialized, and if even a single thread takes another path than the others, all threads will be forced to either execute that branch or be stalled.
			Thus avoiding unnecessary branching is crucial for exploiting the GPU's potential.
			This makes the GPU unsuitable for algorithms that heavily rely on branching such as most search algorithms or which operate on tree data structures.
				
			\paragraph{Input and Output}
			Traditionally shader programs take a set of read-only textures and parameters as input and produce no output other than the rendered image.
			Compute shaders, however, support read-write access textures and buffers that can be written to and read from both the CPU and GPU.
			In Unity, the read-write textures are called \emph{render textures} and the buffers \emph{compute buffers}.
			The number of render textures that can be used simultaneously is limited, and the exact number depends on the GPU.
			The same is true for the buffers that are additionally limited in size. Both the stride, the size of a single buffer element, and the number of elements are limited.
			The limit is dependent on the device as well.
			We were not able to use more than two render textures and two compute buffers at the same time without experiencing unexpected effects on the Shield tablet.
			
			\paragraph{Data Structures}
			The data structures supported are limited to primitive types, such as floating- and fixed-point numbers and integers, as well as arrays of such.
			Furthermore, structured objects are supported and useful as buffer elements.
			Lists with dynamic lengths, dictionaries or classes are not supported.
			Textures either act as arrays and are indexed using integer coordinates or as textures in the traditional sense and accessed using floating number pairs between $(0,0)$ and $(1,1)$.
			When creating a texture, the data type of its elements is defined. They are mostly arrays of four values, corresponding to the four color channels RGB and alpha.
			The bit-size and number type, floating-, fixed-point or integer, can vary, but not all formats are supported on all devices.
			We used render textures with the format \emph{RGBAFloat} that consists of four floating point numbers per pixel with a precision of 32bit each.
		
			\paragraph{Optimization}
			Typically a naive GPU implementation is not faster than an optimized CPU implementation.
			Optimizing GPU code can result in a dramatic performance boost and is unavoidable when aiming at high performance.
			The optimization of an implementation, however, is not within the scope and time available of this thesis.
			Therefore, the GPU code remained mostly unoptimized.
			Nevertheless, the implementation is fast enough for the evaluation of our supposition that video inpainting can be done in real-time on mobile devices.
		
		\subsection{Image Tracking}\label{imp:unity:tracking}
			To identify the pixels to be inpainted, the object we want to remove is tracked.
			We place the physical object on a tracker image.
			This is an image with a distinctive pattern that can easily be recognized using image features.
			When this pattern is present on a camera frame, its position, scale, and orientation can be computed.
			This is sufficient to derive the relative position of the camera to the tracker image.
			The physical object is given as a 3D model and thus its exact shape and dimension are known as well.
			It is placed on the tracker image with a consistent orientation and its relative position to the camera is known as well.
			
			We are using Vuforia \cite{vuforia} in its version 6.2.10 for this purpose.
			Vuforia is available as a Unity extension and runs on all targeted platforms.
		
		\subsection{Scene Setup}\label{imp:unity:scene_setup}
			\rf{imp:scene:schematic} depicts the scene setup discussed in the following.
			The Vuforia extension provides an \emph{AR camera} asset representing the mobile device's camera and an \emph{image target}, representing the tracker image described in \rf{imp:unity:tracking}.
			The 3D model of the physical object is positioned atop the \emph{image target} and the scene is set up for tracking the physical object.
			The camera stream used by Vuforia for identifying the tracker image is rendered to a \emph{background plane} object that acts as a canvas in the background of the scene.
			Finally, there is a camera capturing the scene with the 3D model and the \emph{background plane}.
			
			Unfortunately, it was not possible to access the video stream rendered to the \emph{background plane} directly on the Nvidia Shield K1 tablet.
			To the best of our knowledge, this is due to a bug in either Vuforia or the way the Shield tablet implements the Android camera API.
			The workaround we eventually found is the following: An additional camera object, named \emph{background camera}, captures the \emph{background plane} Vuforia uses to render the camera stream. 
			This introduces an additional render pass, which increases the time needed for processing a frame.
			
			Due to the tracking, the 3D model of the object always correctly aligned with the image of its physical counterpart on the camera frame.
			The mask texture that defines the hole is created by rendering the 3D model without shading in a uniform color.
			The background remains black indicating the source area $S$ while any pixel with color is part of the target area $T$.
			
			The mask and camera frame are passed to the inpainting algorithm as input.
			The inpainting consists of various compute shader programs.
			They get the input data and dispatch calls from a C\# script that gets triggered every time Unity starts rendering a new frame.
			Eventually, the frame with the inpainted image and a texture containing the rendered 3D model are passed to a third Unity camera, labeled as \emph{orthographic camera} on \rf{imp:scene:schematic}.
			The \emph{orthographic camera} overlays the two textures and renders them to a quad, \emph{render quad} on \rf{imp:scene:schematic}, that is displayed on the screen using an orthographic projection.
			
			\begin{figure}
				\graphicspath{{images/implementation/}}
				\includegraphics[width=\linewidth]{unity_scene_schematic}
				\caption[Schematic of the Unity scene.]{Schematic of the Unity scene. The left camera represents the \emph{AR camera} and \emph{background camera}. The mask and camera frame get passed to the inpainting algorithm which processes the frame.
				The inpainted frame and the rendered object are then passed to the \emph{orthographic camera}, which combines and renders them to the screen.}
				\label{imp:scene:schematic}
			\end{figure}
		
		\subsection{Mapping Function as Texture}\label{imp:gpu:mapping_as_texture}
		The domain of the mapping function $f: T \rightarrow S$ is the set of the hole or target pixels $p \in T$. $T$, however, depends on which mipmap level is considered.
		Thus, the representation of $f$ also depends on the mipmap level.
		Since the mapping function we are optimizing for is defined on pixels, it can be represented as a matrix $F: F_{ij} = f[(x_i, y_j)]$.
		This matrix is stored as a render texture with four floating point values per pixel.
		The first two contain the mapping $F_{ij}$, they are the \emph{x}- and \emph{y}- components of this texture's pixel. The third, the \emph{z}-component, is 1 if the pixel is a target pixel and 0 if it is a source pixel.
		Finally, the \emph{w}-component of the mapping texture's pixels is the fourth entry which is 1 if the mapping for this pixel is valid and 0 otherwise.
		The mapping of a pixel is invalid, if it is not initialized yet, or lays outside the image boundaries.
		Two versions of the mapping are used: The \emph{old} mapping as read-only texture containing the mapping from the previous iteration and the \emph{new} mapping as read-write texture, that takes the newly computed mapping coordinates.
		Between two iterations, they are swapped and the next iteration takes the former \emph{new} mapping as input.
		
		\subsection{Initialization}\label{imp:unity:init}
		The first step of the algorithm is initializing the mapping based on the provided mask and camera frame.
		This requires iterating all target pixels and searching the source pixels to find the one synthesizing the lowest cost.
		Merely iterating all possible pairs of source and target pixels using a loop is not possible using compute shaders due to loop unrolling and resulting long execution times.
		Instead, one thread is assigned for each source and target pixel pair.
		All threads processing the same target pixel are grouped and can thus use shared memory.
		They compute the cost for their assigned pixel pair and store it into an array that lives in the shared memory.
		Following the computation of the cost, the threads are synchronized and the source pixel with minimum cost is identified.
		Finding the minimum of an array can be done by a parallel reduction.
		
		Before the initialization shader program can be dispatched on the GPU, the input data has to be loaded into the textures and buffers.
		The camera frame is retrieved as a texture from the \emph{background camera} and can simply be bound to the shader.
		All the input parameters are stored into a structured buffer and a second buffer is used to flag pixels as initialized.
		Using the mask texture, the coordinates of source and target pixels are identified on the CPU and stored into a texture.
		Storing them into a buffer would be more convenient, but at most two buffers can be bound to a shader program in order to stay compliant with the OpenGLES3 default limits.
		
		For both, the old and the new mapping, the source pixels get set to the identity mapping $f(p') = p' \forall p' \in S$.
		The \emph{z}- and \emph{y}-components get initialized as described in \rf{imp:gpu:mapping_as_texture}.
		Then, the shader program for initializing the target pixels is dispatched.
		Only target pixels with a sufficient number of valid neighbors can be processed.
		After the first batch of pixels has been processed, the mapping textures are swapped and the process is repeated until all target pixels are initialized.
		
		\subsection{Neighborhoods}\label{imp:neighbourhood}
			Both, the color and spatial cost terms operate on a neighborhood around the processed pixel $p$.
			The size and shape of the neighborhoods require careful evaluation.
			\rf{imp:gpu:neighborhood} shows the three kinds of neighborhoods used.
			
			The neighborhood used in the spatial cost term must be large enough to enforce a patch-like structure.
			Using the 8-neighborhood instead of the four principal neighbors leads to an improvement.
			Increasing the size even more did not clearly improve the result, as discussed in \rf{res:neighbour}.
			
			For the color cost term, a neighborhood too small will not capture the structures.
			However, computing the color cost gradient includes computing the color cost four times.
			Using any neighborhood larger than the 12-neighborhood exceeds the maximum number of temporary variables, because of  the loops unrolling.
			Thus using the 12-neighborhood was the best option, though we assume that a larger neighborhood might be able to capture high-frequency structures better.
			
			\begin{figure}
				\graphicspath{{images/implementation/}}
				\centering
				\begin{subfigure}{0.32\linewidth}
					\includegraphics[width=\linewidth]{8_neighborhood}
					\caption{8-neighborhood}
				\end{subfigure}
				\begin{subfigure}{0.32\linewidth}
					\includegraphics[width=\linewidth]{12_neighborhood}
					\caption{12-neighborhood}
				\end{subfigure}
				\begin{subfigure}{0.32\linewidth}
					\includegraphics[width=\linewidth]{24_neighborhood}
					\caption{24-neighborhood}
				\end{subfigure}
				\caption{The neighborhoods used.}
				\label{imp:gpu:neighborhood}
			\end{figure}
		
		\subsection{Optimization}
		The optimization takes the \emph{old} mapping containing the initialized mapping and writes the newly computed mapping coordinates into the \emph{new} mapping texture.
		To check whether a mapping of a pixel points into the hole, the mask texture is used.
		
		Each target pixel gets assigned a thread that first computes the gradient of the cost function, and then checks if the new mapping points into the hole.
		If this sic the case, the \emph{random neighbor} procedure is applied.
		Finally, the mapping is clamped to the edges of the image borders if it lays outside, and is written into the \emph{new} mapping render texture.
		
		This is repeated for a fixed number of times and between each iteration, the two mapping textures are swapped.
				
		\subsection{Mapping Propagation}
		When moving to a higher resolution level in the image pyramid, the mapping from the previous level is used as initialization.
		The implementation is not strictly following the discrete definition from \rf{method:algo:propagation}, but uses a hybrid approach of both definitions, the one using discrete coordinates and the one using unified texture coordinates.
		This is due to the fact that the mapping is stored as a discrete texture and can be made continuous using bilinear interpolation.
		When implementing the mapping propagation in shader code, was assumed that using bilinear interpolation on the mapping texture is the best approach.
		However, the interpolated values can only be accessed using unified texture coordinates.
		Thus, the discrete coordinates of the target pixels have to be transformed into unified texture coordinates, which are then used for looking up the interpolated value.
		This is done for all target pixel on the new mipmap level.
		The resulting mapping is then the initialization to the optimization.
		
		\subsection{Mapping Texture Access}
			The idea of using the gradient descent method initially motivated to consider the mapping as a continuous function.
			The data stored in the texture, however, is inherently discrete and is made continuous by using bilinear interpolation.
			This introduces two different coordinate systems for the mapping texture:
				\begin{enumerate}
					\item Using integer pixel indices in $\{0,\dots,w-1\} \times \{0,\dots, h-1\}$, where $w$ is the texture width and $h$ its height. This is identical to accessing a two dimensional array.
					\item Using unified texture coordinates with values in $[0,1] \times [0,1]$. These are also called \emph{uv-coordinates} with \emph{u} and \emph{v} instead of \emph{x} and \emph{y}.
				\end{enumerate}
				Additionally, the mapping texture is padded with a margin of one pixel on all sides.
				When trying to access a pixel outside the valid texture coordinates, it is clamped to the outermost row or column. These rows and columns have a $w$-component that is 0, marking them as invalid mappings.
				This makes additional boundary checks unnecessary and avoids unnecessary branching on the GPU.
				This padding shifts all coordinates by one pixel.
				
				When using uv-coordinates, the coordinate $(0,0)$ is the bottom left corner of the bottom left pixel\footnote{This is following the OpenGL definition, DirectX uses a different convention, but Unity hides these differences to the programmer.}.
				We want to access the center of the pixel because of the interpolation though.
				Sampling the center of the pixel returns the same value as when accessing the texture as an array using integer indices.
				Therefore, the \emph{uv}-coordinates must be shifted by half a pixel in both dimensions.
				
				All the above should be transparent to the algorithm which operates on integer coordinates with $(0,0)$ as the first valid pixel in the bottom corner.
				
				This results in six different coordinate systems for accessing and sampling the mapping texture.
				\begin{enumerate}
					\item Padded using integer coordinates $(x_o, y_o)$
					\item Padded using \emph{uv}-coordinates $(X_o, Y_o)$
					\item Not padded using integer coordinates $(x_i,y_i)$
					\item Not padded using \emph{uv}-coordinates $(X_i, Y_i)$
					\item Not padded and corrected for \emph{uv}-coordinates, in integer coordinates $(\tilde{x_i}, \tilde{y_i})$
					\item Not padded and corrected for \emph{uv}-coordinates, in uv-coordinates $(\tilde{Y_i}, \tilde{Y_i})$
				\end{enumerate}
				
				The first is used when accessing the mapping as an array and the second when using \emph{uv}-coordinates.
				Any interpolated texture access has to be performed using the second coordinate systems, so the others have to be transformed first.
				The algorithm is using the third kind of coordinates and needs an interpolated view on the mapping.
				The sixth coordinate system is used when sampling the camera frame and mask.
				
			\subsubsection{Transformations}
				Due to the complexity introduced by the different views on the mapping textures, we discuss the transformation from one system into another in this section.
				This is done for the $x$-component only, the \emph{y}-component works analog, using the height instead of the width.
				
				For each coordinate system, the width of the texture is denoted with $w$ and the same subscripts as the indices used for this system. For example $w_o$ when using padded integer coordinates $(x_o, y_o)$.
				The uppercase notation $X$ denotes \emph{uv}-coordinates and lowercase $x$ the integer coordinates.
				
				In general the equation $X = x/w$ hold for all coordinate systems.
				Additionally, the relation between the systems is given as:
				\begin{equation}
				x_o = x_i -1 = \tilde{x_i} - 1..
				\end{equation}
				This yields
				\begin{equation}
				X_o = \frac{x_o}{w_o} = \frac{x_i+1.5}{w_o},
				\end{equation}
				which is used for accessing the interpolated mapping values in the initialization and the mapping refinement steps.
				
				When rendering the final frame, the $(\tilde{X_i}, \tilde{Y_i})$ coordinates are given.
				Using the above equation, they are transformed for the mapping texture lookup in the following manner:
				\begin{equation}
				X_o = \frac{\tilde{X_i}\tilde{w_i}+1.5}{w_o}.
				\end{equation}
		
		
		%-------------------------------------%
		%             PYTHON		
		%-------------------------------------%
		\section{Python Prototype}\label{imp:python_prototype}
		The prototype of the algorithm was implemented in Python version 3.6 using \emph{numpy} \cite{numpy} for extended array operation support and \emph{Cython} \cite{cython} for performance improvement.
		The implementation simulates the constraints from the GPU.
		Since it does not come with the benefits of a GPU it is much slower.
		Processing a single frame at a resolution of $512 \times 512$ pixels takes 4.5h, whereas the GPU implementation takes 352.62\emph{ms}.
		Working on this prototype allowed us to quickly try new ideas and understand their effects more easily thanks to good debugging tools and the ability to easily plot results.
		The prototype does not support any of the augmented reality components.
		
		The Python implementation has some features, which are not implemented in the GPU version.
		It uses the convergence criterion based on the total error described in \rf{method:algo:convergence} which helps to reduce the computational cost and to avoid over-fitting.
		
		Some parameter values are depending on the mipmap level, most relevant the $\alpha$-weight and a parameter to control the step size of the gradient descent minimization.
		Since this introduces more parameters that have to be set manually, we did nopt incorporate this in the GPU implementation.
		
		Furthermore, the selection of the mipmap level for the initialization is dependent on the number of source and target pixels.
		First, the level with at least three source and one target pixel $L_{b}$ was identified and then the $L_{b+2}$ level is selected for the initialization.
		This most often leads to an initial resolution of $8 \times 8$ pixels.
		However, experiments have shown that $16 \times 16$ pixels works best in most cases.
		If the size of the hole does not vary drastically, this provides the simpler initial condition and is used for the GPU implementation.
		
		Initially, we wanted to not only refine the mapping level by level but also be able to go back to a lower resolution if the mapping became unstable.
		This would preserve the high-frequency texture structures while readjusting the entire patches to the low-frequency details.
		Therefore, the mapping data structure is designed hierarchically as a pyramid $M$ of arrays $M_{m}$ representing the mapping for the $m$-th mipmap level of the image.
		Every layer of this pyramid only stores the adjustments made during the optimization of its mipmap level only.
		Thus, when reading a mapping coordinate for the $m$-th level from this pyramid $M_m$, all layers from $m$ to the level of the initialization $M_l$ have to be considered.
		The mapping of pixel $(y,x)$ at mipmap level $L_m$ is then given by $M_{tot}(y,x) = \sum_{i=m}^{l}2^{i-m}M_i(y//2^{i-m},x//2^{i-m})$ where $//$ is the integer division.
		Dividing the coordinates by two at each step up the pyramid is necessary because the resolution gets smaller and the pixel size larger with each level.
		Multiplying the result by $2^{\text{steps up the pyramid}}$, is necessary for the same reason.
		
		%At level seen from the resolution of level $m$, the mapping at level $m+1$ has only half the resolution, so there for every two pixel in $m+1$ there is an additional pixel in between at level $m$ in both dimensions.
		
		
%		Every entry $M_{tot}(y,x)$ is a coordinate where the mapping function $f(y,x) = M_{tot}(y,x)$
%		Initialized starting with the lowest resolution
%		$M_l = \begin{bmatrix}
%		(0,0) & (0,1) \\
%		(1,0) & (1,1)
%		\end{bmatrix}
%		$
%		$M_{l-1} = \begin{bmatrix}
%		M_l & M_l \\
%		M_l & M_l
%		\end{bmatrix}
%		$
%		
%		\begin{align}
%		M_0 &= \left.\begin{bmatrix}
%		M_l & \dots & M_l \\
%		\vdots & \ddots & \vdots \\
%		M_l & \dots & M_l \\
%		\end{bmatrix}
%		\right\}l \\[-0.5\normalbaselineskip]
%		& \hspace{2em}\underbrace{\hspace{2.2cm}} \\
%		& \hspace{2cm}l
%		\end{align}

		

		
%-------------------------------------%
%             Comparison		
%-------------------------------------%	
%	\section{Comparison/Differences}
%		\begin{itemize}
%			
%			\item Initialization
%			\paragraph{Python} Get highest threshold $M_t$ mipmap-level where a given number of pixels is inside the hole (e.g. 1) and outside hole (e.g. 3) and then take as start level $M_s = M_t -2$.
%			This ensures that there is enough information present already, while no detailed structures are there yet. This is the desirable init-condition. Little structure, yet enough information on texture.
%			
%			\paragraph{Unity} Use 8 or 16 pixels in width.
%			
%			\item Mapping
%			\paragraph{Python} Hierarchical,
%			
%			\item Move to next mipmap-level
%			 \paragraph{Python} Overlaying another layer of repeated pattern
%			$\begin{bmatrix}
%			(0,0) & (0,1) \\
%			(1,0) & (1,1)
%			\end{bmatrix}
%			$)
%			This pattern will preserve the structure of the known mapping by assigning blocks of four "new" pixels to the last mapped block of four pixels. Simply subdividing the mapping into these 2x2 blocks.
%			
%			\paragraph{Unity} Interpolate previous levels mapping.
%			
%			\item  Mipmap creation
%			\paragraph{Python} 2x2px blocks averaged
%			\paragraph{Unity} Automatically done by Unity. \Q{By Unity or GPU/DX/OpenGL? How exactly is it done? I could not figure out so far...}
%		\end{itemize}
