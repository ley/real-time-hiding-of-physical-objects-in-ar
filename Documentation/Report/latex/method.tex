\chapter{Method}\label{method}
	Based on the algorithm introduces by Herling and Broll in their PixMix paper \cite{pixmix2014}, we propose an algorithm suitable for a GPU implementation.
	We also present an overview of the entire algorithm in the second part of this chapter.
	 
			
	%--------------HIDE--------------%
	\section{Our Approach}\label{method:our_approach}
		Though strongly leaning on the PixMix algorithm for guidance, we introduced some changes before we implement the algorithm on the GPU.
		
		Most important is the introduction of a different optimization method and, as prerequisite, converting the discrete nature of the image inpainting problem into a continuous one.
		Further, we propose a normalization for the cost function, discuss the use of a robust distance function and show how the gradient of the cost function can be computed.
		We also discuss how these modifications can lead an invalid mapping and how to deal with that.
		
		\subsection{Optimization}
			Our initial observation is that the cost function is differentiable.
			We augment the discrete image pixel domain by applying bilinear interpolation to the image, and the discrete color sampling function $i$, as well as the mapping function $f$, to continuous functions.

			Given this continuous nature, a large set of optimization methods is available.
			We use gradient descent with a carefully considered step size.
			
			The gradient of the cost function can be computed on each pixel in parallel and the mapping if updated locally using this gradient.
			This makes our approach a local optimization with every local update, affecting all neighboring pixels during the next iteration.
			Because the pixels are highly connected through these neighborhoods, no local optimization step is independent of the others.
						
			\subsubsection{Spatial Cost Term}
				The spatial error term has been altered to make it more suitable for the gradient descent optimization and to further relax the patch structure by dropping the implicit angular constraint present in the original formulation.
				The definition from PixMix requires the top right neighboring pixel of $p$ to be mapped to the top right neighbor of the $f(p)$.
				This explicit spatial structure is relaxed by only requiring the distance to be the same.
				Note that this only leads to rotation invariance, if we change the color cost term accordingly, which we did not implemented for this thesis.
								
				We observe a drift of the mapping towards the edge of the hole caused by the spatial cost term.
				This is because the spatial cost term is trying to preserve neighborhoods to create patch-like structures.
				As a consequence, the mapping of a pixel with a source pixel in its neighborhood converges to the neighborhood of that source pixel.
				Even though only target pixels close to the edge of the hole have source pixels in their neighborhood, their mapping will pull all surrounding pixels' mapping towards the edge as well, as the optimization proceeds.
				Therefore, we limit the neighborhood $N_s(p)$ around $p$ to the target pixels.
				
				Moreover, we set the weight parameter $\omega_s = \frac{1}{||N_s||}$.
							
				This leads to the following adapted definition of the spatial cost term:
				\begin{equation}
					cost'_{spatial}(p) = \frac{1}{||N_s||} \sum_{\overrightarrow{v} \in N'_s} c[d_s[f(p),f(p + \v)], ||\v||],
				\end{equation}
				
				where $N'_s = N_s \cap T$ is the neighborhood reduced to the target pixels and 
				\begin{equation}
					c[a,b] = \begin{cases}
					|b-a|, & \text{if}\ |b-a|\leq \tau \\
					\tau, & \text{otherwise}
					\end{cases}
				\end{equation}
				is the absolute norm clamped by $\tau$. This $\tau$ is the same as discussed in \rf{method:our:other:robust_distance}.
				As consequence, the neighboring pixel can be at any position on the sphere of radius $||\v||$, but since this must hold for any potential neighborhood, the lowest error is still the same as for the definition by Herling and Broll.
				The above definition of the cost function will be referred to as $cost_{spatial}$  and the above $N'_s$ as $N_s$ from here on.
%				We will use the new definitions of the spatial cost term $cost'_{spatial}$ and neighborhood $N'_s$ discussed in this section from here on and refer to them as 
			
			\subsubsection{Gradient}			
				In this section, we discuss how the gradient of the cost function is built, as it constitutes the core part of the algorithm.
				Since the color cost function is a linear combination of the color and the spatial cost terms, its gradient is a linear combination of the gradient of these two terms.
				\begin{equation}\label{method:our:gradient:eq}
					\nabla cost(f) = \sum_{\forall p \in T} \nabla cost(p) = \sum_{\forall p \in T} \alpha ~ \nabla cost_{color}(p) + (1-\alpha)\nabla cost_{spatial}(p).
				\end{equation}
				
				\paragraph{Spatial Cost Gradient}
					The spatial cost term aims at keeping a pixels mapping $f(p)$ at the proper distance of its neighbor's $p_n = p + \v$mapping.
					That is
					\begin{equation}
						d_s[f(p), f(p+\v)] = ||\v|| \quad \forall \v \in N_s.
					\end{equation}
					The gradient is the vector pointing towards the point that satisfied the above equation best. 
					$\Delta s = f(p+\v) - p'$
					\begin{align}\label{method:our:gradient:spatial}
						\nabla cost_{spatial}(p,p') & = \sum_{\v \in N_s} \Delta s  \frac{d_s[\Delta s]-||\v||}{||\Delta s||}\\
						& = \sum_{\v \in N_s} [[f(p+\v) - p']  \frac{d_s[f(p+\v) - p']-||\v||}{||f(p+\v) - p'||},
					\end{align}
					where $p'$ is the candidate source pixel or the previous iterations mapping $f_{n-1}(p)$
					
				
				\paragraph{Color Cost Gradient}
					The color cost uses the pixel color values as input that is not analytically differentiable.
					Instead, the finite difference approximation is used to compute the gradient of the color cost $\nabla cost_{color}(p)$ for a pixel $p$.
					The gradient is then
					\begin{equation}
						\nabla cost_{color} (p) = (\nabla_x cost_{color}(p), \nabla_y cost_{color}(p),
					\end{equation}
					where
					\begin{align}\label{method:our:gradient:color}
						\nabla_x cost_{color}(p) &= \frac{cost_{color}(p+[\epsilon,0]) - cost_{color}(p-[\epsilon,0 ])}{2\epsilon} \\
						\nabla_y cost_{color}(p) &= \frac{cost_{color}(p+[0, \epsilon]) - cost_{color}(p-[0, \epsilon ])}{2\epsilon},
					\end{align}
					where $\epsilon > 0$ is set to 0.5 throughout the rest of this discussion.
					This guarantees that we do not over- or undersample the image by following the Nyquist-Shannon sampling theorem.
					
			\subsubsection{Step Size}
				Gradient descent algorithms are inherently sensitive to overshooting if the magnitude of the gradient is too large.
				To avoid overshooting and to speed up the convergence, the gradient typically gets scaled or clamped.
				
				The color error, by definition, is bound by $d_c[\text{white}, \text{black}]$ where white and black are pixels with the corresponding colors.
				So it can be easily normalized to $[0,1]$, as shown in \rf{method:our:color_cost_norm}.
				Since computing the color cost gradient uses finite differences with a total distance $2\epsilon = 1$pixel in between the sampling, the gradient can never exceed $[max_c, max_c]$ where $max_c$ is the maximum possible color cost.
				After the normalization to $[0,1]$ introduced in \rf{method:our:color_cost_norm}, this yields that $\nabla cost_{color} \in [0,\sqrt{2}]$.
				
				As we will discuss in \rf{method:out:other:spatial_normalization}, the spatial cost term is normalized to be at most 1 and correspondingly the gradient of the spatial cost term is normalized to be at most of length 1.

				The total cost is a weighted sum of the spatial and color cost using the blending parameter $\alpha$.
				Considering the definition in \rf{pixmix:total_cost}, the total cost has its maximum value of $\sqrt{2}$ with $\alpha = 1$.
				When the additional correction of the color cost term discussed in \rf{method:our:color_cost_norm} is applied, this maximum can even be higher.
				
				Again, the Nyquist-Shannon sampling theorem tells us that a step size of at most 0.5 pixels is ideal to avoid undersampling of the image.
				We, however, want to allow for slightly higher step sizes for fast conversion, if a mapping is very far from a good state.
				
				Experiments with the Python prototype have shown that the applied normalizations and clamping of the gradients lead to a step size of around 0.5 pixels with a few higher outliers.
				After a few iterations the step size typically drops drastically and only a few pixels.
				\rf{method:step_size:total} shows the total cost for 22 iterations on a high-resolution level.
				The cost never exceeds 0.23 and drops drastically after 10 iterations.
				In \rf{method:step_size:vs_plot} every dot is a single target pixel, the \emph{y} axis is the value of the spatial cost term and \emph{x} the color cost term of the last step of this levels optimization.
				We used these distributions to verify that the step size was mostly below 0.5 pixels and not drastically below and also to study the impact of the $\alpha$-weight.
				
				\begin{figure}
					%SET IMAGE PATH
					\graphicspath{{images/method/step_size/}}
					\centering
					\begin{subfigure}[t]{0.35\linewidth}
						\centering
						\includegraphics[height=0.8\linewidth]{spatial_vs_color}
						\caption[Spatial cost term plotted against the color cost term.]{The spatial cost term plotted against the color cost term. Each dot represents a pixel with the \emph{y} axis being the value of the spatial cost term and \emph{x} the color cost term. The data is taken from a single optimization step.}					
						\label{method:step_size:vs_plot}
					\end{subfigure}
					\begin{subfigure}[t]{0.35\linewidth}
						\centering
						\includegraphics[height=0.8\linewidth]{total_error}		
						\caption{Average of the total cost term for 22 iterations of optimization.}
						\label{method:step_size:total}
					\end{subfigure}
				\caption{Spatial cost term, color cost term and total cost.}
				\end{figure}
				
		\subsection{Other Modifications}\label{method:our_approach:other}
			Experiments with the Python prototype (see \rf{imp:python_prototype}) have lead us to introduce normalization of the cost and spatial cost term.
			We discuss these normalizations and the robust distance function used in the spatial cost term in the following section.
			
			\subsubsection{Robust Distance Function}\label{method:our:other:robust_distance}
				The definition of the spatial cost term in \rf{method:pixmix:spatial_cost:eq} introduces a robust distance function $d_s$.
				The capped L2 norm is used for $d_s$.
				\begin{equation}\label{method:our:robust_distance}
					d_s[p, p'] =
					\begin{cases}
						||p'-p||, & \text{if}\ ||p'-p||\leq \tau \\
						\tau, & \text{otherwise}
					\end{cases},
				\end{equation}
				where $\tau > 1$ is a clamping threshold in pixel unit and $||\cdot||$ the L2 norm.
				
				In the following discussion we assume $d_s[p,p'] = ||p'-p||$ for small distances.
				The above definition is minimal if $||f(p+\v) - p'|| == ||\v||~ \forall \v \in N_s$.
				If $||f(p+\v) - p'|| > ||\v||$ the mapping $f(p)$ is too far away from $p$ and must be pulled closer.
				In case of $||f(p+\v) - p'|| < ||\v||$ $f(p)$ is too close to $p'$ and must get pushed away.
				When $f(p+\v) = p'$, the above equation is undefined.
				If this happens, the contribution of this neighbor to the spatial cost can be ignored.
				This strategy assumes that this case is rare and in the course of the optimization this case will be resolved quickly.
								
			\subsubsection{Spatial Cost Normalization}\label{method:out:other:spatial_normalization}
				The robust distance function discussed in the previous paragraph has a significant impact on the maximum possible value of the spatial cost term.
				This maximum is proportional to $\tau$, and so is the contribution of the spatial cost term to the total cost term.
				We divide the spatial cost term with $\tau$ to normalize it to $[0,1]$.
				Due to this modification the range of the spatial cost term is independent of $\tau$.
			
			\subsubsection{Color Cost Normalization}\label{method:our:color_cost_norm}
				We normalize the color cost term to $[0,1]$ to make it independent from the color format used.
				In addition to that, the overall contrast of the image has a significant impact on the color cost's magnitude. Images with little contrast result in a color cost that is persistently smaller than for the same image with higher contrast.
				To counteract that, the standard deviation of the image's colors is computed for every frame first and the color cost then divided by its norm.
				This results in the following definition:
				\begin{align}\label{method:our:color_cost_normalization}
					cost'_{color}(p) &= cost_{color} \cdot \frac{1}{||c_{max}||~||c_{std}||}\\
					&= \sum_{\v \in N_c} ||i[p+\v] - i[f(p) + \v)]|| ~ \omega_c(p+\v) \cdot \frac{1}{||c_{max}||~||c_{std}||},
				\end{align}
				with $c_{max}$ indicating the pixel value for the color white and $c_{std}\in \mathbb{R}^3$ the standard deviations of the RGB color channels.
				Note that the alpha channel is ignored and the L2 norm of the color differences computed per color channel is used as pixel intensity distance measure $d_c$.
				The above definition of $cost'_{color}$ will be referred to as $cost_{color}$ in the following.

		
	\section{Algorithm} \label{method:algo}
		This section describes the complete algorithm in detail.
		It consists of an initialization of the mapping that performs an exhaustive search, then an iterative optimization of the color and spatial cost simultaneously.
		The initialization is performed on a low-resolution mipmap level and the optimization iterates on each level until convergence and then moves on to the next higher resolution.
		Since the cost is optimized for each pixel independently using gradient descent, no guarantee on the convergence can be given.
		It is a local optimization, hoping to find at least a good local minimum and reducing the risk of getting stuck in a bad one by carefully guiding the process from low-resolution to high-resolution.
		
		\TODO{Algorithm overview latex thing.}
		
			\subsection{Initialization}\label{method:algo:init}
				The initialization is performed on the highest mipmap level at which enough structure is already present.
				If the resolution is too low, the color cost cannot capture the structural characteristics of the image.
				If it is too high, the initialization will be very slow.
				Starting at a resolution with a width of 16 pixels has proven to work best.
			
				For all known pixels the mapping is set to point to itself, $f(p') = p' \forall p' \in S$.
				The unknown pixels $p$ are initialized by exhaustively searching all potential target pixels $p'$  for the one generating the smallest color cost $f(p,p').$
				That is $f(p) = argmin_{p'\in S} ~cost_{color}(p, p') ~\forall p' \in S,  ~p \in T$.
				
				A source pixel $p$ can be initialized only if there are enough pixel in its neighborhood to robustly compute the color cost.
				For this reason, the initialization consists of multiple iterations.
				Initially, the pixels at the edge of $S$ with enough valid neighbors are mapped.
				The initialization then proceeds inwards, shell by shell, until all pixels are mapped.
				The minimum number of mapped neighbors depends on the neighborhood size and is discussed in \rf{imp:neighbourhood}.
				
				Note that we only use the color error during the initialization.
				Firstly, because the spatial error is not defined unless there are mapped target pixels in the neighborhood.
				Secondly, it is desirable for the initial mapping to be widely spread.
				The spatial cost will pull together pixels to patches during the optimization, so having a larger variety of good candidates reduces the risk of getting stuck in a local minimum.
				
				However, according to the Nyquist-Shannon sampling theorem, this is under-sampling the image pixel space.
				As a result, we do not capture the entire frequency spectrum as desired.
				More on this in \rf{result:nyquist}.
				Instead, a grid of density 0.5 pixels, that samples interpolated pixel color values, should be used.				
								
%				For every pixel inside the hole, do exhaustive search on the known pixel the following way:
%				If there are at least $n$ (e.g. 2) neighbors that are either outside the hole, or mapped already: Compute neighbors average color. Perform exhaustive search on all known pixels, store the best match. Do so until all pixels are mapped. Will converge for a reasonable $n$
				
			\subsection{Optimization}
				The optimization performed on a given mipmap level consists of iteratively computing the gradient of the cost function and updating the mapping in its negative direction. 
				The following describes a single iteration of the optimization.
				We assume the mipmap level and the mapping function $f_{n-1}$ from the last iteration or initialization given.
				
				The gradient of the spatial cost term can easily be computed using the derivation from \rf{method:our:gradient:spatial}, the robust distance function defined in \rf{method:our:robust_distance} and the normalization by dividing the spatial cost by $\tau$, as described in \rf{method:out:other:spatial_normalization}.
				Computing the color cost gradient is done using a finite difference approach, as described in \rf{method:our:gradient:color} including the normalization introduced in \rf{method:our:color_cost_normalization}.
				\rf{method:our:color_cost_normalization} deals with the case of having a pixel with an invalid mapping in either the source or target neighborhood.
				The final gradient is then build as $\nabla cost(p) = \alpha ~\nabla cost_{color}(p) + (1-\alpha) \nabla cost_{spatial}(p)$ and finally the mapping is updated by adding the negative gradient to the last estimate of $f$.
				\begin{equation}
					f_n(p) = f_{n-1}(p) - \nabla cost(p).
				\end{equation}
				
				If the new mapping $f_n(p)$ is not within the image borders, it gets clamped to the nearest edge pixel.
				The mapping is then also checked to not point into the source area, a case discussed in the following section, and then eventually stored as result of this iteration.
				Note that the new value is first used in the following iteration, even if neighboring pixels have not been processed yet.
				During a single iteration, all pixels share the same mapping function.
			
						
			\subsubsection{Mapping Back Into the Hole}\label{method:algo:back_to_hole}
				Since the proposed gradient descent approach does not test and assign potential better mappings, but gradually moves them to a place of low cost, a new mapping might end up pointing into the hole, leading to an invalid mapping.
				
				We can handle this case in various ways and will discuss three of them.
				An evaluation of how they affect the resulting inpainting is given in \rf{res:hole}.
				
%					Describe and discuss the different procedures tried and refer to results.
%					\Q{Discuss issue \textit{all threads getting blocked because of branching on GPU } here or in implementation chapter?}\\
%					\A{ Overall, I would describe the constraints coming from the GPU already in this chapter, but not go into too much details. Imagine the level of granularity of an image showing the barriers over time in the process (actually it would be nice to have this image). For this specific question, if it is an unexpected GPU effect leading to a bad result, maybe it is better to have it later. }
							
				\paragraph{Force}\label{method:hole:force}
				Whenever a pixel $p$ is mapped into the hole, it gets pushed outwards by a force in direction $v_{out} = f(p) - c$, where $c$ in the center of the hole.
				We compute the center of the hole as the projection of the center of mass of the 3D model or manually select it for static test images.
				The resulting mapping is then $f_{corr}(p) = f(p) + v_{out}$, which, however, might still be inside the target area.
				To escape the hole more quickly $v_{out}$ is scaled to be larger, if closer to the center, and smaller, if closer to the edge of $T$ as follows $v'_{out} = v_{out}/||v_{out}||^2$.
				
				This is a simple approximation of the projection of the original mapping onto the border of $T$, with the advantage of staying as close to the original mapping as possible.
				However, additional to not having any guarantee of escaping the hole within one iteration, the chance of getting mapped back into the hole in a subsequent iteration is rather high.
					
				\paragraph{Neighbor with Smallest Error}
				Instead of trying to stay close to the original mapping, the pixel gets remapped to a potentially good pixel anywhere in the image.
				Potentially good with respect to the spatial cost function means close to the neighbors mapping.
				Therefore, we use the neighbors' mappings as a starting point, from which we add a random offset vector $\v$ of length, creating eight candidate pixels when using the eight-neighborhood.
				For these candidates, we compute the cost and choose the one that synthesizes the lowest cost.
				More formally this is $f_{corr}(p) = \underset{p' + \v, \forall p' \in N}{argmin} cost(p, p' + \v)$ with $N$ as the eight-neighborhood.

				Starting with a set of candidates generating a low spatial cost and picking the best establish a fair chance of finding a good match and being further away from the hole.
				It is less likely to be mapped back into the hole in the subsequent iterations and might even reduce the risk of this pixel getting stuck in a local minimum.
				The procedure, however, is rather expensive due to computing the cost eight times.
				This is especially severe in the context of parallel computation on the GPU, which causes many other threads to halt if even a single thread executes a different branch.
					
				\paragraph{Random Neighbor}
				We can simplify the previously described approach by randomly choosing a neighbor and adding a random vector of length one.
				This way we only generate one candidate instead of considering many.
				This approach is much faster and turned out to produce results close or even better than the approach considering multiple candidates (see \rf{res:hole}).

			\subsubsection{Convergence Criteria}\label{method:algo:convergence}
				The approach used for minimizing this cost function is local, and the cost function itself has many local minima.
				Therefore choosing when to stop the optimization is not trivial.
				The most straightforward solution is to stop after a fixed number of iteration. This allows for precise manual control and is simple to implement, but can easily under- or overfit.
				
				A more advanced suggestion is to track the total cost $cost(f)$ for all previous iterations and stop if the cost has not been reduced for a certain number of iterations.
				The mapping function generating the lowest cost can then be used as final mapping.
				This avoids under- and overfitting, but introduces a new parameter instead: For how many iterations is the total cost allowed to increase?
				It can also effectively improve the performance since only the smallest number of iterations necessary is performed.
			
			\subsection{Mapping Propagation to Next Mipmap Level}\label{method:algo:propagation}
				Once the optimization on a mipmap level $L_{n-1}$ has stopped, we pass the resulting mapping function $f_{n+1}$ to the next lower level $L_n$.
				In this context, lower implies higher resolution by the definition of the mipmap levels.
				If the true border of $T$ did not align with the pixel grid by chance, the target area on level $L_n$ is enclosing the true hole more tightly than on level $L_{n+1}$.
				We set the mapping to the identity for all source pixels according to the following definition: $f_n(p') = p'~\forall p' \in S$.
				
				We initialize the target pixels by the corresponding value of the last levels mapping.
				More precisely: $f_n(p) = 2 f_{n-1}(p//2) + (p_x \mod 2, p_y \mod 2)$ where $//$ is the integer division and assuming $p = (p_x, p_y) \in \{0, \dots \text{width}\} \times \{0, \dots \text{width}\}$ in integer coordinates.
				In case of using unified texture coordinates for $\tilde{p} \in [0,1]\times[0,1]$ and bilinear interpolation this simplifies to $f_n(\tilde{p}) = f_{n+1}(\tilde{p})$.
				
			\subsection{Mapping Propagation to Next Frame}\label{method:algo:next_frame}
				Due to the tracking of the physical object, we can compute the transformations describing the movement of the camera in between two frames.
				Therefore, we can use the approach discussed in \rf{method:pixmix:video}.
				It consists of simple transformation of the mapping $f^{init}_{n+1} = H^{-1}f_{n}H$ where $f^{init}_{n+1}$ is the initialization of the mapping for the new frame, $f_{n}$ the final mapping function from the old frame and $H$ the homography describing the transformation of the object.
				
				However, within the scope of this project, we did not implement this final part of the video inpainting pipeline.
				Instead, the current implementation treats every frame independently.