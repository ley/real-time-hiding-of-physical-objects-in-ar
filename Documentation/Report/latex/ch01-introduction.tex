

% set counter to n-1:
\setcounter{chapter}{0}
\chapter{Introduction}
%	\textcolor{red}{General Remark: Additional to the chapters/sections (horizontal structure) I want to embed a vertical structure consisting of the key aspects that were driving the project/design and based on which the results can be discussed and evaluated.\\
%		They are:
%		\begin{itemize}
%			\item It's aiming for a \emph{real-time and mobile} application, performance is critical. Implications are: It's \emph{data-parallel}, no expensive search algorithm possible.
%			\item It's a highly non-convex optimization problem which should be solved in a consistent manner $\rightarrow$ \emph{hierarchically} solve problem at lower dimension (less likely to get stuck in a bad minimum) and move to the harder problem.
%			\item The result should look good: This is achieved by using a \emph{cost functions} that has proven to be good (PixMix).
%		\end{itemize}
%		I will be trying to highlight the reference to these aspects using \PERF, \HIER, \COST labels throughout the draft of this document.
%	}

	%-----INTRODUCTION-----%
	In the following sections, we present the key concepts discussed in this thesis and state the goal we aim to achieve.

	\section{Problem Description}
		The human experience of reality comprises many layers of sensory perception, analysis, and interpretation of such, memories and imagination.
		Seeing an image is never pure perception, but analysis and associations naturally go along with it and allow us to deduce the context and the function of the image content.
		Providing additional layers to this experience allows augmenting the analysis and associations and eventually the understanding of context and function.
		If virtual visual layers are overlaid over the perception of reality, for example by adding sensor measurements to a live video stream, this is called \emph{augmented reality}.
		Augmented reality (AR) applications alter the perception and thus analysis of reality by adding context dependent visual information, while preserving most of the real experience.
		This is in contrast to virtual reality, which focuses mostly on the artificial virtual layers of perception.
		
		In the advent of technology enabling augmented reality, the inpainting problem has gained significance in the field of real-time applications.
		When aimed to enhance a users experience of reality with a virtual layer, it is often not sufficient to merely add content. The additional content can be in conflict with reality.
		This is the case when a real object gets replaced by a virtual counterpart, or the real object disturbs the overall scenery.
		Also from an artist's perspective, removing the familiar or expected, shifts the viewers focus to the designated center of attention.
		Thus the ability to remove content is crucial for any elaborate augmented reality application.
		Altering the experience of reality by exclusively removing content is also referred to as \emph{Diminished Reality}.
		
		Image inpainting deals with the problem of completing images that have been corrupted or where parts have been removed on purpose.
		The missing pixels have to be estimated such that the image looks complete and the reconstructed areas blend with the image nicely.
		
		A particular case of inpainting in the context of diminished reality is \emph{Object Removal}.
		Its aim is the reconstruction of the background occluded by the object that is to be removed.
		This is in contrast to \emph{Restoration}, where the aim is to fix areas missing due to noise, corruption or overlaid text.
		In the case of restoration, the object itself and not the background is to be reconstructed.
		
		The project motivating this thesis originates from the context of a museum exhibition.
		The visitor aims the camera of a tablet computer at a sculpture and sees an animated equivalent of the sculpture on the screen.
		Preserving the background with all its static and dynamic components is crucial for the intended illusion
		At the same time, the real physical object should not be visible at any moment.
		Since the virtual equivalent of the sculpture is moving, its visual hull is not guaranteed to overlap the real objects hull.
		As a consequence, the real object has to be removed from the video stream before rendering the virtual counterpart.

	\section{Goal and Final Product}
		The primary objective of this thesis is to propose an algorithm solving the inpainting problem in the context of real-time augmented reality applications. 
		To study and verify the algorithm's performance and correctness, a diminished reality application is developed, which handles the challenges imposed by the scenario described earlier.
		More explicitly, the desired properties of said application are:
		\begin{itemize}
			\item The ability to run on \emph{mobile devices}, supporting both Android and iOS. \PERF
			\item The video stream should be processed and presented in \emph{real-time}. \PERF \HIER
			\item The resulting inpainting should be \emph{visually pleasing and consistent} with the rest of the video. \COST \HIER
			\item The application should be \emph{general purpose}, no prior knowledge or restrictions on the scene and environment is assumed. Furthermore, the area to be inpainted should not be constrained in shape or size.
		\end{itemize}
		Our goal is to present an algorithm for the video inpainting problem that conforms to the state of the art.
		Further, we wish to proof that such an algorithm can process a video in real-time on a mobile device, provided that the computational power of the GPU is used.
		
	\section{Constraints}\label{intro:constraints}
		All the properties mentioned above impose a number of tight constraints, listed next:
		
		\paragraph{Performance}
		One major concern when targeting real-time processing on a mobile device with limited computational power is the performance.
		Processing a single frame must take at most a fraction of a second and includes a number of preparing steps before the actual inpainting.
		Since most modern mobile devices are equipped with a decently powerful GPU, leveraging that in order to achieve the required performance is an obvious choice.
		This allows for a high computational throughput but implies a number of limitations on the structure of the algorithm.
		%Every shader program executed by a thread on the GPU must be independent from the other threads and is strictly limited in its computational complexity. \TODO{Reference to section GPU specific details} \PERF
		
		\paragraph{Different Devices} We aim at supporting a largest possible set of devices with differing processing power. This requires an approach that can be adapted to produce an acceptable frame-rate.
		\HIER \COST
		
		\paragraph{Scene Complexity} Not limiting the algorithm to certain classes of scenes or images prohibits heuristic simplifications of the inpainting problem or using any prior knowledge specific to the scene.
	
	\section{Our Approach}
		In the following chapters, we first discuss the relevant prior work in the field of image and video inpainting, especially the work of Herling and Broll and their PixMix algorithm \cite{pixmix2014}.
		Based on PixMix, we propose a new algorithm that can easily be parallelized and implemented on the GPU in \rf{method}.
		We formulate the inpainting problem as a minimization of a cost function, which is evaluated per pixel for all pixels that lay inside the hole.
		We minimize this cost function for each pixel independently using a gradient descent optimization.

		In \rf{imp} we present two implementations of our algorithm.
		To experiment and study the algorithm, we first made a prototype implementation using Python.
		Once we were satisfied with the results, we implemented the entire pipeline, including the tracking and removal of a physical object from a video stream that makes use of the GPU.
		The results we achieved are very good in most cases. We present them in \rf{results}.