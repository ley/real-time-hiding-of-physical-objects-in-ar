# Real-time Hiding of Physical Objects in AR
This repository contains all data (source code, documentation and build) of the Master thesis of Niclas Scheuing _Real-time Hiding of Physical Objects in AR_.

### Repository structure:
#### benchmark_imgs
 - all input data for the python and the Unity implementation.
 
#### code
 - code/arinpainter: The ARInpainter Unity project
 - code/prototype: python prototye implementation
 - code/recorder: Unity project for recording frames and matching mask images.

#### Documentation
 - Documentation/Report: The project report "thesis.pdf" and latex source files and images.
 - Documentation/Presentations: Midterm and final presentaiton slides, latex source files and images.

#### external
 - store external dependencies here

# ARInpainter
The Unity implementation
 
## Required
### Unity
Version 2017.1.3f. Any higher version is not compatible with the SHIELD tablet.
### Vuforia
Version 6.2.10. Any higher version is not compatible with SHIELD tablet.

## Installation for iOS
- Build for iOS
- The version of Unity used produces invalid xcode project files. Follow these instructions : https://stackoverflow.com/a/41680328
- In xCode change in project settings "Supported Platforms" to "iOS"
- Fix signing certificate stuff.


## Scene setup explained

### In short
The rendering is done in 3 passes:
1. PrimaryCamera renders the scene with background
2. PrimaryCamera renders mask for pixels requiring inpainting
3. OrthoCamera takes both results and applies inpainting.

### Vuforia Configuration
Make sure the dataset is active and loaded. Also 'video background' must be enabled.

### Cameras
#### AR Camera
Default Vuforia GameObject required for tracking. This has to be enabled and the WorldCenterMode set to CAMERA for correct tracking behaviouir

#### PrimaryCamera
This camera is rendering the scene into a RenderTexture. It must be enabled and the renderTarget set to the 'BackgroundRenderTexture'. It will render the background and the virtual object using the regular rendering process.
The MainRendering script contains the method 'CreateMask' this is called by the OrthoCamera and renders a mask image of the inpaint areas into a RenderTexture.
The rendering of the Background&Object and the Mask are triggered independently and might not be executed in strict order/time interval. This depends on when the rendering of the PrimaryCamera and OrthoCamera is triggered by Unity.
The reason the Background&Object rendering is not triggered by the OrthoCamera as well, is an incompatibility with the Android version. It simply does not render the VideoBackground the other way.

#### OrthoCamera
Gets the rendering of the mask into the MaskRenderTexture by the PrimaryCamera, retrieves the rendered background&object image from the PrimaryCamera via BackgroundRenderTexture.
It then renders both onto a Quad (acting as canvas) using the inpaint-shader.
This camera must also be enabled.

# Recorder
Records and stores camera frames and a mask for the tracked object.
The requirements and setup of the Recorder is essentially the same as the ARInpainter. But instead of inpainting based on background and mask, it stores both to persistent memory.

## Installation
Clone git repo.
Open Unity.
Import Package > Custom Package... Find 'vuforia-unity.6.2.10.unitypackage' from whereever you put it after downloading it. (I typically put it into the '<proj_root>/external' directory)
Don't do the API Update if Unity is asking you.

### Project Debuging
If the Unity project does not run on some devices for some reason, try the following:
 - Error: "MissingComponentException: Compute Shaders not supported".
    - On Android it should run if OpenGLES 3.1 or Vulkan	rendering is supported.Check in Unity Edit>Project Settings>Player/Android Tab/Other Settings/Rendering and make sure OpenGLES2 is NOT in the 'Graphics APIs' List.
	- When running from the Unity Editor on Windows, Edit>Project Settings>Player/PC,Mac&Linux Tab/Other Settings/Rendering. Change the order of the rendering API, let Unity update stuff and change it back. No idea why, but it works.
 - The entire screen is showing only one single color on Android.
   Check in Unity Edit>Project Settings>Player/Android Tab/Other Settings/Rendering and make sure OpenGLES2 is NOT in the 'Graphics APIs' List.
 - On Android App crashes and Android Device Monitor logs:
	"06-25 15:14:03.703: W/InputDispatcher(27186): channel 'b622b07 com.android.launcher3/com.android.launcher3.Launcher (server)' ~ Consumer closed input channel or an error occurred.  events=0x9"
	"06-25 15:14:03.703: E/InputDispatcher(27186): channel 'b622b07 com.android.launcher3/com.android.launcher3.Launcher (server)' ~ Channel is unrecoverably broken and will be disposed!"
	If there are many services stopping and restarting before that, this probably means, there is a memory leak, or check this: https://answers.unity.com/questions/650082/crash-on-android.html

   
## Helpful tools
### MonoDevelop Syntax Highlighting for compute shaders
https://forum.unity.com/threads/monodevelop-syntax-highlighting-for-compute-shaders.164982/
Edit the dll file and change the 'version' in the following block to your MonoDevelop version if the plugin is not working
<Dependencies>
		<Addin id="Core" version="5.9.6" />
		<Addin id="Ide" version="5.9.6" />
		<Addin id="SourceEditor2" version="5.9.6" />
	</Dependencies>

### RenderDoc
Shader debugging tool
https://renderdoc.org/
Integrates with Unity: https://docs.unity3d.com/Manual/RenderDocIntegration.html